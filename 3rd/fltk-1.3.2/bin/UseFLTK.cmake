#
# automatically generated - do not edit

include_directories("/usr/local/include")

if(FLTK_EXE_LINKER_FLAGS)
   list(APPEND CMAKE_EXE_LINKER_FLAGS "${FLTK_EXE_LINKER_FLAGS}")
endif(FLTK_EXE_LINKER_FLAGS)
