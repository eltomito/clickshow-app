var structXUtf8FontStruct =
[
    [ "ascent", "structXUtf8FontStruct.html#aae0c2bad1cbd637d1e7610401d866421", null ],
    [ "descent", "structXUtf8FontStruct.html#ab04d8dd96e6c29fbd22dce1592d50044", null ],
    [ "encodings", "structXUtf8FontStruct.html#af243fea4ffc63db95cee860d510af212", null ],
    [ "fid", "structXUtf8FontStruct.html#a10491f8a3fcde970f1c90c5d2b415201", null ],
    [ "font_name_list", "structXUtf8FontStruct.html#aff5c8a276bf54a747384cd6f927e4cd5", null ],
    [ "fonts", "structXUtf8FontStruct.html#af41d79cbe497df18e03e0447a9b0c7f6", null ],
    [ "nb_font", "structXUtf8FontStruct.html#a2e22f8e848c9c562e0a41044727f6d00", null ],
    [ "ranges", "structXUtf8FontStruct.html#a7de9efb810d173f56567c23a1f30ec2b", null ]
];