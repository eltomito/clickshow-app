# Install script for directory: /home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/test

# Set the install prefix
if(NOT DEFINED CMAKE_INSTALL_PREFIX)
  set(CMAKE_INSTALL_PREFIX "/usr/local")
endif()
string(REGEX REPLACE "/$" "" CMAKE_INSTALL_PREFIX "${CMAKE_INSTALL_PREFIX}")

# Set the install configuration name.
if(NOT DEFINED CMAKE_INSTALL_CONFIG_NAME)
  if(BUILD_TYPE)
    string(REGEX REPLACE "^[^A-Za-z0-9_]+" ""
           CMAKE_INSTALL_CONFIG_NAME "${BUILD_TYPE}")
  else()
    set(CMAKE_INSTALL_CONFIG_NAME "Debug")
  endif()
  message(STATUS "Install configuration: \"${CMAKE_INSTALL_CONFIG_NAME}\"")
endif()

# Set the component getting installed.
if(NOT CMAKE_INSTALL_COMPONENT)
  if(COMPONENT)
    message(STATUS "Install component: \"${COMPONENT}\"")
    set(CMAKE_INSTALL_COMPONENT "${COMPONENT}")
  else()
    set(CMAKE_INSTALL_COMPONENT)
  endif()
endif()

# Install shared libraries without execute permission?
if(NOT DEFINED CMAKE_INSTALL_SO_NO_EXE)
  set(CMAKE_INSTALL_SO_NO_EXE "1")
endif()

# Is this installation the result of a crosscompile?
if(NOT DEFINED CMAKE_CROSSCOMPILING)
  set(CMAKE_CROSSCOMPILING "FALSE")
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/adjuster" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/adjuster")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/adjuster"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/adjuster")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/adjuster")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/adjuster" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/adjuster")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/adjuster")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/arc" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/arc")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/arc"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/arc")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/arc")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/arc" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/arc")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/arc")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/ask" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/ask")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/ask"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/ask")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/ask")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/ask" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/ask")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/ask")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/bitmap" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/bitmap")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/bitmap"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/bitmap")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/bitmap")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/bitmap" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/bitmap")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/bitmap")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/blocks" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/blocks")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/blocks"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/blocks")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/blocks")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/blocks" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/blocks")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/blocks")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/boxtype" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/boxtype")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/boxtype"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/boxtype")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/boxtype")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/boxtype" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/boxtype")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/boxtype")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/browser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/browser")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/browser"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/browser")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/browser")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/browser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/browser")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/browser")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/button" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/button")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/button"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/button")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/button")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/button" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/button")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/button")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/buttons" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/buttons")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/buttons"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/buttons")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/buttons")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/buttons" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/buttons")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/buttons")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/checkers" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/checkers")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/checkers"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/checkers")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/checkers")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/checkers" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/checkers")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/checkers")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/clock" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/clock")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/clock"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/clock")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/clock")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/clock" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/clock")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/clock")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/colbrowser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/colbrowser")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/colbrowser"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/colbrowser")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/colbrowser")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/colbrowser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/colbrowser")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/colbrowser")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/color_chooser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/color_chooser")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/color_chooser"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/color_chooser")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/color_chooser")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/color_chooser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/color_chooser")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/color_chooser")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cursor" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cursor")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cursor"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/cursor")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/cursor")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cursor" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cursor")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cursor")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/curve" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/curve")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/curve"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/curve")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/curve")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/curve" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/curve")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/curve")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/demo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/demo")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/demo"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/demo")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/demo")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/demo" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/demo")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/demo")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/device" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/device")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/device"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/device")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/device")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/device" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/device")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/device")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/doublebuffer" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/doublebuffer")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/doublebuffer"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/doublebuffer")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/doublebuffer")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/doublebuffer" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/doublebuffer")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/doublebuffer")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/editor" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/editor")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/editor"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/editor")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/editor")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/editor" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/editor")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/editor")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fast_slow" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fast_slow")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fast_slow"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/fast_slow")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/fast_slow")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fast_slow" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fast_slow")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fast_slow")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/file_chooser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/file_chooser")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/file_chooser"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/file_chooser")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/file_chooser")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/file_chooser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/file_chooser")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/file_chooser")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fonts" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fonts")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fonts"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/fonts")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/fonts")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fonts" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fonts")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fonts")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/forms" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/forms")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/forms"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/forms")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/forms")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/forms" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/forms")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/forms")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/hello" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/hello")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/hello"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/hello")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/hello")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/hello" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/hello")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/hello")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/help" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/help")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/help"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/help")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/help")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/help" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/help")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/help")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/iconize" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/iconize")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/iconize"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/iconize")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/iconize")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/iconize" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/iconize")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/iconize")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/image" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/image")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/image"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/image")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/image")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/image" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/image")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/image")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/inactive" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/inactive")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/inactive"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/inactive")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/inactive")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/inactive" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/inactive")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/inactive")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/input")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/input")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input_choice" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input_choice")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input_choice"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/input_choice")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/input_choice")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input_choice" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input_choice")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/input_choice")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/keyboard" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/keyboard")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/keyboard"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/keyboard")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/keyboard")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/keyboard" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/keyboard")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/keyboard")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/label" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/label")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/label"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/label")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/label")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/label" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/label")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/label")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/line_style" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/line_style")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/line_style"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/line_style")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/line_style")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/line_style" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/line_style")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/line_style")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/list_visuals" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/list_visuals")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/list_visuals"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/list_visuals")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/list_visuals")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/list_visuals" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/list_visuals")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/list_visuals")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/mandelbrot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/mandelbrot")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/mandelbrot"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/mandelbrot")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/mandelbrot")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/mandelbrot" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/mandelbrot")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/mandelbrot")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/menubar" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/menubar")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/menubar"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/menubar")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/menubar")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/menubar" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/menubar")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/menubar")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/message" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/message")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/message"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/message")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/message")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/message" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/message")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/message")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/minimum" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/minimum")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/minimum"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/minimum")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/minimum")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/minimum" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/minimum")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/minimum")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/native-filechooser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/native-filechooser")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/native-filechooser"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/native-filechooser")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/native-filechooser")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/native-filechooser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/native-filechooser")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/native-filechooser")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/navigation" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/navigation")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/navigation"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/navigation")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/navigation")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/navigation" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/navigation")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/navigation")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/output" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/output")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/output"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/output")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/output")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/output" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/output")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/output")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/overlay" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/overlay")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/overlay"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/overlay")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/overlay")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/overlay" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/overlay")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/overlay")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pack" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pack")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pack"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/pack")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/pack")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pack" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pack")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pack")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/pixmap")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/pixmap")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap_browser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap_browser")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap_browser"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/pixmap_browser")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/pixmap_browser")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap_browser" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap_browser")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/pixmap_browser")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/preferences" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/preferences")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/preferences"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/preferences")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/preferences")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/preferences" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/preferences")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/preferences")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/radio" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/radio")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/radio"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/radio")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/radio")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/radio" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/radio")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/radio")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resize" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resize")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resize"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/resize")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/resize")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resize" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resize")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resize")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resizebox" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resizebox")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resizebox"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/resizebox")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/resizebox")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resizebox" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resizebox")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/resizebox")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/rotated_text" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/rotated_text")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/rotated_text"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/rotated_text")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/rotated_text")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/rotated_text" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/rotated_text")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/rotated_text")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/scroll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/scroll")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/scroll"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/scroll")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/scroll")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/scroll" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/scroll")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/scroll")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/subwindow" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/subwindow")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/subwindow"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/subwindow")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/subwindow")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/subwindow" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/subwindow")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/subwindow")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/sudoku" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/sudoku")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/sudoku"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/sudoku")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/sudoku")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/sudoku" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/sudoku")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/sudoku")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/symbols" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/symbols")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/symbols"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/symbols")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/symbols")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/symbols" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/symbols")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/symbols")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tabs" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tabs")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tabs"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/tabs")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/tabs")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tabs" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tabs")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tabs")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/table" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/table")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/table"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/table")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/table")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/table" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/table")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/table")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/threads" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/threads")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/threads"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/threads")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/threads")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/threads" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/threads")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/threads")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tile" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tile")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tile"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/tile")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/tile")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tile" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tile")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tile")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tiled_image" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tiled_image")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tiled_image"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/tiled_image")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/tiled_image")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tiled_image" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tiled_image")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tiled_image")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tree" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tree")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tree"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/tree")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/tree")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tree" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tree")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/tree")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/utf8" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/utf8")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/utf8"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/utf8")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/utf8")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/utf8" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/utf8")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/utf8")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/valuators" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/valuators")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/valuators"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/valuators")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/valuators")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/valuators" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/valuators")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/valuators")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/unittests" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/unittests")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/unittests"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/unittests")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/unittests")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/unittests" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/unittests")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/unittests")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/CubeView" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/CubeView")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/CubeView"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/CubeView")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/CubeView")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/CubeView" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/CubeView")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/CubeView")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cube" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cube")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cube"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/cube")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/cube")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cube" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cube")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/cube")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fractals" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fractals")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fractals"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/fractals")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/fractals")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fractals" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fractals")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fractals")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fullscreen" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fullscreen")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fullscreen"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/fullscreen")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/fullscreen")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fullscreen" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fullscreen")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/fullscreen")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/glpuzzle" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/glpuzzle")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/glpuzzle"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/glpuzzle")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/glpuzzle")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/glpuzzle" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/glpuzzle")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/glpuzzle")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/gl_overlay" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/gl_overlay")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/gl_overlay"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/gl_overlay")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/gl_overlay")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/gl_overlay" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/gl_overlay")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/gl_overlay")
    endif()
  endif()
endif()

if("x${CMAKE_INSTALL_COMPONENT}x" STREQUAL "xUnspecifiedx" OR NOT CMAKE_INSTALL_COMPONENT)
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/shape" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/shape")
    file(RPATH_CHECK
         FILE "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/shape"
         RPATH "")
  endif()
  list(APPEND CMAKE_ABSOLUTE_DESTINATION_FILES
   "/usr/local/share/doc/FLTK/examples/shape")
  if(CMAKE_WARN_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(WARNING "ABSOLUTE path INSTALL DESTINATION : ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
  if(CMAKE_ERROR_ON_ABSOLUTE_INSTALL_DESTINATION)
    message(FATAL_ERROR "ABSOLUTE path INSTALL DESTINATION forbidden (by caller): ${CMAKE_ABSOLUTE_DESTINATION_FILES}")
  endif()
file(INSTALL DESTINATION "/usr/local/share/doc/FLTK/examples" TYPE EXECUTABLE FILES "/home/tomas/src/clickydude-new/app/3rd/fltk-1.3.2/bin/examples/shape")
  if(EXISTS "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/shape" AND
     NOT IS_SYMLINK "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/shape")
    if(CMAKE_INSTALL_DO_STRIP)
      execute_process(COMMAND "/usr/bin/strip" "$ENV{DESTDIR}/usr/local/share/doc/FLTK/examples/shape")
    endif()
  endif()
endif()

