# This file is part of ClickShow
# Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
#
# ClickShow is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# ClickShow is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.

#######################################################################
# basic setup
#######################################################################
cmake_minimum_required(VERSION 2.6)
project(CLICKY)

#set(CMAKE_CXX_FLAGS_DEBUG "${CMAKE_CXX_FLAGS_DEBUG} -Wall -pedantic-errors")

set(CMAKE_CXX_FLAGS "-std=c++11 -Wreturn-type ${CMAKE_CXX_FLAGS}")

set(CMAKE_CXX_FLAGS_DEBUG "-O0 ${CMAKE_CXX_FLAGS_DEBUG} -DDEBUG=1")

set(CMAKE_CXX_FLAGS_RELEASE "${CMAKE_CXX_FLAGS_RELEASE} -O0")

set(CLICKY_SUPER_DIR ${CMAKE_CURRENT_SOURCE_DIR}/../../..)
set(APP_SRC ${CLICKY_SUPER_DIR}/app/src)
set(APP_3RD ${CLICKY_SUPER_DIR}/app/3rd)
set(SHARED_SRC ${CLICKY_SUPER_DIR}/shared/src)
set(SHARED_3RD ${CLICKY_SUPER_DIR}/shared/3rd)

#set(EXECUTABLE_OUTPUT_PATH ${CLICKY_BINARY_DIR})

find_package (Threads)
find_package (X11)
find_library (LIB_RT rt)

#set(Boost_DEBUG 1)
set(Boost_USE_STATIC_LIBS        ON) # only find static libs
set(Boost_USE_MULTITHREADED      ON)
set(Boost_USE_STATIC_RUNTIME    OFF)
#find_package(Boost COMPONENTS date_time program_options)

set(BUILD_TYPE_DIR release)
if( "${CMAKE_BUILD_TYPE}" STREQUAL "Debug" )
	set(BUILD_TYPE_DIR debug)
endif()

message(BUILD_TYPE_DIR: ${BUILD_TYPE_DIR})
message(CMAKE_BUILD_TYPE: ${CMAKE_BUILD_TYPE})

set(Boost_INCLUDE_DIRS ${SHARED_3RD}/boost/build/${BUILD_TYPE_DIR}/include/)
set(BOOST_LIB_DIR ${SHARED_3RD}/boost/build/${BUILD_TYPE_DIR}/lib)
set(Boost_LIBRARIES	${BOOST_LIB_DIR}/libboost_date_time.a ${BOOST_LIB_DIR}/libboost_program_options.a ${BOOST_LIB_DIR}/libboost_system.a ${BOOST_LIB_DIR}/libboost_thread.a ${BOOST_LIB_DIR}/libboost_chrono.a ${BOOST_LIB_DIR}/libboost_regex.a)

cmake_policy(SET CMP0015 NEW) #link directories are relative to CMAKE_CURRENT_SOURCE_DIR

include_directories(. ${CLICKY_SUPER_DIR} ${APP_3RD}/fltk-1.3.2 ${Boost_INCLUDE_DIRS})
link_directories(${APP_3RD}/fltk-1.3.2/lib)
set(Fltk_LIBRARIES ${APP_3RD}/fltk-1.3.2/lib/libfltk.a)

add_executable(clicky
							${APP_SRC}/clicky.cpp
							${APP_SRC}/cmd_args.cpp
							${APP_SRC}/text_encoder.cpp
							${APP_SRC}/styles.cpp
							${APP_SRC}/style_engine.cpp
							${APP_SRC}/sim_geom.cpp
							${APP_SRC}/layout_engine.cpp
							${APP_SRC}/simple_layout_engine.cpp
							${APP_SRC}/style_prefs.cpp
							${APP_SRC}/control_panel.cpp
							${APP_SRC}/panels.cpp
							${APP_SRC}/subtitles/subtitles.cpp
							${APP_SRC}/subtitles/subtitle.cpp
							${APP_SRC}/subtitles/subtitle_doc.cpp
							${APP_SRC}/text_encoder.cpp
							${APP_SRC}/file_utils.cpp
							${APP_SRC}/heart.cpp
							${APP_SRC}/net_heart.cpp
							${APP_SRC}/title_panel.cpp
							${APP_SRC}/sub_click_panel.cpp
							${APP_SRC}/quick_image.cpp
							${APP_SRC}/stock_image.cpp
							${APP_SRC}/prefs.cpp
							${APP_SRC}/drag_line.cpp
							${APP_SRC}/font_panel.cpp
							${APP_SRC}/candy_panel.cpp
							${APP_SRC}/options_panel.cpp
							${APP_SRC}/font_chooser.cpp
							${APP_SRC}/my_color_chooser.cpp
							${APP_SRC}/file_panel.cpp
							${APP_SRC}/search_panel.cpp
							${APP_SRC}/edit_panel.cpp
							${APP_SRC}/info_panel.cpp
							${APP_SRC}/net_panel.cpp
							${APP_SRC}/billboard_panel.cpp
							${APP_SRC}/ask_dialog.cpp
							${APP_SRC}/expiration.cpp
							${APP_SRC}/panel_window.cpp
							${APP_SRC}/help_panel.cpp
							${APP_SRC}/cycle_button.cpp
							${APP_SRC}/clicky_input.cpp
							${APP_SRC}/clicky_slider.cpp
							${APP_SRC}/about_panel.cpp
							${APP_SRC}/drag_bar.cpp
							${APP_SRC}/translator/translator.cpp
							${APP_SRC}/lang_switch.cpp
							${APP_SRC}/widget_brain.cpp
							${APP_SRC}/cl.cpp
							${APP_SRC}/body_layout.cpp
							${APP_SRC}/body_pack.cpp
							${APP_SRC}/brain_mlinput.cpp
							${APP_SRC}/rainbow.cpp
							${APP_SRC}/rainbow_style.cpp
							${APP_SRC}/utf8_tools.cpp
							${SHARED_SRC}/debug_fn.cpp
							${SHARED_SRC}/err.cpp
							${SHARED_SRC}/err_desc.cpp
							${SHARED_SRC}/str_line_iterator.cpp
							${APP_SRC}/clicky_spinner.cpp
							${APP_SRC}/brain_spinner.cpp
							${APP_SRC}/billboard_window.cpp
							${APP_SRC}/parser/parser.cpp
							${APP_SRC}/candy/candy_syntax.cpp
							${APP_SRC}/colors/color_utils.cpp
							${APP_SRC}/colors/color_prefs.cpp
							${APP_SRC}/subtitle_formats/timing/srt.cpp
							${APP_SRC}/subtitle_formats/timing/plain.cpp
							${APP_SRC}/subtitle_formats/base/base_format.cpp
							${APP_SRC}/subtitle_formats/candy/candy_format.cpp
							${APP_SRC}/subtitle_formats/multi/multi_format.cpp
							${APP_SRC}/subtitle_formats/srttags/srttags_format.cpp
							${APP_SRC}/subtitle_formats/srttags/srttag_syntax.cpp
							${APP_SRC}/subtitle_formats/subtitle_formats.cpp
							${APP_SRC}/player/auto_player.cpp
							${APP_SRC}/player/player_panel.cpp
							${APP_SRC}/recorder/sub_recorder.cpp
							${APP_SRC}/recorder/recorder_panel.cpp
							${APP_SRC}/controls/paddles.cpp
							${APP_SRC}/controls/time_paddles.cpp
							${APP_SRC}/controls/time_shifter.cpp
							${APP_SRC}/sub_list/sub_list_button.cpp
							${APP_SRC}/sub_list/sub_list_numbered_button.cpp
							${APP_SRC}/sub_list/sub_list_panel2.cpp
							${SHARED_SRC}/str_utils.cpp
							${SHARED_SRC}/subtitle_formats/core/srt_core.cpp
#--- net stuff
							${APP_SRC}/client/clicky_client.cpp
							${APP_SRC}/client/client_con.cpp
							${APP_SRC}/client/convars/client_var_file_in.cpp
							${APP_SRC}/client/convars/client_var_file_out.cpp
							${APP_SRC}/client/convars/client_var_show.cpp
							${APP_SRC}/client/convars/client_var_play.cpp
							${APP_SRC}/client/convars/client_var_hi_bye.cpp
							${APP_SRC}/client/convars/client_var_perms.cpp
							${APP_SRC}/client/timed_sub_queue.cpp
							${SHARED_SRC}/net/node/convars/con_var.cpp
							${SHARED_SRC}/net/node/convars/con_var_msg.cpp
							${SHARED_SRC}/net/node/convars/con_var_file_in.cpp
							${SHARED_SRC}/net/node/convars/con_var_file_out.cpp
							${SHARED_SRC}/net/node/out_packet.cpp
							${SHARED_SRC}/net/node/out_packet_pool.cpp
							${SHARED_SRC}/net/packets/net_utils.cpp
							${SHARED_SRC}/net/packets/udp_packet.cpp
							${SHARED_SRC}/net/packets/udp_packet_hi.cpp
							${SHARED_SRC}/net/packets/udp_packet_chat.cpp
							${SHARED_SRC}/net/packets/udp_packet_show.cpp
							${SHARED_SRC}/net/packets/udp_packet_msg.cpp
							${SHARED_SRC}/net/packets/auto/auto_packet.cpp
#--- tunnel stuff
							${SHARED_SRC}/net/http/http_utils.cpp
							${SHARED_SRC}/net/http/http_request.cpp
							${SHARED_SRC}/net/http/client/http_client.cpp
							${SHARED_SRC}/net/hole/udp_pack.cpp
							${SHARED_SRC}/net/hole/udp_con.cpp
							${SHARED_SRC}/net/hole/udp_pack.cpp
							${SHARED_SRC}/net/hole/hole_con.cpp
							${SHARED_SRC}/net/hole/blob_queue.cpp
							${SHARED_SRC}/net/hole/drill/hole_drill.cpp
							${SHARED_SRC}/net/hole/drill/easy_drill.cpp
							)

target_link_libraries(clicky
											${Fltk_LIBRARIES}
											"-lXext"
											"-lXft"
											"-lfontconfig"
											"-lXinerama"
											"-lpthread"
											"-ldl"
											"-lm"
											"-lX11"
											"-lXss"
											${Boost_LIBRARIES}
											${CMAKE_THREAD_LIBS_INIT}
											${LIB_RT}
										)
