/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _ABOUT_CLICKY_H_
#define _ABOUT_CLICKY_H_

#define ABOUT_CLICKY_HTML \
N_("<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-%i Tomáš Pártl<br>\
released under GNU GPL 3.0 or later.\
</p>\
<p>\
Clickshow is available for Linux and Windows<br>\
and uses FLTK (www.fltk.org) and BOOST (www.boost.org)\
</p>\
<p>supported by:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>")

#endif //_ABOUT_CLICKY_H_
