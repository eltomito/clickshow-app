/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "about_panel.h"

#include "clicky_debug.h"

/*
#include <cstring>
#include <string>
*/

// =================== class AboutPanel =========================

	AboutPanel::	AboutPanel( const char *about_text, Heart *hrt, int x, int y, int w, int h)
	: ControlPanel ( hrt, x, y, w, h, (const char *)"ABOUT" )
	{
		heart = hrt;;

		layout = new SimpLE();

		button_ok = new Fl_Button( 0, 0, 0, 0, _("Ok") );
		help_view = new Fl_Help_View( 0, 0, 0, 0, NULL );
		help_view->value( about_text );

		end();


		//callbacks
		button_ok->callback((Fl_Callback*)cb_ok);

		//-- layout --

	static const int okb_width = 50; 
	static const int okb_height = 34; 
	static const int okb_top_margin = 5; 
	static const int okb_bottom_margin = 5; 

	static const int okb_strip_height = okb_height + okb_top_margin + okb_bottom_margin;

		layout->add( button_ok,
						-1*( okb_width / 2 ), -1*(okb_height + okb_bottom_margin) ,
						0.5, 1.0,	//relx, rely
						0.0, 0.0,	//relw, relh
						0, 0,		//margins
						okb_width, okb_height );		//min pix

		layout->add_cell( help_view, 0, 0, 10, 10,	//cx, cy, cw, ch
											10, 10,				//columns, rows
											5, 5,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											10, okb_strip_height + 5,		//margins
											0, 0 );		//spaces

		//-- style --

		//I can't assign styles because the whole panel and the widgets can be deleted and created anew
		heart->get_style_prefs()->HelpOkButton->apply( button_ok );
		heart->get_style_prefs()->HelpPanel->apply( this );
		help_view->color( FL_BLACK, FL_BLACK );
		help_view->textcolor( FL_WHITE );
		help_view->textfont( FL_HELVETICA );
		help_view->textsize( 16 );
		help_view->box( FL_NO_BOX );

		//-- refresh stuff --

		refresh_layout();
		refresh_style();
	}

    AboutPanel::~AboutPanel()
    {
        delete layout;
    }

	bool AboutPanel::get_size( int &W, int &H )
	{
		W = w();
		H = h();

		return true;
	}

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void AboutPanel::refresh_layout()
	{
		layout->refresh_layout( this );
	}

	void AboutPanel::cb_ok(Fl_Button* b, void*)
	{
		//find what window this panel is in and hide it.
		AboutPanel *p = (AboutPanel *)b->parent();
		Fl_Window *win = p->window();
		if( win != NULL )
			win->hide();
	}
