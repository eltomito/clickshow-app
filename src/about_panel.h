/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _About_Panel_h
#define _About_Panel_h

//#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Help_View.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "heart.h"
#include "clicky_button.h"

/*
 * the about panel class
 */
class AboutPanel : public ControlPanel
{
	SimpLE	*layout;

	Fl_Button *button_ok;
	Fl_Help_View *help_view;

	static	void cb_ok(Fl_Button* b, void*);

public:
	AboutPanel( const char *about_text, Heart *hrt, int x, int y, int w, int h);
	~AboutPanel();

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void refresh_layout();
	bool get_size( int &W, int &H );
};

#endif //_About_Panel_h
