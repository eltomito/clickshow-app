/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "ask_dialog.h"

#include "clicky_debug.h"

#include <cstring>
#include <string>
#include <algorithm>

//=== class AskButton ===

void AskButton::print()
{
	LOG_RENDER_INFO("AskButton \"%s\": x=%i, y=%i, w=%i, h=%i, argument=%li, this=%lx\n", label(), x(), y(), w(), h(), argument(), (long)this );
}

//=== class AskDialog ===

AskDialog::AskDialog( Heart *hrt, const char *label, const char *choices )
		: ControlPanel( hrt, 0, 0, 0, 0 )
{
	//init
	buttons.clear();
	user_choice = -1;

	//set label
	label_button = new Fl_Button( 0,0,0,0,NULL );
	label_button->copy_label( label );

	//set label style
	heart->get_style_prefs()->AskLabel->apply( label_button );

	//set dialog style
	heart->get_style_prefs()->AskPanel->apply( this );

	//create choice buttons
	create_choice_buttons( choices );

	//create a layout
	create_layout();

	//refresh style
	refresh_style();
	
	LOG_RENDER_INFO("AskDialog:\n%s", Cl::widget_to_string( this ).c_str() );
}

AskDialog::~AskDialog()
{
}

void AskDialog::print()
{
	for( unsigned int i = 0; i < buttons.size(); i++ )
		buttons[i].print();
}

void AskDialog::cb_choice( Fl_Button *b, void *data )
{
	AskDialog *a = (AskDialog *)b->parent();
	a->user_choice = b->argument();

	LOG_RENDER_DETAIL("AskDialog: user clicked choice #%i!\n", a->user_choice );

	b->parent()->parent()->hide();
}

void AskDialog::create_choice_buttons( const char *choices )
{
	std::string		butlab;
	AskButton 		*b;
	const char 	*str;
	const char		*bar;
	const	char		*lab;
	char		*endptr;

	int		i;
	long 		user_arg;
	Fl_Color user_clr;

	//set default choices
	if( choices == NULL )
		choices = _("CANCEL|OK");

	//count choice buttons
	i = 1;
	str = strchr( choices, '|' );
	while( str != NULL )
	{
		i++;
		str = strchr( str+1, '|' );
	};

	//create choice buttons
	buttons.resize( i );

	end();

	//add labels to choice buttons
	str = choices;
	i = 0;

	while( str!=NULL )
	{
		bar = strchr( str, '|' );

		if( bar==NULL )
		{
			butlab = str;
			str = NULL;
		}
		else
		{
			butlab.assign( str, (bar-str) );
			str = bar+1;
		};

		i++;

		b = &buttons[ i-1 ];

		//parse the # commands in the label string
		user_arg = (long)i-1;
		user_clr = 0xffffffff;
		if( butlab.size() >= 2 )
		{
			lab = butlab.c_str();
			while( lab[0] == '#' )
			{
				switch( lab[1] )
				{
					case 'c':
						user_clr = strtoul( &lab[2], &endptr, 0 );
					break;

					default:
						user_arg = strtol( &lab[1], &endptr, 0 );
					break;
				};
				while( endptr[0]==' ' )
					endptr++;
				butlab.erase( 0, (endptr-lab) );
				lab = butlab.c_str();

			};
		};

		b->copy_label( butlab.c_str() );
		heart->get_style_prefs()->AskButton->apply( b );
		if( user_clr != 0xffffffff )
			b->color( user_clr );

		b->argument( (int)user_arg );
		b->callback( (Fl_Callback *)cb_choice );
		b->show();
		b->print();
	};
}

void AskDialog::create_layout()
{
	int	label_x, label_y;
	int	label_w, label_h;
	int	bw, bh;
	int	bx, by;
	int	tw, th;
	int	maxbh;

	//this preserves the layout
	resizable( NULL );

	//measure the label
	label_w = 0;
	label_h = 0;
	label_button->measure_label( label_w, label_h );
	label_w += 2*laborder_w;
	label_h += 2*laborder_h;

	//layout the buttons
	bx = hborder;
	by = vborder + label_h + vspace;

	maxbh = 0;

	for( int i = 0; i < (int)buttons.size(); i++ )
	{
		bw = 0;
		bh = 0;
		buttons[i].measure_label( bw, bh );
		bw += 2*laborder_w;
		bh += 2*laborder_h;
		buttons[i].resize( bx, by, bw, bh );

		buttons[i].print();

		if( bh > maxbh )
			maxbh = bh;

		bx += hspace + bw;
	};

	tw = bx - hspace + hborder;
	tw = std::max( (int)tw, (int)(label_w + 2*vborder) );

	th = 2*vborder + label_h + vspace + maxbh;

	label_x = (tw - label_w) /2;
	label_y = vborder;

	label_button->resize( label_x, label_y, label_w, label_h );

	resize( 0, 0, tw, th );
}

//=== class AskWindow ===

//AskWindow::AskWindow( Heart *hrt, Fl_Widget *parent, int x, int y, const char *winlabel, const char *label, const char *choices )
//			:Fl_Window( 0,0,0,0,NULL )
AskWindow::AskWindow( Heart *hrt, Fl_Widget *parent, AW_Anchor anc, int x, int y, const char *winlabel, const char *label, const char *choices )
			:Fl_Window( 0,0,0,0,NULL )
{
	copy_label( winlabel );

	color( FL_BLACK );

	//remember settings
	real_parent = parent;
	anchor_x = x;
	anchor_y = y;
	anchor_type = anc;

	//create the dialog
	dlg = new AskDialog( hrt, label, choices );
	end();

	//adjust my size
	size( dlg->w(), dlg->h() );

	//make myself modal
	//set_modal();
	hide();
};

AskWindow::~AskWindow()
{
	delete dlg;
};

int AskWindow::ask( bool wait_for_answer )
{
/*
	Fl_Window *win;
	int	ax, ay;

	win = real_parent->window();

	ax = real_parent->x() + anchor_x;
	ay = real_parent->y() + anchor_y;
*/
	LOG_RENDER_INFO("ask() started...\n");

	if( real_parent != NULL )
	{
		int	X = real_parent->x() + anchor_x;
		int	Y = real_parent->y() + anchor_y;

		if( 0!=( anchor_type & VCENTER ) )
			Y -= h()/2;

		if( 0!=( anchor_type & HCENTER ) )
			X -= w()/2;

		if( 0!=( anchor_type & BOTTOM ) )
			Y += real_parent->h() - h();

		if( 0!=( anchor_type & RIGHT ) )
			X += real_parent->w() - w();

		//make sure the window is visible
		if( X < 0 )
			X = 0;
		if( Y < 0 )
			Y = 0;

		position( X, Y );
	};

	show();

	activate();
	dlg->activate();

	if( !wait_for_answer )
		return 0;

	take_focus();

	LOG_RENDER_DETAIL("ask() is gonna wait....\n");

	while( visible() )
		Fl::wait();

	LOG_RENDER_DETAIL("ask() ends.\n");

	return dlg->user_choice;
};
