/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Ask_Dialog_h
#define _Ask_Dialog_h

#include "clicky_debug.h"

#include "control_panel.h"

/*
 * SubtitleButton class - a wrapper for Fl_Button with a argument-free constructor
 */
class AskButton : public Fl_Button
{
public:

	AskButton( int x=0, int y=0, int w=0, int h=0, const char *label=NULL ) : Fl_Button( x,y,w,h,label )
	{
		clear_visible();
	};

	AskButton( const AskButton &sb ) : Fl_Button( sb.x(),sb.y(),sb.w(),sb.h(),NULL )
	{
		copy_label( sb.label() );
	};

	~AskButton()
	{
		const char *l = label();
		if( l == NULL )
			l = (const char *)"<NULL>";
		LOG_GEN_INFO("Destroying AskButton \"%s\", x = %i, y = %i, w = %i, w = %i\n",
			l, x(), y(), w(), h() );
	};
	
	AskButton& operator=(const AskButton& sb)
	{
		resize( sb.x(), sb.y(), sb.w(), sb.h() );
		copy_label( sb.label() );
		return *this;
	};
	
	void print();

};

class AskDialog : public ControlPanel
{
	typedef std::vector<AskButton>	ButtonVector_T;
	ButtonVector_T	buttons;

	Fl_Button *label_button;

	static const int 	laborder_w = 20;
	static const int 	laborder_h = 10;

	static const int 	vspace = 10;
	static const int 	hspace = 5;

	static const int 	vborder = 5;
	static const int 	hborder = 5;

	void create_choice_buttons( const char *choices );
	void create_layout();

	static void cb_choice( Fl_Button *b, void *data );

public:
	int	user_choice;

	AskDialog( Heart *hrt, const char *label, const char *choices );
	~AskDialog();

	void print();
};

#include <FL/Fl_Window.H>

class AskWindow : public Fl_Window
{
public:
	typedef unsigned char AW_Anchor;

	static const AW_Anchor BOTTOM = 1;
	static const AW_Anchor RIGHT = 1 << 1;
	static const AW_Anchor TOPLEFT = 0;
	static const AW_Anchor HCENTER = 1 << 2;
	static const AW_Anchor VCENTER = 1 << 2;

	static const AW_Anchor TOPRIGHT = RIGHT;
	static const AW_Anchor BOTTOMLEFT = BOTTOM;
	static const AW_Anchor BOTTOMRIGHT = BOTTOM|RIGHT;

	static const AW_Anchor CENTER = HCENTER|VCENTER;

private:
	AskDialog	*dlg;

	int	anchor_x, anchor_y;
	Fl_Widget *real_parent;

	AW_Anchor	anchor_type;

public:

//	AskWindow( Heart *hrt, Fl_Widget *parent, int x, int y, const char *winlabel, const char *label, const char *choices );
	AskWindow( Heart *hrt, Fl_Widget *parent, AW_Anchor anc, int x, int y, const char *winlabel, const char *label, const char *choices );
	~AskWindow();

	int ask( bool wait_for_answer = true);
};

#endif //_Ask_Dialog_h
