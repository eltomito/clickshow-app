/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "billboard_panel.h"

#include "clicky_debug.h"

//========================== class BillboardPanel =========================================

BillboardPanel::BillboardPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, (const char *)"FONT" )
	{
		layout = new SimpLE();	

		button_close = new ClickyButton( 0,0,0,0, "" );
		lbutt_bb = new Fl_Check_Button( 0, 0, 80, 34, _("Projector") );
		lbutt_full = new Fl_Check_Button( 0, 0, 80, 34, _("Fullscreen") );
		lbutt_ontop = new Fl_Check_Button( 0, 0, 80, 34, _("On Top") );

		end();	//to stop adding widgets to this group
		
		//callbacks
		button_close->callback((Fl_Callback*)cb_close);
		lbutt_bb->callback((Fl_Callback*)cb_bb);
		lbutt_full->callback((Fl_Callback*)cb_full);
		lbutt_ontop->callback((Fl_Callback*)cb_ontop);

		//init values
		set_state_from_prefs();

		//tooltips
		lbutt_bb->tooltip( _("Open a separate subtitle window.") );
		lbutt_full->tooltip( _("Make the separate subtitle window fullscreen.") );
		lbutt_ontop->tooltip( _("Put the title at the top\nof the separate subtitle window.") );

		//-- style --

		heart->get_style_engine()->assign_style( StylePrefs::BBCLOSE_BUTTON, (Fl_Button *)button_close );
		heart->get_style_engine()->assign_style( StylePrefs::BBOPTS_BUTTON, lbutt_bb );
		heart->get_style_engine()->assign_style( StylePrefs::BBOPTS_BUTTON, lbutt_full );
		heart->get_style_engine()->assign_style( StylePrefs::BBOPTS_BUTTON, lbutt_ontop );

		//-- layout --

		create_layout();

		//-- refresh --

		refresh_layout();
		refresh_style();
	};

	BillboardPanel::~BillboardPanel()
	{
		delete layout;
	};

	void BillboardPanel::set_state_from_prefs()
	{
		lbutt_bb->value( heart->get_prefs()->bb_visible );
		lbutt_full->value( heart->get_prefs()->bb_fullscreen );
		lbutt_ontop->value( heart->get_prefs()->bb_ontop );
	};

	/*
	 * creates a layout based on which widgets are shown
	 */
	void BillboardPanel::create_layout()
	{
		int	num_cols = base_w+1;
		int	color_col = base_w;
		int	font_col = base_w;

		static const int closeb_size = 21;
		static const int closeb_margin = 0;

		static const int spacex = 10;
		static const int pixx = 10;

//		static const int marginw = closeb_size+closeb_margin;
		static const int marginw = 0;

		layout->add_cell( lbutt_bb, 0, 0, base_w, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add_cell( lbutt_full, 0, 1, base_w, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add_cell( lbutt_ontop, 0, 2, base_w, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add( button_close,
							-1*(closeb_size), 0,			//pixx, pixy
							1.0, 0.0,	//relx, rely
							0.0, 0.0,	//relw, relh
							0, 0,		//margins
							closeb_size, closeb_size );		//min pix
	};

	bool BillboardPanel::get_size( int &w, int &h )
	{
		int	fh;
		int	cw;
		
		cw = base_w;

		fh = heart->get_style_prefs()->NormalButton->ls.Size;
		h = fh*6;
		w = cw * h / base_h;
		return true;
	};

void BillboardPanel::refresh_layout()
{
	layout->refresh_layout( this );
};

//=== callbacks ===

void BillboardPanel::cb_close(Fl_Button* b, void*)
{
	BillboardPanel *p = (BillboardPanel *)b->parent();
	p->heart->show_billboard_options( 0 );
	LOG_RENDER_DETAIL2("Closing the bb, man!\n" );
}

void BillboardPanel::cb_bb(Fl_Check_Button* b, void*)
{
	BillboardPanel *p = (BillboardPanel *)b->parent();
	bool show = ( b->value() == 1 );
	if( show ) {
		p->heart->fix_billboard_prefs();
		p->heart->fix_window_fullscreen();
	} else {
		p->heart->store_billboard_prefs();
	}
	p->heart->show_billboard( show, false );
	LOG_GUI_DETAIL("BILLBOARD visibility switched!!\n");
}

bool BillboardPanel::confirm_full()
{
	std::string query = _("\
The projector will make the controls invisible!\n\
Do you want to make it fullscreen anyway?\n\
(If you do, you can close it by pressing ESC.)");
	std::string buttons = _("#0 #c0xff000000 Make it fullscreen!|#1 #c0x0000ff00 Do nothing");
	return ( 0 == heart->ask( _("Attention!"), query.c_str(), buttons.c_str() ) );
}

void BillboardPanel::cb_full(Fl_Check_Button* b, void*)
{
	BillboardPanel *p = (BillboardPanel *)b->parent();
	bool state = (b->value() == 1);
	if( state && ( -1 == p->heart->bb_find_new_screen_if_necessary() )) {
		if( !p->confirm_full() ) {
			b->value( 0 );
			return;
		}
	}
	p->heart->bb_fullscreen( state, false );
	LOG_GUI_DETAIL("BILLBOARD fullscreen switched!!\n");
}

void BillboardPanel::cb_ontop(Fl_Check_Button* b, void*)
{
	BillboardPanel *p = (BillboardPanel *)b->parent();
	bool state = (b->value() == 1);
	p->heart->bb_ontop( state, false );
	LOG_GUI_DETAIL("BILLBOARD on top switched to state: %s, value: %i!!\n", state ? "ON":"OFF", b->value());
}
