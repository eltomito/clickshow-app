/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Billboard_Panel_h
#define _Billboard_Panel_h

#include <FL/Fl.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Button.H>
//#include <FL/Fl_Color_Chooser.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_prefs.h"
#include "clicky_button.h"

/*
 * the billboard panel class 
 */
class BillboardPanel : public ControlPanel
{
	static const int	base_h = 3;
	static const int	base_w = 8;

	SimpLE	*layout;

	Fl_Button			*button_close;
	Fl_Check_Button	*lbutt_bb;
	Fl_Check_Button	*lbutt_full;
	Fl_Check_Button	*lbutt_ontop;

	static void cb_bb(Fl_Check_Button* b, void*);
	static void cb_full(Fl_Check_Button* b, void*);
	static void cb_ontop(Fl_Check_Button* b, void*);
	static void cb_close(Fl_Button* b, void *panel);

	void create_layout();	

public:
	BillboardPanel(Heart *hrt, int x, int y, int w, int h);
	~BillboardPanel();

	void set_bb_visible_state( bool state ) { lbutt_bb->value( state ? 1 : 0 ); };
	void set_bb_fullscreen_state( bool state ) { lbutt_full->value( state ? 1 : 0 ); };
	void set_bb_ontop_state( bool state ) { lbutt_ontop->value( state ? 1 : 0 ); };
	void set_state_from_prefs();

	void refresh_layout();
	bool get_size( int &w, int &h );
protected:
	bool confirm_full();
};

#endif //_Billboard_Panel_h
