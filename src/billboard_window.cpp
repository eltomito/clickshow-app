/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "billboard_window.h"
#include "clicky_debug.h"

BBWindow::BBWindow( Heart *hrt, int x, int y, int w, int h )
	: Fl_Window(x,y,w,h), heart( hrt )
{
	Prefs *p = heart->get_prefs();
	size_range( 100, 100, 10000, 10000 );
	resizable( this );
	TitlePanelCfg	cfg( StylePrefs::BB_TITLE_DISPLAY, &p->bb_title_border_size );
	title_panel = new TitlePanel( heart, 0, 0, w, h, cfg, p->bb_ontop );
	end();
	callback( &graceful_death );
	show();	
	if( p->bb_fullscreen ) {
		fullscreen();
	} else {
		fullscreen_off();
	}
}

BBWindow::~BBWindow()
{
	title_panel = NULL;
}

void BBWindow::set_ontop( bool state )
{
	title_panel->set_position( state );
	title_panel->refresh_layout();
}

void BBWindow::resize( int X, int Y, int W, int H )
{
	Fl_Window::resize(X,Y,W,H);
	heart->billboard_geom_changed();
}

void BBWindow::graceful_death(Fl_Widget* bbw, void*) {
	((BBWindow *)bbw)->heart->show_billboard( false );
}
