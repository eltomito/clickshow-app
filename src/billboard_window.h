/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BB_Window_h
#define _BB_Window_h

#include <FL/Fl.H>
#include <FL/Fl_Window.H>

#include "title_panel.h"

class Heart;

class BBWindow : public Fl_Window
{
protected:
	Heart 		*heart;
	TitlePanel	*title_panel;

public:
	BBWindow( Heart *hrt, int x, int y, int w, int h );
	~BBWindow();

	TitlePanel *get_title_panel() { return title_panel; };

	void set_ontop( bool state = true );

protected:
	void resize( int X, int Y, int W, int H );
	static void graceful_death(Fl_Widget* bbw, void*);
};

#endif //_BB_Window_h
