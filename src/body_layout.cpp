/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "body_layout.h"

// === Body Layout Brain

bool BodyLayoutBrain::natural_size( int &W, int &H )
{
	((BodyLayout *)body() )->natural_size( W, H );
	return true;
}

// === Body Layout ===

BodyLayout::BodyLayout( int x, int y, int w, int h, const char *l )
				: Fl_Group( x, y, w, h, l )
{
	user_data( (void *)NULL );	//set WidgetBrain to NULL
	WidgetBrain::set_widget_brain(( Fl_Widget *)this, new BodyLayoutBrain( this ) );
}

/*
 * gets the natural widget size if available or its actual size otherwise.
 */
void BodyLayout::get_widget_layout_size( Fl_Widget *w, int &W, int &H )
{
	WidgetBrain *wb = WidgetBrain::get_widget_brain( w );
	if( wb != NULL )
	{
		if( wb->natural_size( W, H ) )
			return;
	};
	W = w->w();
	H = w->h();

}

void BodyLayout::resize( int x, int y, int w, int h )
{
	 // Not calling Fl_Group::resize to avoid Fl_group attempting to scale its children
	 // which is just a waste of time since I am about to redo the layout anyway.
	Fl_Widget::resize( x, y, w, h );
	refresh_layout();
}

// === static layout functions ===

/**
 *
 * returns the minimum number of widgets to contain exactly n visible ones starting from the beginning of warr.
 *
 * parameters:
 *		- n - how many visible widgets we need
 *		- warr - the array of widgets to scan
 *		- warr_size - the size of warr
 *
 * returns:
 *		the minimum number of widgets to encompass n visible ones or warr_size
 *		if there just isn't enough visible widgets left.
 *		If there are 0 visible widgets left, it returns 0.
 *
 */
int BodyLayout::span_n_visible( int n, Fl_Widget **warr, int warr_size )
{
	int cnt = 0;
	int i;
	
	for( i = 0; i < warr_size; i++ )
	{
		if( warr[i]->visible() )
		{
			cnt++;
			if( cnt >= n )
				break;
		};
	};

	if( cnt > 0 )
		return i;
	return 0;
}

/**
 * Measures the size of a set of widgets when arranged in a rectangle.
 *
 * The widgets are put next to each other in a row (or column) until break_after widgets have been layed out.
 * Then, a new row (or column) is started for the next break_after widgets, etc.
 *
 * parameters:
 *		- warr - an array of pointers to Fl_Widget's  
 *		- warr_size - number of widgets in the array  
 *		- W, H - where to store the resulting width and heights  
 *		- rows_first - whether the widgets are arranged in rows or columns
 *		- break_after - how many widgets go in each row (or column - depending on the value of rows_first)
 *		- left_b, right_b, top_b, bottom_b - sizes in pixels of borders to leave around the widgets  
 *		- maj_spacing - spacing along the major axis 
 *		- min_spacing - spacing along the minor axis 
 *
 *	returns:
 *		- true, unless:
 *			warr==NULL or warr_size <= 0 or there are no visible widgets in warr.
 *
 */
bool BodyLayout::rect_size( Fl_Widget ** warr, int warr_size, int &W, int &H,
									bool rows_first,
									int break_after,
									int left_b, int right_b, int top_b, int bottom_b,
									int maj_spacing, int min_spacing )
{
	int ww, hh;
	int max_w, max_h, sum_w, sum_h;
	int cur_i;
	int cur_len;
	int num_visible;

	if(( warr == NULL )||( warr_size < 1 ))
		return false;

	if( break_after <= 0 )
		break_after = warr_size;

	max_w = 0;
	max_h = 0;
	sum_w = 0;
	sum_h = 0;
	cur_i = 0;
	num_visible = 0;

	while( cur_i < warr_size )
	{
		//cur_len = std::min( break_after, (warr_size - cur_i) );

		cur_len = span_n_visible( break_after, &warr[cur_i], (warr_size - cur_i) );

		if( cur_len == 0 )
			break;

		num_visible += BodyLayout::pack_size( &warr[cur_i], cur_len, ww, hh, rows_first,
										0,0,0,0,
										maj_spacing );
		max_w = std::max( ww, max_w );
		max_h = std::max( hh, max_h );
		sum_w += ww;
		sum_h += hh;
		
		cur_i += cur_len;
	}

	if( num_visible <= 0 )
		return false;

	if( rows_first )
	{
		W = max_w + left_b + right_b;
		H = sum_h + top_b + bottom_b + ( (num_visible-1)/break_after )*min_spacing;
	}
	else
	{
		H = max_h + top_b + bottom_b;
		W = sum_w + left_b + right_b + ( (num_visible-1)/break_after )*min_spacing;
	};

	LOG_RENDER_DETAIL2("H=%i, W=%i, max_w=%i, max_h=%i, sum_w=%i, sum_h=%i\n", W, H, max_w, max_h, sum_w, sum_h );

	return true;
}

/**
 * Arranges widgets in a rectangle.
 *
 * The widgets are put next to each other in a row (or column) until break_after widgets have been layed out.
 * Then, a new row (or column) is started for the next break_after widgets, etc.
 *
 * parameters:
 *		- warr - an array of pointers to Fl_Widget's  
 *		- warr_size - number of widgets in the array  
 *		- X, Y, W, H - dimensions of the rectangle to arrange the widgets into  
 *		- rows_first - whether the widgets are arranged in rows or columns
 *		- break_after - how many widgets go in each row (or column - depending on the value of rows_first)
 *		- left_b, right_b, top_b, bottom_b - sizes in pixels of borders to leave around the widgets  
 *		- maj_spacing - spacing along the major axis 
 *		- min_spacing - spacing along the minor axis 
 *		- align - how to align the widgets
 *
 *	returns:
 *		- false if there are no widgets to lay out, true otherwise
 *
 */
bool BodyLayout::layout_rect( Fl_Widget ** warr, int warr_size,
									int X, int Y, int W, int H,
									bool rows_first,
									int break_after,
									int left_b, int right_b, int top_b, int bottom_b,
									int maj_spacing, int min_spacing,  Cl_AlignType align )
{
	int ww, hh;
	int cur_i;
	int cur_len;
	int pix_b;

	if(( warr == NULL )||( warr_size < 1 ))
		return false;

	if( break_after <= 0 )
		break_after = warr_size;

	//alignment
	if( rows_first )
		pix_b = Y + top_b;
	else
		pix_b = X + right_b;

	cur_i = 0;

	while( cur_i < warr_size )
	{
		//cur_len = std::min( break_after, (warr_size - cur_i) );
		
		cur_len = span_n_visible( break_after, &warr[cur_i], (warr_size - cur_i) );		

		BodyLayout::pack_size( &warr[cur_i], cur_len, ww, hh, rows_first,
										0,0,0,0,
										maj_spacing );

		if( rows_first )
		{
			BodyLayout::layout_pack( &warr[cur_i], cur_len, X, pix_b, W, hh,
											true,
											left_b, right_b, 0, 0,
											maj_spacing, align );
			pix_b += min_spacing + hh;
		}
		else
		{
			LOG_RENDER_DETAIL2("pix_b=%i, Y=%i, ww=%i, H=%i, top_b=%i, bottom_b=%i, spacing=%i\n",
											pix_b, Y, ww, H, top_b, bottom_b, maj_spacing );
			
			BodyLayout::layout_pack( &warr[cur_i], cur_len, pix_b, Y, ww, H,
											false,
											0, 0, top_b, bottom_b,
											maj_spacing, align );
			pix_b += min_spacing + ww;
		};
		cur_i += cur_len;
	};
	return true;
}

/**
 * Measures the size of a set of widgets when packed next to each other.
 *
 * parameters:
 *		- warr - an array of pointers to Fl_Widget's  
 *		- warr_size - number of widgets in the array  
 *		- W, H - where to store the resulting width and heights  
 *		- is_horiz - whether the pack is horizontal  
 *		- left_b, right_b, top_b, bottom_b - sizes in pixels of borders to leave around the widgets  
 *		- maj_spacing - spacing along the major axis  
 *
 *	returns:
 *		- number of visible widgets in warr
 *
 */
int BodyLayout::pack_size( Fl_Widget ** warr, int warr_size, int &W, int &H,
									bool is_horiz,
									int left_b, int right_b, int top_b, int bottom_b,
									int maj_spacing )
{
	int ww, hh;
	int maxw, maxh, sumw, sumh;
	int visiwid;

	if(( warr == NULL )||( warr_size < 1 ))
		return 0;

	maxw = 0;
	maxh = 0;
	sumw = 0;
	sumh = 0;

	visiwid = 0;

	//get maximum and summary dimensions
	for( int i = 0; i < warr_size; i++ )
	{
		ww = 0;
		hh = 0;
		if( warr[i]->visible() )
		{
			BodyLayout::get_widget_layout_size( warr[i], ww, hh );
			
			LOG_RENDER_DETAIL2("...pack sizing a widget with w=%i, h=%i\n", ww, hh );
			
			maxw = std::max( maxw, ww );
			maxh = std::max( maxh, hh );
			sumw += ww;
			sumh += hh;
			visiwid += 1;
		};
	};

	if( is_horiz )
	{
		W = sumw + std::max((visiwid-1),0)*maj_spacing;
		H = maxh;
	}
	else
	{
		W = maxw;
		H = sumh + std::max((visiwid-1),0)*maj_spacing;
	};
	
	W += left_b + right_b;
	H += top_b + bottom_b;

	return visiwid;
}

/**
 * Lays out widgets next to each other
 *
 * parameters:
 *		- warr - an array of pointers to Fl_Widget's  
 *		- warr_size - number of widgets in the array  
 *		- X, Y, W, H - coordinates of the rectangle to layout the widgets into  
 *		- is_horiz - whether the pack is horizontal
 *		- left_b, right_b, top_b, bottom_b - sizes in pixels of borders to leave around the widgets  
 *		- maj_spacing - spacing along the major axis  
 *
 *	returns:  
 *		- true unless warr is NULL or war_size is less than 1  
 *
 */
bool BodyLayout::layout_pack( Fl_Widget **warr, int warr_size, int X, int Y, int W, int H,
								bool is_horiz,
								int left_b, int right_b, int top_b, int bottom_b,
								int maj_spacing, Cl_AlignType align )
{
	int ww, hh, aa, bb;
	int cur_aa, cur_bb;
	int A, B, C, D;
	int a_lowb, a_hib, b_lowb, b_hib;
	int pixa, pixb;
	int a_padding;
	int visiwid;

	visiwid = BodyLayout::pack_size( warr, warr_size, ww, hh, is_horiz,
										left_b, right_b, top_b, bottom_b,
										maj_spacing );
	if( visiwid <= 0 )
		return false;

	//initial orientation
	if( is_horiz )
	{
		A = X;
		B = Y;
		C = W;
		D = H;
		a_lowb = left_b;
		a_hib = right_b;
		b_lowb = top_b;
		b_hib = bottom_b;
		aa = ww;
		bb = hh;
	}
	else
	{
		A = Y;
		B = X;
		C = H;
		D = W;
		b_lowb = left_b;
		b_hib = right_b;
		a_lowb = top_b;
		a_hib = bottom_b;
		aa = hh;
		bb = ww;
	}

	//major axis alignment
	if( align&CL_ALIGN_END )
		pixa = A + C - aa;
	else if( align&CL_ALIGN_CENTER )
		pixa = A + (C - aa)/2;
	else
		pixa = A + a_lowb;

	if(( align&CL_ALIGN_EVEN )&&( visiwid > 1 ))
		a_padding = (C-aa) / (visiwid - 1);
	else
		a_padding = 0;

	//layout the widgets!
	for( int i = 0; i < warr_size; i++ )
	{
		if( warr[i]->visible() )
		{
			ww = 0;
			hh = 0;
			BodyLayout::get_widget_layout_size( warr[i], ww, hh );
			if( is_horiz )
			{
				cur_aa = ww;
				cur_bb = hh;
			}
			else
			{
				cur_aa = hh;
				cur_bb = ww;
			};

			//minor axis alignment

			if( align&CL_ALIGN_HIGH )
				pixb = B  + D - b_hib - cur_bb;
			else if( align&CL_ALIGN_MIDDLE )
				pixb = B + b_lowb + (D - b_lowb - b_hib - cur_bb )/2;
			else if( align&CL_ALIGN_FILL )
			{
				pixb = B + b_lowb;
				cur_bb = D - b_lowb - b_hib;
			}
			else
				pixb = B + b_lowb;

			//resize
			if( is_horiz )
				warr[i]->resize( pixa, pixb, cur_aa, cur_bb );
			else
				warr[i]->resize( pixb, pixa, cur_bb, cur_aa );

			pixa += cur_aa + maj_spacing + a_padding;
		};
	};
	return true;	
}
