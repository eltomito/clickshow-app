/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Body_Layout_h
#define _Body_Layout_h

#include <algorithm>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include <FL/Fl_Group.H>
#include "widget_brain.h"

class BodyLayout;

class BodyLayoutBrain : public WidgetBrain
{
public:
	BodyLayoutBrain( BodyLayout *bl ) : WidgetBrain( (Fl_Widget *)bl ) {};
	~BodyLayoutBrain(){};

	virtual bool natural_size( int &W, int &H );
};

class BodyLayout : public Fl_Group
{
public:
	BodyLayout( int x, int y, int w, int h, const char *l = NULL );
	virtual ~BodyLayout() {};

	virtual void refresh_layout() {};
	
	virtual void resize( int x, int y, int w, int h );
	
	virtual int natural_size( int &W, int &H ) { W = 0; H = 0; return 0; };	

	/*
	 * gets the natural widget size if available or its actual size otherwise.
	 */
	static void get_widget_layout_size( Fl_Widget *w, int &W, int &H );

	//== basic layout algorithms ==

	static bool rect_size( Fl_Widget ** warr, int warr_size, int &W, int &H,
									bool rows_first,
									int break_after,
									int left_b = 0, int right_b = 0, int top_b = 0, int bottom_b = 0,
									int maj_spacing = 0, int min_spacing = 0 );

	static bool layout_rect( Fl_Widget ** warr, int warr_size,
									int X, int Y, int W, int H,
									bool rows_first,
									int break_after,
									int left_b = 0, int right_b = 0, int top_b = 0, int bottom_b = 0,
									int maj_spacing = 0, int min_spacing = 0,  Cl_AlignType align = 0 );

	static int pack_size( Fl_Widget ** warr, int warr_size, int &W, int &H,
									bool is_horiz = true,
									int left_b = 0, int right_b = 0, int top_b = 0, int bottom_b = 0,
									int maj_spacing = 0 );

	static bool layout_pack( Fl_Widget ** warr, int warr_size, int X, int Y, int W, int H,
									bool is_horiz = true,
									int left_b = 0, int right_b = 0, int top_b = 0, int bottom_b = 0,
									int maj_spacing = 0, Cl_AlignType align = 0 );

	//== basic layout algorithms helper functions ==

	static int span_n_visible( int n, Fl_Widget **warr, int warr_size );
};

#endif //_Body_Layout_h
