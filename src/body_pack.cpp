/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "body_pack.h"

#include <algorithm>

BodyPack::BodyPack(  bool is_horiz, int x, int y, int w, int h, const char *l )
			: BodyLayout( x, y, w, h, l )
{
	horizontal( is_horiz );
	borders( 0,0,0,0 );
	spacing( 0 );
	align( 0 );
}

void BodyPack::borders( int l, int r, int t, int b )
{
	left_b = l;
	right_b = r;
	top_b = t;
	bottom_b = b;
}

int BodyPack::natural_size( int &W, int &H )
{
	return BodyLayout::pack_size( (Fl_Widget**)array(), children(), W, H,
		horizontal(),
		left(), right(), top(), bottom(),
		spacing() );
}

void BodyPack::refresh_layout()
{
	BodyLayout::layout_pack( (Fl_Widget**)array(), children(), x(), y(), w(), h(),
		horizontal(),
		left(), right(), top(), bottom(),
		spacing(), align() );
}

/*
void BodyPack::resize( int x, int y, int w, int h )
{
	BodyLayout::resize( x, y, w, h );
	refresh_layout();
};
*/
