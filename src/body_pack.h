/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Body_Pack_h
#define _Body_Pack_h

#include "body_layout.h"

/*
 *This class could use PackBodyGeom in the future but let's not overdo it for now.
 */
class BodyPack : public BodyLayout
{
private:
	bool	horiz;
	Cl_AlignType alignment;

	int left_b;
	int right_b;
	int top_b;
	int bottom_b;
	int space;

public:
	BodyPack( bool is_horiz=true, int x=0, int y=0, int w=0, int h=0, const char *l = NULL );
	~BodyPack() {};

	void spacing( int s ) { space = s;};
	int spacing() { return space;};

	void horizontal( bool is_horiz ) { horiz = is_horiz; };
	bool horizontal() { return horiz; };

	void align( Cl_AlignType a ) { alignment = a; };
	Cl_AlignType align() { return alignment; };

	void borders( int l, int r, int t, int b );
	int left() { return left_b;};
	int right() { return right_b;};
	int top() { return top_b;};
	int bottom() { return bottom_b;};

	virtual int natural_size( int &W, int &H );

	//void draw();

	//virtual void resize( int x, int y, int w, int h );

	void refresh_layout();
};

#endif //_Body_Pack_h
