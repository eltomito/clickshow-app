/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "brain_mlinput.h"
#include <FL/fl_draw.H>

MLInputBrain::MLInputBrain( Fl_Widget *w, int chars_per_line, int lines   ) : WidgetBrain( w )
{
	width( chars_per_line );
	height( lines );
};

void MLInputBrain::width( int cols )
{
		num_cols = cols;
}

int MLInputBrain::width()
{
		return num_cols;
}

void MLInputBrain::height( int lines )
{
		num_lines = lines;
}

int MLInputBrain::height()
{
		return num_lines;
}

bool MLInputBrain::natural_size( int &W, int &H )
{
	char teststr[ max_teststr_len + 1 ];
	int testlen;
	int ww, hh;

	if(( num_cols < 0 )||( num_lines < 0 ))
		return false;

	Fl_Multiline_Input *mli = (Fl_Multiline_Input *)body();

	//generate a text string
	if( num_cols <= max_teststr_len )
		testlen = num_cols;
	else
		testlen = max_teststr_len;
	
	for( int i = 0; i < testlen; i++ )
		teststr[i] = 'W';

	teststr[ testlen ] = 0;

	//measure test string in the right font
	Fl_Font oldfont = fl_font();
	Fl_Fontsize oldsize = fl_size();
	fl_font( mli->textfont(), mli->textsize() );
	ww = 0;
	hh = 0;
	fl_measure( &teststr[0], ww, hh, true );
	fl_font( oldfont, oldsize );

	//compute the width of num_cols
	if( testlen != num_cols )
		ww = (ww * num_cols) / testlen;

	W = ww;
	H = hh * num_lines;
	return true;
}
