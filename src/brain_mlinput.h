/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BrainMLInput_h
#define _BrainMLInput_h

#include "widget_brain.h"
#include <FL/Fl_Multiline_Input.H>

class MLInputBrain : public WidgetBrain
{
private:
	int num_lines;
	int num_cols;

public:
	static const int max_teststr_len = 100;

	MLInputBrain( Fl_Widget *w, int chars_per_line, int lines  );
	~MLInputBrain() {};

	virtual bool natural_size( int &W, int &H );

	void width( int cols );
	void height( int rows );
	int width();
	int height();
};

#endif //_BrainMLInput_h
