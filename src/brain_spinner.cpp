/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "brain_spinner.h"
#include <FL/fl_draw.H>

SpinnerBrain::SpinnerBrain( Fl_Widget *w, int _digits ) : WidgetBrain( w )
{
	digits( _digits );
};

void SpinnerBrain::digits( int _digits )
{
		num_digits = _digits;
}

int SpinnerBrain::digits()
{
		return num_digits;
}

bool SpinnerBrain::natural_size( int &W, int &H )
{
	char teststr[ max_teststr_len + 1 ];
	int testlen;
	int ww, hh;

	if( num_digits < 0 )
		return false;

	Fl_Spinner *sp = (Fl_Spinner *)body();
	int nd = num_digits + 3; //add space for the controls

	//generate a text string
	if( num_digits <= max_teststr_len )
		testlen = nd;
	else
		testlen = max_teststr_len;
	
	for( int i = 0; i < testlen; i++ )
		teststr[i] = '0';

	teststr[ testlen ] = 0;

	//measure test string in the right font
	Fl_Font oldfont = fl_font();
	Fl_Fontsize oldsize = fl_size();
	fl_font( sp->textfont(), sp->textsize() );
	ww = 0;
	hh = 0;
	fl_measure( &teststr[0], ww, hh, true );
	fl_font( oldfont, oldsize );

	//compute the width of num_cols
	if( testlen != nd )
		ww = (ww * nd) / testlen;

	W = ww;
	H = hh;

	LOG_RENDER_INFO("SPINNER WIDTH = %i\n", ww );

	return true;
}
