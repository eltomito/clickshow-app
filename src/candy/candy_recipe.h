/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Candy_Recipe_h_
#define _Candy_Recipe_h_ 1

#include "../markup/markup_edit.h"
//#include "../parser/parser.h"
//#include "candy_syntax.h"
#include "../rainbow.h"

class CandyRecipe {
public:
	CandyRecipe() : final_color(0) {};
	CandyRecipe( RainbowMarkup &rm, ComposedMEdit &ed, int _final_color ) : markup(rm), edit(ed), final_color(_final_color) {};
	~CandyRecipe() {};

	void clear() { final_color = 0; markup.clear(); edit.clear(); };

	int final_color;
	RainbowMarkup markup;
	ComposedMEdit edit;
};

#endif //_Candy_Recipe_h_
