/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "candy_syntax.h"
#include <algorithm>
#include "shared/src/default_log.h"

//============ BigState ============

BigState::BigState( int _initial_color )
	: markValid(false), charsDeleted(0), closeParenth(0), last_bslash_index(-1),
	initial_color(_initial_color), final_color(_initial_color),
	color_before_parenth(_initial_color)
{
	hsMarks = boost::make_shared<HSMarks>( this );
	hsAfterDude = boost::make_shared<HSAfterDude>( this );
};

//======== CandySyntax ===========

CandyRecipe CandySyntax::parse( const std::string &str, int initial_color )
{
	Parser parser;

	BigState bs(initial_color);

	PHSetPtr hs = boost::make_shared<HSBase>( &bs );
	parser.parse( str.c_str(), hs );

	//add initial markup if needed
	RainbowMarkup::iterator it = bs.rm.begin();
	if(( bs.rm.size() == 0 )||( it->start() > 0 )) {
		bs.rm.insert( it, RainbowStyle( 0, initial_color, 0, RainbowStyle::M_COLOR ) );
	};

	return CandyRecipe( bs.rm, bs.ed, bs.final_color );
};

/** If str points to a paint mark (e.g. |0|,
  * this function returns the byte length of the paint mark.
  * -1 is returned otherwise.
 */
int matchPaintMark( const char *str, int *val = NULL ) {
	const char *s = str;
	if( s[0] != '|' ) return -1;
	++s;
	int v = 0;
	while( (s[0] >= '0') && (s[0] <= '9') ) {
		v *= 10;
		v += (int)(s[0] - '0');
		++s;
	}
	if( s[0] != '|' ) { return -1; }
	if( val ) { *val = v; }
	return s - str + 1;
};

/*
 * This is meant to be called by the action
 * hooks of various mark handlers
 * to find out if the mark at pos is escaped
 * and therefore invalid.
 * It also deletes the escape mark if so.
 */
bool amIEscaped( int pos, PHSet &phSet ) {
	BigState &bs = ((HSBigState &)phSet).bigState;
	if( (bs.last_bslash_index >= 0) && (bs.last_bslash_index == pos - 1) ) {
		//this mark is escaped. Make the escape disappear and that's it.
		bs.ed.applyOnMe( boost::make_shared<SimpleMEdit>( pos - 1, 1 ) );
		bs.last_bslash_index = -1;
		return true;
	}
	return false;
};

//--------------- phGenEscape -----------

int phGenEscape::isMine( const char *str, int pos, PHSet &phSet ) {

	int byteLen;
	int cnt = utf8_runlength( str, (ucsT)'\\', &byteLen );
	bslashCnt = cnt;
	return ( cnt > 0 ) ? byteLen : -1;
}

PHARes phGenEscape::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	if( bslashCnt % 2 ) {
		((HSBigState&)phSet).bigState.last_bslash_index = pos + bslashCnt - 1;
	}
	return PHARes( matchLen );
};

//--------------------- phEndParenthMark ---------------

int phEndParenthMark::isMine( const char *str, int pos, PHSet &phSet ) {
	ucsT ucs;
	int charlen;
	ucs = utf8_getchar( str, &charlen );
	HSEndParenth &hsep = (HSEndParenth &)phSet;

	LOG_CANDY_DETAIL("looking for '%c', found '%c'\n",(char)(hsep.closingParenth&0xff),(char)(ucs&0xff) );

	return ( ucs == hsep.closingParenth ) ? charlen : -1;
};

PHARes phEndParenthMark::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	HSEndParenth &hsep = (HSEndParenth &)phSet;

	if( amIEscaped( pos, phSet ) ) { return PHARes( matchLen ); }

	BigState &bs = hsep.bigState;
	bs.rm.push_back( RainbowStyle( pos + matchLen, hsep.bigState.color_before_parenth, 0, RainbowStyle::M_COLOR ) );
	if( hsep.isVanishing ) {
		bs.ed.applyOnMe( boost::make_shared<SimpleMEdit>( pos, matchLen ) );
	}
	//The end of candy parentheses is a mark condition,
	//so we should push the Mark Handler Set now.
	return PHARes( hsep.bigState.hsMarks, matchLen, true );
};

//---------------------- phSkipSpaces ----------------

int phSkipSpaces::isMine( const char *str, int pos, PHSet &phSet ) {
	int charlen;
	const char *s = str;
	ucsT ucs = utf8_getchar( s, &charlen );
	while( utf8_is_space( ucs ) ) {
		s += charlen;
		ucs = utf8_getchar( s, &charlen );
	}
	return ( s > str ) ? ( s - str ) : -1;
};

PHARes phSkipSpaces::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	return PHARes( matchLen );
};

bool isDudeMark( ucsT ucs ) {
	return( (ucs == (ucsT)'*')||(ucs == (ucsT)'@')||(ucs == (ucsT)'+')
	||(ucs == (ucsT)'-')||(ucs == (ucsT)'~')||(ucs == (ucsT)'^')||(ucs == (ucsT)'`') );
};

int dudeMarkColor( ucsT ucs ) {
	switch( ucs ) {
		case (ucsT)'*':
			return 0;
		break;
		case (ucsT)'@':
			return 1;
		break;
		case (ucsT)'+':
			return 2;
		break;
		case (ucsT)'-':
			return 3;
		break;
		case (ucsT)'~':
			return 5;
		break;
		case (ucsT)'^':
			return 6;
		break;
		case (ucsT)'`':
			return 7;
		break;
		default:
		break;
	};
	return -1;
};

//---------------------- phDudeMark ------------------

int phDudeMark::isMine( const char *str, int pos, PHSet &phSet ) {
	ucsT ucs;

	int charlen;
	ucs = utf8_getchar( str, &charlen );
	return isDudeMark( ucs ) ? charlen : -1;
};

PHARes phDudeMark::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	ucsT ucs;
	int charlen;
	int clri;

	HSMarks &hsm = (HSMarks &)phSet;

	if( amIEscaped( pos, phSet ) ) { return PHARes( matchLen ); }

	ucs = utf8_getchar( str, &charlen );
	clri = dudeMarkColor( ucs );
	if( clri < 0 ) {
			throw std::invalid_argument( "phDudeMark::action called on something weird!" );
	}
	hsm.bigState.final_color = clri;
	hsm.bigState.rm.push_back( RainbowStyle( pos, clri, 0, RainbowStyle::M_COLOR ) );
	hsm.bigState.ed.applyOnMe( boost::make_shared<SimpleMEdit>( pos, 1, "-" ) );

	PHARes res( hsm.bigState.hsAfterDude, matchLen, true );

	return res;
};

//---------------------- phDudeParenthMark ------------------

/** Detects a dude mark followed by an open parentheses,
 *  i.e., things like this: *(
 * It's the start of a new voice named in parentheses
 * such as @(John) which will be turned into just (John)
 * In the appropriate color.
 */
int phDudeParenthMark::isMine( const char *str, int pos, PHSet &phSet ) {
	ucsT ucs;

	int charlen;
	ucs = utf8_getchar( str, &charlen );
	if( !isDudeMark( ucs ) ) {
		return -1;
	}
	if( str[ charlen ] == '(' ) {
		return charlen + 1;
	}
	return -1;
};

PHARes phDudeParenthMark::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	ucsT ucs;
	int charlen;
	int clri;

	HSMarks &hsm = (HSMarks &)phSet;

	if( amIEscaped( pos, phSet ) ) { return PHARes( matchLen ); }

	ucs = utf8_getchar( str, &charlen );
	clri = dudeMarkColor( ucs );
	if( clri < 0 ) {
			throw std::invalid_argument( "phDudeParenthMark::action called on something weird!" );
	}
	hsm.bigState.final_color = clri;
	hsm.bigState.rm.push_back( RainbowStyle( pos, clri, 0, RainbowStyle::M_COLOR ) );
	hsm.bigState.ed.applyOnMe( boost::make_shared<SimpleMEdit>( pos, 1 ) );

	PHARes res( matchLen, true );

	return res;
};

//---------------------- phParenthMark ------------------

int phParenthMark::isMine( const char *str, int pos, PHSet &phSet ) {
	int charlen;
	return CandySyntax::isOpenParenthMark( utf8_getchar( str, &charlen ) ) ? charlen : -1;
};

PHARes phParenthMark::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	ucsT ucs;
	int charlen;
	int clri = 4;
	bool isVanishing;

	HSMarks &hsm = (HSMarks &)phSet;

	if( amIEscaped( pos, phSet ) ) { return PHARes( matchLen ); }

	ucs = utf8_getchar( str, &charlen );
	isVanishing = CandySyntax::isVanishingMark( ucs );
	hsm.hsEndParenth->init( CandySyntax::closingParenth( ucs ), isVanishing );
	PHARes res( hsm.hsEndParenth, matchLen, true );

	hsm.bigState.rm.push_back( RainbowStyle( pos, clri, 0, RainbowStyle::M_COLOR ) );
	if( isVanishing ) {
		hsm.bigState.ed.applyOnMe( boost::make_shared<SimpleMEdit>( pos, matchLen ) );
	}
	hsm.bigState.color_before_parenth = hsm.bigState.final_color;
	return res;
};

//------------------ phMarkCondition ----------------

int phMarkCondition::isMine( const char *str, int pos, PHSet &phSet ) {
	int skip = -1;
	int charlen;
	ucsT ucs;

	if( pos == 0 ) {
		skip = 0;
	} else if( str[0] == '\n' ) {
		skip = 1;
	} else {
		ucs = utf8_getchar( str, &charlen );
//		LOG_CANDY_DETAIL2("Is terminator? charlen: %i, ucs: %x \"%s\"...\n", charlen, ucs, str );
		if( CandySyntax::isSentenceTerminator( ucs ) && ( str[charlen] == ' ' ) ) {
			skip = charlen + 1;
//			LOG_CANDY_DETAIL2("YES!\n" );
		} else {
//			LOG_CANDY_DETAIL2("NO.\n" );
		}
	}
	return skip;
}
PHARes phMarkCondition::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	return PHARes( ((HSBase&)phSet).bigState.hsMarks, matchLen );
};

//------------------- phPaint ------------------

int phPaint::isMine( const char *str, int pos, PHSet &phSet ) {
	int l = matchPaintMark( str );
	return ( l > 0 ) ? l : -1;
};

PHARes phPaint::action( const char *str, int pos, int matchLen, PHSet &phSet ) {
	if( amIEscaped( pos, phSet ) ) { return PHARes( matchLen ); }

	BigState &bs = ((HSBigState&)phSet).bigState;
	int clri = 0;
	int l = matchPaintMark( str, &clri );
	assert( l == matchLen );
	bs.rm.push_back( RainbowStyle( pos, clri, 0, RainbowStyle::M_COLOR ) );
	bs.ed.applyOnMe( boost::make_shared<SimpleMEdit>( pos, l ) );
	bs.final_color = clri;
	return PHARes( l );
};
