/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Candy_Syntax_h_
#define _Candy_Syntax_h_ 1

#include "../parser/parser.h"
#include "../rainbow.h"
#include "../markup/markup_edit.h"
#include "../utf8_tools.h"
#include "candy_recipe.h"
#include "shared/src/default_log.h"

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>
//#include <boost/enable_shared_from_this.hpp>

typedef unsigned int ucsT;

//-- hander set pointers --

class HSEndParenth;
typedef boost::shared_ptr<HSEndParenth> HSEndParenthPtr;
class HSMarks;
typedef boost::shared_ptr<HSMarks> HSMarksPtr;
class HSAfterDude;
typedef boost::shared_ptr<HSAfterDude> HSAfterDudePtr;

//-- BigState --

class BigState {
public:
	BigState( int _initial_color = 0 );
	~BigState() {};

	bool markValid;
	ucsT closeParenth;
	int initial_color;

	int charsDeleted;

	int last_bslash_index;
	int final_color;
	int color_before_parenth;
	RainbowMarkup rm;
	ComposedMEdit ed;

//-- reusable handler sets --
	HSMarksPtr hsMarks;
	HSAfterDudePtr hsAfterDude;
};

class HSBigState : public PHSet
{
public:
	HSBigState( BigState *_bigState ) : bigState(*_bigState) {};
	~HSBigState() {};
	BigState	&bigState;
};

//--- reusable handlers ---

PHDEF1(phGenEscape,int,bslashCnt)
PHDEF(phPaint)

//-- HSBase --
PHDEF(phMarkCondition)

class HSBase : public HSBigState
{
public:
	HSBase( BigState *_bigState ) : HSBigState(_bigState) {
		handlers.push_back( boost::make_shared<phGenEscape>() );
		handlers.push_back( boost::make_shared<phPaint>() );
		handlers.push_back( boost::make_shared<phMarkCondition>() );
	};
	~HSBase() {};

	PHARes defaultAction( const char *str, int pos, PHSet &phSet ) { return PHARes( str, 1 ); };
};

PHDEF(phDudeMark)
PHDEF(phParenthMark)
PHDEF(phSkipSpaces)
PHDEF(phDudeParenthMark)

//--- HSMarks ---

class HSMarks : public HSBigState
{
public:
	HSMarks( BigState *_bigState ) : HSBigState(_bigState) {
		handlers.push_back( boost::make_shared<phGenEscape>() );
		handlers.push_back( boost::make_shared<phPaint>() );
		handlers.push_back( boost::make_shared<phDudeParenthMark>() ); //things like *(
		handlers.push_back( boost::make_shared<phDudeMark>() );
		handlers.push_back( boost::make_shared<phParenthMark>() );
		handlers.push_back( boost::make_shared<phSkipSpaces>() ); //Marks can start after a run of spaces.
		hsEndParenth = boost::make_shared<HSEndParenth>( _bigState );
	};
	~HSMarks() {};
	PHARes defaultAction( const char *str, int pos, PHSet &phSet ) { return PHARes( str, 1, true ); };
	HSEndParenthPtr hsEndParenth;
};

//--- HSAfterDude ---

class HSAfterDude : public HSBigState
{
public:
	HSAfterDude( BigState *_bigState ) : HSBigState(_bigState) {
		handlers.push_back( boost::make_shared<phGenEscape>() );
		handlers.push_back( boost::make_shared<phParenthMark>() );
		hsEndParenth = boost::make_shared<HSEndParenth>( _bigState );
	};
	~HSAfterDude() {};
	PHARes defaultAction( const char *str, int pos, PHSet &phSet ) { return PHARes( 0, true ); };
	HSEndParenthPtr hsEndParenth;
};

//--- HSEndParenth ---

PHDEF(phEndParenthMark)

class HSEndParenth : public HSBigState
{
public:
	HSEndParenth( BigState *_bigState, ucsT _closeChar = 0, bool _vanish = false ) : HSBigState(_bigState), closingParenth(_closeChar), isVanishing(_vanish) {
		handlers.push_back( boost::make_shared<phGenEscape>() );
		handlers.push_back( boost::make_shared<phEndParenthMark>() );
		handlers.push_back( boost::make_shared<phPaint>() );
		LOG_CANDY_DETAIL("Created HSEndParenth!\n" );
	};
	~HSEndParenth() {};

	void init( ucsT _closeChar, bool _vanish = false ) {
		closingParenth = _closeChar;
		isVanishing = _vanish;
	};

	PHARes defaultAction( const char *str, int pos, PHSet &phSet ) { return PHARes( str, 1, false ); };

	ucsT closingParenth;
	bool isVanishing;
};

//======= CandySyntax =====

class CandySyntax
{
private:
	CandySyntax() {};
	~CandySyntax() {};
public:
	static CandyRecipe parse( const std::string &str, int initial_color = 0 );

	static bool isSentenceTerminator( ucsT ucs ) {
		return (ucs == (ucsT)'.')||(ucs == (ucsT)'?')||(ucs == (ucsT)'!')
				||(ucs == (ucsT)':')||(ucs == (ucsT)0xe280a6 );	//0xe280a6 == "..."
	};
	static bool isOpenParenthMark( ucsT ucs ) {
		return (ucs == (ucsT)'(')||(ucs == (ucsT)'[')||(ucs == (ucsT)'{');
	};
	static bool isVanishingMark( ucsT ucs ) {
		return (ucs == (ucsT)'{')||(ucs == (ucsT)'}');
	};
	static ucsT closingParenth( ucsT ucs ) {
		switch( ucs ) {
			case (ucsT)'(':
				return (ucsT)')';
			break;
			case (ucsT)'[':
				return (ucsT)']';
			break;
			case (ucsT)'{':
				return (ucsT)'}';
			break;
			default:
			break;
		}
		return 0;
	};
};

#endif //_Candy_Syntax_h_
