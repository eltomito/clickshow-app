/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "candy_panel.h"

#include "clicky_debug.h"

CandyColorButton::CandyColorButton( int index, const char *label, int x, int y, int w, int h ) : ClickyButton( x, y, w, h, label )
{
	color_index = index;
}

//========================== class CandyPanel =========================================

const char *CandyPanel::color_names[ candy_color_count ] = {"*","@","+","-","()","~","^","`"};

CandyPanel::CandyPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, (const char *)"FONT" )
	{
		layout = new SimpLE();	

		for( int i = 0; i < candy_color_count; i++ ) {
			buttons_color[i] = new CandyColorButton( i, color_names[i] );
		}
		button_mode = new ClickyButton( 0, 0, 0, 0, "" );
		button_reset = new ClickyButton( 0, 0, 0, 0, _("Reset") );
		button_close = new ClickyButton( 0,0,0,0, "" );

		button_load = new ClickyButton( 0, 0, 0, 0, _("Load") );
		button_save = new ClickyButton( 0, 0, 0, 0, _("Save") );

		color_chooser = new Fl_Color_Chooser( 0,0,200,100,"" );
		//simplify_color_chooser( color_chooser );

		end();	//to stop adding widgets to this group

		//mode setup
		mode_states.push_back(_("Off"));
		mode_states.push_back(_("On"));
		mode_states.push_back(_("Auto"));
		mode_states.push_back(_("#@@*+-"));
		current_mode = heart->get_prefs()->parse_candy_colors % mode_states.size();
		button_mode->label( mode_states[current_mode].c_str() );

		//callbacks
		button_close->callback((Fl_Callback*)cb_close);
		button_mode->callback((Fl_Callback*)cb_mode);
		button_reset->callback((Fl_Callback*)cb_reset);
		button_load->callback((Fl_Callback*)cb_load);
		button_save->callback((Fl_Callback*)cb_save);

		color_chooser->callback((Fl_Callback*)cb_color_chooser);

		//-- style --

		heart->get_style_engine()->assign_style( StylePrefs::CANDYOPTS_BUTTON, (Fl_Button *)button_mode );
		heart->get_style_engine()->assign_style( StylePrefs::CANDYOPTS_BUTTON, (Fl_Button *)button_reset );
		heart->get_style_engine()->assign_style( StylePrefs::CANDYOPTS_BUTTON, (Fl_Button *)button_load );
		heart->get_style_engine()->assign_style( StylePrefs::CANDYOPTS_BUTTON, (Fl_Button *)button_save );
		heart->get_style_engine()->assign_style( StylePrefs::CPCLOSE_BUTTON, (Fl_Button *)button_close );

		for( int i = 0; i < candy_color_count; i++ ) {
			buttons_color[i]->callback((Fl_Callback*)cb_color);
			heart->get_style_prefs()->CandyOptsButton->apply( (Fl_Button *)buttons_color[i] );
			//heart->get_style_engine()->assign_style( StylePrefs::CANDYOPTS_BUTTON, (Fl_Button *)buttons_color[i] );
		}

		//color choser setup
		active_color = 0;
		set_active_color( 0 );

		//-- layout --

		create_layout();

		//-- refresh --

		refresh_layout();
		refresh_style();
	};

	CandyPanel::~CandyPanel()
	{
		delete layout;
	};

	void CandyPanel::set_active_color( int c )
	{
		double	r, g, b;
		Fl_Color	flcolor;

		bool same_color = false;

		c = c % candy_color_count;
		if( c == active_color ) {
			same_color = true;
		}

		if( !same_color ) {
			heart->get_style_prefs()->CandyOptsButton->apply( (Fl_Button *)buttons_color[ active_color ] );
			buttons_color[ active_color ]->redraw();
		}

		buttons_color[ c ]->box( FL_NO_BOX ); 
		buttons_color[ c ]->redraw();

		active_color = c % candy_color_count;

		flcolor = heart->get_prefs()->candy_colors[ active_color ];
		CandyPanel::flcolor2rgb( flcolor, r, g, b );
		color_chooser->rgb( r, g, b );
	}

	/*
	 * creates a layout based on which widgets are shown
	 */
	void CandyPanel::create_layout()
	{
		layout->add_cell( button_reset, 0, 0, firstw, 1,	//cx, cy, cw, ch
											totalw, totalh,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces

		layout->add_cell( button_mode, firstw, 0, secondw, 1,	//cx, cy, cw, ch
											totalw, totalh,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginw,		//margins
											spacex, spacey );		//spaces

		for( int i = 0; i < candy_color_count; i++) {
			layout->add_cell( buttons_color[i], (i%4)*firstw/2, 1 + (i/4), secondw/2, 1,	//cx, cy, cw, ch
												totalw, totalh,				//columns, rows
												pixx, pixy,			//pixx, pixy
												0.0, 0.0,	//relx, rely
												1.0, 1.0,	//relw, relh
												marginw, marginh,		//margins
												spacex, spacey );		//spaces
		}

		layout->add( button_close,
							-1*(closeb_size), 0,			//pixx, pixy
							1.0, 0.0,	//relx, rely
							0.0, 0.0,	//relw, relh
							0, 0,		//margins
							closeb_size, closeb_size );		//min pix

		layout->add_cell( color_chooser, firstw + secondw, 0, chooserw, totalh,	//cx, cy, cw, ch
										totalw, totalh,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0.0, 0.0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

		layout->add_cell( button_load, firstw + secondw + chooserw, 0, fourthw, 1,	//cx, cy, cw, ch
											totalw, totalh,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex + closeb_size, spacey );		//spaces

		layout->add_cell( button_save, firstw + secondw + chooserw, 1, fourthw, 1,	//cx, cy, cw, ch
											totalw, totalh,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex + closeb_size, spacey );		//spaces
	};

	bool CandyPanel::get_size( int &w, int &h )
	{
		int	fh;
		
		fh = heart->get_style_prefs()->NormalButton->ls.Size;
		h = 2*totalh*fh;
		w = 3*totalw*fh;
		return true;
	};

	void CandyPanel::refresh_layout()
	{
		layout->refresh_layout( this );
	};

Fl_Color CandyPanel::rgb2flcolor( double r, double g, double b )
{
	Fl_Color red, green, blue;
	
	red = (Fl_Color)( (double)255*r );
	green = (Fl_Color)( (double)255*g );
	blue = ((Fl_Color)( double)255*b );

	LOG_GEN_INFO("converting: r %i, g %i, b %i\n", red, green, blue );

	return( (red<<24) + (green<<16) + (blue<<8) );
};

void CandyPanel::flcolor2rgb( Fl_Color flcolor, double &r, double &g, double &b )
{
	if( flcolor < 265 ) {
		unsigned char cr, cg, cb;
		Fl::get_color( flcolor, cr, cg, cb );
		r = (double)cr / 256;
		g = (double)cg / 256;
		b = (double)cb / 256;
	} else {
		r = ((double)( (flcolor&0xff000000) >> 24 ))/256;
		g = ((double)( (flcolor&0x00ff0000) >> 16 ))/256;
		b = ((double)( (flcolor&0x0000ff00) >> 8 ))/256;
	}
	LOG_GEN_INFO("converted Fl_Color %x -> r %f, g %f, b %f\n", flcolor, r, g, b );
};

//=== callbacks ===

	void CandyPanel::cb_color_chooser(Fl_Color_Chooser* b, void*)
	{
		double red, green, blue;
		Fl_Color clr;

		CandyPanel *p = (CandyPanel *)b->parent();

		red = b->r();
		green = b->g();
		blue = b->b();
		
		clr = rgb2flcolor( red, green, blue );

		LOG_GEN_INFO("r %f, g %f, b %f, color = %x\n", (float)red, (float)green, (float)blue, clr );

		p->heart->set_candy_color( p->active_color, clr );
	};

	void CandyPanel::cb_close(Fl_Button* b, void*)
	{
		CandyPanel *p = (CandyPanel *)b->parent();
		p->heart->show_candy_options( 0 );
		LOG_GEN_INFO("Close, man!\n" );  
	};

	void CandyPanel::cb_color(Fl_Button* b, void*)
	{
		CandyPanel *p = (CandyPanel *)b->parent();
		p->set_active_color( ((CandyColorButton *)b)->get_color_index() );
		LOG_GEN_INFO("Color, man!\n" );  
	}

	void CandyPanel::cb_reset(Fl_Button* b, void*)
	{
		CandyPanel *p = (CandyPanel *)b->parent();
		p->heart->reset_candy_colors();
		p->set_active_color( p->get_active_color() );
		LOG_GEN_INFO("Reset, man!\n" );  
	}

	void CandyPanel::cb_load(Fl_Button* b, void*)
	{
		CandyPanel *p = (CandyPanel *)b->parent();
		p->heart->load_or_save_candy_colors( false );
		p->set_active_color( p->get_active_color() );
		LOG_GEN_INFO("Loading the load, man!\n" );
	}

	void CandyPanel::cb_save(Fl_Button* b, void*)
	{
		CandyPanel *p = (CandyPanel *)b->parent();
		p->heart->load_or_save_candy_colors( true );
		LOG_GEN_INFO("Saving the load, man!\n" );
	}

	void CandyPanel::cycle_mode() {
		current_mode = (current_mode+1) % mode_states.size();
		button_mode->label( mode_states[ current_mode ].c_str() );
		heart->switch_candy_parse( current_mode );
		heart->switch_candy_render( current_mode > 0 );
	}

	void CandyPanel::cb_mode(Fl_Button* b, void*)
	{
		CandyPanel *p = (CandyPanel *)b->parent();
		p->cycle_mode();
		//p->heart->clicked_font_chooser();
		LOG_GEN_INFO("Mode, man!\n" );  
	}

