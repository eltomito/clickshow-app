/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Candy_Panel_h
#define _Candy_Panel_h

#include <FL/Fl.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Button.H>

#include <vector>
#include <string>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_prefs.h"
#include "my_color_chooser.h"
#include "clicky_button.h"
#include "candy_color_constants.h"

class CandyColorButton : public ClickyButton
{
private:
	int color_index;

public:
	CandyColorButton( int index, const char *label, int x = 0, int y = 0, int w = 0, int h = 0 );
	~CandyColorButton() {};

	int get_color_index() { return color_index; };
};

/*
 * the candy panel class 
 */
class CandyPanel : public ControlPanel
{
	static const int candy_color_count = CLICKY_CANDY_COLOR_COUNT;
	static const char *color_names[ candy_color_count ];

	static const int 	firstw = 2;
	static const int 	secondw = 2;
	static const int 	chooserw = 4;
	static const int 	fourthw = 3;
	static const int	totalw = firstw + secondw + chooserw + fourthw;
	static const int	closeb_size = 21;
	static const int	closeb_margin = 0;
	static const int	totalh = 3;

	static const int	pixx = 4;
	static const int	pixy = 4;
	static const int	marginw = 4;
	static const int	marginh = 4;
	static const int	spacex = 4;
	static const int	spacey = 4;

	SimpLE	*layout;

	Fl_Button	*button_mode;
	Fl_Button	*button_reset;
	Fl_Button	*button_load;
	Fl_Button	*button_save;
	Fl_Button	*buttons_color[ candy_color_count ];
	Fl_Button	*button_close;

	Fl_Color_Chooser	*color_chooser;

	int max_border;

	unsigned	int	visible_flags;

	static void cb_mode(Fl_Button* b, void *panel);
	static void cb_color(Fl_Button* b, void *panel);
	static void cb_reset(Fl_Button* b, void *panel);
	static void cb_load(Fl_Button* b, void *panel);
	static void cb_save(Fl_Button* b, void *panel);
	static void cb_close(Fl_Button* b, void *panel);
	static void cb_color_chooser(Fl_Color_Chooser* b, void*);

	void color_visible( bool state );
	void font_visible( bool state );
	void cycle_mode();

	void create_layout();	

	//mode button state
	std::vector<std::string> mode_states;
	int current_mode;
	
	//color buttons state
	int active_color;

	void set_active_color( int c );
	int get_active_color() { return active_color; };

public:
	CandyPanel(Heart *hrt, int x, int y, int w, int h);
	~CandyPanel();

	void refresh_layout();
	bool get_size( int &w, int &h );

	static Fl_Color rgb2flcolor( double r, double g, double b );
	static void flcolor2rgb( Fl_Color flcolor, double &r, double &g, double &b );

};

#endif //_Candy_Panel_h
