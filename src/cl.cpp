/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cl.h"

#include <FL/fl_draw.H>

/*
 */
int Cl::cl_box_dx( Fl_Boxtype t )
{
	if( t < FL_FREE_BOXTYPE )
	{
		return Fl::box_dx( t );
	};

	switch( t )
	{
		case CLICKY_BLOCK_UP_BOX:
			return( block_box_line_w + block_box_border_w );
			break;
		case CLICKY_BLOCK_DOWN_BOX:
			return( block_box_h_skew + block_box_line_w + block_box_border_w );
			break;
	};
	return 0;
}

/*
 */
int Cl::cl_box_dy( Fl_Boxtype t )
{
	if( t < FL_FREE_BOXTYPE )
	{
		return Fl::box_dy( t );
	};

	switch( t )
	{
		case CLICKY_BLOCK_UP_BOX:
			return( block_box_line_w + block_box_border_h );
			break;
		case CLICKY_BLOCK_DOWN_BOX:
			return( block_box_v_skew + block_box_line_w + block_box_border_h );
			break;
	};
	return 0;
}

/*
 */
int Cl::cl_box_dw( Fl_Boxtype t )
{
	if( t < FL_FREE_BOXTYPE )
	{
		return Fl::box_dw( t );
	};

	switch( t )
	{
		case CLICKY_BLOCK_UP_BOX:
			return( block_box_h_skew + 2*( block_box_line_w + block_box_border_w ) );
			break;
		case CLICKY_BLOCK_DOWN_BOX:
			return( block_box_h_skew + 2*( block_box_line_w + block_box_border_w ) );
			break;
	};
	return 0;
}

/*
 */
int Cl::cl_box_dh( Fl_Boxtype t )
{
	if( t < FL_FREE_BOXTYPE )
	{
		return Fl::box_dh( t );
	};

	switch( t )
	{
		case CLICKY_BLOCK_UP_BOX:
			return( block_box_v_skew + 2*( block_box_line_w + block_box_border_h ) );
			break;
		case CLICKY_BLOCK_DOWN_BOX:
			return( block_box_v_skew + 2*( block_box_line_w + block_box_border_h ) );
			break;
	};
	return 0;
}

void Cl::cl_draw_box( int x, int y, int w, int h, Fl_Boxtype t, Fl_Color c )
{

	//-- draw a stock Fl box --

	if( t < FL_FREE_BOXTYPE )
	{
		Fl_Box_Draw_F *bdf = Fl::get_boxtype( t );
		if( bdf == NULL )
			return;
		
		bdf( x, y, w, h, c );
		return;
	};

	// -- draw a special clicky box --

	int old_color;

	switch( t )
	{
		case CLICKY_BLOCK_UP_BOX:

			old_color = fl_color();
			fl_color( c );

			fl_xyline( x, y, x + w - block_box_h_skew - block_box_line_w );
			fl_line( x + w - block_box_h_skew - block_box_line_w, y, x + w - block_box_line_w, y+block_box_v_skew );
			fl_yxline( x + w - block_box_line_w, y+block_box_v_skew, y + h - block_box_line_w );
			fl_xyline( x + w - block_box_line_w, y + h - block_box_line_w, x+block_box_h_skew );
			fl_line( x + block_box_h_skew, y + h - block_box_line_w, x, y + h - block_box_v_skew );
			fl_yxline( x, y + h - block_box_v_skew, y );

			fl_color( old_color );

		break;

		case CLICKY_BLOCK_DOWN_BOX:

			LOG_RENDER_DETAIL2("DRAWING DOWN BOX!!! color = %i\n", (int)c);

			old_color = fl_color();
			fl_color( c );

			fl_xyline( x+block_box_h_skew, y+block_box_v_skew, x + w - block_box_line_w );
			fl_yxline( x + w - block_box_line_w, y+block_box_v_skew, y + h - block_box_line_w );
			fl_xyline( x + w - block_box_line_w, y + h - block_box_line_w, x+block_box_h_skew );
			fl_yxline( x + block_box_h_skew, y + h - block_box_line_w, y+block_box_v_skew );

			fl_color( old_color );

		break;
	};
}

Fl_Boxtype Cl::cl_down_box( Fl_Boxtype t )
{
 	if( t < FL_FREE_BOXTYPE )
	{
		return fl_down( t );
	};
	return( (Fl_Boxtype)( (int)t + 1 ) );
}

void Cl::cl_draw_label( int x, int y, int w, int h, const char *l,
								Fl_Color c, Fl_Font font, int size, Fl_Boxtype b,
								Fl_Align align, Fl_Image *image, int symbols )
{
	if( l == NULL )
		return;

	fl_font( font, size );
	fl_color( c );

	LOG_RENDER_DETAIL2("dx=%i, dy=%i, dw=%i, dh=%i\n",Cl::cl_box_dx( b ), Cl::cl_box_dy( b ),
								Cl::cl_box_dw( b ), Cl::cl_box_dh( b ) );

	fl_draw( l, x + Cl::cl_box_dx( b ), y + Cl::cl_box_dy( b ), w - Cl::cl_box_dw( b ), h - Cl::cl_box_dh( b ),
				align, image, symbols );
}

bool Cl::natural_size( Fl_Widget *widget, int &W, int &H )
{
	int ww, hh;
	ww = 0;
	hh = 0;
	widget->measure_label( ww, hh );

	ww += Cl::cl_box_dw( widget->box() );
	hh += Cl::cl_box_dh( widget->box() );

	W = ww;
	H = hh;
	return true;
}

void Cl::widget_to_string( std::string &dst, Fl_Widget *wd, int maxDepth, std::string &pref )
{
	if( wd == NULL ) {
		dst.append("<NULL>");
		return;
	};
	StrUtils::Printf(dst, "%s(x,y,w,h) = (%d, %d, %d, %d), %s, label: \"%s\"",
								pref.c_str(),
								wd->x(),
								wd->y(),
								wd->w(),
								wd->h(),
								wd->visible() ? "visible" : "hidden",
								wd->label() == NULL ? "" : wd->label()
							);

	Fl_Group *gr = wd->as_group();
	if( gr == NULL ) {
		dst.append("\n");
		return;
	};
	StrUtils::Printf(dst,"children: %d\n", gr->children() );

	if(( maxDepth == 0 )||( gr->children() == 0 )) { return; }

	if( maxDepth > 0 ) { --maxDepth; }
	pref.append("  ");

	for( int i = 0; i < gr->children(); i++ ) {
		widget_to_string( dst, gr->child( i ), maxDepth, pref );
	};
}

void Cl::widget_to_string( std::string &dst, Fl_Widget *wd, int maxDepth )
{
	std::string pref = "";
	widget_to_string( dst, wd, maxDepth, pref );
}

std::string Cl::widget_to_string( Fl_Widget *wd, int maxDepth )
{
	std::string dst;
	widget_to_string( dst, wd, maxDepth );
	return dst;
}

