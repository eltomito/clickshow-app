/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cl_h_
#define _Cl_h_

#include "clicky_debug.h"

#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>
#include "cl_enums.h" //"

#include "shared/src/str_utils.h"

class Cl
{
private:

	//-- how much is the block box skewed in each direction (horizontal and vertical)
	static const int block_box_h_skew = 5;
	static const int block_box_v_skew = 5;
	static const int block_box_line_w = 1;
	static const int block_box_border_w = 2;		//border between the inside of the box and the label
	static const int block_box_border_h = 2;		//border between the inside of the box and the label

public:

	//-- box type stuff --

	static int cl_box_dx( Fl_Boxtype t );
	static int cl_box_dy( Fl_Boxtype t );
	static int cl_box_dw( Fl_Boxtype t );
	static int cl_box_dh( Fl_Boxtype t );

	static Fl_Boxtype cl_down_box( Fl_Boxtype t );

	static void cl_draw_box( int x, int y, int w, int h, Fl_Boxtype t, Fl_Color c );

	//-- label stuff --

	static void cl_draw_label( int x, int y, int w, int h, const char *l,
								Fl_Color c, Fl_Font font, int size, Fl_Boxtype b,
								Fl_Align align, Fl_Image *image, int symbols );

	//-- widget utilities --

	static bool natural_size( Fl_Widget *widget, int &W, int &H );
	
	//-- debug utilities
	
	static void widget_to_string( std::string &dst, Fl_Widget *wd, int maxDepth, std::string &pref );
	static void widget_to_string( std::string &dst, Fl_Widget *wd, int maxDepth = -1 );
	static std::string widget_to_string( Fl_Widget *wd, int maxDepth = -1 );

};

#endif //_Cl_h_

