/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cl_Enums_h_
#define _Cl_Enums_h_

//--- box types --

static const Fl_Boxtype CLICKY_BLOCK_UP_BOX	 	= FL_FREE_BOXTYPE;
static const Fl_Boxtype CLICKY_BLOCK_DOWN_BOX 	= (Fl_Boxtype)( (int)FL_FREE_BOXTYPE + 1 );

// -- alignment --

typedef unsigned int Cl_AlignType;

static const Cl_AlignType CL_ALIGN_START = 0;
static const Cl_AlignType CL_ALIGN_END = 1;
static const Cl_AlignType CL_ALIGN_CENTER = 2;
static const Cl_AlignType CL_ALIGN_EVEN = 4;
static const Cl_AlignType CL_ALIGN_LOW = 0;
static const Cl_AlignType CL_ALIGN_HIGH = 8;
static const Cl_AlignType CL_ALIGN_MIDDLE = 16;
static const Cl_AlignType CL_ALIGN_FILL = 32;

#endif //_Cl_Enums_h_
