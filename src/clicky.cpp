/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "style_engine.h"
#include "panels.h"
#include "style_prefs.h"
#include "expiration.h"


#include "clicky_debug.h"

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Tooltip.H>

Heart *heart;

void no_escape_callback(Fl_Widget*, void*) {
	if (Fl::event()==FL_SHORTCUT && Fl::event_key()==FL_Escape) {
		 //Escape closes the billboard or does nothing.
		if( heart->is_billboard_shown_on_ctrl_screen() ) {
			heart->show_billboard( false );
		}
		return;
	}

	heart->clicked_quit();
	//exit(0);
}

int main(int argc, char **argv)
{
	AutoPacket::StaticInit();

	LOG_BB_INFO("Number of screens: %i\n", Fl::screen_count() );
	
	Fl_Double_Window *w;

	//-- hard-coded expiration --

	if( !clicky_is_still_good() )
	{
		LOG_GEN_INFO("Clicky has expired. Sorry. Clicky is very sad he can't help you!!!\n\n:(((\n");
		return 0;
	};

	heart = new Heart();

	int exit_code;
	if( !heart->ProcessCmdArgs( argc, (const char**)argv, exit_code ) ) {
		delete heart;
		heart = NULL;
		exit( exit_code );
	}
	
	heart->DisableScreensaver();

	do
	{

		w = new Fl_Double_Window( heart->get_prefs()->main_window_x, heart->get_prefs()->main_window_y, heart->get_prefs()->main_window_width, heart->get_prefs()->main_window_height );
		w->callback( &no_escape_callback );

		heart->set_main_window( w );
		TotalPanel *tp = new TotalPanel( heart, 0,0,heart->get_prefs()->main_window_width, heart->get_prefs()->main_window_height);
		w->end();

		w->size_range( 100, 100, 10000, 10000 );
		w->resizable( w );

		LOG_GEN_INFO("clicky fullscreen = %i\n", heart->get_prefs()->full_screen_mode );

		heart->set_fullscreen( heart->get_prefs()->full_screen_mode );

		//init subtitles
		if( !heart->do_restart ) {
			heart->show_about_text();
			Fl::lock();	//This makes fltk aware of multi-threading
		} else {
			heart->before_restart();
		}

		//init tooltips
		Fl_Tooltip::color( (Fl_Color)0xb0b0e000 );		//(Fl_Color)0x10102000
		Fl_Tooltip::textcolor( FL_BLACK );	//FL_YELLOW
		Fl_Tooltip::enable( 1 );

		//show the thing
		w->show();

		LOG_GEN_INFO("Clicky is going to run()!!!\n" );

		heart->do_restart = false;

		if( heart->get_prefs()->bb_visible ) {
			heart->fix_billboard_prefs();
			heart->fix_window_fullscreen();
			heart->_show_billboard( true );
			heart->get_bb_panel()->set_state_from_prefs();
		}

		Fl::run();

		//-- store preferences before exit
		if( !heart->get_prefs()->full_screen_mode )
		{
			heart->get_prefs()->store_window_dims( w );
		};
		heart->store_billboard_prefs();

		//delete heart->get_dash_panel()->info_panel;
		delete w;
		heart->_show_billboard(false);

		if( heart->do_restart )
			heart->after_restart();

	} while( heart->do_restart );

	heart->DisableScreensaver( false );

	delete heart;

	AutoPacket::StaticCleanup();
}
