/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Button_h
#define _Clicky_Button_h

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Repeat_Button.H>

#include "cl.h"
#include "clicky_debug.h"
#include "widget_brain.h"

template <class T>
class ClickyButtonT : public T //Fl_Button
{
public:
	ClickyButtonT( int x, int y, int w, int h, const char *label );
	~ClickyButtonT();

	void draw();

	//--widget brain
	virtual bool natural_size( int &W, int &H );

};

#include <FL/fl_draw.H>

template <class T>
ClickyButtonT<T>::ClickyButtonT( int x, int y, int w, int h, const char *label )
	: T( x, y, w, h, label )
{
	T::user_data( (void *)new WidgetBrain( (Fl_Widget *)this ) );
};

template <class T>
ClickyButtonT<T>::~ClickyButtonT()
{
	delete (WidgetBrain *)T::user_data();
	T::user_data( NULL );
};

template <class T>
void ClickyButtonT<T>::draw()
{
	if (T::type() == FL_HIDDEN_BUTTON) return;

	Fl_Color old_color, old_labelcolor;

	if( !T::active_r() ) {
		old_color = T::color();
		old_labelcolor = T::labelcolor();
		T::color( fl_color_average(old_color, FL_BLACK, .33f) );
		T::labelcolor( fl_color_average(old_labelcolor, FL_BLACK, .33f) );
	}

	fl_rectf( T::x(), T::y(), T::w(), T::h(), FL_BLACK ); 	//TODO: The background color should be user-settable at least.
	T::draw();

	if( !T::active_r() ) {
		T::color( old_color );
		T::labelcolor( old_labelcolor );
	}
}

//-- widget brain --

template <class T>
bool ClickyButtonT<T>::natural_size( int &W, int &H )
{
	return Cl::natural_size( this, W, H );
};

typedef ClickyButtonT<Fl_Button> ClickyButton;
typedef ClickyButtonT<Fl_Repeat_Button> ClickyRepeatButton;

#endif //_Clicky_Button_h
