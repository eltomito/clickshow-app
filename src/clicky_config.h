/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Config_h
#define _Clicky_Config_h

//-- debug --
#ifndef WIN32

// under windows, DEBUG is defined in the build config
//#define	DEBUG 1

#endif

// -- translation --

#define TRANSLATION 1

#ifdef TRANSLATION
#include "translator/translator_macros.h"
#endif //TRANSLATION

// -- compile-time options --

#define ASK_BEFORE_QUIT	1
#define CLICKY_NET 1

//-- about --

#ifndef CLICKY_EXPIRES

#define ABOUT_CLICKY_SHORT N_("*  CLICKSHOW %i.%i  *\n\"Click for worlwide understanding!!\"")

//\n.o°o..o°o..o°o..o°o..o°o..o°o..o°o..o°o..o°o."

#else //CLICKY_EXPIRES

#define ABOUT_CLICKY_SHORT N_("*** ClickShow %i.%i ***\nDEMO - expires at the end of 2013!!")

#endif //CLICKY_EXPIRES

#endif //_Clicky_Debug_h
