/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_input.h"
#include "heart.h"

#include <FL/fl_draw.H>

ClickyInput::ClickyInput( Heart *_heart, int colWidth, const char *_label, bool _horiz )
	: BodyPack( _horiz ), heart(_heart), cb_common(NULL)
{
	spacing(3);
	borders( 5, 5, 2, 2 );	
	
	pack_group = new BodyPack( _horiz );
	pack_group->align( CL_ALIGN_FILL );
	pack_group->spacing( 5 );

	button_label = new ClickyButton(0,0,0,0,_label);

	pack_border = new BodyPack( true );
	pack_border->align( CL_ALIGN_FILL );
	pack_border->spacing( 5 );

	end();

	input_widget = new Fl_Input( 0, 0, 0, 0 );
	WidgetBrain::set_widget_brain( input_widget, new MLInputBrain( input_widget, colWidth, 1 ) );

	pack_border->add( input_widget );

	//wireup the callbacks
	input_widget->callback( (Fl_Callback*)cb_input );
	button_label->callback( (Fl_Callback*)cb_button );
	
	//set styles
	heart->get_style_engine()->assign_style( StylePrefs::TEXT_INPUT, input_widget );
	//heart->get_style_engine()->assign_style( StylePrefs::INPUT_LABEL, button_label );
	heart->get_style_engine()->assign_style( StylePrefs::GROUP_BORDER_DEFAULT, pack_border );
	heart->get_style_engine()->assign_style( StylePrefs::GROUP_BORDER_NONE, pack_group );

	//init valid state
	set_valid( true );
}

void ClickyInput::set_valid( bool _valid )
{
	if( _valid ) {
		heart->get_style_prefs()->InputLabel->apply( button_label );
	} else {
		heart->get_style_prefs()->InputLabelInvalid->apply( button_label );
	}
	button_label->redraw();
}

bool ClickyInput::Validate()
{
	bool res = validate_fn();
	set_valid( res );
	return res;
}

void ClickyInput::cb_input(Fl_Input* b, void*)
{
	ClickyInput *ci = (ClickyInput *)b->parent()->parent()->parent();
	ci->input_callback();
}

void ClickyInput::cb_button(Fl_Button* b, void*)
{
	ClickyInput *ci = (ClickyInput *)b->parent()->parent();
	ci->button_callback();
}

ClickyInput::~ClickyInput() {};

void ClickyInput::input_callback()
{
	if( cb_common == NULL ) { return; }
	(*cb_common)( (Fl_Widget*)this, (void*)CBSource::INPUT );
}

void ClickyInput::button_callback()
{
	if( cb_common == NULL ) { return; }
	(*cb_common)( (Fl_Widget*)this, (void*)CBSource::BUTTON );
}

// =================== ClickyInputNonEmpty ================

bool ClickyInputNonEmpty::validate_fn()
{
	const char *val = GetValue();
	size_t len = strlen( val );
	if( len == 0 ) { return false; }
	size_t space_len = strspn( val, " \t\n\r" );
	if( space_len == len ) { return false; }
	return true;
}
