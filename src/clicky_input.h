/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Input_h
#define _Clicky_Input_h

#include <FL/Fl.H>
#include <FL/Fl_Input.H>
#include <FL/Fl_Group.H>

#include "cl.h"
#include "clicky_debug.h"
#include "widget_brain.h"
#include "body_pack.h"
#include "clicky_button.h"
#include "brain_mlinput.h"
#include "body_pack.h"

class Heart;

class ClickyInput : public BodyPack
{
public:
	enum CBSource {
		INPUT = 0,
		BUTTON
	};

	ClickyInput( Heart *_heart, int colWidth = 20, const char *_label = "", bool _horiz = true );
	~ClickyInput();

	void SetLabel( const char *_label ) { GetButton()->label( _label ); };
	void SetValue( const char *_value ) { GetInput()->value( _value ); };
	const char *GetValue() { return GetInput()->value(); };

	bool Validate();

	Fl_Input			*GetInput() { return input_widget; };
	ClickyButton	*GetButton() { return button_label; };
	BodyPack			*GetInputPack() { return pack_border; };
	BodyPack			*GetGroupPack() { return pack_group; };

	void callback( Fl_Callback *cb ) { cb_common = cb; };

protected:
	virtual void input_callback();
	virtual void button_callback();
	virtual bool validate_fn() { return true; };

	void	set_valid( bool _value );

	static	void cb_input(Fl_Input* b, void*);
	static	void cb_button(Fl_Button* b, void*);

	Fl_Input			*input_widget;
	ClickyButton	*button_label;
	BodyPack			*pack_border;
	BodyPack			*pack_group;

	Fl_Callback	*cb_common;

	Heart *heart;
};

class ClickyInputNonEmpty : public ClickyInput
{
public:
	ClickyInputNonEmpty( Heart *_heart, int colWidth = 20, const char *_label = "", bool _horiz = true )
		: ClickyInput( _heart, colWidth, _label, _horiz ) {};
	~ClickyInputNonEmpty() {};

protected:
	bool validate_fn();
};

#endif //_Clicky_Input_h
