/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_slider.h"

#include <algorithm>

#include <FL/fl_draw.H>

ClickySlider::ClickySlider( int x, int y, int w, int h, const char *label )
	: Fl_Slider( x, y, w, h, label )
{
	min_slider_size( 10 );
};

ClickySlider::~ClickySlider()
{
};

void ClickySlider::draw()
{
	int box_top, box_height;

	//erase the background
	//fl_rectf( x(), y(), w(), h(), FL_BLACK );

	//draw the box
	box_height = std::max( (int)( slider_size() * (double) h() ), min_pix_size );

	double fraction = ( value() - minimum() ) / ( maximum() - minimum() );

	box_top = y() + (int)( fraction * ( (double)( h() - box_height ) ) );
	//box_bottom = top_y + box_height;

	fl_color( color() );

	fl_rect( x(), box_top, w(), box_height );

	//draw the center line
	fl_yxline( x() + w()/2, y(), box_top );
	fl_yxline( x() + w()/2, box_top + box_height, y()+h() );
};
