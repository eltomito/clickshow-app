/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Slider_h
#define _Clicky_Slider_h

#include <FL/Fl.H>
#include <FL/Fl_Slider.H>

class ClickySlider : public Fl_Slider
{
private:
	int min_pix_size;

public:
	ClickySlider( int x, int y, int w, int h, const char *label );
	~ClickySlider();

	void min_slider_size( int size ) { min_pix_size = size; };
	int min_slider_size() { return min_pix_size; };

	void draw();
};

#endif //_Clicky_Slider_h
