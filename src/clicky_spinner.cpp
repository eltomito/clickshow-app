/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_spinner.h"
#include "heart.h"

#include <FL/fl_draw.H>

ClickySpinner::ClickySpinner( Heart *_heart,
										const char *_label,
										bool _horiz,
										int colWidth )
	: BodyPack( _horiz ), heart(_heart), cb_common(NULL)
{
	spacing(3);
	borders( 5, 5, 2, 2 );

	pack_group = new BodyPack( _horiz );
	pack_group->align( CL_ALIGN_FILL );
	pack_group->spacing( 5 );

	button_label = new ClickyButton(0,0,0,0,_label);

	pack_border = new BodyPack( true );
	pack_border->align( CL_ALIGN_FILL );
	pack_border->spacing( 5 );

	end();

	spinner_widget = new Fl_Spinner( 0, 0, 0, 0 );
	WidgetBrain::set_widget_brain( spinner_widget, new SpinnerBrain( spinner_widget, colWidth ) );

	pack_border->add( spinner_widget );

	//wireup the callbacks
	spinner_widget->callback( (Fl_Callback*)cb_spinner );
	button_label->callback( (Fl_Callback*)cb_button );

	//set styles
	heart->get_style_engine()->assign_style( StylePrefs::GROUP_BORDER_DEFAULT, pack_border );
	heart->get_style_engine()->assign_style( StylePrefs::GROUP_BORDER_NONE, pack_group );
	heart->get_style_prefs()->NetLabelButton->apply( button_label );
}

void	ClickySpinner::SetTooltip( const char *txt )
{
	spinner_widget->tooltip( txt );
	button_label->tooltip( txt );
}

const char 	*ClickySpinner::GetTooltip()
{
	return spinner_widget->tooltip();
}

void ClickySpinner::cb_spinner(Fl_Spinner* b, void*)
{
	LOG_NET_INFO("SPINNER CALLBACK!\n");
	ClickySpinner *ci = (ClickySpinner *)b->parent()->parent()->parent();
	ci->spinner_callback();
}

void ClickySpinner::cb_button(Fl_Button* b, void*)
{
	ClickySpinner *ci = (ClickySpinner *)b->parent()->parent();
	ci->button_callback();
}

ClickySpinner::~ClickySpinner() {};

void ClickySpinner::spinner_callback()
{
	if( cb_common == NULL ) { return; }
	(*cb_common)( (Fl_Widget*)this, (void*)CBSource::SPINNER );
}

void ClickySpinner::button_callback()
{
	if( cb_common == NULL ) { return; }
	(*cb_common)( (Fl_Widget*)this, (void*)CBSource::BUTTON );
}
