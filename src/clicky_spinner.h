/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Spinner_h
#define _Clicky_Spinner_h

#include <FL/Fl.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Group.H>

#include "cl.h"
#include "clicky_debug.h"
#include "widget_brain.h"
#include "body_pack.h"
#include "clicky_button.h"
#include "brain_spinner.h"
#include "body_pack.h"

class Heart;

class ClickySpinner : public BodyPack
{
public:
	enum CBSource {
		SPINNER = 0,
		BUTTON
	};

	ClickySpinner( Heart *_heart,
						const char *_label = "",
						bool _horiz = true,
						int colWidth = 20 );
	~ClickySpinner();

	void SetLabel( const char *_label ) { GetButton()->label( _label ); };
	void SetValue( double _value ) { GetSpinner()->value( _value ); };
	double GetValue() { return GetSpinner()->value(); };

	Fl_Spinner		*GetSpinner() { return spinner_widget; };
	ClickyButton	*GetButton() { return button_label; };
	BodyPack			*GetSpinnerPack() { return pack_border; };
	BodyPack			*GetGroupPack() { return pack_group; };

	void				SetTooltip( const char *txt );
	const char 		*GetTooltip();

	void callback( Fl_Callback *cb ) { cb_common = cb; };

protected:
	virtual void spinner_callback();
	virtual void button_callback();

	static	void cb_spinner(Fl_Spinner* b, void*);
	static	void cb_button(Fl_Button* b, void*);

	Fl_Spinner		*spinner_widget;
	ClickyButton	*button_label;
	BodyPack			*pack_border;
	BodyPack			*pack_group;

	Fl_Callback	*cb_common;

	Heart *heart;
};

#endif //_Clicky_Spinner_h
