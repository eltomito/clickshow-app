/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "clicky_client.h"

/** Resolve addr and try to connect to it.
 */
bool ClickyClient::Connect( const std::string &addr, const std::string &loginName, const std::string &loginPwd )
{
	storedLoginName = loginName;
	storedLoginPwd = loginPwd;
	return dnsResolver.ResolveToUdp( addr, &ClickyClient::staticConnectToResolved, this, DEFAULT_SERVER_PORT );
}

void ClickyClient::staticConnectToResolved( const udp::endpoint &ep, bool okay, void *cc )
{
	((ClickyClient*)cc)->connectToResolved( ep, okay );
}

void ClickyClient::connectToResolved( const udp::endpoint &ep, bool okay )
{
	if( !okay ) {
		//GetEmitter()->NetMsg( E_CONNECT_FAILED );
		GetEmitter()->SetConnectionState( DISCONNECTED );
		return;
	}
	connectToEndpoint( ep, storedLoginName, storedLoginPwd );
}

/** Connect to an ip address (i.e., without address resolution).
 */
bool ClickyClient::ConnectToIP( const std::string &addr, const std::string &loginName, const std::string &loginPwd )
{
	udp::endpoint ep;
	if( !AsioUtils::StringToEndpoint( addr, ep, DEFAULT_NODE_PORT ) ) {
		LOG_NET_ERROR("Bad address: \"%s\".\n", addr.c_str() );
		return false;
	}
	return connectToEndpoint( ep, loginName, loginPwd );
}

bool ClickyClient::connectToEndpoint( const udp::endpoint &ep, const std::string &loginName, const std::string &loginPwd )
{
	cons.Clear(); //a ClickyClient can only have 1 connection - the one to the server.
	ResetTimeAdjustment();

	//add con
	ClientConPtr  cl = cons.AddCon( ep );
	if( !cl ) { //cl == NULL
		LOG_NET_ERROR("Failed to AddCon( %s ) to the con(nection) pool. Cannot connect to it.\n", AsioUtils::EndpointToString( ep ).c_str() );
		return false;
	}
	SetConnected( CONNECTING );
	if( !Login( cl, REMOTE_SERVER_NAME,"" ) ) {
		LOG_NET_ERROR("Failed to Login() the server into my node.\n" );
		return false;
	}
	//send HI packet
	cl->SendHi( id, loginName, loginPwd );
	return true;
}

bool ClickyClient::Disconnect()
{
	ClientConPtr  cl;

	switch( GetConnState() ) {
		case DISCONNECTED:
			LOG_NET_INFO("Asked to disconnect even though aready disconnected. I'll disconnect harder, then.\n");
			SetConnected( DISCONNECTED );
			cons.Clear();
		break;
		case DISCONNECTING:
			LOG_NET_INFO("Aborting proper disconnection procedure.\n");
			SetConnected( DISCONNECTED );
			cons.Clear();
		break;
		case CONNECTED:
			LOG_NET_INFO("Disconnecting...\n");
			SetConnected( DISCONNECTING );
			cl = GetServerCon();
			if( cl != NULL ) { cl->SendBye(); }
		break;
		case CONNECTING:
			LOG_NET_INFO("Aborting connection attempt now.\n");
			SetConnected( DISCONNECTED );
			cons.Clear();
		break;
		default:
			LOG_NET_ERROR("Weird! Asked to disconnect in an unknown state (%d)! I'll do it anyway...\n", (int)GetConnState() );
			SetConnected( DISCONNECTED );
			cons.Clear();
		break;
	}
	return true;
}

ClientConPtr  ClickyClient::GetServerCon()
{
	ClientConPtr cl = cons.GetFirst();
	if( !cl ) { return ClientConPtr(); } //cl == NULL
	NodeUser *u = cl->GetUser();
	if( u == NULL ) { return ClientConPtr(); }
	if( 0 != u->GetName().compare( REMOTE_SERVER_NAME ) ) { return ClientConPtr(); };
	return cl;
}

bool ClickyClient::SendShow( int num, const std::string &text )
{
	LOCK_ME;

	if( !IsConnected() ) {
		LOG_NET_ERROR("Asked to send Show message but not connected.\n" );
		return false;
	}
	ClientConPtr cl = GetServerCon();
	cl->SendShow( num, text );
	return true;
}

bool ClickyClient::SendPlayerState( const PlayerState &ps )
{
	LOCK_ME;

	if( !IsConnected() ) {
		LOG_NET_ERROR("Asked to send Play message but not connected.\n" );
		return false;
	}
	ClientConPtr cl = GetServerCon();
	cl->SendPlay( ps );
	return true;
}

bool ClickyClient::SendSubtitles( const std::string &subs )
{
	LOCK_ME;

	if( !IsConnected() ) {
		LOG_NET_ERROR("Asked to send Subtitles but not connected.\n" );
		return false;
	}
	ClientConPtr cl = GetServerCon();
	cl->SendSubtitles( subs );
	return true;
}
