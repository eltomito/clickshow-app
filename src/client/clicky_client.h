/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Clicky_Client_h
#define _Clicky_Client_h

#include "shared/src/net/node/clicky_node.h"
#include "client_con.h"
#include "emitter.h"
#include "shared/src/net/dns_resolver.h"

using boost::asio::ip::udp;

#define REMOTE_SERVER_NAME "server"

class ClickyClient : public ClickyNode<ClientCon>
{
public:
	static const unsigned short int DEFAULT_CLIENT_PORT = 0;	//Let the system choose a port number

	/**
	 * @warning Throws an exception when the port is already in use!
	 */
	ClickyClient( boost::asio::io_service &_ioService, const std::string &_id, unsigned short port = DEFAULT_CLIENT_PORT )
					: ClickyNode( _ioService, port, true ), dnsResolver( _ioService ), id(_id)
	{
		users.AddUser(REMOTE_SERVER_NAME,"", PERMS_ALL);
		backupEmitter = new Emitter();
		emitter = backupEmitter;
	};
	~ClickyClient() {};

	void SetEmitter( Emitter *e )
	{
		if( e == NULL ) {
			emitter = backupEmitter;
		} else {
			emitter = e;
		}
	};
	Emitter *GetEmitter() const { return emitter; };

	const std::string &GetId() { return id; };
	void SetId( const std::string &_id ) { id = _id; };

	bool ConnectToIP( const std::string &addr, const std::string &loginName, const std::string &loginPwd );
	bool Connect( const std::string &addr, const std::string &loginName, const std::string &loginPwd );
	bool Disconnect();

	bool SendShow( int num, const std::string &text );
	bool SendSubtitles( const std::string &subs );
	bool SendPlayerState( const PlayerState &ps );

	ClientConPtr GetServerCon();

	ConnT GetConnState() { ClientConPtr c = GetServerCon(); return (c!=NULL) ? c->GetConnState() : DISCONNECTED; };
	bool IsConnected() { return GetConnState() == CONNECTED; };
	bool IsConnecting() { return GetConnState() == CONNECTING; };
	bool IsDisconnected() { return GetConnState() == DISCONNECTED; };
	bool IsDisconnecting() { return GetConnState() == DISCONNECTING; };
	void SetConnected( ConnT state ) { ClientConPtr c = GetServerCon(); if( c!=NULL ) { c->SetConnected(state); }; };

	void AdjustLocalTimeToServer( ClickyTime &localTime ) { timeAdjustment.AdjustTimeInPlace( localTime, true ); };
	void AdjustServerTimeToLocal( ClickyTime &serverTime ) { timeAdjustment.AdjustTimeInPlace( serverTime, false ); };

protected:

	bool connectToEndpoint( const udp::endpoint &ep, const std::string &loginName, const std::string &loginPwd );
	void connectToResolved( const udp::endpoint &ep, bool okay );
	static void staticConnectToResolved( const udp::endpoint &ep, bool okay, void *cc );

protected:
	std::string	id;

	Emitter		*emitter;
	Emitter 		*backupEmitter;
	DNSResolver	dnsResolver;

	std::string storedLoginName;
	std::string	storedLoginPwd;
};

#endif //_Clicky_Client_h
