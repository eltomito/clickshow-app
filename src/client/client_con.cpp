/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "client_con.h"
#include "clicky_client.h"

bool ClientCon::Send( UdpPacket &pkt, bool setSentTime )
{
	if( node == NULL ) {
		LOG_NET_ERROR("Tried to send a packet to a ClientCon but its node is NULL!\n");
		return false;
	}
	return ((ClickyClient*)node)->Send(this, pkt, setSentTime );
};

void ClientCon::handleConnectionStateChange( ConnT oldState, ConnT newState )
{
	LOG_NET_INFO("Changing connection state from %d to %d.\n", (int)oldState, (int)newState );
	if( oldState == newState ) { return; };
	ClickyClient *client = (ClickyClient*)node;
	client->GetEmitter()->SetConnectionState( newState );
	if( newState == DISCONNECTED ) {
		Kill();
	}
}

std::string ClientCon::ToString( const std::string &separator, int verbosity )
{
	std::string dst;
	dst = NodeCon::ToString( separator, verbosity );
	if( verbosity >= 1 ) {
		StrUtils::Printf( dst, "%sSubtitles Out: %s%s",
			separator.c_str(),
			varSubsOut.ToString( ",", verbosity ).c_str(),
			separator.c_str() );
		StrUtils::Printf( dst, "Subtitles In: %s%s",
			varSubsIn.ToString( ",", verbosity ).c_str(),
			separator.c_str() );
		StrUtils::Printf( dst, "Subtitles In: %s", varSubsIn.ToString( ",", verbosity ).c_str() );
	}
	return dst;
}
