/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Client_Con_h
#define _Client_Con_h 1

#include "shared/src/net/node/node_con.h"
#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/con_var_msg.h"
#include "convars/client_var_show.h"
#include "convars/client_var_play.h"
#include "convars/client_var_hi_bye.h"
#include "convars/client_var_perms.h"
#include "convars/client_var_file_in.h"
#include "convars/client_var_file_out.h"
#include "shared/src/net/node/convars/con_var_file.h"

#include <boost/smart_ptr/shared_ptr.hpp>

using boost::asio::ip::udp;

#define DEFAULT_SUBTITLE_RESEND_TIME 2000

class ClientCon : public NodeCon
{
public:
	ClientCon( const udp::endpoint &_endpoint, void *_client, NodeConIdT _id )
	:	NodeCon( _endpoint, _client, _id ),
		varMsg( *this ),
		varShow( *this, DEFAULT_RESEND_TIME ),
		varPlay( *this, DEFAULT_RESEND_TIME ),
		varHiBye( *this, DEFAULT_RESEND_TIME ),
		varPerms( *this, DEFAULT_RESEND_TIME ),
	   varSubsIn( *this, FILE_ROLE_SUBTITLES, DEFAULT_RESEND_TIME ),
	   varSubsOut( *this, FILE_ROLE_SUBTITLES, DEFAULT_SUBTITLE_RESEND_TIME ),
	   varSubs( *this,  &this->varSubsIn, &this->varSubsOut )
	{}

	ClientCon()
	:	varShow( *this, DEFAULT_RESEND_TIME ),
		varPlay( *this, DEFAULT_RESEND_TIME ),
		varHiBye( *this, DEFAULT_RESEND_TIME ),
		varMsg( *this ),
		varPerms( *this, DEFAULT_RESEND_TIME ),
	   varSubsIn( *this, FILE_ROLE_SUBTITLES, DEFAULT_RESEND_TIME ),
	   varSubsOut( *this, FILE_ROLE_SUBTITLES, DEFAULT_SUBTITLE_RESEND_TIME ),
	   varSubs( *this, &this->varSubsIn, &this->varSubsOut )
	{};

	ClientCon( const ClientCon &other )
		: NodeCon( other ),
		varMsg( *this ),
		varShow( *this, other.varShow ),
		varPlay( *this, DEFAULT_RESEND_TIME ),
		varHiBye( *this, other.varHiBye ),
		varPerms( *this, other.varPerms ),
	   varSubsIn( *this, other.varSubsIn ),
	   varSubsOut( *this, other.varSubsOut ),
	   varSubs( *this, &this->varSubsIn, &this->varSubsOut )
	{};

	~ClientCon() {};

	bool Send( UdpPacket &pkt, bool setSentTime = true );

	void SendShow( int num, const std::string &text ) { varShow.Send( num, text ); };
	void SendPlay( const PlayerState &ps ) { varPlay.Send( ps ); };

	void SendHi( const std::string &id, const std::string &loginName, const std::string &loginPwd )
	{
		LOG_NET_INFO("Sending HI..\n");
		SetConnected( CONNECTING );
		varHiBye.SendHi( id, loginName, loginPwd );
	};
	void SendBye()
	{
		LOG_NET_INFO("Sending BYE..\n");
		SetConnected( DISCONNECTING );
		varHiBye.SendBye();
	};

	void SendSubtitles( const std::string &subs ) { varSubsOut.Send( subs ); };

	/** this function is called at regular intervals
	 * @param nowTime - the adjusted (server) time to be used as now.
	 */
	virtual void doChores( const ClickyTime &nowTime )
	{
		varHiBye.DoChores( nowTime );
		if( IsConnected() ) {
			varShow.DoChores( nowTime );
			varMsg.DoChores( nowTime );
			varPerms.DoChores( nowTime );
			varSubs.DoChores( nowTime );
		}
	};

	std::string ToString( const std::string &separator = "\n", int verbosity = 1 );

protected:
	void handleHi( UdpPacketHi &pkt ) { varHiBye.Receive( pkt ); };
	void handleBye( UdpPacketBye &pkt ) { varHiBye.Receive( pkt ); };
	void handleShow( UdpPacketShow &pkt ) { varShow.Receive( pkt ); };
	void handlePlay( UdpPacketPlay &pkt ) { varPlay.Receive( pkt ); };
	void handlePerms( UdpPacketPerms &pkt ) { varPerms.Receive( pkt ); };
	void handleFileHeader( UdpPacketFileHeader &pkt ) { varSubs.ReceiveHeader( pkt ); };
	void handleFileBody( UdpPacketFileBody &pkt ) { varSubs.ReceiveBody( pkt ); };

	void handleChat( UdpPacketChat &pkt ) {};
	void handleMsg( UdpPacketMsg &pkt ) { varMsg.Receive( pkt ); };
	void handleInvalid( UdpPacket &pkt ) {};
	void handleConnectionStateChange( ConnT oldState, ConnT newState );
	
	ClientVarShow		varShow;
	ClientVarHiBye		varHiBye;
	ClientVarMsg		varMsg;
	ClientVarPerms		varPerms;
	ClientVarPlay		varPlay;

	ClientVarFileIn		varSubsIn;
	ClientVarFileOut		varSubsOut;
	ConVarFile				varSubs;
};

typedef boost::shared_ptr<ClientCon> ClientConPtr ;

#endif //_Client_Con_h
