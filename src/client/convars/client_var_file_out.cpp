/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "client_var_file_out.h"
#include "../client_con.h"
#include "../clicky_client.h"	

#define REP_FILE_PROGRESS( _state ) \
( (ClickyClient *)(con.GetNode()) )->GetEmitter()->FileProgress( GetFileRole(), GetFileId(), _state, GetAckedSize(), GetTotalSize() );

void ClientVarFileOut::FileStarted()
{
	REP_FILE_PROGRESS( FILE_STATE_NEW );
}

void ClientVarFileOut::FileProgressed()
{
	REP_FILE_PROGRESS( FILE_STATE_CONT );
}

void ClientVarFileOut::FileFinished()
{
	REP_FILE_PROGRESS( FILE_STATE_DONE );
}

void ClientVarFileOut::FileAborted()
{
	REP_FILE_PROGRESS( FILE_STATE_ABORTED );
}
