/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Client_Var_File_Out_h
#define _Client_Var_File_Out_h 1

#include "shared/src/net/node/convars/con_var_file_out.h"

class ClientVarFileOut : public ConVarFileOut
{
public:
	ClientVarFileOut(	NodeCon &_con,
							netFileRoleT _fileRole = FILE_ROLE_INVALID,
							long _resendMilli = 5000 )
	: ConVarFileOut(	_con,
							_fileRole,
							_resendMilli ) {};

	ClientVarFileOut(	NodeCon &_con, const ClientVarFileOut &other )
	: ConVarFileOut(	_con,
							other.fileRole,
							other.resendMilli ) {};

	~ClientVarFileOut() {};

	void Send( const std::string &subs )
	{
		SendString( subs );
	};

	virtual void FileFinished();
	virtual void FileStarted();
	virtual void FileProgressed();
	virtual void FileAborted();
};

#endif //_Client_Var_File_Out_h
