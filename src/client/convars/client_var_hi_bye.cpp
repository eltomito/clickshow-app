/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "client_var_hi_bye.h"
#include "../clicky_client.h"

#define CLIENT_PTR ((ClickyClient *)(con.GetNode()))

void ClientVarHiBye::SendBye()
{
	UdpPacketBye pkt(GetLastNumSent() );
	pkt.IncNum();
	Send( pkt );
	JustSent( pkt );
}

void ClientVarHiBye::SendHi( const std::string &id, const std::string &loginName, const std::string &loginPwd )
{
	UdpPacketHi pkt(GetLastNumSent() );
	pkt.IncNum();
	pkt.Init(id, loginName, loginPwd );
	Send( pkt );
	JustSent( pkt );
}

void ClientVarHiBye::receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u )
{
	if( pkt.type == HI ) {
	//hi
		if( !pkt.IsAck() ) {
			LOG_NET_ERROR("Received a Hi! from this con(nection): %s. Why, dude?!\n", con.ToString(",").c_str() );
			con.Kill();
			return;
		}
		if( CLIENT_PTR->IsConnected() ) {
			LOG_NET_DETAIL("Duplicate Hi! ACK received from server (%s).\n", con.ToString(",").c_str() );
			return;
		}
		LOG_NET_INFO("The server ACKed my Hi! (%s).\n", con.ToString(",").c_str() );
		CLIENT_PTR->SetConnected( CONNECTED );
		return;
	}
	//bye
	if( pkt.IsAck() ) {
		if( con.IsDisconnecting() ) {
			LOG_NET_INFO("This connection ACKed my BYE: %s.\n", con.ToString(",").c_str() );
			con.Kill();
			return;
		} else {
			LOG_NET_ERROR("A connection acknowledged my bye, even thou it isn't disconnecting! Ignoring it.\n");
			return;
		}
	}
	if( act == PROCESS ) {
		LOG_NET_INFO("This connection disconnected from me. Acking its BYE: %s.\n", con.ToString(",").c_str() );
		pkt.SetAck();
		Send( pkt );
		JustSent( pkt );
		con.Kill();
		return;
	}
	return;
}

