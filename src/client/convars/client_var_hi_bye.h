/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Client_Var_Hi_Bye_h
#define _Client_Var_Hi_Bye_h 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/packet_queue_mtop.h"

class ClientVarHiBye : public GenConVar<MTOPQueue>
{
public:
	ClientVarHiBye(	NodeCon &_con, long _resendMilli = 500 )
		: GenConVar<MTOPQueue>( _con, AUTO_RESEND|NEEDS_USER, 0, _resendMilli )
	{
		PacketTypeT tp[] = { HI, BYE, INVALID };
		GetQueue().SetTypes( tp );
	};
	ClientVarHiBye(	NodeCon &_con, const ClientVarHiBye &other )
		: GenConVar<MTOPQueue>( _con, other ) {};
	~ClientVarHiBye() {};

	void SendBye();
	void SendHi( const std::string &id, const std::string &loginName, const std::string &loginPwd );
	void receive(  UdpPacket &pkt, SPQActionT act, NodeUser *u );
};

#endif //_Client_Var_Hi_Bye_h
