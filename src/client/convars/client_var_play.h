/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Client_Var_Play_h_
#define _Client_Var_Play_h_ 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/packet_queue_otop.h"
#include "shared/src/clicky_time.h"
#include "shared/src/player/player_state.h"

class ClientVarPlay : public GenConVar<OTOPQueue<UdpPacketPlay> >
{
public:
	ClientVarPlay(	NodeCon &_con, long _resendMilli = 500 )
		: GenConVar<OTOPQueue<UdpPacketPlay> >( _con, NORMAL, PERM_CLICK_SUBS, _resendMilli ) {};

	ClientVarPlay(	NodeCon &_con, const ClientVarPlay &other ) : GenConVar<OTOPQueue<UdpPacketPlay> >( _con, other ) {};
	~ClientVarPlay() {};

	void Send( const PlayerState &ps );
	void Receive( UdpPacketPlay &pkt );
};

#endif //_Client_Var_Play_h_
