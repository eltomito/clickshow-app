/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "client_var_show.h"
#include "shared/src/net/node/node_con.h"
#include "../client_con.h"
#include "../clicky_client.h"

void ClientVarShow::Send( int num, const std::string &text )
{
	UdpPacketShow pkt( num, text, GetLastNumSent() );
	pkt.IncNum();
	con.Send( pkt );
	JustSent( pkt );
};

void ClientVarShow::Receive( UdpPacketShow &pkt )
{
	SPQActionT act = JustReceived( pkt );
	if( act == PROCESS ) {
		( (ClickyClient *)(con.GetNode()) )->GetEmitter()->ShowSubtitle( pkt.subNum, pkt.subText );
	}
}
