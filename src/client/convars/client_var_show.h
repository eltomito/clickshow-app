/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Client_Var_Show_h
#define _Client_Var_Show_h 1

#include "shared/src/net/node/convars/gen_con_var.h"
#include "shared/src/net/node/convars/packet_queue_otop.h"

class ClientVarShow : public GenConVar<OTOPQueue<UdpPacketShow> >
{
public:
	ClientVarShow(	NodeCon &_con, long _resendMilli = 500 )
		: GenConVar<OTOPQueue<UdpPacketShow> >( _con, NORMAL, PERM_CLICK_SUBS, _resendMilli ) {};
	ClientVarShow(	NodeCon &_con, const ClientVarShow &other ) : GenConVar<OTOPQueue<UdpPacketShow> >( _con, other ) {};
	~ClientVarShow() {};

	void Send( int num, const std::string &text );
	void Receive( UdpPacketShow &pkt );
	//void DoChores( const ClickyTime &nowTime ) { doChores( nowTime ); };
};

#endif //_Client_Var_Show_h
