/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Emitter_h
#define _Emitter_h 1

#include "shared/src/net/net_msg.h"
#include "shared/src/net/net_types.h"
#include "shared/src/clicky_time.h"

class Emitter
{
public:
	Emitter() {};
	virtual ~Emitter() {};

	virtual void ShowSubtitle( int num, const std::string &text ) {};
	virtual void SetPlayerState( const PlayerState &ps ) {};
	virtual void SetConnectionState( ConnT _state ) {};
	virtual void SetAnnouncement( const std::string &text ) {};
	virtual void SetSubtitles( const std::string &subs ) {};
	virtual void SetPerms( UserPermsT perms ) {};
	virtual void NetMsg( NetMsgT code ) {};
	virtual void FileProgress( netFileRoleT fileRole, netFileIdT fileId, netFileStateT state, size_t _recvd, size_t _total ) {};
};

#endif //_Emitter_h
