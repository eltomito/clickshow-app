/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#define LOG_LEVEL 5
#define LOG_MASK (LOGMOD_NET|LOGMOD_FILE|LOGMOD_CANDY)

#include "../clicky_client.h"

#include <string>
#include <stdio.h>
#include <cstdlib>
#include <ctime>
#include <map>
#include <utility>
/*
#include <boost/chrono.hpp>
#include <boost/thread/thread.hpp> 
*/
#include "boost/asio.hpp"

const char defaultSubsName[] = "test_subs.txt";

class ConsoleEmitter : public Emitter
{
protected:
	typedef std::map<netFileStateT, std::string> stateMapT;
	stateMapT stateMap;
public:
	ConsoleEmitter() {
		stateMap.insert( std::pair<netFileStateT, std::string>( FILE_STATE_NEW, "NEW") );
		stateMap.insert( std::pair<netFileStateT, std::string>( FILE_STATE_CONT, "PROGRESSING...") );
		stateMap.insert( std::pair<netFileStateT, std::string>( FILE_STATE_DONE, "FINISHED!") );
		stateMap.insert( std::pair<netFileStateT, std::string>( FILE_STATE_ABORTED, "ABORTED :(") );
	};
	~ConsoleEmitter() {};

	void ShowSubtitle(int num, const std::string &text )
	{
		printf("--- SHOW SUBTITLE ---\n%d\n%s\n--------------------\n", num, text.c_str());
	};
	void SetConnectionState( ConnT state ) {
		switch( state ) {
			case CONNECTING:
				printf("Connecting to server...\n");
			break;
			case CONNECTED:
				printf("---------- CONNECTED! -----------\n");
			break;
			case DISCONNECTING:
				printf("Disconnecting from server...\n");
			break;
			case DISCONNECTED:
				printf("---------- GOOD BYE! ------------\n");
//				exit(0);
			break;
		}
	};
	void NetMsg( NetMsgT code ) {
		printf("**** NET MESSAGE CODE: %d ****\n", code );
	};

	void SetPerms( UserPermsT perms ) {
		printf("'''''''' MY NEW PERMISSIONS ARE: %ld ''''''''''\n", perms );
	};

	void FileProgress( netFileRoleT fileRole, netFileIdT fileId, netFileStateT state, size_t _recvd, size_t _total )
	{
		stateMapT::iterator it = stateMap.find( state );
		const char *stateStr = "<UNKNOWN>";
		if( it != stateMap.end() ) { stateStr = it->second.c_str(); }
		printf("===> file id: %u, file role: %u, %s, (%ld / %ld)", fileId, fileRole, stateStr, _recvd, _total );
	};

	void SetSubtitles( const std::string &subs ) {
		printf("===== NEW SUBTITLES =======\n%s\n", subs.c_str() );
	};
};

void SendSubtitles( ClickyClient &cc, const std::string &subtitlesName ) {
	char *inFile = NULL;
	
	std::string subsName = StrUtils::Trim( subtitlesName );

	FILE *fff = fopen(subsName.c_str(), "r");
	if( fff == NULL ) {
		printf("FAILED to open file \"%s\"\n", subsName.c_str() );
		return;
	}
	fseek( fff, 0, SEEK_END );
	long len = ftell(fff);
	if( len > 0 ) {
		fseek( fff, 0, SEEK_SET );
		inFile = (char *)malloc( len );
		if( inFile == NULL ) {
			printf("FAILED to allocate %ld bytes!\n", len );
		} else {
			size_t readlen = fread( (void*)inFile, len, 1, fff );
			if( readlen != 1 ) {
				printf("fread returned something WEIRD: %ld", readlen );
			} else {
				std::string subs( inFile, len );
				cc.SendSubtitles(subs);
			}
		}
	} else if( len == 0 ) {
		printf("The file \"%s\" has ZERO length.\n", subsName.c_str() );
	} else {
		printf("Failed to open file \"%s\".\n", subsName.c_str() );
	}
	if( inFile != NULL ) {
		free(inFile);
		inFile = NULL;
	}
	fclose(fff);
};

int FireCannon( ClickyClient &cc, std::string &serverAddr, std::string &loginName, std::string &loginPwd, int rounds = 100 ) {
	timespec tspec;
	tspec.tv_sec = 0;
	tspec.tv_nsec = 1000000;

	int i;
	for( i = 0; i < rounds; ++i ) {
		while( cc.IsConnected() ) {
			printf("Cannon is firing DIS!\n");
			cc.Disconnect();
			nanosleep( &tspec, NULL );
		}
		while( !cc.IsDisconnected() ) {
			printf("Waiting after DIS...\n");
			nanosleep( &tspec, NULL );
		}
		while( cc.IsDisconnected() ) {
			printf("Cannon is firing CON!\n");
			cc.Connect( serverAddr, loginName, loginPwd );
			nanosleep( &tspec, NULL );
		}
		while( cc.IsConnecting() ) {
			printf("Waiting after CON...\n");
			nanosleep( &tspec, NULL );
		}
	}
	return i;
};

int main( int argc, char *argv[] ) {

	if(( argc > 1 ) && ( argc < 4 )) {
		printf("Usage: client <server_address> <loginName> <loginPassword> [<clientName>]\nOr just: client\n");
		return 1;
	}

	std::string serverAddr;
	std::string loginName;
	std::string loginPwd;

	if( argc > 1 ) {
		serverAddr = argv[1];
		loginName = argv[2];
		loginPwd = argv[3];
	} else {
		serverAddr = "127.0.0.1:9876";
		loginName = "king";
		loginPwd = "gnik";
	}
	std::string clientName = argc > 4 ? argv[4] : "Konan Barbar";

	boost::asio::io_service ioService;

	srand (time(NULL));
	unsigned short port = 2048 + rand() % 4096;	

	ConsoleEmitter emt;
	ClickyClient cc( ioService, clientName, port );
	cc.SetEmitter( &emt );

	cc.Start();

	if( !cc.Connect(serverAddr, loginName, loginPwd) ) {
		printf("Connect() flopped.\n");
		return 1;
	}

   char inBuf[ 255 ];
   size_t inlen;
   int subNum = 0;
	while( true ) {
		inlen = read( 0, inBuf, sizeof(inBuf) );
		if(inlen > 0) {
			if( ( inlen > 1 )&&( inBuf[0] == '/' ) ) {
				switch( inBuf[1] ) {
					case 'q':
						printf("Bye! Sending BYE packet.\n");
						cc.Disconnect();
					break;
					case 's':
						SendSubtitles( cc, inlen > 3 ? &inBuf[3] : defaultSubsName );
					break;
					case 'f':
						FireCannon( cc, serverAddr, loginName, loginPwd, 10000 );
					break;
				}
			} else {
				inBuf[inlen-1] = 0;
				cc.SendShow( subNum, inBuf );
				subNum++;
			}
		}
	};
	return 0;
}
