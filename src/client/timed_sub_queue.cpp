/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "timed_sub_queue.h"

void TimedSubQueue::Push( const TSQEntry &sub )
{
	LOCK_ME;
	subs.push_back( sub );
};

bool TimedSubQueue::Pop( TSQEntry &dst, const ClickyTime &forTime )
{
	LOCK_ME;
	if( subs.empty() ) { return false; }
	dst = subs.front();
	if( dst.GetWhen() > forTime ) { return false; }
	subs.pop_front();
	return true;
};

void TimedSubQueue::Clear()
{
	LOCK_ME;
	subs.clear();
};
