/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Timed_Sub_Queue_h
#define _Timed_Sub_Queue_h 1

#include <list>

#include "shared/src/semaph.h"
#include "shared/src/clicky_time.h"

class TSQEntry
{
public:
	TSQEntry() {};
	~TSQEntry() {};

	TSQEntry( int _num, const std::string &_text, const ClickyTime &_when ) : num( _num ), text( _text ), when( _when ) {};
	TSQEntry( int _num, const std::string &_text ) : num( _num ), text( _text ) { when = new ClickyTime( true ); };

	const	ClickyTime	&GetWhen() { return when; };
	int					&GetNum() { return num; };
	const	std::string	&GetText() { return text; };

protected:
	int				num;
	std::string		text;
	ClickyTime		when;
};

class TimedSubQueue : Lockable
{
public:
	TimedSubQueue() {};
	~TimedSubQueue() {};

	void Push( const TSQEntry &sub );
	bool Pop( TSQEntry &dst, const ClickyTime &forTime );
	bool IsEmpty() { return subs.empty(); };
	void Clear();
private:
	std::list<TSQEntry> subs;
};

#endif // _Timed_Sub_Queue_h
