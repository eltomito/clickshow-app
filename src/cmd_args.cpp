/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cmd_args.h"
#include "shared/src/err.h"
#include "subtitle_formats/subtitle_formats.h"
#include "translator/translator_macros.h"
#include "subtitles/subtitle_doc.h"
#include "rainbow_converter.h"
#include "prefs.h"

#include <string>
#include <vector>
#include <iostream>

CmdArgs::CmdArgs( int argc, const char **argv, const ColorSet &_colors, Fl_Color _color255 )
: colors(_colors),color255(_color255),desc("ClickShow"),user(""),venue(""),password(""),protocol(2),flags(0)
{
	desc.add_options()
		("help", _("show usage information"))
		("input-file", po::value< std::vector<std::string> >(), _("input file"))
		("output-file", po::value< std::vector<std::string> >(), _("output file"))
		("to-srt-tags", _("Convert the color marks (*@+-~^`) in the input file to tags used in .srt."))
		("no-fullscreen", _("Start in non-fullscreen mode."))
		("server", po::value< std::vector<std::string> >(), _("network server address"))
		("user", po::value< std::vector<std::string> >(), _("login user name"))
		("venue", po::value< std::vector<std::string> >(), _("network venue name of this instance"))
		("password", po::value< std::vector<std::string> >(), _("network password"))
		("protocol", po::value<int>(), _("protocol version (1 or 2)"))
	;
	po::positional_options_description p;
	p.add("input-file", -1);

	po::store(po::command_line_parser(argc, argv).
		options(desc).positional(p).run(), values );
	po::notify( values );
};

int CmdArgs::Action()
{
	if( ArgCount("help") ) {
		std::cout << desc << "\n";
		return 0;
	}
	if( ArgCount("to-srt-tags") ) {
		std::string infile, outfile;
		if( ArgCount("input-file") ) {
			infile = values["input-file"].as< std::vector<std::string> >()[0];
		}
		if( infile.empty() ) {
			printf("%s\n",_("No input file."));
		}
		if( ArgCount("output-file") ) {
			outfile = values["output-file"].as< std::vector<std::string> >()[0];
		}
		SaveCandyAsTag( infile, outfile );
		return 0;
	}
	if( ArgCount("no-fullscreen") ) {
		set_no_fullscreen( true );
	}
	if( ArgCount("user") ) {
		user = values["user"].as< std::vector<std::string> >()[0];
	}
	if( ArgCount("password") ) {
		password = values["password"].as< std::vector<std::string> >()[0];
	}
	if( ArgCount("venue") ) {
		venue = values["venue"].as< std::vector<std::string> >()[0];
	}
	if( ArgCount("server") ) {
		server = values["server"].as< std::vector<std::string> >()[0];
	}
	if( ArgCount("protocol") ) {
		protocol = values["protocol"].as<int>();
		if(( protocol < 1 )||( protocol > 2 )) {
			fprintf(stderr,"Valid protocol versions are 1 and 2, not %i. Sorry, dude!\n", protocol);
			exit(1);
		}
	}
	return -1;
};

/**
 * @returns: number of subtitles saved or a negative error code (see Errr)
 */
int CmdArgs::SaveCandyAsTag( const std::string &infile, const std::string &outfile )
{
	SubtitleDoc *doc = new SubtitleDoc();
	if( doc == NULL ) {
		return (-1)*Errr::ALLOC;
	}
	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_UNKNOWN, true, true );
	int retval = doc->load_from_file( infile.c_str(), cfg );
	if( retval < 0 ) {
		fprintf( stderr, "Tried to open subtitles but: %s\n", Errr::Message( -1 * retval ) );
		return retval;
	}

	Subtitles &subs = *doc->get_subtitles();
	ErrDescsT errs;
	int err = UnMorphToSrtTags( subs, errs );
	if( err ) {
		fprintf( stderr, "Problem: %s.\n%s\n", Errr::Message( err ), ErrDesc::DescsToString( errs ).c_str() );
		return (-1)*err;
	}

	retval = doc->save_to_file( outfile.empty() ? "out.txt" : outfile.c_str() );
	if( retval < 0 ) {
		fprintf( stderr, "Tried to save subtitles but: %s\n", Errr::Message( -1 * retval ) );
	}
	return retval;
}

int CmdArgs::UnMorphToSrtTags( Subtitles &subs, ErrDescsT &errs )
{
	Subtitles::iter it = subs.get_sub_iter( 0 );
	Subtitles::iter endit = subs.get_sub_iter( -1 );
	RainbowStyle initStyle;
	SrtTagsRC rc( colors, color255 );

	while( it != endit ) {
		if( !it->is_comment() ) {
			rc.startSub();
			rainbow_convert( it->get_disp_text().c_str(), it->get_markup(), initStyle, rc );
			it->set_raw_text( rc.getText() );
		}
		++it;
	}
	return Errr::OK;
}
