/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cmd_Args_h
#define _Cmd_Args_h 1

#include "boost/program_options.hpp"
#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "rainbow_style.h"
#include "subtitles/subtitle.h"
#include "subtitles/subtitles.h"
#include "shared/src/flag_macro.h"

namespace po = boost::program_options;

class Prefs;

class CmdArgs {
private:
	unsigned int flags;

public:

	CREATE_FLAG( no_fullscreen, flags, 0 ); //set_no_fullscreen(), is_no_fullscreen()

	CmdArgs( int argc, const char **argv, const ColorSet &_colors, Fl_Color _color255 );
	~CmdArgs() {};

	int Action();
	size_t ArgCount( const char *name ) { return values.count( name ); };

	int SaveCandyAsTag( const std::string &infile, const std::string &outfile );
	int UnMorphToSrtTags( Subtitles &subs, ErrDescsT &errs );

	po::variables_map values;
	po::options_description desc;

	std::string user, venue, password, server;
	int protocol;

private:
	ColorSet colors;
	Fl_Color color255;
	int unMorphToSrtTags( Subtitle &sub );
};

#endif //_Cmd_Args_h
