/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "color_prefs.h"

ColorPrefs::ColorPrefs( Prefs *_prefs, const char *path, const char *vendor ) : prefs(_prefs)
{
	flp = new Fl_Preferences( path, vendor, NULL );
};

ColorPrefs::~ColorPrefs()
{
	delete flp;
	flp = NULL;
};

void ColorPrefs::writeToPrefs()
{
	for( int i = 0; i < NUM_COLORS; ++i ) {
		writeOne( i );
	}
};

void ColorPrefs::readFromPrefs()
{
	for( int i = 0; i < NUM_COLORS; ++i ) {
		readOne( i );
	}
};

int ColorPrefs::makeVarName( int colorIndex, char *dst, int maxLen ) {
	const char prefix[] = "color_";
	const int realLen = sizeof(prefix) + 1;

	if( maxLen < realLen ) {
		return -1;
	}

	strncpy( dst, prefix, sizeof(prefix) - 1 );
	dst[ sizeof(prefix) - 1 ] = '0' + colorIndex;
	dst[ sizeof(prefix) ] = 0;

	return realLen;
};

bool ColorPrefs::writeOne( int colorIndex ) {
	char varName[16];
	char *value;
	Fl_Color clr;
	
	if(( colorIndex >= prefs->def_candy_color_count )||( colorIndex >= NUM_COLORS )) { return false; }

	if( makeVarName( colorIndex, &varName[0], sizeof(varName) ) <= 0 ) { return false; }

	if( 0 == flp->get( varName, value, "ffffff00" ) ) {
		free( value );
		return false;
	}

	clr = ColorUtils::strToFlcolor( value );
	free( value );

	if( clr == ColorUtils::BAD_COLOR ) { return false; }

	prefs->candy_colors[ colorIndex ] = clr;
	return true;
};

bool ColorPrefs::readOne( int colorIndex ) {
	char varName[16];
	char *value;
	Fl_Color clr;
	std::string clrStr;

	if(( colorIndex >= prefs->def_candy_color_count )||( colorIndex >= NUM_COLORS )) { return false; }

	if( makeVarName( colorIndex, &varName[0], sizeof(varName) ) <= 0 ) { return false; }

	clr = prefs->candy_colors[ colorIndex ];
	ColorUtils::flcolorToString( clr, clrStr );
	flp->set( varName, clrStr.c_str() );

	return true;
};
