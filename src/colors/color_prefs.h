/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Color_Prefs_H_
#define _Color_Prefs_H_ 1

#include "../prefs.h"
#include "color_utils.h"

class ColorPrefs
{
public:
	static const int NUM_COLORS = 8;

	ColorPrefs( Prefs *_prefs, const char *path, const char *vendor );
	~ColorPrefs();

	void writeToPrefs();
	void readFromPrefs();

private:
	Prefs	*prefs;
	Fl_Preferences	*flp;

	int makeVarName( int colorIndex, char *dst, int maxLen );
	bool writeOne( int colorIndex );
	bool readOne( int colorIndex );
};

#endif //_Color_Prefs_H_
