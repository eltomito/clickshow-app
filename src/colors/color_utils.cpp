/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "color_utils.h"

void ColorUtils::clrToString( unsigned int clr, std::string &dst, bool hashPrefix )
{
	char buf[64];
	sprintf( buf, "%s%06x", ( hashPrefix ? "#" : "" ), clr & (0xffffff) );
	dst.append( buf );
};

unsigned int ColorUtils::strToClr( const char *str, char **endptr, bool hashPrefix )
{
	char *eptr;

	if( hashPrefix && ( str[0] != '#' ) ) {
		if( endptr ) { endptr[0] = (char *)str; }
		return BAD_COLOR;
	}

	++str;

	long v = strtol( str, &eptr, 16 );
	if( (!v && ( eptr == str )) || ( v > 0xffffff ) || ( v < 0 ) ) {
		if( endptr ) { endptr[0] = (char *)str; }
		return BAD_COLOR;
	}

	if( endptr ) { endptr[0] = eptr; }
	return ((unsigned int)v) & 0xffffff;
};
