/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Color_Utils_H_
#define _Color_Utils_H_ 1

#include <cstdlib>
#include <string>
#include <climits>
//#include "FL/Enumerations.H"
#include <FL/Fl.H>

class ColorUtils
{
public:
	static const unsigned int BAD_COLOR = UINT_MAX;

	static void clrToString( unsigned int clr, std::string &dst, bool hashPrefix = true );
	static unsigned int strToClr( const char *str, char **endptr = NULL, bool hashPrefix = true  );

	static void flcolorToString( Fl_Color clr, std::string &dst ) {
		if( clr < 256 ) { clr = Fl::get_color( clr ); }
		return clrToString( clr >> 8, dst );
	};
	static Fl_Color strToFlcolor( const char *str, char **endptr = NULL ) { return strToClr( str, endptr ) << 8; };
};

#endif //_Color_Utils_H_
