/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <FL/Fl.H>

#include "control_panel.h"
#include "style_engine.h"

// ============ WidgetActivator ============

WidgetActivator::WidgetActivator( WidgetActInfo *widgets )
{
	SetWidgets( widgets );
}

void WidgetActivator::SetWidgets( WidgetActInfo *widgets )
{
	wai.clear();
	if( widgets == NULL ) {	return; }

	while( widgets->wdg != NULL ) {
		wai.push_back( *widgets );
		widgets++;
	};
	update_switchable_mask();
}

void WidgetActivator::AddWidget( Fl_Widget *widget, int method )
{
	if( widget == NULL ) {	return; }
	wai.push_back( WidgetActInfo( widget, method ) );
	update_switchable_mask();
}

void WidgetActivator::AddWidgets( Fl_Widget **widgets, int method )
{
	if( widgets == NULL ) {	return; }

	while( *widgets != NULL ) {
		wai.push_back( WidgetActInfo( *widgets, method ) );
		widgets++;
	};
	update_switchable_mask();
}

void WidgetActivator::update_switchable_mask()
{
	unsigned int mask = 0;
	size_t waisize = wai.size();
	for( size_t i = 0; i < waisize; ++i ) {
		mask = mask << 1;
		if(( wai[i].method == WidgetActInfo::METH_ACTIVATE )||( wai[i].method == WidgetActInfo::METH_SHOW )) {
			mask |= 1;
		}
	}
	switchableMask = mask;
};

void WidgetActivator::RefreshEnabled()
{
	unsigned int mask = 0;
	size_t waisize = wai.size();

	for( size_t i = 0; i < waisize; ++i ) {
		mask = mask << 1;
		switch( wai[i].method ) {
			case WidgetActInfo::METH_ACTIVATE:
				mask |= ( wai[i].wdg->active() ? 1 : 0 );
			break;
			case WidgetActInfo::METH_SHOW:
				mask |= ( wai[i].wdg->visible() ? 1 : 0 );
			break;
		}
	}
	enabledMask = mask;
}

void WidgetActivator::SetActivation( unsigned long mask, bool active )
{
	size_t waisize = wai.size();
	size_t i = 0;
	while(( i < waisize )&&( mask != 0 )) {
		if( (mask & 1) != 0 ) {
			switch( wai[i].method ) {
				case WidgetActInfo::METH_ACTIVATE:
					if( active ) {
						wai[i].wdg->activate();
					} else {
						wai[i].wdg->deactivate();
					}
				break;
				case WidgetActInfo::METH_SHOW:
					if( active ) {
						wai[i].wdg->show();
					} else {
						wai[i].wdg->hide();
					}
				break;
			}
		}
		mask = mask >> 1;
		i++;
	}
}

// === ControlPanelBrain ===

bool ControlPanelBrain::natural_size( int &W, int &H )
{
	return ((ControlPanel *)body() )->get_size( W, H );
}

//================ class ControlPanel =========================

	ControlPanel::ControlPanel(Heart *hrt, int x, int y, int w, int h, const char *name ) : Fl_Group( x, y, w, h )
	{
		heart = hrt;
		aspect = 0;

		if( name != NULL )
			panel_name = name;
		else
			panel_name = "ANONYMOUS";
			
		WidgetBrain::set_widget_brain( this, new ControlPanelBrain( this ) );
	};

	ControlPanel::~ControlPanel()
	{
		//free my widget brain
		WidgetBrain::set_widget_brain( this, NULL );
	};

	void ControlPanel::refresh_style()
	{
		heart->get_style_engine()->refresh_all_styles();
		redraw();
	};

	void ControlPanel::refresh_layout()
	{
	};

	void ControlPanel::resize (int x, int y, int w, int h)
	{
		LOG_RENDER_DETAIL2("%s resizing to ( %i, %i, %i, %i )\n", panel_name, x, y, w, h );
		Fl_Group::resize( x, y, w, h );
		refresh_layout();
	};
