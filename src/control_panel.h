/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Control_Panel_h
#define _Control_Panel_h

#include <FL/Fl.H>
#include <FL/Fl_Group.H>

#include "heart.h"
#include "widget_brain.h"

// ==== Widget Activation ====

class WidgetActInfo
{
public:
	static const int METH_NONE = 0;
	static const int METH_ACTIVATE = 1;
	static const int METH_SHOW = 2;

	Fl_Widget	*wdg;
	int			method;

	WidgetActInfo( Fl_Widget *w = NULL, int _method = METH_ACTIVATE ) : wdg(w), method(_method) {};
	~WidgetActInfo() {};
};

class WidgetActivator
{
protected:
	std::vector<WidgetActInfo> wai;
	unsigned long switchableMask;
	unsigned long enabledMask;

	void update_switchable_mask();
public:
	WidgetActivator( WidgetActInfo *widgets = NULL );
	void SetWidgets( WidgetActInfo *widgets = NULL );
	void AddWidget( Fl_Widget *widget, int method = WidgetActInfo::METH_ACTIVATE );
	void AddWidgets( Fl_Widget **widgets, int method = WidgetActInfo::METH_ACTIVATE );

	void SetActivation( unsigned long mask, bool active = true );
	void Activate( unsigned long mask ) { SetActivation( mask, true ); };
	void Deactivate( unsigned long mask ) { SetActivation( mask, false ); };

	unsigned long GetSwitchableMask() { return switchableMask; };
	unsigned long GetEnabledMask() { return switchableMask; };
	void RefreshEnabled();
};

class ControlPanel;

// === ControlPanelBrain ===

class ControlPanelBrain : public WidgetBrain
{
public:
	static const int check_width = 30;

	ControlPanelBrain( ControlPanel *cp ) : WidgetBrain( (Fl_Widget *)cp ) {};
	~ControlPanelBrain(){};

	virtual bool natural_size( int &W, int &H );
};

// === ControlPanel ===

class ControlPanel : public Fl_Group
{
public:
	const char *panel_name;

	float		aspect;

	Heart		*heart;

	ControlPanel(Heart *hrt, int x, int y, int w, int h, const char *name = NULL );
	virtual ~ControlPanel();

	virtual void	EnableControls( unsigned long mask, bool enable = true ) {};
	virtual void 	DisableControls( unsigned long mask ) { EnableControls( mask, false ); };
	unsigned long	GetEnabledControls() { return 0; };
	unsigned long	GetSwitchableControls() { return 0; }

	virtual void refresh_style();
	virtual void refresh_layout();
	virtual void resize (int x, int y, int w, int h);

	void set_show( bool state ) { if( state ) show(); else hide(); };

	/*
	 * says what size this panel wants to be.
	 * parameters: &w, &h - references to variables where width and height will be stored
	 *	returns: true, if the panel cares about its size and has set w and h or
	 *				false, if the panel doesn't care and hasn't touched w and h.
	 */
	virtual bool get_size( int &w, int &h ) {return false; };
};

#endif //_Control_Panel_h