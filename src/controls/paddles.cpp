/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <FL/Fl.H>

#include "paddles.h"
#include "../style_engine.h"
#include "../heart.h"
#include "../control_panel.h"
#include "../clicky_debug.h"

bool PaddlesBrain::natural_size( int &W, int &H )
{
	return ((Paddles *)body() )->get_size( W, H );
}

Paddles::Paddles( Heart *_heart, const char *lbl, bool minus_first, int style_paddle, int style_label )
	: Fl_Group(0,0,0,0 ), heart(_heart)
{
	paddle_id = 0;

	layout = new SimpLE();

	button_plus = new ClickyButton(0,0,0,0,"+");
	button_minus = new ClickyButton(0,0,0,0,"-");
	button_label = new ClickyButton(0,0,0,0,_(lbl));
	end();	//to stop adding widgets to this group

	//tooltips
//	button_plus->tooltip( _("Play or pause timed subtitles.") );

	//callbacks
	button_plus->callback((Fl_Callback*)cb_plus);
	button_minus->callback((Fl_Callback*)cb_minus);

	//-- layout --

	static const int pixx = 4;
	static const int pixy = 0;
	static const int marginw = 0;
	static const int marginh = 0;
	static const int spacex = 4;
	static const int spacey = 4;

	int minus_cx = 0;
	int plus_cx = 2;
	if( !minus_first ) {
		minus_cx = 2;
		plus_cx = 0;
	}

	layout->add_cell( button_label, 0, 0, 4, 2,	//cx, cy, cw, ch
										4, 4,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_plus, plus_cx, 2, 2, 2,	//cx, cy, cw, ch
										4, 4,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_minus, minus_cx, 2, 2, 2,	//cx, cy, cw, ch
										4, 4,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	//-- style --
	heart->get_style_engine()->assign_style( style_paddle, button_plus );
	heart->get_style_engine()->assign_style( style_paddle, button_minus );
	heart->get_style_engine()->assign_style( style_label, button_label );

	//-- refresh --

	refresh_layout();
	refresh_style();

	PaddlesBrain::set_widget_brain( this, new PaddlesBrain( this ) );
}

Paddles::~Paddles() {
	delete layout;
}

void Paddles::refresh_style()
{
	heart->get_style_engine()->refresh_all_styles();
	redraw();
};

bool Paddles::get_size( int &w, int &h )
{
	int	fh;

	fh = heart->get_style_prefs()->NormalButton->ls.Size;
	h = fh*3;
	w = h;
	return true;
}

void Paddles::refresh_layout()
{
	layout->refresh_layout( this );
}

void Paddles::resize (int x, int y, int w, int h)
{
	Fl_Group::resize( x, y, w, h );
	refresh_layout();
}

void Paddles::cb_plus( Fl_Button* b, void*)
{
	Paddles *p = (Paddles *)b->parent();
	p->clicked( true, p->paddle_id );
}

void Paddles::cb_minus( Fl_Button* b, void*)
{
	Paddles *p = (Paddles *)b->parent();
	p->clicked( false, p->paddle_id );
}

void Paddles::clicked( bool plus, long id )
{
	LOG_GEN_DETAIL("%s! on id %li\n", plus ? "plus" : "minus", id );
}
