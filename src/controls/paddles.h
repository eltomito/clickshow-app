/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Paddles_h_
#define _Paddles_h_

#include <FL/Fl.H>
#include <FL/Fl_Group.H>

#include "../widget_brain.h"
#include "../clicky_button.h"
#include "../style_prefs.h"
#include "../simple_layout_engine.h"

class Heart;

class Paddles;

class PaddlesBrain : public WidgetBrain
{
public:
	PaddlesBrain( Paddles *p ) : WidgetBrain( (Fl_Widget *)p ) {};
	~PaddlesBrain(){};

	virtual bool natural_size( int &W, int &H );
};

class Paddles : public Fl_Group
{
protected:
	Heart *heart;

	int	paddle_id;

	SimpLE	*layout;

	ClickyButton *button_plus;
	ClickyButton *button_minus;
	ClickyButton *button_label;
public:
	Paddles( Heart *_heart, const char *lbl, bool minus_first = true, int style_paddle = StylePrefs::NORMAL_BUTTON, int style_label = StylePrefs::NO_BORDER_BUTTON );
	~Paddles();

	void set_id( int _id ) { paddle_id = _id; };

	void refresh_style();
	void refresh_layout();
	void resize (int x, int y, int w, int h);

	void plus_tooltip( const char *str ) { button_plus->tooltip( str ); };
	void minus_tooltip( const char *str ) { button_minus->tooltip( str ); };

	/*
	 * says what size this panel wants to be.
	 * parameters: &w, &h - references to variables where width and height will be stored
	 *	returns: true, if the panel cares about its size and has set w and h or
	 *				false, if the panel doesn't care and hasn't touched w and h.
	 */
	bool get_size( int &w, int &h );

protected:

	static void cb_plus( Fl_Button* b, void*);
	static void cb_minus( Fl_Button* b, void*);

	virtual void clicked( bool plus, long id );
};

#endif //_Paddles_h_
