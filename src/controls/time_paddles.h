/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Time_Paddles_h_
#define _Time_Paddles_h_

#include "paddles.h"

class TimePaddles : public Paddles
{
protected:
	double	increment;
public:
	TimePaddles( Heart *_heart, const char *lbl, double _increment, int style_paddle = StylePrefs::NORMAL_BUTTON, int style_label = StylePrefs::NO_BORDER_BUTTON );
	~TimePaddles() {};
protected:
	virtual void clicked( bool plus, long id );
	virtual void action( double inc );
};

#endif //_Time_Paddles_h_
