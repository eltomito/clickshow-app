/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "time_shifter.h"

bool TimeShifterBrain::natural_size( int &W, int &H )
{
	return ((TimeShifter *)body() )->get_size( W, H );
}

void TSPaddles::action( double inc )
{
	time_shifter.incomingAction( inc );
}

void TimeShifter::incomingAction( double inc )
{
	action( inc );
}

TimeShifter::TimeShifter( Heart *_heart, int style_paddle, int style_label ) : Fl_Group(0,0,0,0), heart(_heart)
{
	layout = new SimpLE();

	TSPaddles *tp = new TSPaddles( *this, _heart, "0.1s", -0.1 );
	tp->plus_tooltip(_("Skip 0.1 second forward."));
	tp->minus_tooltip(_("Rewind 0.1 second."));
	paddles.push_back( tp );
	tp = new TSPaddles( *this, _heart, "1s", -1 );
	tp->plus_tooltip(_("Skip 1 second forward."));
	tp->minus_tooltip(_("Rewind 1 second."));
	paddles.push_back( tp );
	tp = new TSPaddles( *this, _heart, "5s", -5 );
	tp->plus_tooltip(_("Skip 5 seconds forward."));
	tp->minus_tooltip(_("Rewind 5 seconds."));
	paddles.push_back( tp );
	end();

	//-- layout --

	static const int pixx = 4;
	static const int pixy = 0;
	static const int marginw = 0;
	static const int marginh = 0;
	static const int spacex = 4;
	static const int spacey = 4;

	int l = paddles.size();
	for( int i = 0; i < l; ++i ) {
		layout->add_cell( paddles[i], i, 0, 1, 1,	//cx, cy, cw, ch
											3, 1,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces
	}

	//-- refresh --

	refresh_layout();
	refresh_style();

	TimeShifterBrain::set_widget_brain( this, new TimeShifterBrain( this ) );
}

TimeShifter::~TimeShifter()
{
	int l = paddles.size();
	for( int i = 0; i < l; ++i ) {
		delete paddles[i];
		paddles[i] = NULL;
	}
	paddles.clear();
}

bool TimeShifter::get_size( int &w, int &h )
{
	int	fh;

	fh = heart->get_style_prefs()->NormalButton->ls.Size;
	h = fh*4;
	w = h*3;
	return true;
}

void TimeShifter::resize (int x, int y, int w, int h)
{
	Fl_Group::resize( x, y, w, h );
	refresh_layout();
}

void TimeShifter::refresh_layout()
{
	layout->refresh_layout( this );
}

void TimeShifter::refresh_style()
{
	heart->get_style_engine()->refresh_all_styles();
	redraw();
}

void TimeShifter::action( double inc )
{
	LOG_GEN_DETAIL("the time is shifting! ...by %lf seconds.\n", inc );	
}
