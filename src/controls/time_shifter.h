/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _TimeShifter_h_
#define _TimeShifter_h_

#include <FL/Fl.H>
#include <FL/Fl_Group.H>

#include "../widget_brain.h"
#include "../clicky_button.h"
#include "../style_prefs.h"
#include "../simple_layout_engine.h"
#include "time_paddles.h"
#include <vector>

class Heart;
class TimeShifter;

class TSPaddles : public TimePaddles
{
public:
	TSPaddles( TimeShifter &ts, Heart *_heart, const char *lbl, double _increment, int style_paddle = StylePrefs::NORMAL_BUTTON, int style_label = StylePrefs::NO_BORDER_BUTTON )
		: TimePaddles(_heart,lbl,_increment,style_paddle,style_label), time_shifter(ts) {};
	~TSPaddles() {};
protected:
	void action( double inc ); //( ts->action( inc )

	TimeShifter &time_shifter;
};

class TimeShifterBrain : public WidgetBrain
{
public:
	TimeShifterBrain( TimeShifter *p ) : WidgetBrain( (Fl_Widget *)p ) {};
	~TimeShifterBrain(){};

	virtual bool natural_size( int &W, int &H );
};

class TimeShifter : public Fl_Group
{
protected:
	Heart *heart;

	SimpLE	*layout;
	std::vector<TSPaddles*>	paddles;

public:
	TimeShifter( Heart *_heart, int style_paddle = StylePrefs::NORMAL_BUTTON, int style_label = StylePrefs::NO_BORDER_BUTTON );
	~TimeShifter();

	void refresh_style();
	void refresh_layout();
	void resize (int x, int y, int w, int h);

	/*
	 * says what size this panel wants to be.
	 * parameters: &w, &h - references to variables where width and height will be stored
	 *	returns: true, if the panel cares about its size and has set w and h or
	 *				false, if the panel doesn't care and hasn't touched w and h.
	 */
	bool get_size( int &w, int &h );

	void incomingAction( double inc );

protected:

	//overload this to capture increments
	virtual	void action( double inc );
};

#endif //_TimeShifter_h_
