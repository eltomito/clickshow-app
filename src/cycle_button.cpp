/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cycle_button.h"
#include "clicky_debug.h"

CycleButton::CycleButton( int x, int y, int w, int h, const char **_labels, int init_state )
	: ClickyButton( x, y, w, h, NULL )
{
	if( !_labels ) {
		state = -1;
		return;
	}
	while( _labels[0] ) {
		labels.push_back( _labels[0] );
		++_labels;
	}
	set_state( init_state );
	callback( (Fl_Callback*)cb_click );
};

int CycleButton::set_state( int n )
{
	int res = state;
	if(( n >= 0 )&&( n < labels.size() )) {
		state = n;
		label( labels[state].c_str() );
	} else {
		LOG_RENDER_ERROR("Cannot set state to %i when there are only %i labels!\n", n, labels.size() );
		if( labels.size() == 0 ) {
			state = -1;
		}
	}
	return res;
};

int CycleButton::next_state()
{
	if( state >= 0 ) {
		int s = state + 1;
		if( s >= labels.size() ) {
			s = 0;
		}
		set_state( s );
	}
	return state;
};

void CycleButton::cb_click( CycleButton* b, void* )
{
	if( b->next_state() >= 0 ) {
		b->on_state_change( b->state );
	}
};
