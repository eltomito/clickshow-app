/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cycle_Button_h
#define _Cycle_Button_h

#include <FL/Fl.H>
#include <FL/Fl_Button.H>

#include <vector>
#include <string>

#include "clicky_debug.h"
#include "clicky_button.h"

class CycleButton : public ClickyButton
{
protected:
	std::vector<std::string>	labels;
	int								state;

public:
	CycleButton( int x, int y, int w, int h, const char **_labels, int init_state = 0 );
	~CycleButton() {};

	int get_state() { return state; };
	int set_state( int n );

protected:
	int next_state();
	virtual void on_state_change( int n ) {};

	static void cb_click( CycleButton *b, void * );
};

#endif //_Cycle_Button_h
