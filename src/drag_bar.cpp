/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "drag_bar.h"
#include <FL/fl_draw.H>

DragBar::DragBar( Fl_Widget *movedwidget, int x, int y, int w, int h, Fl_Color bgclr, Fl_Color clr )
	: Fl_Widget( x, y, w, h, NULL )
{
	moved_widget = movedwidget;
	color( bgclr );
	bar_color( clr );
}

void DragBar::draw()
{
	char dashes[] = { 1, 5, 0 };

	//fl_rectf( x(), y(), w(), h(), color() ); //background

	fl_color( barcolor );
	fl_line_style( FL_DOT, 0, &dashes[0] );
	fl_xyline( x() + trough_offset_x, y() + (h()-trough_width )/2, x()+w()-trough_offset_x );
	fl_xyline( x() + trough_offset_x, y() + (h()+trough_width )/2, x()+w()-trough_offset_x );
	fl_line_style( 0 );
	//draw_box( FL_PLASTIC_UP_FRAME, barcolor );
}

int DragBar::handle( int event )
{
	switch( event )
	{
		case FL_ENTER:
			LOG_RENDER_DETAIL2("The mouse has ENTERED!\n");
			return 1;	//we want to track mouse movement
			break;

		case FL_LEAVE:
			LOG_RENDER_DETAIL2("The mouse has LEFT!\n");
			return 1;
			break;

		case	FL_PUSH:
			dragpoint_x = Fl::event_x();
			dragpoint_y = Fl::event_y();
			LOG_RENDER_DETAIL2("PUSHED! dragpoint_x=%i, dragpoint_y=%i\n", dragpoint_x, dragpoint_y);
			return 1;	//without this, I wouldn't get FL_DRAG events.
			break;

		case	FL_RELEASE:
			LOG_RENDER_DETAIL2("RELEASED!\n");
			if( moved_widget != NULL )
				moved_widget->redraw();

			return 1;
			break;

		case	FL_DRAG:
			if( moved_widget != NULL )
			{
				moved_widget->position( moved_widget->x() + Fl::event_x() - dragpoint_x, moved_widget->y() + Fl::event_y() - dragpoint_y ); 
				LOG_RENDER_DETAIL2("DRAGGED! event_x=%i, event_y=%i, x=%i, y=%i\n", Fl::event_x(), Fl::event_y(), moved_widget->x(), moved_widget->y() );
			};
			return( 1 );
			break;
		};

	return Fl_Widget::handle( event );
};
