/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _DragBar_h
#define _DragBar_h

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Widget.H>

#include "clicky_debug.h"

class	DragBar : public Fl_Widget
{
private:
	Fl_Widget	*moved_widget;
	Fl_Color		barcolor;

	int	dragpoint_x, dragpoint_y;

	static const int trough_width = 6;
	static const int trough_offset_x = 10;

public:

	DragBar( Fl_Widget *moved_widget, int x, int y, int w, int h, Fl_Color bgclr=FL_BLACK, Fl_Color clr=FL_GRAY );
	~DragBar() {};

	int handle( int event );

	void draw();
	//void resize( int x, int y, int w, int h );
	
	//void debug_print();

	void bar_color( Fl_Color clr ) { barcolor = clr; };
	Fl_Color bar_color() { return barcolor; };
};

#endif //_DragBar_h
