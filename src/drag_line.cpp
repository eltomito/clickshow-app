/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "drag_line.h"

#include "clicky_debug.h"

#include <FL/fl_draw.H>

DragLine::DragLine( double rel_pos, bool horiz, Fl_Color clr, Fl_Color hiclr, Fl_Color bgclr, int width )
			:Fl_Widget( 0, 0, 0, 0, "" )
{
	color( bgclr );
	labelcolor( clr );
	labeltype( FL_NO_LABEL );
	box( FL_FLAT_BOX );
	
	hi_color = hiclr;	
	
	bar_width = width;
	horizontal = horiz;

	set_limits( 0, 1 );
	set_position( rel_pos );

	set_move_callback( NULL );

	pushed = false;
	mouse_here = false;
	
	resize( 0, 0, 0, 0);
};

int DragLine::get_bar_width()
{
	return bar_width;
};

void DragLine::set_move_callback( MoveCallbackT *cb )
{
	move_callback = cb;
};

void DragLine::set_limits( double minrelpos, double maxrelpos, int minpos, int maxpos )
{
	min_rel_pos = minrelpos; 
	max_rel_pos = maxrelpos; 
	min_abs_pos = minpos;
	max_abs_pos = maxpos;
};

void	DragLine::invert()
{
	double	rmin;
	int		amin;
	
	rmin = min_rel_pos;
	amin = min_abs_pos;
	
	min_rel_pos = (double)1 - max_rel_pos;
	max_rel_pos = (double)1 - rmin;

	if( horizontal )
	{
		min_abs_pos = parent()->h() - max_abs_pos;
		max_abs_pos = parent()->h() - amin;
	}
	else
	{
		min_abs_pos = parent()->w() - max_abs_pos;
		max_abs_pos = parent()->w() - amin;
	};
	set_position( (double)1.0 - cur_pos );
};

bool DragLine::set_position( double pos )
{
	LOG_RENDER_DETAIL2("DragLine: position requested: %f, minrelpos %f, maxrelpos %f\n",
				(float)pos, (float)min_rel_pos, (float)max_rel_pos); 

	if(( pos >= min_rel_pos )&&( pos <= max_rel_pos ))
	{
		int abspos;
		if( horizontal )
			abspos = (int)( (double) parent()->h() * pos);
		else
			abspos = (int)( (double) parent()->w() * pos);

	LOG_RENDER_DETAIL2("DragLine: abspos = %i, min = %i, max = %i\n", abspos, min_abs_pos, max_abs_pos-bar_width );


		if( ( abspos >= min_abs_pos) && (( (abspos+bar_width) <= max_abs_pos )||( max_abs_pos < 0 )) )
		{
			cur_pos = pos;
			resize( 0, 0, 0, 0 );
			return true;
		};
	};	
	return false;
};

double	DragLine::get_position()
{
	return cur_pos;
};

/*
 * returns the absolute position within the parent widget
 */
int DragLine::get_abs_position()
{
	if( horizontal )
		return (int)( (double)parent()->h()*cur_pos );
	else
		return (int)( (double)parent()->w()*cur_pos );
};

int DragLine::handle( int event )
{
	switch( event )
	{
		case FL_ENTER:
			LOG_RENDER_DETAIL2("Drag Line ENTERED!\n");
			mouse_here = true;
			redraw();	// !!! IMPORTANT !!! Don't call draw() here!
							// it segfaults if the mouse is in the drag line on program startup.
			return 1;	//we want to track mouse movement
			break;

		case FL_LEAVE:
			mouse_here = false;
			pushed = false;
			redraw();
			LOG_RENDER_DETAIL2("Drag Line LEFT!\n");
			return 1;
			break;

		case	FL_PUSH:
			pushed = true;
			LOG_RENDER_DETAIL2("Drag Line PUSHED!\n");
			return 1;	//without this, I wouldn't get FL_DRAG events.
			break;

		case	FL_RELEASE:
			pushed = false;
			return 1;
			break;

		case	FL_DRAG:
			double new_pos;
			if( horizontal )
				new_pos = (double)( Fl::event_y() - parent()->y() ) / (double)parent()->h();
			else		
				new_pos = (double)( Fl::event_x() - parent()->x() ) / (double)parent()->w();
			if( set_position( new_pos ) )
			{
				if( move_callback != NULL )
					move_callback( this );
			};
			LOG_RENDER_DETAIL2("Drag Line DRAGGED! new position=%f\n", (float)new_pos);
			LOG_RENDER_DETAIL2("event_x = %i, event_y = %i, x=%i, y=%i\n", Fl::event_x(), Fl::event_y(), parent()->x(), parent()->y() );

			return( 1 );
			break;

		};

	return Fl_Widget::handle( event );
};

void DragLine::draw()
{
	Fl_Color	last_color;
	
	last_color = fl_color();

	if( !mouse_here )
		fl_color( labelcolor() );
	else
		fl_color( hi_color );

	if( horizontal )
		fl_xyline( x(), y() + bar_width/2, x()+w() );
	else
		fl_yxline( x() + bar_width/2, y(), y()+h() );
	
	fl_color( last_color );
	fl_line_style( 0 );

};

/*
 * resize ignores all arguments and calculates its size based on the size of its parent window
 */
void DragLine::resize( int, int, int, int )
{
	int x, y, w, h;

	if( horizontal )
	{
		x= 0;
		y = parent()->y() + (int)( (double)parent()->h() * cur_pos );
		w = parent()->w();
		h = bar_width;
	}
	else
	{
		x = parent()->x() + (int)( (double)parent()->w() * cur_pos );
		y = 0;
		w = bar_width;
		h = parent()->h();
	};
	Fl_Widget::resize( x, y, w, h);
};

/*
 */
void DragLine::debug_print()
{
	LOG_RENDER_DETAIL2("bar_width = %i, horizontal = %i, cur_pos = %lf\n", bar_width, (int)horizontal, cur_pos );
	LOG_RENDER_DETAIL2("min_rel_pos = %lf, max_rel_pos = %lf\n", min_rel_pos, max_rel_pos );
	LOG_RENDER_DETAIL2("min_abs_pos = %i, max_abs_pos = %i\n", min_abs_pos, max_abs_pos );
	LOG_RENDER_DETAIL2("x() = %i, y() = %i, w() = %i, h() = %i\n", x(), y(), w(), h() );
};
