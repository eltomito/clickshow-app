/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _DragLine_h
#define _DragLine_h

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Widget.H>

class	DragLine : public Fl_Widget
{
public:
	typedef void( MoveCallbackT )(DragLine *);

private:
	MoveCallbackT	*move_callback;
	int				bar_width;
	bool				horizontal;
	double			cur_pos;
	double			min_rel_pos;
	double			max_rel_pos;
	int				min_abs_pos;
	int				max_abs_pos;
	
	bool				pushed;
	bool				mouse_here;
	
	Fl_Color			hi_color;
public:

	DragLine( double rel_pos, bool horiz=true, Fl_Color clr=FL_WHITE, Fl_Color hiclr=FL_GRAY, Fl_Color bgclr=FL_BLACK, int width=20 );
	
	void set_limits( double minrelpos, double maxrelpos, int minpos=0, int maxpos=-1 );
	bool set_position( double pos );
	void set_move_callback( MoveCallbackT *cb );

	int get_bar_width();
	double	get_position();

	/*
	 * returns the absolute position within the parent widget
	 */
	int		get_abs_position();

	//flip the drag line position and limits
	void	invert();

	int handle( int event );

	void draw();
	void resize( int x, int y, int w, int h );
	
	void debug_print();
};

#endif //_DragLine_h
