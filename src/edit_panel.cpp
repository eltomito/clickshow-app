/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "edit_panel.h"

//================== class EditPanel =========================

	EditPanel::EditPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, (const char *)"EDIT" )
	{
		heart = hrt;;

		//layout = new SimpLE();

		//vertical pack
		vert_pack = new BodyPack( true );
		vert_pack->align( CL_ALIGN_FILL );
		vert_pack->spacing( 5 );

		ctrl_pack = new BodyPack( false );
		ctrl_pack->align( CL_ALIGN_EVEN|CL_ALIGN_FILL );
		ctrl_pack->spacing( 10 );

		button_use = new ClickyButton( 0, 0, 0, 0, _("Use") );
		button_cancel = new ClickyButton( 0, 0, 0, 0, _("Cancel") );

		end();

		//stuff mlinput_text inside a BodyPack to give it a frame
		input_pack = new BodyPack();
		input_pack->align( CL_ALIGN_FILL );
		input_pack->borders( 2,2,2,2 );

		mlinput_text = new Fl_Multiline_Input( 0, 0, 0, 0 );
		WidgetBrain::set_widget_brain( mlinput_text, new MLInputBrain( mlinput_text, 20, 4 ) );
		mlinput_text->cursor_color( FL_WHITE );

		end();

		vert_pack->add( input_pack );

		//tooltips
		mlinput_text->tooltip( _("Edit the subtitle text here\nand press \"Use\" when done\nor \"Cancel\" to discard your changes.\nAn empty line splits the subtitle in two.\n") );
		button_use->tooltip( _("Apply the edited text to the subtitle.") );
		button_cancel->tooltip( _("Discard all changes and stop editing.") );

		//brain loading
		WidgetBrain::set_user_data( button_use, (void *)this );
		WidgetBrain::set_user_data( button_cancel, (void *)this );

		//callbacks
		button_use->callback((Fl_Callback*)cb_use);
		button_cancel->callback((Fl_Callback*)cb_cancel);

		//-- style --
		Fl_Widget* all_buttons[] = { button_use, button_cancel, NULL };
		heart->get_style_engine()->assign_style( StylePrefs::EDIT_BUTTON, &all_buttons[0] );
		heart->get_style_engine()->assign_style( StylePrefs::TEXT_INPUT, mlinput_text );
		heart->get_style_engine()->assign_style( StylePrefs::SEARCH_WIDGET, input_pack );

		refresh_layout();
		refresh_style();
	};

   EditPanel::~EditPanel()
   {
       //delete layout;
   };

	bool EditPanel::get_size( int &w, int &h )
	{
		vert_pack->natural_size( w, h );
		return true;
	};

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void EditPanel::refresh_layout()
	{
		vert_pack->resize( x(), y(), w(), h() );
	};

/*
 * set text
 */
void EditPanel::set_text( const char *txt )
{
	mlinput_text->replace( 0, mlinput_text->size(), txt );
	mlinput_text->position( 0 );
};

/*
 * get text
 */
const char *EditPanel::get_text()
{
	return mlinput_text->value();
};

//-- callbacks --

void EditPanel::cb_use(Fl_Button* b, void*)
{
	EditPanel *p = (EditPanel  *)WidgetBrain::get_user_data( (Fl_Widget*)b );
	LOG_RENDER_DETAIL2("EditPanel: clicked \"use\"!!\n" );
	p->heart->clicked_edit_use();
};

void EditPanel::cb_cancel(Fl_Button* b, void*)
{
	EditPanel *p = (EditPanel  *)WidgetBrain::get_user_data( (Fl_Widget*)b );
	LOG_RENDER_DETAIL2("EditPanel: clicked \"cancel\"!!\n" );
	p->heart->clicked_edit_cancel();
};
