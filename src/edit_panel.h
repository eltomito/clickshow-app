/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Edit_Panel_h
#define _Edit_Panel_h

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Multiline_Input.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "clicky_button.h"
#include "body_pack.h"
#include "brain_mlinput.h"

/*
 * the search panel class
 */
class EditPanel : public ControlPanel
{
	SimpLE	*layout;

	Fl_Multiline_Input *mlinput_text;
	ClickyButton	*button_use;
	ClickyButton	*button_cancel;

	BodyPack *vert_pack;
	BodyPack *ctrl_pack;
	BodyPack *input_pack;

	static void cb_use(Fl_Button* b, void*);
	static void cb_cancel(Fl_Button* b, void*);

public:
	EditPanel(Heart *hrt, int x, int y, int w, int h);
    ~EditPanel();

	void set_text( const char *txt );
	const char *get_text();

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void refresh_layout();
	bool get_size( int &w, int &h );
};


#endif //_Edit_Panel_h
