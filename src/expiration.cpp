/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "expiration.h"

#include <ctime>
#include <iostream>

/*
 * checks if clicky has NOT expired
 * parameters:
 *		final_year - the year at the end of which clicky expires (e.g. 2013)
 * returns:
 		true, if clicky is still good :]
 */
bool _clicky_is_still_good( int final_year )
{
    time_t t = std::time(0);   // get time now
    struct tm * now = std::localtime( & t );
	if( (now->tm_year + 1900) > final_year )
		return false;

	return true;
};
