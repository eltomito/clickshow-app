/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Expiration_h
#define _Expiration_h

#include "clicky_debug.h"
#include "config.h"

bool _clicky_is_still_good( int final_year );

#ifdef CLICKY_EXPIRES

#define clicky_is_still_good() _clicky_is_still_good( CLICKY_EXPIRES )

#else //CLICKY_EXPIRES

#define clicky_is_still_good() true

#endif //CLICKY_EXPIRES

#endif // _Expiration_h
