/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "file_panel.h"

#include "clicky_debug.h"

#include <FL/names.h>
#include <FL/Fl_Native_File_Chooser.H>
#include "body_layout.h"

void FileMoreButton::on_state_change( int n )
{
	file_panel->set_more( n != 0 );
};

const char *FileMoreButton::plus_minus[] = {"+","-",NULL};

// =================== class FilePanel =========================

	FilePanel::FilePanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, (const char *)"FILE" )
	{
		//heart = hrt;;
		//layout = new SimpLE();

		button_load = new ClickyButton( 0, 0, 0, 0, _("Open Subtitles") );
		button_save = new ClickyButton( 0, 0, 0, 0, _("Save Subtitles") );
		button_quit = new ClickyButton( 0, 0, 0, 0, _("Quit") );
		button_textprefs = new ClickyButton( 0, 0, 0, 0, _("Text Settings") );
		button_options = new ClickyButton( 0, 0, 0, 0, _("Options") );

		button_more = new FileMoreButton( this );
		button_net = new ClickyButton( 0, 0, 0, 0, _("Net") );
		button_billboard = new ClickyButton( 0, 0, 0, 0, _("Projector") );
		button_recorder = new ClickyButton( 0, 0, 0, 0, _("Recorder") );

		lbutt_spaces = new Fl_Check_Button( 0, 0, 80, 34, _("Spaces") );
		//WidgetBrain::set_widget_brain( lbutt_spaces, NULL ); 
		WidgetBrain::set_widget_brain( lbutt_spaces, new CheckButtonBrain( lbutt_spaces ) );

		end();

		//widget enabler
		wdgact.AddWidget( button_load, WidgetActInfo::METH_SHOW );
		wdgact.AddWidget( button_save, WidgetActInfo::METH_SHOW );
		wdgact.AddWidget( lbutt_spaces, WidgetActInfo::METH_SHOW );

		//tooltips
		button_load->tooltip( _("Load new subtitles.") );
		button_save->tooltip( _("Save the current subtitles\nincluding all markings and edits.") );
		button_quit->tooltip( _("End this program.") );
		button_textprefs->tooltip( _("Set the size, color, font\nand position of the displayed subtitles.") );
		button_options->tooltip( _("Set the language, fullscreen mode\nand orientation of the user interface.") );

		button_more->tooltip( _("Show more / less.") );
		button_net->tooltip( _("Show newtwork settings.") );
		button_billboard->tooltip( _("Show settings for a separate\nsubtitle window useful with a projector.") );
		button_recorder->tooltip( _("Show the subtitle-timing recorder.") );

		lbutt_spaces->tooltip( _("If checked, a blank screen is displayed\nafter each subtitle.") );

		//init button states
		lbutt_spaces->value( (int)	heart->get_prefs()->with_spaces );
		show_morables( false );

		//callbacks
		lbutt_spaces->callback((Fl_Callback*)cb_spaces);
		button_quit->callback((Fl_Callback*)cb_quit);
		button_load->callback((Fl_Callback*)cb_load);
		button_save->callback((Fl_Callback*)cb_save);

		button_textprefs->callback((Fl_Callback*)cb_textprefs);
		button_options->callback((Fl_Callback*)cb_options);
		//button_more->callback((Fl_Callback*)cb_more);
		button_net->callback((Fl_Callback*)cb_net);
		button_billboard->callback((Fl_Callback*)cb_billboard);
		button_recorder->callback((Fl_Callback*)cb_recorder);

		//-- style --
		Fl_Widget* all_buttons[] = {	button_load, button_options, button_quit,
												button_textprefs, button_more,
												button_net, button_billboard, button_save,
												button_recorder,
												NULL };

		heart->get_style_engine()->assign_style( StylePrefs::FILE_BUTTON, &all_buttons[0] );
		heart->get_style_engine()->assign_style( StylePrefs::FILE_CBUTTON, lbutt_spaces );

		refresh_style();
		refresh_layout();
		wdgact.RefreshEnabled();
	}

    FilePanel::~FilePanel()
    {
        //delete layout;
    }

	void FilePanel::show_morables( bool state )
	{
		show_more = state;
		if( state ) {
			button_net->show();
			button_billboard->show();
			button_save->show();
			button_recorder->show();
		} else {
			button_net->hide();
			button_billboard->hide();
			button_save->hide();
			button_recorder->hide();
		}
	}

	void	FilePanel::EnableControls( unsigned long mask, bool enable )
	{
		wdgact.SetActivation( mask, enable );
	}

	bool FilePanel::get_size( int &w, int &h )
	{
		Fl_Widget* all[] = { button_load, button_options, button_quit,
									button_textprefs, lbutt_spaces, button_more,
									button_net, button_billboard, button_save,
									button_recorder };
		if( show_more ) {
			return BodyLayout::rect_size( &all[0], 10, w, h, //&all[0], 9, w, h,
														false, 4,
														10, 10, 5, 10,
														2, 10 );
		}
		return BodyLayout::rect_size( &all[0], 6, w, h,
													false, 4,
													10, 10, 5, 10,
													2, 10 );
	}

	void FilePanel::refresh_layout()
	{
		LOG_RENDER_DETAIL2("w()=%i, h()=%i\n", w(), h() );

		Fl_Widget* all[] = { button_load, button_options, button_quit,
									button_textprefs, lbutt_spaces, button_more,
									button_net, button_billboard, button_save,
									button_recorder };
		if( show_more ) {
			button_net->show();
			button_billboard->show();
			button_save->show();
			button_recorder->show();
			BodyLayout::layout_rect( &all[0], 10,					//Fl_Widget ** warr, int warr_size,
											x(), y(), w(), h(),			//int X, int Y, int W, int H,
											false, 4,						//bool rows_first, int break_after,
											10, 10, 5, 10,					//int left_b, int right_b, int top_b, int bottom_b,
											2, 10, CL_ALIGN_FILL );		//int maj_spacing, int min_spacing,  Cl_AlignType align
		} else {
			button_net->hide();
			button_billboard->hide();
			button_save->hide();
			button_recorder->hide();
			BodyLayout::layout_rect( &all[0], 6,					//Fl_Widget ** warr, int warr_size,
											x(), y(), w(), h(),			//int X, int Y, int W, int H,
											false, 4,						//bool rows_first, int break_after,
											10, 10, 5, 10,					//int left_b, int right_b, int top_b, int bottom_b,
											2, 10, CL_ALIGN_FILL );		//int maj_spacing, int min_spacing,  Cl_AlignType align
		}
	}

	void FilePanel::cb_spaces(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->clicked_spaces( p->lbutt_spaces->value() );
		LOG_RENDER_DETAIL2("And the new value iiiiiiiiiiiis: %i!\n", p->lbutt_spaces->value() );
	}

	void FilePanel::cb_quit(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->clicked_quit();
	};

	void FilePanel::cb_load(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		std::string	fname;

		p->heart->clicked_load();
	};

	void FilePanel::cb_save(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->clicked_save();
	};

	void FilePanel::cb_textprefs(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->show_text_options( 2 );
	};

	void FilePanel::cb_options(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->show_misc_options( 2 );
	};

	void FilePanel::cb_net(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->show_net_options( 2 );
	};

	void FilePanel::cb_billboard(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->show_billboard_options( 2 );
	};

	void FilePanel::cb_recorder(Fl_Button* b, void*)
	{
		FilePanel *p = (FilePanel *)b->parent();
		p->heart->show_recorder( 2 );
	};

	void FilePanel::set_more( bool state )
	{
		show_morables( state );
		heart->refresh_dashboard( true );
	};

	int FilePanel::file_dialog( std::vector<std::string> &fnames, bool save, int max_files )
	{
		Fl_Native_File_Chooser fnfc;
		Fl_Native_File_Chooser::Type type = Fl_Native_File_Chooser::BROWSE_FILE;
		int count = 0;

		if( save )
		{
			type = Fl_Native_File_Chooser::BROWSE_SAVE_FILE;
			fnfc.title( _("Choose a name for the subtitles") );
			fnfc.options( Fl_Native_File_Chooser::SAVEAS_CONFIRM );
			if( !fnames.empty() && !fnames[0].empty() )
				fnfc.preset_file( fnames[0].c_str() );
		}
		else
		{
			if( max_files > 1 ) {
				type = Fl_Native_File_Chooser::BROWSE_MULTI_FILE;
				char chooser_title[200];
#ifdef snprintf
				snprintf(&chooser_title[0], sizeof( chooser_title ), _("Choose at most %d subtitle files to load."), max_files );
#else
				sprintf(&chooser_title[0], _("Choose at most %d subtitle files to load."), max_files );
#endif //snprintf
				fnfc.title( &chooser_title[0] );
			} else {
				fnfc.title( _("Choose subtitles to load") );
			}
		};

		fnfc.type( type );

		fnfc.filter( _("Plain or Timed Subtitles\t*.{txt,srt}\nPlain Subtitles\t*.txt\nTimed Subtitles\t*.srt\nAll files\t*") );
		fnfc.directory( heart->get_prefs()->load_path.c_str() );           // default directory to use
		// Show native chooser
		switch ( fnfc.show() )
		{
			case -1:
				LOG_RENDER_DETAIL2("ERROR: %s\n", fnfc.errmsg() );
				fnames.clear();
				return 0;
				break;
			case  1:
 				LOG_RENDER_DETAIL2("CANCEL\n");
				fnames.clear();
				return 0;
 				break;
 			default:
	 			count = fnfc.count(); 
	 			for( int i = 0; i < count; i++ ) {
	 				LOG_RENDER_DETAIL2("PICKED (%i): %s\n", i, fnfc.filename(i) );
	 				fnames.push_back( (new std::string( fnfc.filename(i) ))[0] );
	 			}
			break;
		};
		return count;
	};
