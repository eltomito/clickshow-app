/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _File_Panel_h
#define _File_Panel_h

//#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "heart.h"
#include "ask_dialog.h"
#include "clicky_button.h"
#include "cycle_button.h"

class FilePanel;

class FileMoreButton : public CycleButton
{
protected:
	FilePanel					*file_panel;
public:
	static const char			*plus_minus[];

	FileMoreButton( FilePanel *fp ) : CycleButton(0,0,0,0,plus_minus), file_panel( fp ) {};
protected:
	void on_state_change( int n );
};

/*
 * the file panel class
 */
class FilePanel : public ControlPanel
{
	SimpLE	*layout;

	ClickyButton	*button_load;
	ClickyButton	*button_save;
	ClickyButton	*button_quit;

	ClickyButton	*button_textprefs;
	ClickyButton	*button_options;

//	ClickyButton	*button_more;
	FileMoreButton	*button_more;
	ClickyButton	*button_net;
	ClickyButton	*button_billboard;
	ClickyButton	*button_recorder;

	Fl_Check_Button *lbutt_spaces;

	WidgetActivator wdgact;

	static	void cb_spaces(Fl_Button* b, void*);
	static	void cb_quit(Fl_Button* b, void*);
	static	void cb_load(Fl_Button* b, void*);
	static	void cb_save(Fl_Button* b, void*);
	static	void cb_textprefs(Fl_Button* b, void*);
	static	void cb_options(Fl_Button* b, void*);
//	static	void cb_more(Fl_Button* b, void*);
	static	void cb_net(Fl_Button* b, void*);
	static	void cb_billboard(Fl_Button* b, void*);
	static	void cb_recorder(Fl_Button* b, void*);

	bool		show_more;

public:
	static const unsigned long	SWITCH_LOAD = 1;
	static const unsigned long	SWITCH_SAVE = 1 << 1;
	static const unsigned long	SWITCH_SPACES = 1 << 2;
	static const unsigned long	SWITCH_ALL = SWITCH_LOAD|SWITCH_SAVE|SWITCH_SPACES;

	FilePanel(Heart *hrt, int x, int y, int w, int h);
	~FilePanel();

	void set_more( bool state );

	void	EnableControls( unsigned long mask, bool enable = true );

	int file_dialog( std::vector<std::string> &fnames, bool save=false, int max_files=1 );

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void refresh_layout();
	bool get_size( int &w, int &h );

protected:
	void show_morables( bool state );

};

#endif //_File_Panel_h
