#include "file_utils.h"

char *FileUtils::read_file( const char *fname, size_t &len, int &err, bool terminate )
{
	FILE		*f;
	size_t	fsize, bytes_in;
	char		*fdata;
	struct stat		stat_buf;

	if( 0 != fl_stat( fname, &stat_buf ) ) { err = Errr::FILE_STAT; return NULL; };

	f = fl_fopen( fname, "r" );
	if( f == NULL ) { err = Errr::FILE_OPEN; return NULL; };

	fsize = (size_t)stat_buf.st_size;
	fdata = (char *)malloc( fsize + ( terminate ? 1 : 0 ) );
	if( fdata == NULL ) { fclose( f ); err = Errr::ALLOC; return NULL; };

	len = fread( (void *)fdata, 1, fsize, f );
	fclose( f );

	//note: I cannot check if bytes_in == fsize,
	//because fread() may translate \r\n into \n.
	if( !len ) {
		free( fdata );
		err = Errr::FILE_READ;
		return NULL;
	};

	if( terminate ) { fdata[ len ] = 0; }
	err = Errr::OK;
	return fdata;
}

int FileUtils::write_file( const char *fname, const char *data, size_t len )
{
	FILE *f = fl_fopen( fname, "w" );
	if( f == NULL )
		return Errr::FILE_OPEN;		//failed to open the file

	size_t outlen = fwrite( (void *)data, 1, len, f );
	fclose( f );

	if( outlen != len ) {
		return Errr::FILE_WRITE;		//something went wrong during writing the file. Probably out of disk space.
	}

	return Errr::OK;
}

void FileUtils::parse_path( const char *path, std::string &dirs, std::string &bare, std::string &ext )
{
	const char *extp = fl_filename_ext( path );
	const char *namep = fl_filename_name( path );
	if( extp ) {
		ext.assign( extp + 1 );
		bare.assign( namep, (extp - namep) );
	} else {
		ext.clear();
		bare.assign( namep );
	}
	dirs.assign( path, namep - path );
}

std::string &FileUtils::compose_path( std::string &dst, const std::string &dirs, const std::string &bare, const std::string &ext )
{
	dst.assign( dirs );
	if( !dirs.empty() && !(dirs.at( dirs.size() - 1 ) == '/') ) {
		dst.push_back('/');
	}
	dst.append( bare );
	dst.push_back('.');
	dst.append( ext );
	return dst;
}
