/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _File_Utils_
#define _File_Utils_

#include <FL/fl_utf8.h>
#include <FL/filename.H>
#include "shared/src/err.h"

#include <string>

class FileUtils
{
public:

	/*
	 * Returns the data read or NULL.
	 * len is set to the size of the data loaded,
	 * err holds the resulting error code,
	 * terminate == true means you want a '0' byte added to the end of the data.
	 *              In that case, len doesn't include the final 0.
	 */
	static char *read_file( const char *fname, size_t &len, int &err, bool terminate );

	/*
	 * Returns an error code.
	 */
	static int write_file( const char *fname, const char *data, size_t len );

	/*
	 * ext doesn't contain the separating dot. So it's "txt", not ".txt"
	 */
	static void parse_path( const char *path, std::string &dirs, std::string &bare, std::string &ext );
	static std::string &compose_path( std::string &dst, const std::string &dirs, const std::string &bare, const std::string &ext );

private:
	FileUtils() {};
	~FileUtils() {};
};

#endif //_File_Utils_
