/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "font_chooser.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <string>

FontChooser::FontChooser( Fl_Font numfonts, int x, int y, int w, int h, const char *label ) : Fl_Select_Browser( x, y, w, h, label )
{
	std::string	fontname;
	const char *tmpfontname;
	int			attrs;

	if( numfonts == 0 )
	{
		numfonts = Fl::set_fonts("*");
	};

	num_fonts = numfonts;

	//add all fonts to the list
	for( Fl_Font i=0; i<num_fonts; i++ )
	{
		fontname.clear();
		tmpfontname = Fl::get_font_name( i, &attrs );

		if( tmpfontname == NULL )
			break;

		if( attrs&FL_BOLD )
			fontname.append( "@b" );
		if( attrs&FL_ITALIC )
			fontname.append( "@i" );
		fontname.append( "@." );
		fontname.append( tmpfontname );

		add( fontname.c_str() );
	};
};

FontChooser::~FontChooser()
{
};
