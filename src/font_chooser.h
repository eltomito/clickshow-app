/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Font_Chooser_h
#define _Font_Chooser_h

#include <FL/Fl_Select_Browser.H>

class FontChooser : public Fl_Select_Browser
{
	Fl_Font	num_fonts;

public:

	FontChooser( Fl_Font numfonts, int x, int y, int w, int h, const char *label );
	~FontChooser();
};






#endif //_Font_Chooser_h