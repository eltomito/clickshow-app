#ifndef _font_faces_h_
#define _font_faces_h_ 1

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include <string>

#include "clicky_debug.h"

class FontFaces {
protected:
	Fl_Font	faces[4];

public:
	FontFaces( Fl_Font font = 0 ) {
		setFont( font );
	};
	~FontFaces() {};

	void setFont( Fl_Font font ) {
		faces[0] = find_font_face_fail_free( font, 0 );
		faces[FL_BOLD] = find_font_face_fail_free( font, FL_BOLD );
		faces[FL_ITALIC] = find_font_face_fail_free( font, FL_ITALIC );
		faces[FL_BOLD|FL_ITALIC] = find_font_face_fail_free( font, FL_BOLD|FL_ITALIC );
	};

	const Fl_Font *getFaces() { return &faces[0]; };

	static Fl_Font find_font_face_fail_free( Fl_Font font, int attr ) {
		Fl_Font f = find_font_face( font, attr );
		if( f >= 0 ) { return f; }
		return attr;	//returns the SANS font with the required attributes
	};

	/** find the family member with required attributes
	 * e.g., for a normal font, it finds its FL_BOLD equivalent.
	 * @returns a font from the same family
	 *          with the required qualifications or -1 if none found.
	 */
	static Fl_Font find_font_face( Fl_Font font, int attr ) {
		const char	*tmpName;
		int					fwdAttr, bwdAttr;
		int					ff = font;
		tmpName = Fl::get_font_name( ff, &fwdAttr );
		std::string famName = tmpName;
		std::string fwdName;
		LOG_CANDY_DETAIL("font fam: %i, \"%s\", attr: %i, need attr: %i\n", ff, tmpName, fwdAttr, attr );

		if( fwdAttr == attr ) { return font; }

		++ff;
		tmpName = Fl::get_font_name( ff, &fwdAttr );

		while(( fwdAttr != 0 )&&( fwdAttr != attr )) {
			++ff;
			tmpName = Fl::get_font_name( ff, &fwdAttr );
			LOG_CANDY_DETAIL("fwdFont: %i, \"%s\", attr: %i\n", ff, tmpName, fwdAttr );
		}

		if( fwdAttr == attr ) { fwdName = tmpName; }
		int bf;

		if( font > 0 ) {
			bf = font - 1;
			tmpName = Fl::get_font_name( bf, &bwdAttr );
			LOG_CANDY_DETAIL("1st bwdFont: %i, \"%s\", attr: %i\n", bf, tmpName, bwdAttr );
	
			while(( bwdAttr != 0 )&&( bwdAttr != attr )&&( bf >= 0 )) {
				--bf;
				tmpName = Fl::get_font_name( bf, &bwdAttr );
				LOG_CANDY_DETAIL("bwdFont: %i, \"%s\", attr: %i\n", bf, tmpName, bwdAttr );
			}
		} else {
			bf = ff;
			bwdAttr = -1;
		}

		Fl_Font res = -1;
		if(( bwdAttr == attr )&&( fwdAttr == attr )) {
			res = longerMatch( famName.c_str(), fwdName.c_str(), tmpName, ff, bf );
		} else if( bwdAttr == attr ) {
			res = bf;
		} else if( fwdAttr == attr ) {
			res = ff;
		}
		LOG_CANDY_DETAIL("The winner font is: %i\n", res );
		return res;
	};

protected:
	static Fl_Font longerMatch( const char *orig, const char *a, const char *b, Fl_Font fontA, Fl_Font fontB ) {
		int i = 0;
		while(( orig[i] != 0 )&&( a[i] == orig[i] )&&( b[i] == orig[i] )) {
			++i;
		}
		if( b[i] != orig[i] ) { return fontA; } 
		if( a[i] != orig[i] ) { return fontB; }
		return fontA;
	};
};

#endif //_font_faces_h_
