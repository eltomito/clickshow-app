/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "font_panel.h"

#include "clicky_debug.h"

//========================== class FontPanel =========================================

FontPanel::FontPanel(Heart *hrt, int x, int y, int w, int h,FontPanelCfg *_cfg)
	: ControlPanel ( hrt, x, y, w, h, (const char *)"FONT" ), cfg(_cfg)
	{
		layout = new SimpLE();	

		spinner_size = new Fl_Spinner( 0,0,0,0,_("Size: ") );
		spinner_border = new Fl_Spinner( 0,0,0,0, _("Border: ") );
		button_test = new ClickyButton( 0, 0, 0, 0, _("Test") );
		button_color = new ClickyButton( 0, 0, 0, 0, _("Color") );
		button_font = new ClickyButton( 0, 0, 0, 0, _("Font") );
		button_close = new ClickyButton( 0,0,0,0, "" );

		color_chooser = new Fl_Color_Chooser( 0,0,200,100,"" );
		//simplify_color_chooser( color_chooser );

		font_chooser = new FontChooser( 0, 0,0,0,0,"" );

		end();	//to stop adding widgets to this group

		visible_flags = 0;

		//spinner setup
		spinner_size->type( FL_INT_INPUT );
		spinner_size->range( 8, 1000 );
		spinner_size->value( *cfg->size );		//XXX
		spinner_size->align( FL_ALIGN_LEFT );

		spinner_border->type( FL_INT_INPUT );
		spinner_border->range( 0, 99 );
		spinner_border->value( *cfg->border );		//XXX
		spinner_border->align( FL_ALIGN_LEFT );
		
		max_border = 1;

		//color chooser setup

		Fl_Color clr = *cfg->color;			//XXX
		if( clr < 256 )
			clr = 0xffffff00;

		double red = (double)( clr >> 24 ) / (double)255;
		double green = (double)( (clr >> 16)&0xff ) / (double)255;
		double blue = (double)( (clr >> 8)&0xff ) / (double)255;

		color_chooser->rgb( red, green, blue );

		color_chooser->hide();

		//font chooser setup

		font_chooser->hide();

		//callbacks
		spinner_size->callback((Fl_Callback*)cb_size);
		spinner_border->callback((Fl_Callback*)cb_border);
		button_close->callback((Fl_Callback*)cb_close);
		button_color->callback((Fl_Callback*)cb_color);
		button_font->callback((Fl_Callback*)cb_font);
		button_test->callback((Fl_Callback*)cb_test);

		color_chooser->callback((Fl_Callback*)cb_color_chooser);
		font_chooser->callback((Fl_Callback*)cb_font_chooser);

	//-- style --

		heart->get_style_engine()->assign_style( StylePrefs::NORMAL_LIST, font_chooser );
		heart->get_style_engine()->assign_style( StylePrefs::OPTION_BUTTON, (Fl_Button *)spinner_size );
		heart->get_style_engine()->assign_style( StylePrefs::OPTION_BUTTON, (Fl_Button *)spinner_border );
		heart->get_style_engine()->assign_style( StylePrefs::OPTION_BUTTON, (Fl_Button *)button_color );
		heart->get_style_engine()->assign_style( StylePrefs::OPTION_BUTTON, (Fl_Button *)button_font );
		heart->get_style_engine()->assign_style( StylePrefs::OPTION_BUTTON, (Fl_Button *)button_test );
		heart->get_style_engine()->assign_style( StylePrefs::FPCLOSE_BUTTON, (Fl_Button *)button_close );

/*
		Fl_Widget* all_widgets[] = { button_next, button_up, button_down, button_clear, NULL };
*/

		//-- layout --

		create_layout();

		//-- refresh --

		refresh_layout();
		refresh_style();
	};

	FontPanel::~FontPanel()
	{
		delete layout;
	};

	void FontPanel::set_cfg( FontPanelCfg *_cfg )
	{
		cfg = _cfg;
		spinner_size->value( *cfg->size );
		spinner_border->value( *cfg->border );		//XXX
		Fl_Color clr = *cfg->color;			//XXX
		if( clr < 256 ) {	clr = 0xffffff00; }
		double red = (double)( clr >> 24 ) / (double)255;
		double green = (double)( (clr >> 16)&0xff ) / (double)255;
		double blue = (double)( (clr >> 8)&0xff ) / (double)255;
		color_chooser->rgb( red, green, blue );
	};

	/*
	 * creates a layout based on which widgets are shown
	 */
	void FontPanel::create_layout()
	{
		int	num_cols = base_w+1;
		int	color_col = base_w;
		int	font_col = base_w;

		static const int closeb_size = 21;
		static const int closeb_margin = 0;

		static const int spacex = 10;
		static const int pixx = 10;

//		static const int marginw = closeb_size+closeb_margin;
		static const int marginw = 0;

		if( color_visible() )
		{
			num_cols += color_w;
			font_col += color_w;
		};
		if( font_visible() )
			num_cols += font_w;

		layout->add_cell( spinner_size, base_w-2, 1, 2, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add_cell( spinner_border, base_w-2, 2, 2, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add_cell( button_test, 0, 0, 3, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add_cell( button_color, 0, 1, 3, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add_cell( button_font, 0, 2, 3, 1,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces

		layout->add( button_close,
							-1*(closeb_size), 0,			//pixx, pixy
							1.0, 0.0,	//relx, rely
							0.0, 0.0,	//relw, relh
							0, 0,		//margins
							closeb_size, closeb_size );		//min pix

		if( color_visible() )
			layout->add_cell( color_chooser, color_col, 0, color_w, 3,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces


		if( font_visible() )
			layout->add_cell( font_chooser, font_col, 0, font_w, 3,	//cx, cy, cw, ch
											num_cols, base_h,				//columns, rows
											pixx, 4,			//pixx, pixy
											0.0, 0.0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, 8,		//margins
											spacex, 2 );		//spaces
	};

	bool FontPanel::get_size( int &w, int &h )
	{
		int	fh;
		int	cw;
		
		cw = base_w;
		if( color_visible() )
			cw += color_w;
		if( font_visible() )
			cw += font_w;

		fh = heart->get_style_prefs()->NormalButton->ls.Size;
		h = fh*6;
		w = cw * h / base_h;
		return true;
	};

/*
 */
void FontPanel::set_max_border( int new_max )
{
	if( new_max <= 0 )
		return;

/*
	double old_rel = (double)spinner_border->value();
	double old_abs = old_rel * (double)max_border / (double)100;

	double new_rel = (double) 100 * old_abs / (double)new_max;

	max_border = new_max;

	spinner_border->value( (int)new_rel );
*/

	double old_rel = (double)spinner_border->value();
	double old_abs = old_rel * (double)max_border / (double)100;

//	double new_rel = (double)100 * (double)heart->get_prefs()->title_border_size / (double)new_max;	//XXX
	double new_rel = (double)100 * (double)*cfg->border / (double)new_max;	//XXX

	max_border = new_max;
	spinner_border->value( (int)new_rel );

}

	void FontPanel::show_controls( bool color, bool font )
	{
		color_visible( color );
		font_visible( font );

		layout->remove_all();
		create_layout();
		refresh_layout();

		if( color )
			color_chooser->show();
		else
			color_chooser->hide();

		if( font )
			font_chooser->show();
		else
			font_chooser->hide();
	};

	void FontPanel::color_visible( bool state )
	{
		visible_flags = ( visible_flags & (~COLOR_VISIBLE) ) | ( state * COLOR_VISIBLE );
	};

	void FontPanel::font_visible( bool state )
	{
		visible_flags = ( visible_flags & (~FONT_VISIBLE) ) | ( state * FONT_VISIBLE );
	};

	void FontPanel::refresh_layout()
	{
		layout->refresh_layout( this );
	};

Fl_Color FontPanel::rgb2flcolor( double r, double g, double b )
{
	Fl_Color red, green, blue;
	
	red = (Fl_Color)( (double)255*r );
	green = (Fl_Color)( (double)255*g );
	blue = ((Fl_Color)( double)255*b );

	LOG_RENDER_DETAIL2("converting: r %i, g %i, b %i\n", red, green, blue );

	return( (red<<24) + (green<<16) + (blue<<8) );
};

//=== callbacks ===

	void FontPanel::cb_color_chooser(Fl_Color_Chooser* b, void*)
	{
		double red, green, blue;
		Fl_Color clr;

		FontPanel *p = (FontPanel *)b->parent();

		red = b->r();
		green = b->g();
		blue = b->b();
		
		clr = rgb2flcolor( red, green, blue );

		LOG_RENDER_DETAIL2("r %f, g %f, b %f, color = %x\n", (float)red, (float)green, (float)blue, clr );

		//p->heart->set_title_font_color( clr );			//XXX color
		p->cfg->set_color( clr );
	};

	void FontPanel::cb_font_chooser(FontChooser* b, void*)
	{
		Fl_Font f;

		FontPanel *p = (FontPanel *)b->parent();
		f = (Fl_Font)b->value();
		LOG_GEN_INFO("---- Selected font: %i\n", f - 1 );
		if( f > 0 ) {
			//p->heart->set_title_font( f-1 );				//XXX font
			p->cfg->set_font( f - 1 ); 
		}
	};

	void FontPanel::cb_close(Fl_Button* b, void*)
	{
		FontPanel *p = (FontPanel *)b->parent();
		p->heart->show_text_options( 0 );
		LOG_RENDER_DETAIL2("Close, man!\n" );  
	};

	void FontPanel::cb_size(Fl_Spinner* b, void*)
	{
		FontPanel *p = (FontPanel *)b->parent();
//		p->heart->set_title_font_size( (int)b->value() );		//XXX size
		p->cfg->set_size( (int)b->value() );		//XXX size
		LOG_RENDER_DETAIL2("Size, man!\n" );  
	};

	void FontPanel::cb_border(Fl_Spinner* b, void*)
	{
		FontPanel *p = (FontPanel *)b->parent();
		double new_border =  (double)b->value() * (double)p->max_border / (double)100;
//		p->heart->set_title_border_size( (int)new_border );										//XXX border
		p->cfg->set_border( (int)new_border );										//XXX border
		LOG_RENDER_DETAIL2("Border = %i, man!\n", (int)new_border  );  
	}

	void FontPanel::cb_color(Fl_Button* b, void*)
	{
		FontPanel *p = (FontPanel *)b->parent();
		p->heart->clicked_font_color_chooser();
		LOG_RENDER_DETAIL2("Color, man!\n" );  
	}

	void FontPanel::cb_font(Fl_Button* b, void*)
	{
		FontPanel *p = (FontPanel *)b->parent();
		p->heart->clicked_font_chooser();
		LOG_RENDER_DETAIL2("Font, man!\n" );  
	}

	void FontPanel::cb_test(FontChooser* b, void*)
	{
		FontPanel *p = (FontPanel *)b->parent();

		p->heart->show_about_text();
	}

