/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Font_Panel_h
#define _Font_Panel_h

#include <FL/Fl.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Button.H>
//#include <FL/Fl_Color_Chooser.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_prefs.h"
#include "font_chooser.h"
#include "my_color_chooser.h"
#include "clicky_button.h"

class Heart;

class FontPanelCfg
{
public:
	Fl_Color	*color;
	Fl_Font	*font;
	int		*size;
	int		*border;
protected:
	Heart		*heart;

protected:
	FontPanelCfg( Heart *_heart, Fl_Color *_color, Fl_Font *_font, int *_size, int *_border )
		: heart(_heart), color(_color), font(_font), size(_size), border(_border) {};

public:
	~FontPanelCfg() {};

	virtual	void set_color( Fl_Color val ) {};
	virtual	void set_font( Fl_Font val ) {};
	virtual	void set_size( int val ) {};
	virtual	void set_border( int val ) {};
};

class AttachedFPCfg : public FontPanelCfg
{
public:
	AttachedFPCfg( Heart *_heart ) : FontPanelCfg( _heart, NULL, NULL, NULL, NULL )
	{
		color = &_heart->get_prefs()->title_font_color;
		font = &_heart->get_prefs()->title_font;
		size = &_heart->get_prefs()->title_font_size;
		border = &_heart->get_prefs()->title_border_size;
	};
	~AttachedFPCfg() {};
	void set_color( Fl_Color val ) { heart->set_title_font_color( val ); };
	void set_font( Fl_Font val ) { heart->set_title_font( val ); };
	void set_size( int val ) { heart->set_title_font_size( val ); };
	void set_border( int val ) { heart->set_title_border_size( val ); };
};

class DetachedFPCfg : public FontPanelCfg
{
public:
	DetachedFPCfg( Heart *_heart ) : FontPanelCfg( _heart, NULL, NULL, NULL, NULL )
	{
		color = &_heart->get_prefs()->bb_title_font_color;
		font = &_heart->get_prefs()->bb_title_font;
		size = &_heart->get_prefs()->bb_title_font_size;
		border = &_heart->get_prefs()->bb_title_border_size;
	};
	~DetachedFPCfg() {};
	void set_color( Fl_Color val ) { heart->set_bb_title_font_color( val ); };
	void set_font( Fl_Font val ) { heart->set_bb_title_font( val ); };
	void set_size( int val ) { heart->set_bb_title_font_size( val ); };
	void set_border( int val ) { heart->set_bb_title_border_size( val ); };
};

/*
 * the font panel class 
 */
class FontPanel : public ControlPanel
{
	static const unsigned int	COLOR_VISIBLE = 1;
	static const unsigned int	FONT_VISIBLE = 1 << 1;

	static const int	 base_h = 3;
	static const int base_w = 8;
	static const int color_w = 4;
	static const int	 font_w = 6;

	SimpLE	*layout;

	Fl_Spinner	*spinner_size;
	Fl_Spinner	*spinner_border;
	Fl_Button	*button_test;
	Fl_Button	*button_color;
	Fl_Button	*button_font;
	Fl_Button	*button_close;

	Fl_Color_Chooser	*color_chooser;
	FontChooser			*font_chooser;

	int max_border;

	unsigned	int	visible_flags;

	static void cb_size(Fl_Spinner* b, void*);
	static void cb_border(Fl_Spinner* b, void*);
	static void cb_color(Fl_Button* b, void *panel);
	static void cb_font(Fl_Button* b, void *panel);
	static void cb_close(Fl_Button* b, void *panel);
	static void cb_color_chooser(Fl_Color_Chooser* b, void*);
	static void cb_font_chooser(FontChooser* b, void*);
	static void cb_test(FontChooser* b, void*);

	void color_visible( bool state );
	void font_visible( bool state );

	void create_layout();

protected:
	FontPanelCfg	*cfg;

public:
	FontPanel(Heart *hrt, int x, int y, int w, int h, FontPanelCfg *_cfg);
	~FontPanel();

	void set_cfg( FontPanelCfg *_cfg );

	void refresh_layout();
	bool get_size( int &w, int &h );

	bool color_visible() { return( 0 != (visible_flags&COLOR_VISIBLE) ); };
	bool font_visible() { return( 0 != (visible_flags&FONT_VISIBLE) ); };

	void show_controls( bool color=false, bool font=false );

	void set_max_border( int new_max );

	static Fl_Color rgb2flcolor( double r, double g, double b );
};

#endif //_Font_Panel_h
