/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "heart.h"
#include "panels.h"
#include "clicky_debug.h"

#include "about_clicky.h"
#include "help_panel.h"
#include "panel_window.h"
#include "about_panel.h"
#include "subtitle_formats/subtitle_formats.h"
#include "billboard_window.h"
#include "colors/color_prefs.h"

#include "player/auto_player.h"
#include "player/player_panel.h"
#include "player/player_timer.h"

#include "recorder/sub_recorder.h"
#include "recorder/recorder_panel.h"

#include "clicky_version.h"
#include "clicky_copyright_year.h"

#include "../../shared/src/str_utils.h"

#include <FL/Fl_Double_Window.H>
#include <FL/Fl_Native_File_Chooser.H>

#include <ctime>

#ifndef _WIN32
#include <X11/extensions/scrnsaver.h>
#endif //_WIN32

const char *ltable[] = lang_table;
const char *xtable[] = trans_table;

Heart::Heart()
{
	heart = this;

	do_restart = false;

	set_global_shortcut_mode();

	prefs = new Prefs( "Eltomito", "Clickydude" );
	prefs->load();

#ifdef TRANSLATION

	translator = new Translator( &ltable[0], &xtable[0] );
	int langid = translator->lang_id( prefs->language_name.c_str() );
	translator->language( std::max( langid, 0 ) );

#endif //TRANSLATION

	bb_window = NULL;

	about_text_full.clear();
	const char about_full_template[] = ABOUT_CLICKY_HTML;
	StrUtils::Printf( about_text_full, _(about_full_template), VERSION_MAJOR, VERSION_MINOR, CLICKY_COPYRIGHT_YEAR );

	about_text_short.clear();
	const char about_short_template[] = ABOUT_CLICKY_SHORT;
	StrUtils::Printf( about_text_short, _(about_short_template), VERSION_MAJOR, VERSION_MINOR );

	style_prefs = new StylePrefs( prefs );
	subdoc = new SubtitleDoc();

	total_panel = NULL;
	auto_player = new AutoPlayer( *this );
	sub_recorder = new SubRecorder( *this );

	info_window = NULL;
	about_window = NULL;

	erase_subtitles();

	shown_sub = SHOWN_SUB_CLEAR;
	edited_sub = -1;

	render_candy_colors = (get_prefs()->parse_candy_colors > 0);

	netHeart = new NetHeart( this );
};

Heart::~Heart()
{
	_show_billboard( false );

	delete style_prefs;
	style_prefs = NULL;

	delete subdoc;
	subdoc = NULL;

	prefs->save();

	delete prefs;
};

bool Heart::ProcessCmdArgs( int argc, const char **argv, int &exit_code )
{
	ColorSet clrs;
	RainbowStyle::AppendToColorSet( clrs, get_prefs()->candy_colors, sizeof( heart->get_prefs()->candy_colors ) / sizeof( Fl_Color ) );
	CmdArgs args( argc, argv, clrs, get_prefs()->title_font_color );
	exit_code = args.Action();
	if( exit_code >= 0 ) {
		return false;
	}

	if( args.is_no_fullscreen() ) {
		get_prefs()->full_screen_mode = false;
	}

	if( !args.user.empty() ) {
		heart->get_prefs()->net_default_login = args.user;
	}
	if( !args.password.empty() ) {
		heart->get_prefs()->net_default_pwd = args.password;
	}
	if( !args.venue.empty() ) {
		heart->get_prefs()->net_default_venue = args.venue;
	}
	if( !args.server.empty() ) {
		heart->get_prefs()->net_default_server = args.server;
	}

	heart->get_prefs()->protocol_version = args.protocol;

	exit_code = 0;
	return true;
};

FilePanel *Heart::get_file_panel() { return get_dash_panel()->file_panel; };

void Heart::refresh_dashboard( bool redraw )
{
	get_main_panel()->refresh_layout();
	if( redraw ) { 
		get_main_panel()->redraw();
	}
	Fl::awake();
};

void Heart::focus_default_widget()
{
	Fl::focus( total_panel );
}

/** Makes sure the billboard preferences are sane.
 * Note that this method MUST NOT change the bb_visible pref.
 * @returns Whether any changes to the prefs have been actually made.
 */
bool Heart::fix_billboard_prefs()
{
	int screen_num = prefs->bb_screen_num;
	int is_fullscreen = prefs->bb_fullscreen;
	int x = prefs->bb_window_x;
	int y = prefs->bb_window_y;
	int w = prefs->bb_window_width;
	int h = prefs->bb_window_height;
	bool res = false;

	int screen_count = Fl::screen_count();
	int main_screen_num = ScreenUtils::screen_num( MainWindow );

	//screen number
	if(( screen_num < 0 )||( screen_num >= screen_count )) {
		res = true;
		if( MainWindow ) {
			if( main_screen_num < 0 ) {
				LOG_GUI_ERROR("Main window is on screen %i!", main_screen_num );
				screen_num = 0;
			} else if( main_screen_num < ( screen_count - 1 ) ) {
				screen_num = main_screen_num + 1;
			} else if( main_screen_num > 0 ) {
				screen_num = main_screen_num - 1;
			} else {
				screen_num = 0;
			}
		}
		LOG_GUI_ERROR("Main window: %lx, bb screen: %i", (unsigned long)MainWindow, screen_num );
	}

	//dimensions
	int X, Y, W, H;
	Fl::screen_work_area( X, Y, W, H, screen_num );

	ScreenUtils::shrink_frame_by_percent( X, Y, W, H, 10, 20 ); 
	res |= ScreenUtils::fit_frame_in_frame( x, y, w, h, X, Y, W, H );

	//fullscreen
	if( screen_num != main_screen_num ) {
		is_fullscreen = true;
	} else {
		is_fullscreen = false;
	}
	if( is_fullscreen != prefs->bb_fullscreen ) {
		res = true;
		get_bb_panel()->set_bb_fullscreen_state( is_fullscreen );
	}

	//store
	prefs->bb_screen_num = screen_num;
	prefs->bb_fullscreen = is_fullscreen;
	prefs->bb_window_x = x;
	prefs->bb_window_y = y;
	prefs->bb_window_width = w;
	prefs->bb_window_height = h;

	return res;
};

/** Turns main window's fullscreen off if the billboard is on the same screen.
 * This method does nothing if the billboard screen doesn't exist.
 * No preferences are ever changed.
 * @returns {bool} whether fullscreen state of the main window has been changed.
 */
bool Heart::fix_window_fullscreen()
{
	if( !MainWindow->fullscreen_active() ) {
		return false;
	}
	int bb_screen = bb_screen_number();
	int main_screen = ScreenUtils::screen_num( MainWindow );
	if( bb_screen != main_screen ) {
		return false;
	}
	set_fullscreen( false, true );
	MainWindow->show();	//raise
	return true;
};

/** Extracts position, size, fullscreen state, etc. from the billboard window and store it in the prefs.
 */
void Heart::store_billboard_prefs()
{
	if( !bb_window ) {
		prefs->bb_visible = 0;
		return;
	}
	prefs->bb_visible = 1;
	prefs->bb_screen_num = ScreenUtils::screen_num( bb_window );
	prefs->bb_fullscreen = ( 0 == bb_window->fullscreen_active() ) ? 0 : 1;
	prefs->bb_window_x = bb_window->x();
	prefs->bb_window_y = bb_window->y();
	if( !prefs->bb_fullscreen ) {
		prefs->bb_window_width = bb_window->w();
		prefs->bb_window_height = bb_window->h();
	}
}

void Heart::show_billboard( bool state, bool set_indicator ) {
	prefs->bb_visible = state;
	Fl::awake( &Heart::show_bb_awake_handler, (void*)this );
	if( set_indicator ) {
		get_bb_panel()->set_bb_visible_state( state );
	}
	font_panel_edits_bb( state );
}

void Heart::show_bb_awake_handler( void *_heart ) {
	((Heart *)_heart)->_show_billboard( ((Heart *)_heart)->prefs->bb_visible );
}

void Heart::_show_billboard( bool state )
{
	if( state && !bb_window ) {
		LOG_BB_DETAIL("BBWindow: bb_w = %i, bb_h = %i\n", prefs->bb_window_width, prefs->bb_window_height );
		bb_window = new BBWindow( this, prefs->bb_window_x, prefs->bb_window_y, prefs->bb_window_width, prefs->bb_window_height );
		bb_title_panel_revive_text();
		//bb_window->show();
		return;
	}
	if( !state && bb_window ) {
		bb_window->hide();
		delete bb_window;
		bb_window = NULL;
	}
};

int Heart::get_bb_height()
{
	TitlePanel *tp = get_bb_title_panel();
	return tp ? tp->h() : prefs->bb_window_height;
};

bool Heart::is_billboard_shown_on_ctrl_screen()
{
	if( !is_billboard_shown() ) {
		return false;
	}
	return (ScreenUtils::screen_num( bb_window ) == ScreenUtils::screen_num( MainWindow ));
}

/** Finds a new screen for the billboard if necessary.
 * @returns
 * - 0: not necessary
 * - 1: it was necessary and it was successfully done
 * - -1: it was necessary but there's no good screen for the billboard
 */
int Heart::bb_find_new_screen_if_necessary()
{
	int bb_screen = bb_screen_number();
	int main_screen = ScreenUtils::screen_num( MainWindow );
	if( bb_screen != main_screen ) {
		return 0;
	}

	int screen_count = Fl::screen_count();
	if( screen_count <= 1 ) {
		return -1;
	}
	int new_screen = main_screen + 1;
	if( new_screen >= screen_count ) {
		new_screen = main_screen - 1;
	}
	ScreenUtils::window_to_screen( bb_window, new_screen );
	return 1;
};

void Heart::bb_fullscreen( bool state, bool set_indicator )
{
	if( is_billboard_shown() ) {
		if( state ) {
			bb_window->fullscreen();
		} else {
			bb_window->fullscreen_off();
		}
	}
	prefs->bb_fullscreen = state;
	if( set_indicator ) {
		get_bb_panel()->set_bb_fullscreen_state( state );
	}
};

void Heart::bb_ontop( bool state, bool set_indicator )
{
	if( is_billboard_shown() ) {
		bb_window->set_ontop( state );
		bb_window->redraw();
	}
	prefs->bb_ontop = state ? 1 : 0;
	if( set_indicator ) {
		get_bb_panel()->set_bb_ontop_state( state );
	}
};

void Heart::bb_title_panel_set_text( const char *text, const RainbowMarkup *markup )
{
	TitlePanel *tp = get_bb_title_panel();
	if( tp ) {
		tp->set_text( text, markup );
	}
	if( markup ) { last_sub_markup = *markup; };
	if( text )
		last_sub_text = text;
	else
		last_sub_text.clear();
};

void Heart::bb_title_panel_revive_text()
{
	TitlePanel *tp = get_bb_title_panel();
	if( tp ) {
		tp->set_text( last_sub_text.c_str(), &last_sub_markup );
	}
}

TitlePanel *Heart::get_bb_title_panel()
{
	return bb_window ? bb_window->get_title_panel() : NULL;
};

bool Heart::DisableScreensaver( bool state )
{
#ifdef WIN32	//SetThreadExecutionState

	HKEY	hKey;
	LONG openRes = ::RegOpenKeyEx(	HKEY_CURRENT_USER,
									"Control Panel\\Desktop",
									0,
									KEY_QUERY_VALUE|KEY_SET_VALUE,
									&hKey );
	if( state ) {
		::SetThreadExecutionState( ES_CONTINUOUS|ES_DISPLAY_REQUIRED|ES_SYSTEM_REQUIRED );
		storedScreensaverValue = -1;
		if( openRes == ERROR_SUCCESS ) {
			char	origVal[64];	//It's supposed to be "0" or "1", so this shouldbe way enough.
			DWORD	origValLen = 0;
			DWORD valType;
			LONG lRes = RegQueryValueEx( hKey, "ScreenSaveActive", NULL, &valType, NULL, &origValLen );
			if(( lRes == ERROR_SUCCESS)&&( valType == REG_SZ )&&( origValLen > 1 )&&( origValLen <= sizeof(origVal) )) {
				lRes = RegQueryValueEx( hKey, "ScreenSaveActive", 0, &valType, (LPBYTE)origVal, &origValLen );
				storedScreensaverValue = (origVal[0] == '1') ? 1 : 0;
			}
			::RegSetValueEx( hKey, "ScreenSaveActive", 0, REG_SZ, (const BYTE*)"0", 2 );
		}
	} else {
		::SetThreadExecutionState( ES_CONTINUOUS );
		if(( openRes == ERROR_SUCCESS )&&( storedScreensaverValue >= 0 )) {
			::RegSetValueEx( hKey, "ScreenSaveActive", 0, REG_SZ,
								(const BYTE *)( (storedScreensaverValue == 1 ) ? "1" : "0" ),
								2 );
		}
	}
	if( openRes ) {
		::RegCloseKey( hKey );
	}
	return true;

#else //WIN32

/*
 * FIXME: Apparently, this isn't enough.
 * I should probably call XResetScreenSaver periodically.
 * See http://stackoverflow.com/questions/31498114/how-to-programatically-prevent-linux-computer-from-sleeping-or-turning-on-screen?rq=1
 */

	Display *disp = XOpenDisplay((char *)0);
	int xev, xerr;
	if( XScreenSaverQueryExtension( disp, &xev, &xerr ) ) {
		XScreenSaverSuspend( disp, state );
		return true;
	}
	return false;

#endif //WIN32 //SetThreadExecutionState
};


void Heart::set_subdoc( SubtitleDoc *newdoc ) {
	if( subdoc ) {
		delete subdoc;
	}
	if( newdoc == NULL ) {
		newdoc = new SubtitleDoc();
	}
	subdoc = newdoc;
}

void Heart::erase_subtitles()
{
	subdoc->get_subtitles()->clear();
};

/*
 * resets the subtitle view after new subtitles have been loaded
 */
void Heart::refresh_subtitles()
{
	if( !do_restart )
	{
		//not doing restart
		edited_sub = -1;

		if( subdoc->get_subtitles()->size() > 0 )
		{
			next_sub = 0;
			next_space = false;
		}
		else
		{
			next_sub = -1;
		};
		shown_sub = SHOWN_SUB_CLEAR;

		if( get_list_panel() != NULL )
			get_list_panel()->set_subtitles( subdoc->get_subtitles() );

		if( get_title_panel() != NULL )
			get_title_panel()->set_text();
	}
	else //do_restart
	{
		if( get_list_panel() != NULL )
			get_list_panel()->set_subtitles( subdoc->get_subtitles() );

		//show_sub( shown_sub );				//for some reason, this doesn't work. FIX IT!
	};
	go_to_sub( next_sub );
	update_gui_mode( true );
};

#include "shared/src/net/node/user_perms.h"

void Heart::update_gui_mode( bool redraw )
{
	UserPermsT perms = get_net_heart()->GetPerms();
	unsigned int sub_flags = (subdoc && subdoc->get_subtitles()) ? subdoc->get_subtitles()->get_flags() : 0;

	LOG_GUI_INFO("Perms = %lx, subtitle flags = %x\n", perms, sub_flags );

	get_dash_panel()->file_panel->EnableControls( FilePanel::SWITCH_LOAD|FilePanel::SWITCH_SAVE, perms & PERM_LOAD_SUBS );
	get_dash_panel()->file_panel->EnableControls( FilePanel::SWITCH_SPACES, perms & PERM_CLICK_SUBS );

	bool allow_handclick = (perms & PERM_CLICK_SUBS)
		&& ( !(sub_flags & Subtitles::FLAG_timed) || !(sub_flags & Subtitles::FLAG_fully_timed) );

	bool allow_autoplay = (perms & PERM_CLICK_SUBS)
		&& (sub_flags & Subtitles::FLAG_timed);

	get_dash_panel()->search_panel->set_show( allow_handclick || allow_autoplay );
	get_dash_panel()->click_panel->set_show( allow_handclick );
	get_dash_panel()->player_panel->set_show( allow_autoplay );

	get_dash_panel()->net_panel->UpdateMode();

	refresh_dashboard( redraw );
}

bool Heart::show_list_sub( int subNum, bool send_to_net )
{
	if( subNum != SHOWN_SUB_CLEAR ) {
		if( subNum < 0 ) return false;
		LOG_PLAYER_DETAIL2("showing subtitle %i.\n", subNum);
		if( !show_sub( subNum, "", send_to_net ) ) return false;
		go_to_sub( subNum + 1, false );	//the subtitle number is valid. Let's move the list to it!
		next_space = prefs->with_spaces;
		return true;
	}
	LOG_PLAYER_DETAIL2("hiding subtitle.\n");
	next_space = false;
	return show_sub( SHOWN_SUB_CLEAR, "", send_to_net );
}

//=== get functions ===

StylePrefs *Heart::get_style_prefs()
{
	return style_prefs;
};

StyleEngine	*Heart::get_style_engine()
{
	return style_prefs->get_engine();
};

Subtitles *Heart::get_subtitles()
{
	return subdoc->get_subtitles();
};

TotalPanel *Heart::get_total_panel() { return total_panel; };
TitlePanel *Heart::get_title_panel() { return total_panel->title_panel; };
SubListPanel *Heart::get_list_panel() { return total_panel->sublist_panel; };
MainPanel *Heart::get_main_panel() { return total_panel->main_panel; };
DashPanel *Heart::get_dash_panel() { return total_panel->main_panel->dash; };
EditPanel *Heart::get_edit_panel() { return total_panel->main_panel->dash->edit_panel; };
NetPanel *Heart::get_net_panel() { return get_dash_panel()->net_panel; };
BillboardPanel *Heart::get_bb_panel() { return get_dash_panel()->billboard_panel; };
OptionsPanel *Heart::get_options_panel() { return get_dash_panel()->options_panel; };

//=== set functions ==

Fl_Window *Heart::set_main_window( Fl_Window *main_win )
{
	Fl_Window	*old = MainWindow;
	MainWindow = main_win;
	return old;
};

//=== subtitle seeking functions ===

/** Displays subtitle number n and sets shown_sub = n.
 * @param n - Number of the subtitle to display or SHOWN_SUB_HANDTYPED or SHOWN_SUB_CLEAR
 * @param default_text - if n is out of range, display default_text instead.
 * @param sendToNet - whether this subtitle should be sent out to the network.
 *
 * If n is out of range, default_text is displayed and shown_sub is set to SHOWN_SUB_HANDTYPED.
 *
 * Assumes that subtitles != NULL.
 *
 * @returns: true if a valid subtitle has been shown, false otherwise (n is out of range).
 */
bool Heart::show_sub( int n, const char *default_text, bool sendToNet )
{
//	bool doRainbow = render_candy_colors || ( subdoc->get_subtitles()->type() == SubtitleType::MULTI_SUBTITLES );

	if(( shown_sub >= 0 )||( shown_sub == SHOWN_SUB_HANDTYPED )) { last_shown_sub = shown_sub; }

	if(( n >= 0 )&&( n < subdoc->get_subtitles()->size() ))
	{
		Subtitle *sub = subdoc->get_subtitles()->get_subtitle( n );
		const char *t = NULL;
		const RainbowMarkup *rm = NULL;
		if( !sub->is_comment() ) {
			t = sub->get_disp_text().c_str();
			rm = &sub->get_markup();
		}

		LOG_GEN_INFO("showing subtitle %i \"%s\"\n", n, t );
		get_title_panel()->set_text( t, rm );
		bb_title_panel_set_text( t, rm );
		sub_recorder->record_show( *sub, n );
		shown_sub = n;
		get_list_panel()->set_mark( sub->is_marked() );
		if( sendToNet ) { netHeart->SendSubtitle( shown_sub, sub->get_disp_text() ); }
	}
	else
	{
		if( n != SHOWN_SUB_HANDTYPED ) {
			n = SHOWN_SUB_CLEAR;
			default_text = "";
			sub_recorder->record_hide();
		}
		get_title_panel()->set_text( default_text );
		bb_title_panel_set_text( default_text );
		get_list_panel()->set_mark( false );
		shown_sub = n;

		if( sendToNet ) {	netHeart->SendSubtitle( shown_sub, default_text ); }

		return false;
	};
	return true;
};

/*
 * assumes that subtitles != NULL
 */
void Heart::list_to_sub( int n )
{
	if(( n >= 0 )&&( n <= subdoc->get_subtitles()->size() ))
	{
		get_list_panel()->set_first_sub( n );
	}
}

// -- announcements --
void Heart::totalpanel_geom_changed()
{
	if( !is_billboard_shown() ) {
		get_dash_panel()->font_panel->set_max_border( get_title_panel()->h() );
	}
}

void Heart::billboard_geom_changed()
{
	if( is_billboard_shown() ) {
		get_dash_panel()->font_panel->set_max_border( get_bb_title_panel()->h() );
	}
}

//=== click handlers ===

/*
 * returns the number of the subtitles shown (starting at 0)
 *				or -1 if there are no subtitles loaded.
 */
int Heart::clicked_show_next()
{
	if( next_sub < 0 )
	{
		show_sub( SHOWN_SUB_CLEAR );
		return -1;	//no subtitles to show
	};

	if( ( prefs->with_spaces )&&( next_space ) )
	{
		show_sub( SHOWN_SUB_CLEAR );
		next_space = false;
	}
	else
	{
		if( show_sub( next_sub ) )
		{
			if( get_subtitles()->get_subtitle( next_sub )->is_comment() ) 
				next_space = false; 
			else
				next_space = true;

			next_sub++;
			list_to_sub( next_sub );
			sub_to_player_recorder( next_sub );
		};
	};
	return next_sub;
};

void Heart::clicked_clear()
{
	if(( shown_sub >= 0 )||( shown_sub == SHOWN_SUB_HANDTYPED ))
	{
		show_sub( SHOWN_SUB_CLEAR );
		next_space = false;
	}
	else
	{
		if( last_shown_sub >= 0 )
		{
			show_sub( last_shown_sub );
			next_space = true;
		};
	};
}

void Heart::clicked_spaces( bool state )
{
	prefs->with_spaces = state;
};

/*
 * 1 - one step down
 * -1 - one step up
 * 2 - page down
 * -2 - page up
 */
 int Heart::clicked_go( int dir )
{
	if( dir > 1 ) dir = 5;
	if( dir < -1 ) dir = -5;
	int sub_num = next_sub + dir;
	if( sub_num < 0 ) {
		sub_num = 0;
	} else if( sub_num >= subdoc->get_subtitles()->size() ) {
		sub_num = subdoc->get_subtitles()->size() - 1;
	}
	go_to_sub( sub_num, true );
	sub_to_player_recorder( sub_num );
	LOG_GEN_INFO("current subtitle is %i\n", sub_num );
	return sub_num;
};

/*
 * the public function for setting the top sub on the list to an arbitrary value
 * parameters:
 *		n - the number of teh subtitle to be the new top on the list
 *		clear_shown - if clearing the title panel is requested
 * returns:
 *		true if n was a valid subtitle number, false otherwise
 */
bool Heart::go_to_sub( int n, bool clear_shown )
{
		if(( n <= subdoc->get_subtitles()->size() )&&( n >= 0 ))
		{
			next_sub = n;
			next_space = false;
			list_to_sub( next_sub );
			if( clear_shown )
			{
				show_sub( SHOWN_SUB_CLEAR, "", false );
				next_space = false;
			};
			return true;
		};

	return false;
}

void Heart::sub_to_player_recorder( int sub_num )
{
	Subtitle *sub = subdoc->get_subtitles()->get_subtitle( sub_num );
	if( sub ) {
		if( auto_player ) auto_player->SeekToSub( *sub );
		if( sub_recorder ) sub_recorder->SeekToSub( *sub, subdoc->get_subtitles()->get_subtitle( sub_num - 1 ) );
	}
}

bool Heart::go_to_sub( const char *numstr, bool clear_shown )
{
	int subnum;

	if( numstr == NULL )
		return false;

	int res = sscanf( numstr, "%d", &subnum );	
	if( res != 1 )
		return false;		//something went wrong - the text probably isn't a number

	if( subnum > 0 )
		subnum--;			//let the user call the first subtitle "subtitle 1"	

	return go_to_sub( subnum, clear_shown );
}

int Heart::clicked_mark()
{
	int mark_sub = -1;

	if( shown_sub >= 0 ) {
		if( shown_sub < subdoc->get_subtitles()->size() ) {
			mark_sub = shown_sub;
		}
	} else if(( last_shown_sub >= 0 )&&( last_shown_sub == (next_sub - 1) )) {
		mark_sub = last_shown_sub;
	}

	if( mark_sub >= 0 ) {
		LOG_GEN_INFO("marking...\n");
		int rv = subdoc->get_subtitles()->get_subtitle( mark_sub )->mark( 2 );
		get_list_panel()->set_mark( rv );
		return rv;
	}

	return( -1 );
};


int Heart::mark_sub( int sub_i )
{
	if(( sub_i >= 0 )&&( sub_i < subdoc->get_subtitles()->size() ))
	{
		LOG_GEN_INFO("marking subtitle %i\n", sub_i );
		int rv = subdoc->get_subtitles()->get_subtitle( sub_i )->mark( 2 );
		get_list_panel()->refresh_subs();
		return rv;
	};
	return( -1 );
};

void Heart::show_sub_click_panel( int mode )
{
	DashPanel *d = get_dash_panel();
	if( !d ) return;
	SubClickPanel *p = d->click_panel;
	if( !p ) return;
	switch( mode ) {
		case 0:
			p->hide();
		break;
		case 1:
			p->show();
		break;
		case 2:
			if( p->visible() ) {
				p->hide();
			} else {
				p->show();
			}
		break;
		default:
			LOG_GEN_ERROR("bad argument: mode = %i! (only 0, 1, 2 make sense)\n", mode );
		break;
	}
	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
}

// === title panel click handlers ===
	
void Heart::clicked_edit_shown()
{
	if( shown_sub >= 0 )
		edit_subtitle( shown_sub );
}

//====== FilePanel click handlers ============

void Heart::clicked_quit()
{
#ifdef ASK_BEFORE_QUIT

	std::string	query;
	std::string buttons;

	query.clear();
	buttons.clear();

	if( sub_recorder && !sub_recorder->clicked_reset() ) {
		return; //the quit has been canceled
	}

	int	res;

	if( get_subdoc()->is_modified() )
	{
		query = _("There are unsaved changes in the subtitles!");
		buttons = _("#0 #c0xff000000 Quit and lose changes|#2 #c0x0000ff00 Save subtitles|#1 #c0x0000ff00 Do nothing");
	}
	else
	{
		query = _("Do you really want to quit?");
		buttons = _("#0 #c0xff000000 Quit|#1 #c0x0000ff00 Don't Quit");
	};

	res = ask( _("Attention!"), query.c_str(), buttons.c_str() );
	if( res == 1 )
		return;

	if( res == 2 )
	{
		clicked_save();
		return;
	};

#endif //ASK_BEFORE_QUIT

	if( auto_player && auto_player->IsRunning() ) {
		auto_player->Pause();
		int cnt = 5;
		while( auto_player && auto_player->IsRunning() && cnt ) {
			boost::this_thread::sleep_for( boost::chrono::seconds(1) );
			--cnt;
		}
	}

	if( bb_window ) { bb_window->hide(); }
	if( MainWindow ) {	MainWindow->hide(); };
};

void free_docs( std::vector<SubtitleDoc *> docs ) {
	int len = docs.size();
	for( int i = 0; i < len; i++ ) {
		if( docs[i] != NULL ) {
			delete docs[i];
			docs[i] = NULL;
		}
	}
}

/** Loads new subtitles from a std::string instead of a file.
 */
void Heart::load_subtitles_from_string( const std::string &subs, const std::string &fileName )
{
	SubtitleDoc *newdoc = new SubtitleDoc();
	if( newdoc == NULL ) {
		LOG_GEN_INFO("FAILED to allocate a new SubtitleDoc!\n");
		return;
	}

	int fmt = SubtitleFormats::GuessFormatFromFileName( fileName );
	if( fmt == SubtitleFormats::FMT_UNKNOWN ) {
		fmt = SubtitleFormats::GuessFormat( subs.c_str() );
	}

	SubtitleFormats::ParserCfg cfg( fmt, SubtitleFormats::CandyParseFromPrefs( prefs->parse_candy_colors ), false );

	newdoc->load_from_string( subs.c_str(), fileName, cfg );
//	color_subtitles( newdoc->get_subtitles() ); //deprecated
	newdoc->set_savename( fileName.c_str() );
	set_subdoc( newdoc );

	LOG_GEN_INFO("LOADED %d subtitles from string. fileName = \"%s\".\n", newdoc->get_subtitles()->size(), fileName.c_str() );

	refresh_subtitles();
	list_to_sub( 0 );
	show_sub( SHOWN_SUB_CLEAR, "", false );

}

void Heart::load_multi_subtitles( std::vector<std::string> &file_names ) {
	std::vector<SubtitleDoc *> docs;

	int numsubs = file_names.size();
	if( numsubs <= 0 ) {
		LOG_GEN_INFO("Asked to load multiple subtitles but no file names were given.\n");
		return;
	}

	LOG_GEN_INFO("Loading multiple (%d) subtitle files!\n", numsubs);

	//load all subtitle files into separate documents
	std::string errmsg;

	std::string savename;
	std::vector<int> badfiles;
	SubtitleDoc *tmpdoc = NULL;
	int retval;

 	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_UNKNOWN, SubtitleFormats::CandyParseFromPrefs( prefs->parse_candy_colors ), false );

	for( int i = 0; i < numsubs; i++ ) {
		if( tmpdoc == NULL ) {
			tmpdoc = new SubtitleDoc();
		}
		LOG_GEN_INFO("Loading %s!\n", file_names[i].c_str() );
		cfg.Format( SubtitleFormats::GuessFormatFromFileName( file_names[i] ) );
		retval = tmpdoc->load_from_file( file_names[i].c_str(), cfg );
		if( retval < 0 ) {
			badfiles.push_back( i );
			delete tmpdoc;
			tmpdoc = NULL;
		} else {
			docs.push_back( tmpdoc );
			tmpdoc->generate_savename( file_names[i].c_str() );
			tmpdoc = NULL;
		}
	}

	//do some sanity checking
	
	if( docs.empty() ) {
		ask( _("Oops!"), _("No files could be opened."), _("Uh huh.") );
		return;
	}
	if( !badfiles.empty() ) {
		errmsg = _("These files could not be loaded:");
		int numbads = badfiles.size();
		for( int i = 0; i < numbads; i++ ) {
			errmsg.append("\n");
			errmsg.append( file_names[badfiles[i]] );
		}
		int answer = ask( _("Problem!"), errmsg.c_str(), _("#0 #c0xff000000 Cancel|#1 #c0x0000ff00 Continue anyway") );
		if( answer == 0 ) {
			free_docs( docs );
			return;
		}
	}

	int numdocs = docs.size();
	SubtitleDoc *newdoc;

	//it's just one subtitle file
	if( numdocs == 1 ) {
		newdoc = docs[0];
	} else {
		newdoc = new SubtitleDoc();
		int err = SubtitleDoc::merge_multiple( *newdoc, docs );
		free_docs( docs );
		if( err ) {
			ask( _("Oops!"), _("Failed to merge subtitles!"), _("Uh huh.") );
			return;
		}
	}

	newdoc->generate_savename( file_names[0].c_str() );

	set_subdoc( newdoc );

	refresh_subtitles();
	show_sub( SHOWN_SUB_CLEAR );
}

void Heart::recolor_subtitles(Subtitles *subs)
{
	SubtitleFormats::ParserCfg cfg(	SubtitleFormats::FMT_UNKNOWN,
											subs->is_candy() ? SubtitleFormats::PARSE_ON : SubtitleFormats::PARSE_OFF,
											false );
	ErrDescsT errs;
	int err = SubtitleFormats::Morph( *subs, cfg, errs );
	if( err ) {
		LOG_CANDY_ERROR("Failed to recolor subtitles: %s\n", Errr::Message( err ) );
	}
};

int Heart::show_file_dialog(  std::vector<std::string> &fnames, bool save, int max_files )
{
	return get_dash_panel()->file_panel->file_dialog( fnames,save, max_files ); 
}

void Heart::clicked_save()
{
	std::vector<std::string> fnames( 1, get_subdoc()->save_name );

	if( show_file_dialog( fnames, true ) > 0 )
	{
		//make sure the name ends with .txt
		std::string &fname = fnames[fnames.size() - 1];

		set_load_path( fname.c_str() );

		size_t barename_i;
		size_t ext_i;
		parse_file_name( fname, barename_i, ext_i );
		if( ext_i == std::string::npos )
			fname.append(".txt");

		//save it
		int retval = subdoc->save_to_file( fname.c_str() );
		LOG_GEN_INFO("SubtitleDoc::save_to_file returned %i\n", retval );
		subdoc->set_filename( fname.c_str() );
	};
};

void Heart::clicked_load()
{
	std::vector<std::string> fnames;

	if( sub_recorder && !sub_recorder->clicked_reset() ) {
		return; //the loading has been canceled
	}

    int action = 0;

    //warn the user if subtitles modified and not saved
	if( get_subdoc()->is_modified() )
	{
        action = ask( _("Attention!"), _("There are unsaved changes in the subtitles!"),
                     _("#0 #c0xff000000 Lose changes|#1 #c0x0000ff00 Save subtitles") );
	};

    if( action == 0 )
    {
    		int count = get_dash_panel()->file_panel->file_dialog( fnames, false, Heart::MAX_MERGE_SUBS );
			LOG_GEN_INFO("LOADING %i SUBTITLES!!!", count);

			if( fnames.size() > 0 ) {
				set_load_path( fnames[0].c_str() );
			}

			load_multi_subtitles( fnames );
#ifdef CLICKY_NET
			get_net_heart()->UploadSubtitles( get_subdoc() );
#endif //CLICKY_NET

    }
    else
    {
        clicked_save();
    };
};

/*
 * Sets the prefs->load_path to the directory of path
 * so that passing either a file or a directory as path will work.
 */
void Heart::set_load_path( const char *path )
{
	size_t dirlen = strlen( path );
	if( !fl_filename_isdir( path ) ) {
		const char *tmpname = fl_filename_name( path );
		dirlen -= strlen( tmpname );
	}
	get_prefs()->load_path.assign( path, dirlen );
}

void Heart::parse_file_name( std::string &fname, size_t &barename_start, size_t &ext_start )
{
	ext_start = fname.find_last_of(".");
	barename_start = fname.find_last_of("\\/");
	if( (ext_start != std::string::npos)&&(barename_start != std::string::npos)&&(ext_start < barename_start ) )
		ext_start = std::string::npos;

	if( barename_start == fname.size()-1 )
		barename_start = std::string::npos;
	else
		if( barename_start != std::string::npos )
			barename_start++;
};

// -- candy panel click handlers --

void Heart::set_candy_color( int i, Fl_Color clr )
{
	if( i >= 8 ) {
		LOG_GEN_INFO("ERROR! Candy color out of range: %i\n", i );
		return;		
	}
	get_prefs()->candy_colors[i] = clr;
	get_list_panel()->set_candy_color( i, clr );
	get_list_panel()->redraw();
	get_title_panel()->redraw();
}

void Heart::reset_candy_colors()
{
	get_prefs()->candy_colors[0] = FL_GREEN;
	get_prefs()->candy_colors[1] = FL_YELLOW;
	get_prefs()->candy_colors[2] = FL_CYAN;
	get_prefs()->candy_colors[3] = FL_WHITE;
	get_prefs()->candy_colors[4] = FL_WHITE;
	get_prefs()->candy_colors[5] = FL_MAGENTA;
	get_prefs()->candy_colors[6] = FL_RED;
	get_prefs()->candy_colors[7] = (Fl_Color)0x94a1fd; //instead of FL_BLUE;
	get_list_panel()->load_candy_colors();
	get_list_panel()->redraw();
	get_title_panel()->redraw();
}

void Heart::load_or_save_candy_colors( bool save )
{
	Fl_Native_File_Chooser fnfc;
	if( save ) {
		fnfc.type( Fl_Native_File_Chooser::BROWSE_SAVE_FILE );
		fnfc.options( Fl_Native_File_Chooser::SAVEAS_CONFIRM );

	} else {
		fnfc.type( Fl_Native_File_Chooser::BROWSE_FILE );
	}
	fnfc.filter( _("Clickshow Color Config\t*.ccc\nAll files\t*") );
	fnfc.directory( heart->get_prefs()->load_path.c_str() );
	fnfc.preset_file( "palette.ccc" );
	// Show native chooser
	int res = fnfc.show();
	if(( res == -1 )||( res == 1 )) {
		LOG_RENDER_DETAIL("NO GOOD. %s\n", ( res == -1 ) ? fnfc.errmsg() : "" );
		return;
	};
	ColorPrefs *cp = new ColorPrefs( get_prefs(), fnfc.filename(), "ElTomito" );
	if( !cp ) {
		LOG_CANDY_ERROR("Failed to load or save color config!\n");
		return;
	}
	if( save ) {
		cp->readFromPrefs();
	} else {
		cp->writeToPrefs();
	}
	delete cp;
}

void Heart::show_candy_options( int how )
{
	if( how == 2 )
		how = ( false == get_dash_panel()->candy_panel->visible() );

	if( how == 1 )
		get_dash_panel()->candy_panel->show();
	else
		get_dash_panel()->candy_panel->hide();

	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
};

//-- font panel click handlers --

void Heart::show_text_options( int how )
{
	if( how == 2 )
		how = ( false == get_dash_panel()->font_panel->visible() );

	if( how == 1 )
		get_dash_panel()->font_panel->show();
	else
		get_dash_panel()->font_panel->hide();

	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
};

void Heart::show_net_options( int how )
{
	if( how == 2 )
		how = !get_dash_panel()->net_panel->visible();
	if( how == 1 )
		get_dash_panel()->net_panel->show();
	else
		get_dash_panel()->net_panel->hide();
	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
	get_prefs()->net_panel_visible = get_dash_panel()->net_panel->visible();
};

void Heart::show_billboard_options( int how )
{
	if( how == 2 )
		how = !get_dash_panel()->billboard_panel->visible();
	if( how == 1 )
		get_dash_panel()->billboard_panel->show();
	else
		get_dash_panel()->billboard_panel->hide();
	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
	get_prefs()->bb_panel_visible = get_dash_panel()->billboard_panel->visible();
};

void Heart::set_title_font_size( int size )
{
	style_prefs->set_title_font_size( size );
	get_title_panel()->refresh_layout();
	get_title_panel()->redraw();
};

void Heart::set_title_border_size( int size )
{
	prefs->title_border_size = size;
	get_title_panel()->refresh_layout();
	get_title_panel()->redraw();
};

void Heart::set_title_font_color( Fl_Color clr )
{
	style_prefs->set_title_font_color( clr );

	get_title_panel()->refresh_layout();
	get_title_panel()->redraw();
	get_list_panel()->refresh_mark_button();

};

void Heart::set_title_font( Fl_Font font )
{
	style_prefs->set_title_font( font );
	get_title_panel()->refresh_layout();
	get_title_panel()->redraw();
};

//--- bb variants

void Heart::bb_title_refresh_redraw()
{
	TitlePanel *tp = get_bb_title_panel();
	if( tp ) {
		tp->refresh_layout();
		tp->redraw();
	}
};

void Heart::set_bb_title_font_size( int size )
{
	style_prefs->set_bb_title_font_size( size );
	bb_title_refresh_redraw();
};

void Heart::set_bb_title_border_size( int size )
{
	prefs->bb_title_border_size = size;
	bb_title_refresh_redraw();
};

void Heart::set_bb_title_font_color( Fl_Color clr )
{
	style_prefs->set_bb_title_font_color( clr );
	bb_title_refresh_redraw();
};

void Heart::set_bb_title_font( Fl_Font font )
{
	style_prefs->set_bb_title_font( font );
	bb_title_refresh_redraw();
};

void Heart::clicked_font_color_chooser()
{
	get_dash_panel()->font_panel->show_controls( !get_dash_panel()->font_panel->color_visible(),
														get_dash_panel()->font_panel->font_visible() );
	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
};

void Heart::clicked_font_chooser()
{
	get_dash_panel()->font_panel->show_controls( get_dash_panel()->font_panel->color_visible(),
														!get_dash_panel()->font_panel->font_visible() );
	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
};

void Heart::font_panel_edits_bb( bool state )
{
	get_dash_panel()->font_panel_edits_bb( state );
	get_dash_panel()->refresh_layout();
	get_dash_panel()->redraw();
};

// -- recorder panel --

void Heart::show_recorder( int how )
{
	if( how == 2 )
		how = ( false == get_dash_panel()->recorder_panel->visible() );

	if( how == 1 )
		get_dash_panel()->recorder_panel->show();
	else
		get_dash_panel()->recorder_panel->hide();

	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
};

// -- edit panel controls --

void Heart::clicked_edit_use()
{
	Subtitle *sub;

	LOG_GEN_INFO("Using new subtitle text( edited_sub = %i )...\n", edited_sub );

	if( edited_sub > -1 )
	{
		sub = get_subtitles()->get_subtitle( edited_sub );
		if( sub != NULL )
		{
			LOG_GEN_INFO("...for sub #%i: \"%s\"\n", edited_sub, get_edit_panel()->get_text() );

			SubTiming origTiming = sub->get_timing();

			Subtitles new_subs;
		 	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_PLAIN, SubtitleFormats::CandyParseFromPrefs( prefs->parse_candy_colors ), false );
			int err  = SubtitleFormats::Parse( get_edit_panel()->get_text(), new_subs, cfg );
			if( err ) {
				LOG_GEN_ERROR("Cannot parse edited subtitle: %s\n", Errr::Message( err ) );
			} else {
				int num_new_subs = new_subs.size();
				if( num_new_subs <= 0 )
				{
					//delete the edited subtitle
					get_subtitles()->erase( edited_sub );
					LOG_GEN_INFO("Empty text -> DELETING subtitle %i!!.\n", edited_sub );
				} else {
					if( origTiming.IsSet() ) {
						new_subs.best_fit_timing( origTiming.GetStart(), origTiming.GetEnd(), 0, -1 );
					}
					//split the subtitle being edited into several new ones
					LOG_GEN_INFO("Splitting subtitle %i into %i new subtitles!!!\n", edited_sub, new_subs.size() );
					get_subtitles()->erase( edited_sub );
					get_subtitles()->insert( edited_sub, &new_subs, 0, -1 );
				}
				recolor_subtitles( get_subtitles() );
				if( edited_sub == shown_sub )
				{
					LOG_GEN_INFO("Subtitle %i is currently displayed, redisplaying after edit.\n", edited_sub );
					show_sub( shown_sub );
				};
				get_list_panel()->refresh_subs();
			}
		}
		else
		{
			LOG_GEN_INFO(" BUT subtitle #%i is out of range!!\n", edited_sub );
		};
	}
	else
	{
			LOG_GEN_INFO(" BUT there is no subtite being edited!!\n");
	};

	get_edit_panel()->set_text( NULL );
	edited_sub = -1;
	get_dash_panel()->edit_mode( 0 );
}

void Heart::clicked_edit_cancel()
{
	get_edit_panel()->set_text( NULL );
	edited_sub = -1;
	get_dash_panel()->edit_mode( 0 );
}

void Heart::edit_subtitle( int n )
{
	Subtitle *sub;

	LOG_GEN_INFO("Editing subtitle %i...\n", n);

	sub = get_subtitles()->get_subtitle( n );
	if( sub != NULL )
	{
		std::string txt = sub->get_raw_text();
		LOG_GEN_INFO("text = \"%s\"\n", txt.c_str() );
		get_edit_panel()->set_text( txt.c_str() );
		edited_sub = n;
		get_dash_panel()->edit_mode( 1 );
	}
	else
	{
		LOG_GEN_INFO("BUT the subtitle doesn't exist!!\n" );
	};
}

// -- misc options --

void Heart::show_misc_options( int how )
{
	if( how == 2 )
		how = ( false == get_dash_panel()->options_panel->visible() );

	if( how == 1 )
		get_dash_panel()->options_panel->show();
	else
		get_dash_panel()->options_panel->hide();

	get_main_panel()->refresh_layout();
	get_main_panel()->redraw();
}

void Heart::set_ontop( bool state )
{
	if( state != prefs->on_top )
	{
		prefs->on_top = state;
		total_panel->refresh_layout();
		if( info_window != NULL )
			info_window->init_position();
	};
}

void Heart::set_subtitle_numbering( bool state )
{
	prefs->subtitle_numbers = state;
	get_list_panel()->refresh_layout();
}

void Heart::set_fullscreen( bool state, bool update_indicator )
{
	int X, Y, W, H;

	LOG_GEN_INFO("Heart: setting fullscreen = %i\n", state );

	if( state )
	{
		if( prefs->full_screen_mode == 0 )
			prefs->store_window_dims( MainWindow );

		MainWindow->fullscreen();
	}
	else
	{
		MainWindow->fullscreen_off();
		prefs->recall_window_dims( X, Y, W, H );
		if( !prefs->fix_window_dims( X, Y, W, H ) )
		{
			total_panel->resize( X, Y, W, H );
			MainWindow->resize( X, Y, W, H );
		};
	};

	prefs->full_screen_mode = state;
	get_options_panel()->set_fullscreen( state );

	total_panel->refresh_layout();

	if( info_window != NULL )
	{
		delete info_window;
		info_window = NULL;
	};
	if( about_window != NULL )
	{
		delete about_window;
		about_window = NULL;
	};
};

// -- search panel controls --

void	Heart::show_text( const char *text )
{
		LOG_GEN_INFO("setting text to %s\n", text );
		show_sub( SHOWN_SUB_HANDTYPED, text );
};

void Heart::find_sub( const char *text, bool fwd )
{
	int n;
	Subtitles *subs = get_subtitles();

	if( next_sub < 0 )
		return;

	if( 0 == strcmp( text, SubtitleDoc::MARK_STRING ) )
	{
		//search for a mark, not a text
		n = subs->find_marked_subtitle( next_sub, fwd );
	}
	else
	{
		n = subs->find_subtitle( text, next_sub, fwd );
	};

	if( n >= 0 )
	{
		list_to_sub( n );
		next_sub = n;
	};
};

void Heart::show_about_text()
{
	show_sub( SHOWN_SUB_HANDTYPED, about_text_short.c_str(), false );
};

// -- info panel controls --

void Heart::show_help( int how )
{
	const char label[] = N_("Keyboard Controls");
	const char help[] = N_("@->|Show next subtitle|Space|Show next subtitle or start playing timed subtitles| 0 ; C ;Backspace;@<-|Hide subtitle|@2<-|Browse subtitles up|@2->|Browse subtitles down|\
Enter; * |Mark/Unmark displayed subtitle|Left Mouse Button|Mark/Unmark subtitle|Right Mouse Button|Edit subtitle\
|Ctrl+Left Mouse Button|Display subtitle immediately");

	bool state;
	if( how == 2 ) {
		state = (info_window == NULL);
	} else {
		state = (how == 1);
	}
	
	if( !state ) {
		delete info_window;
		info_window = NULL;
		return;
	}

	Fl_Group::current( NULL );

	if( info_window == NULL )
	{
		//int hh = 8*get_list_panel()->h()/10;

		HelpPanel *hlp = new HelpPanel( _(label), _(help), this, 0, 0, 500, 300 );

		info_window = new PanelWindow(  this, hlp, get_list_panel(), PanelWindow::CENTER, 0.5, 0.5, &label[0] );

		MainWindow->add( info_window );
	};
	info_window->show();
};

void Heart::show_about( int how )
{
	bool state;
	if( how == 2 ) {
		state = ( about_window == NULL );
	} else {
		state = ( how == 1 );
	}

	if( !state ) {
		if( about_window != NULL ) {
			delete about_window;
			about_window = NULL;
		}
		return;
	}

	const char label[] = N_("About ClickShow");

	Fl_Group::current( NULL );

	if( about_window == NULL )
	{
		AboutPanel *ap = new AboutPanel( about_text_full.c_str(), this, 0,0,600,400 );
		about_window = new PanelWindow(  this, ap, get_list_panel(), PanelWindow::CENTER, 0.5, 0.5, label ); //_(&label[0]) );
		MainWindow->add( about_window );
	};
	about_window->show();
}

void Heart::switch_candy_render( bool state )
{
	render_candy_colors = state;
	get_title_panel()->refresh_style();
	get_list_panel()->refresh_style();
}

void Heart::switch_candy_parse( int state )
{
	get_prefs()->parse_candy_colors = state;
}

// == asking the user ==

int Heart::ask( const char *label, const char *question, const char *choices )
{
	AskWindow	*askw;
	int			retval;

	Fl_Group::current( NULL );

	int yoff = -50;
	if( !(bool)prefs->on_top )
		yoff = 50;

	askw = new AskWindow( this, get_dash_panel(), AskWindow::BOTTOMLEFT, 50, yoff, label, question, choices );
	MainWindow->add( askw );

	total_panel->deactivate();

	retval = askw->ask();

	delete askw;

	total_panel->activate();

	return retval;
}

// -- language functions --

void Heart::set_language( int l )
{
	if( l != get_translator()->language() )
		if( get_translator()->language( l ) )
		{
			get_prefs()->language_name = get_translator()->lang_name( l );
			restart();
		};
}

void Heart::restart()
{
	do_restart = true;
	if( bb_window ) { bb_window->hide(); };
	show_about( 0 );
	show_help( 0 );
	MainWindow->hide();
}

/*
 * call this after the old panels have been destroyed and before the new ones are created
 */
void Heart::after_restart()
{
	// forget about the old widgets
	get_style_engine()->clear_all_makeup();
};

/*
 * call this just before calling Fl::run() after a restart
 */
void Heart::before_restart()
{
	get_dash_panel()->options_panel->show();
	get_main_panel()->refresh_layout();
};

// === global keyboard shortcuts ===

bool Heart::sc_can_browse_subs() {
	return !auto_player || !auto_player->IsRunning();
}

bool Heart::sc_can_handclick_subs() {
	DashPanel *dash = get_dash_panel();
	return dash && dash->click_panel && dash->click_panel->visible();
}

bool Heart::sc_can_start_player() {
	DashPanel *dash = get_dash_panel();
	return dash && dash->player_panel && dash->player_panel->visible() && auto_player && auto_player->CanStart();
}

bool Heart::sc_can_pause_player() {
	return auto_player && auto_player->IsRunning();
};

/*
 * handles the global keyboard shortcuts
 *
 * returns:
 *		1 - if the event was used
 *		0 - event not used
 */
int Heart::handle_global_shortcuts( int event )
{
	if(( event == FL_SHORTCUT )||( event == FL_KEYDOWN ))
	{
		LOG_GUI_DETAIL2("1\n");
		if( Fl::event_key( FL_Right ) )
		{
			LOG_GUI_DETAIL2("2\n");
			if( sc_can_handclick_subs() ) {
				LOG_GUI_DETAIL2("3\n");
				clicked_show_next();
				return 1;
			}
		}
		else if( Fl::event_text()[0] == ' ')
		{
			if( sc_can_start_player()||sc_can_pause_player() ) {
				auto_player->StartOrPause();
				return 1;
			}
			if( sc_can_handclick_subs() ) {
				clicked_show_next();
				return 1;
			}
		}
		else if( Fl::event_key( FL_Up ) )
		{
			if( !sc_can_browse_subs() ) return 0;
			clicked_go( -1 );
			return 1;
		}
		else if( Fl::event_key( FL_Down ) )
		{
			if( !sc_can_browse_subs() ) return 0;
			if( shown_sub >= 0 )
			{
				clicked_clear();
			} 
			else
			{
				clicked_go( 1 );
			}
			return 1;
		}
		else if( Fl::event_key( FL_Page_Up ) )
		{
			if( !sc_can_browse_subs() ) return 0;
			clicked_go( -2 );
			return 1;
		}
		else if( Fl::event_key( FL_Page_Down ) )
		{
			if( !sc_can_browse_subs() ) return 0;
			clicked_go( 2 );
			return 1;
		}
		//else if( Fl::event_key( FL_KP+'0' ) )
		else if( ( Fl::event_text()[0] == '0' )||( Fl::event_text()[0] == 'c' )
					||( Fl::event_text()[0] == 'C' )||( Fl::event_key() == FL_BackSpace )
					|| Fl::event_key( FL_Left ) )
		{
			if( !sc_can_handclick_subs() ) return 0;
			clicked_clear();
			return 1;
		}
		//else if( Fl::event_key( FL_KP+'*' ) )
		//( Fl::event_text()[0] =='m' )||( Fl::event_text()[0] =='M' )
		else if( ( Fl::event_text()[0] =='*' )||Fl::event_key( FL_Enter )||Fl::event_key( FL_KP_Enter ) )						//*
		{
			clicked_mark();
			return 1;
		};
	};
	return 0;
}

//---- network

void Heart::show_incoming_sub( int subNum, const char *subText )
{
	if( subNum >= 0 ) {
		Subtitles *subs = get_subtitles();
		if(( subs == NULL )||( subs->size() < subNum )) {
			//the subtitle has a number but we either don't have any subtitles loaded
			//or the loaded subtitles are shorter than the ones this subtitle is coming from.
			subNum = SHOWN_SUB_HANDTYPED;	//So, let's show it as hand-typed text.
		} else {
			go_to_sub( subNum + 1 );	//this is one of our subtitles, let's move the list to it!
		}
	}
	show_sub( subNum, subText, false );
	next_space = prefs->with_spaces;
}
