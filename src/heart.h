/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Heart_h
#define _Heart_h

#include "clicky_config.h"
#include "style_prefs.h"
#include "subtitles/subtitles.h"
#include "prefs.h"
#include "subtitles/subtitle_doc.h"

#include "net_heart.h"
#include "screen_utils.h"
#include "cmd_args.h"

#ifdef TRANSLATION
#include "translator/translator.h"
#include "translator/translator_macros.h"
#include "translation.h"
#endif //TRANSLATION

class TitlePanel;
class	SubListPanel;
class	FilePanel;
class	NetPanel;
class	SubClickPanel;
class	DashPanel;
class MainPanel;
class TotalPanel;
class	ControlPanel;
class EditPanel;
class InfoPanel;
class OptionsPanel;
class BillboardPanel;
class AskWindow;
class PanelWindow;
class BBWindow;
class AutoPlayer;
class SubRecorder;
//class InfoTextPanel;

class Heart
{
private:
	Heart 				*heart; //points to "this" so that the translator can find me.
	NetHeart				*netHeart;

	StylePrefs			*style_prefs;
	SubtitleDoc			*subdoc;
	Prefs					*prefs;

	bool					render_candy_colors;

	//AskWindow			*info_window;
	PanelWindow			*info_window;
	PanelWindow			*about_window;

	BBWindow				*bb_window;

	TotalPanel		*total_panel;
	AutoPlayer		*auto_player;
	SubRecorder		*sub_recorder;

	Fl_Window	*MainWindow;

	int	shown_sub;	//number of the currently displayed subtitle
	int	last_shown_sub;	//number of the last subtitle displayed 

	RainbowMarkup	last_sub_markup;
	std::string		last_sub_text;

	static const int SHOWN_SUB_CLEAR = -2;
	static const int SHOWN_SUB_HANDTYPED = -1;

	int	next_sub;	//number of the top sub in the subtitle list
	bool	next_space;	//if the next thing to display is space (as opposed to a subtitle)

	int	edited_sub;	//number of the subtitle being edited or -1 if none

	std::string about_text_full;
	std::string	about_text_short;

	int	storedScreensaverValue;	//-1 - none, 0 - screensaver was off, 1 - screensaver was on

	typedef unsigned int ShortcutModeT;
	static const ShortcutModeT SHORTCUT_MODE_NONE = 0;
	static const ShortcutModeT SHORTCUT_MODE_HANDCLICK = 1;
	static const ShortcutModeT SHORTCUT_MODE_DEFAULT = SHORTCUT_MODE_HANDCLICK;

	ShortcutModeT	shortcut_mode;

public:
	static const int MAX_MERGE_SUBS = 4;	//maximum number of subtitle files to show at once

	bool					do_restart;

#ifdef TRANSLATION

	Translator *translator;

#endif //TRANSLATION

	Heart();
	~Heart();

	inline Prefs		*get_prefs() { return prefs; };
	StylePrefs			*get_style_prefs();
	StyleEngine			*get_style_engine();
	Subtitles			*get_subtitles();
	SubtitleDoc			*get_subdoc() { return subdoc; };
	void					set_subdoc( SubtitleDoc *newdoc );

	AutoPlayer			*get_auto_player() { return auto_player; };
	SubRecorder			*get_sub_recorder() { return sub_recorder; };

	TotalPanel			*get_total_panel();
	TitlePanel			*get_title_panel();
	SubListPanel		*get_list_panel();
	MainPanel			*get_main_panel();
	DashPanel			*get_dash_panel();
	OptionsPanel		*get_options_panel();
	EditPanel			*get_edit_panel();
	NetPanel				*get_net_panel();
	FilePanel         *get_file_panel();
	BillboardPanel		*get_bb_panel();

	NetHeart				*get_net_heart() { return netHeart; };

	Translator			*get_translator() { return translator; };

	//--- cmd args ---
	bool ProcessCmdArgs( int argc, const char **argv, int &exit_code );

	//--- misc ---

	/** clear subtitles
	 */
	void erase_subtitles(); //Player

	/** call this after loading new subtitles or changing their number
	 */
	void refresh_subtitles(); //Player

	/** Refreshes the controls part of the gui (the main panel)
	 */
	void refresh_dashboard( bool redraw = false );

	/** Disables or enables controls according to the current perms and type of subtitles
	 */
	void update_gui_mode( bool redraw = false );

	//-- set functions --

	void set_total_panel( TotalPanel *tp ) {	total_panel = tp; };

/*
	void set_dash_panel( DashPanel *dp ) { dash_panel = dp; };
	void set_title_panel( TitlePanel *tp ){ 	title_panel = tp; };
	void set_list_panel( SubListPanel *lp );
*/

	/** Disable or enable the screensaver
	 * FIXME: It only works on Windows.
	 * @returns true if ti worked, false otherwise.
	 */
	bool DisableScreensaver( bool state = true );

	/*
	 * returns the old main window
	 */
	Fl_Window *set_main_window( Fl_Window *main_win );

	void focus_default_widget();

	//-- detached title (billboard) panel --
	void show_billboard( bool state = true, bool set_indicator = true );
	static void show_bb_awake_handler( void *_heart );
	void _show_billboard( bool state );
	void bb_fullscreen( bool state = true, bool set_indicator = true );
	void bb_ontop( bool state = true, bool set_indicator = true );
	bool fix_billboard_prefs();
	bool fix_window_fullscreen();
	void store_billboard_prefs();

	//-- announcements --
	void totalpanel_geom_changed();
	void billboard_geom_changed();

	int get_next_sub() { return next_sub; };
	int get_shown_sub() { return shown_sub; };

	bool is_valid_sub_num( int n ) {
		if(( !subdoc )||( !subdoc->get_subtitles() )) return false;
		return ( n >= 0 )&&( n < subdoc->get_subtitles()->size() );
	};

	/** The public function for showing an arbitrary subtitle while also updating the list position.
	 *
	 * if subNum is >= 0, then the list is moved so that subNum + 1 is at the top.
	 *
	 * parameters:
	 *		- subNum - the number of teh subtitle to be shown
	 *		- send_to_net - should I broadcast this to the net?
	 *
	 * returns:
	 *		- true if n was a valid subtitle number, false otherwise
	 */
	bool show_list_sub( int subNum, bool send_to_net = true );
	bool hide_list_sub( bool send_to_net = true ) { return show_list_sub( SHOWN_SUB_CLEAR, send_to_net ); };

	/** The public function for setting the top sub on the list to an arbitrary value.
	 *
	 * parameters:
	 *		- n - the number of teh subtitle to be the new top on the list
	 *		- clear_shown - if clearing the title panel is requested
	 *
	 * returns:
	 *		- true if n was a valid subtitle number, false otherwise
	 */
	bool go_to_sub( int n, bool clear_shown = true ); //Player

	/** Jumps to a subtitle of the number identified by the string passed as a parameter.
	 *
	 * parameters:
	 *		- numstr - the number of the subtitle to jump to as a text string. E.g. "125"
	 *		- clear_shown - if clearing the title panel is requested
	 *
	 *	returns:
	 *		- true if the string represented a valid subtitle number.
	 */
	bool go_to_sub( const char *numstr, bool clear_shown = true ); //Player


	/** Tells the player and the recorder that this is the current subtitle now.
	 */
	void sub_to_player_recorder( int sub_num );

	//-- SubClickPanel controls --

	int clicked_show_next(); //Player
	/*
	 * dir: 1 means forward, -1 means backward
	 */
	int clicked_go( int dir=1 ); //Player
	void clicked_clear(); //Player
	void clicked_spaces( bool state ); //Player
	int clicked_mark(); //Player
	int mark_sub( int sub_i ); //Player
	void show_sub_click_panel( int mode = 1 ); //0 - hide, 1 - show, 2 - toggle

	//-- player mode stuff --

	void enable_manual_player( bool state = true ) {};
	void show_manual_player( bool state = true ) {};

	//-- FilePanel controls --

	int show_file_dialog(  std::vector<std::string> &fnames, bool save=false, int max_files=1 );

	void clicked_quit();
	void clicked_save();
	void clicked_load();
//	void load_subtitles( const char *file_name );
	void load_multi_subtitles( std::vector<std::string> &file_names );
	void load_subtitles_from_string( const std::string &subs, const std::string &fileName = "" );

	//-- billboard

	void			show_billboard_options( int how );
	bool			is_billboard_shown() { return ( bb_window != NULL ); };
	bool			is_billboard_shown_on_ctrl_screen();
	int			get_bb_height();
	TitlePanel	*get_bb_title_panel();
	int			bb_find_new_screen_if_necessary();

	//-- candy colors
	void color_subtitles(Subtitles *subs);
	void recolor_subtitles(Subtitles *subs);

	//-- candy panel controls --
	
	void show_candy_options( int how );
	void set_candy_color( int i, Fl_Color clr );
	void reset_candy_colors();
	void load_or_save_candy_colors( bool save );

	//-- font panel controls --

	/*
	 * parameters: how - 0..hide, 1..show, 2..toggle
	 */
	void show_text_options( int how );
	void show_misc_options( int how );
	void show_net_options( int how );
	void show_recorder( int how );

	void set_title_font_size( int size );
	void set_title_border_size( int size );
	void set_title_font_color( Fl_Color clr );
	void set_title_font( Fl_Font font );

	void set_bb_title_font_size( int size );
	void set_bb_title_border_size( int size );
	void set_bb_title_font_color( Fl_Color clr );
	void set_bb_title_font( Fl_Font font );

	void clicked_font_color_chooser();
	void clicked_font_chooser();

	void font_panel_edits_bb( bool state );

	//-- misc options --

	void set_ontop( bool state );
	void set_fullscreen( bool state, bool update_indicator = false );
	void set_subtitle_numbering( bool state );

	// -- search panel controls --

	void show_text( const char *text ); //Player
	void find_sub( const char *text, bool fwd ); //Player???

	// -- edit panel controls --

	void clicked_edit_use();
	void clicked_edit_cancel();

	void edit_subtitle( int n );
	
	// -- title panel controls --
	
	void clicked_edit_shown();
	void clicked_mark_shown();

	// -- info panel controls --
	
	void show_help( int how = 2 );
	void show_about( int how = 2 );
	void switch_candy_render( bool state );
	void switch_candy_parse( int state );
	bool get_candy_render() { return render_candy_colors; };

	// -- misc --

	void show_about_text();
	void set_load_path( const char *path );

	// -- ask the user --

	int ask( const char *label, const char *question, const char *choices );

	// -- language functions --

	void set_language( int l );

	// -- network
	
	void show_incoming_sub( int subNum, const char *subText );

	/*
	 * tells clicky to restart
	 */
	void restart();

	/*
	 * call this after the old panels have been destroyed and before the new ones are created
	 */
	void after_restart();

	/*
	 * call this just before calling Fl::run() after a restart
	 */
	void before_restart();

	// -- global keyboard shortcuts --

	bool sc_can_browse_subs();
	bool sc_can_handclick_subs();
	bool sc_can_start_player();
	bool sc_can_pause_player();
	void set_global_shortcut_mode( ShortcutModeT mode = SHORTCUT_MODE_DEFAULT ) { shortcut_mode = mode; };
	ShortcutModeT get_global_shortcut_mode() { return shortcut_mode; };

	/*
	 * handles the global keyboard shortcuts
	 *
	 * returns:
	 *		1 - if the event was used
	 *		0 - event not used
	 */
	int handle_global_shortcuts( int event );
	
private:
	bool show_sub( int n, const char *default_text = "", bool sendToNet = true  );
	void list_to_sub( int n );

	void parse_file_name( std::string &fname, size_t &barename_start, size_t &ext_start );
	void generate_savename( std::string &fname, const char *origname );

	void bb_title_panel_set_text( const char *text = NULL, const RainbowMarkup *markup = NULL );
	void bb_title_panel_revive_text();
	void bb_title_refresh_redraw();
	int bb_screen_number() { return bb_window ? ScreenUtils::screen_num( (Fl_Window *)bb_window ) : prefs->bb_screen_num; };

};

#endif //_Heart_h
