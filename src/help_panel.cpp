/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "help_panel.h"

#include "clicky_debug.h"

#include <cstring>
#include <string>

// =================== class HelpPanel =========================

	HelpPanel::	HelpPanel( const char *label_text, const char *help_text, Heart *hrt, int x, int y, int w, int h)
	: ControlPanel ( hrt, x, y, w, h, (const char *)"HELP" )
	{
		heart = hrt;;

		layout = NULL;

		button_ok = new Fl_Button( 0, 0, 0, 0, _("Ok") );
		button_label = new Fl_Button( 0, 0, 0, 0, NULL );
		button_label->copy_label( label_text );

		key_buttons.clear();
		desc_buttons.clear();
		keys_per_desc.clear();
		text_to_buttons( help_text );

		end();


		//callbacks
		button_ok->callback((Fl_Callback*)cb_ok);

		//-- layout --

		create_layout();

		//-- style --

		//I can't assign styles because the whole panel and the widgets can be deleted and created anew
		heart->get_style_prefs()->HelpOkButton->apply( button_ok );
		heart->get_style_prefs()->HelpLabelButton->apply( button_label );
		heart->get_style_prefs()->HelpPanel->apply( this );

		refresh_layout();
		refresh_style();
	};

    HelpPanel::~HelpPanel()
    {
        delete layout;
    };

	bool HelpPanel::get_size( int &w, int &h )
	{
		w = width;
		h = height;

		return true;
	};

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void HelpPanel::refresh_layout()
	{
		layout->refresh_layout( this );
	};

	void HelpPanel::cb_ok(Fl_Button* b, void*)
	{
		//find what window this panel is in and hide it.
		HelpPanel *p = (HelpPanel *)b->parent();
		Fl_Window *win = p->window();
		if( win != NULL )
			win->hide();
	};
/*
 * the format of the text is:
 * "key_symbol_1a;key_symbol_1b;key_symbol_1c...|key description 1|key_symbol_2|key description 2|..."
 * NOT YET:You can use "||" to add vertical space between entries
 */
void HelpPanel::text_to_buttons( const char *text )
{
	int	old_key_cnt, old_desc_cnt;
	int	new_key_cnt, new_desc_cnt;
	int	key_i, desc_i;

	const char *s, *ks, *ke;
	const char *key_s;
	const char *desc_s;

	int	key_len, key_cnt;
	int	desc_len;

	std::string	key_string;
	std::string	onekey_string;
	std::string	desc_string;

	LOG_RENDER_DETAIL2("Converting text to buttons.\n");

	old_key_cnt = key_buttons.size();
	old_desc_cnt = desc_buttons.size();

//	if( text == NULL )
//		return;

	//count buttons in the new description
	new_desc_cnt = 0;
	new_key_cnt = 0;

	s = text;
	desc_i = 0;
	key_i = 0;

	while( s != NULL )
	{
		key_s = s;
		//find the start of the description string
		s = strchr( key_s, (int)'|' );
		if( s != NULL )
		{
			key_len = s - key_s;
			key_string.assign( key_s, key_len );
			desc_s = s+1;

			//find the end of the description string
			s = strchr( desc_s, (int)'|' );
			if( s!=NULL )
			{
				desc_len = s - desc_s;
				desc_string.assign( desc_s, desc_len );
				s++;
			}
			else
				desc_string.assign( desc_s );

			//get a button for this description
			if( desc_i >= old_desc_cnt )
			{
				desc_buttons.resize( desc_i+1 );
				desc_buttons[ desc_i ] = new Fl_Button(0,0,0,0,NULL );

				keys_per_desc.resize( desc_i+1 );
			};
			desc_buttons[ desc_i ]->copy_label( desc_string.c_str() );

			LOG_RENDER_DETAIL2("desc = \"%s\", keys = \"%s\"\n", desc_string.c_str(), key_string.c_str() );

			//parse the key string
			key_cnt = 0;
			ks = key_string.c_str();
			ke = strchr( ks, (int)';' );

			while( ks != NULL )
			{
				if( ke != NULL )
					onekey_string.assign( ks, (int)(ke-ks) );
				else
					onekey_string.assign( ks );

				//get a button for this key
				if( key_i >= old_key_cnt )
				{
					key_buttons.resize( key_i+1 );
					key_buttons[ key_i ] = new Fl_Button(0,0,0,0,NULL );
				};
				key_buttons[ key_i ]->copy_label( onekey_string.c_str() );

				LOG_RENDER_DETAIL2("\tkey = \"%s\"\n", onekey_string.c_str() );

				key_i++;
				key_cnt++;

				if( ke == NULL )
					ks = NULL;
				else
				{
					ks = ke + 1;
					ke = strchr( ks, (int)';' );
				};
			};

			keys_per_desc[ desc_i ] = key_cnt;

			LOG_RENDER_DETAIL2("\tkey count = %i\n", key_cnt );
		}
		else
		{
			LOG_RENDER_DETAIL2("no description string given for the last key!\n");
			s = NULL;
		};
		desc_i++;
	};

	LOG_RENDER_DETAIL2("%i desc buttons and %i key buttons created.\n", desc_buttons.size(), key_buttons.size() );
	
};

void HelpPanel::measure_key_button( Fl_Button *b, int &W, int &H )
{
	int ww, hh;
	
	ww = 0;
	hh = 0;
	b->measure_label( ww, hh );
	W = ww + kb_margin_w;
	H = hh + kb_margin_h;
};

void HelpPanel::measure_desc_button( Fl_Button *b, int &W, int &H )
{
	int ww, hh;
	
	ww = 0;
	hh = 0;
	b->measure_label( ww, hh );
	W = ww;
	H = hh + kb_margin_h;
};

/*
 */
void HelpPanel::create_layout()
{
	int total_w, total_h;
	int bw, bh;
	int kw, kh;
	int dw, dh;

	int max_key_w;
	int max_key_h;
	int max_desc_w;
	int max_desc_h;

	int key_cnt, key_i;

	int kx, dx, by;

	int xxx, yyy;

	int row_h;

	ButtonStyle *keystyle;
	ButtonStyle *descstyle;

	LOG_RENDER_DETAIL2("Creating layout...\n");

	keystyle = heart->get_style_prefs()->HelpKeyButton;
	descstyle = heart->get_style_prefs()->HelpDescButton;

	//find the widest and tallest buttons
	max_key_w = 0;
	max_desc_w = 0;
	max_key_h = 0;
	max_desc_h = 0;

	LOG_RENDER_DETAIL2("   finding the largest button dimesnions...\n");

	key_i = 0;

	row_heights.resize( desc_buttons.size() );

	for( int i = 0; i < desc_buttons.size(); i++ )
	{
		//apply the style first, so that the label is measured with the right font
		descstyle->apply( desc_buttons[i] );

		key_cnt = keys_per_desc[i];

		kw = 0;

		for( int j = 0; j < key_cnt; j++ )
		{
			keystyle->apply( key_buttons[ key_i ] );

			int KW = 0;
			kh = 0;
			measure_key_button( key_buttons[ key_i ], KW, kh );
			max_key_h = std::max( kh, max_key_h );
			
			kw += KW;
			
			key_i ++;
		};

		kw += space_between_keys * (key_cnt - 1);

		max_key_w = std::max( kw, max_key_w );

		dw = 0;
		dh = 0;
		measure_desc_button( desc_buttons[i], dw, dh );
		max_desc_w = std::max( dw, max_desc_w );
		max_desc_h = std::max( dh, max_desc_h );

		row_heights[i] = std::max( dh, kh );

		LOG_RENDER_DETAIL2("   current kw=%i, kh=%i, dw=%i, dh=%i, row height=%i\n", kw, kh, dw, dh, row_heights[i] );
		LOG_RENDER_DETAIL2("   maximum kw=%i, kh=%i, dw=%i, dh=%i\n", max_key_w, max_key_h, max_desc_w, max_desc_h );
	};

	//create a layout
	if( layout != NULL )
		delete layout;

	layout = new SimpLE();

	//calculate total panel width
	width = max_key_w + max_desc_w + column_space_w + 2*kb_offset_x;

	//the label button
	bw = 0;
	bh = 0;
	measure_key_button( button_label, bw, bh );

	layout->add( button_label,
					(width - bw)/2, space_above_label,		//pixx, pixy
							0, 0,		//relx, rely
							0, 0,		//relw, relh
							0, 0,	//marginw, marginh
							bw, bh );	//minpixw, minpixh

	//layout all buttons row by row
	total_w = 0;
	total_h = 0;

	by = space_above_label + space_below_label + bh + kb_offset_y;

	dx = kb_offset_x + max_key_w + column_space_w;

	key_i = 0;

	for( int i = 0; i < desc_buttons.size(); i++ )
	{
		row_h = row_heights[i];

		kx = kb_offset_x + max_key_w;

		//layout key buttons from right to left
		for( int j = 0; j < keys_per_desc[i]; j++ )
		{
			kw = 0;
			kh = 0;
			measure_key_button( key_buttons[ key_i ], kw, kh );

			xxx = kx - kw;

			LOG_RENDER_DETAIL2("   adding key  x=%i, y=%i, w=%i, h=%i\n", xxx, by, kw, kh );

			layout->add( key_buttons[ key_i ],
							xxx, by,		//pixx, pixy
							0, 0,		//relx, rely
							0, 0,		//relw, relh
							0, 0,	//marginw, marginh
							kw, kh );	//minpixw, minpixh

			kx = xxx - space_between_keys;

			key_i++;
		};

		dw = 0;
		dh = 0;
		measure_desc_button( desc_buttons[i], dw, dh );

		LOG_RENDER_DETAIL2("   adding desc x=%i, y=%i, w=%i, h=%i\n", dx, by, dw, dh );

		layout->add( desc_buttons[i],
							dx, by,		//pixx, pixy
							0, 0,		//relx, rely
							0, 0,		//relw, relh
							0, 0,	//marginw, marginh
							dw, dh );	//minpixw, minpixh

		yyy = row_h + row_space_h;

		LOG_RENDER_DETAIL2("   moving down by %i\n", yyy );

		by += yyy;
	};

	//the ok button

	layout->add( button_ok,
							( width - ok_button_width ) /2, -1*( ok_button_height + space_below_ok ),		//pixx, pixy
							0, 1,		//relx, rely
							0, 0,		//relw, relh
							0, 0,	//marginw, marginh
							ok_button_width, ok_button_height );	//minpixw, minpixh


	//calculate total panel height
	height = by + space_below_keys + ok_button_height + space_below_ok;
};
