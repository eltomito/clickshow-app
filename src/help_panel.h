/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Help_Panel_h
#define _Help_Panel_h

//#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Text_Buffer.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "heart.h"

/*
 * the info panel class
 */
class HelpPanel : public ControlPanel
{
	SimpLE	*layout;

	std::vector<Fl_Button *>key_buttons;
	std::vector<Fl_Button *>desc_buttons;
	std::vector<int>keys_per_desc;
	std::vector<int>row_heights;

	Fl_Button *button_ok;
	Fl_Button *button_label;

	static	void cb_ok(Fl_Button* b, void*);

public:
	HelpPanel( const char *label_text, const char *help_text, Heart *hrt, int x, int y, int w, int h);
	~HelpPanel();

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void refresh_layout();
	bool get_size( int &w, int &h );

private:
	void text_to_buttons( const char *text );
	void create_layout();

	void measure_key_button( Fl_Button *b, int &W, int &H );
	void measure_desc_button( Fl_Button *b, int &W, int &H );

		static const int space_above_label = 10;
		static const int space_below_label = 10;

		static const int space_between_keys = 10;

		static const int kb_margin_w = 10;
		static const int kb_margin_h = 10;

		static const int kb_offset_x = 20;
		static const int kb_offset_y = 20;

		static const int column_space_w = 25;
		static const int row_space_h = 10;
		
		static const int space_below_keys = 10;
		static const int space_below_ok = 10;

		static const int ok_button_height = 40;
		static const int ok_button_width = 80;

		int	height;
		int	width;
};

#endif //_Help_Panel_h
