/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "info_panel.h"

#include "clicky_debug.h"

// =================== class InfoPanel =========================

	InfoPanel::InfoPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, "INFO" )
	{
		heart = hrt;;

		layout = new SimpLE();

		button_help = new ClickyButton( 0, 0, 0, 0, _("Controls") );
		button_about = new ClickyButton( 0, 0, 0, 0, _("About") );
		button_candy = new ClickyButton(0,0,80,34,_("  *@@+-") );

		end();

		//tooltips
		button_help->tooltip( _("Show keyboard shortcuts.") );
		button_about->tooltip( _("Show information about this program.") );
		button_candy->tooltip( _("Show settings for colored subtitles.") );

		//callbacks
		button_help->callback((Fl_Callback*)cb_help);
		button_about->callback((Fl_Callback*)cb_about);
		button_candy->callback((Fl_Callback*)cb_candy);

		//-- layout --
		Fl_Widget* updown[] = { button_help, button_about, button_candy, NULL };
		layout->add_column( updown,
									2, 2,			//pixx, pixy
									0.0, 0.0,	//relx, rely
									1.0, 1.0,	//relw, relh
									20, 4,			//marginw, marginh - The 20 is a KLUDGE to cover up a bug in - I guess - Fl_Scroll.
									0, 5 );		//spacex, spacey

		//-- style --
		heart->get_style_engine()->assign_style( StylePrefs::FILE_BUTTON, &updown[0] );
		heart->get_style_engine()->assign_style( StylePrefs::FILE_CBUTTON, button_candy );

		refresh_layout();
		refresh_style();
	};

    InfoPanel::~InfoPanel()
    {
        delete layout;
    };

	bool InfoPanel::get_size( int &w, int &h )
	{
		int	fh;

		fh = heart->get_style_prefs()->NormalButton->ls.Size;
		h = fh*6;
		w = (3*h)/2;
		return true;
	};

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void InfoPanel::refresh_layout()
	{
		layout->refresh_layout( this );
	};

	void InfoPanel::cb_help(Fl_Button* b, void*)
	{
		InfoPanel *p = (InfoPanel *)b->parent();
		p->heart->show_help();
		//p->heart->redraw_dash();
	};

	void InfoPanel::cb_candy(Fl_Button* b, void*)
	{
		InfoPanel *p = (InfoPanel *)b->parent();
		p->heart->show_candy_options( 2 );
	};

	void InfoPanel::cb_about(Fl_Button* b, void*)
	{
		InfoPanel *p = (InfoPanel *)b->parent();
		p->heart->show_about();
	};
