/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Info_Panel_h
#define _Info_Panel_h

//#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>

#include "clicky_button.h"

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "heart.h"
#include "ask_dialog.h"

/*
 * the info panel class
 */
class InfoPanel : public ControlPanel
{
	SimpLE	*layout;

	ClickyButton	*button_help;
	Fl_Button		*button_about;
	ClickyButton	*button_candy;

	static	void cb_help(Fl_Button* b, void*);
	static	void cb_about(Fl_Button* b, void*);
	static	void cb_candy(Fl_Button* b, void*);

public:
	InfoPanel(Heart *hrt, int x, int y, int w, int h);
	~InfoPanel();

	//-- these functions should be generalized and probably go in the ControlPanel class --

	void refresh_layout();
	bool get_size( int &w, int &h );
};

#endif //_Info_Panel_h
