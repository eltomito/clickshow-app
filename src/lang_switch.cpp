/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "lang_switch.h"

#include "translator/translator.h"
#include "style_prefs.h"

LangSwitch::LangSwitch( Heart *hrt, int x, int y, int w, int h )
			: ControlPanel( hrt, x, y, w, h, "LANGSWITCH" )
{
	create_lang_buttons();
	refresh_layout();
}
				
int LangSwitch::create_lang_buttons()
{
	int ww, hh;

	Translator *t = heart->get_translator();
	int numlangs = t->lang_count();
	ButtonStyle *bs = heart->get_style_prefs()->LangButton;

	lang_buttons.resize( numlangs );
	
	for( int i=0; i < numlangs; i++ )
	{
		lang_buttons[i] = new lang_button_T( 0,0,0,0,t->lang_name(i) );
		bs->apply( lang_buttons[i] );
		lang_buttons[i]->value( 0 );
		
		lang_buttons[i]->callback((Fl_Callback*)cb_lang);
		lang_buttons[i]->argument( (long)i );

		ww = 0;
		hh = 0;
		measure_lang_button( lang_buttons[i], ww, hh );
		lang_buttons[i]->size( ww, hh );
		add( lang_buttons[i] );
	};
	
	//select the current language
	lang_buttons[ t->language() ]->value( 1 );
	
	return lang_buttons.size();
}

void LangSwitch::measure_lang_button( lang_button_T *b, int &W, int &H )
{
	b->measure_label( W, H );
	W += check_width + 2*label_border_w;
	H += 2* label_border_h;
}

bool LangSwitch::get_size( int &W, int &H )
{
	int total_h = 0;
	int max_w = 0;

	for( int i=0; i < lang_buttons.size(); i++ )
	{
		total_h += lang_buttons[i]->h();
		max_w = std::max( max_w, lang_buttons[i]->w() );
	}; 

	W = max_w + 2*border_w;
	H = total_h + 2*border_h + ( std::max( (int)(lang_buttons.size() - 1), 0 ) *  button_space );

	return true;
}

void LangSwitch::refresh_layout()
{
	int pixx = border_w + x();
	int pixy = border_h + y();

	for( int i=0; i < lang_buttons.size(); i++ )
	{
		lang_buttons[i]->position( pixx, pixy );
		pixy += button_space + lang_buttons[i]->h();
	};
}

void LangSwitch::activate_button( lang_button_T* b )
{
	for( int i=0; i < lang_buttons.size(); i++ )
	{
		if( lang_buttons[i] == b )
			lang_buttons[i]->value( 0 );
		else
			lang_buttons[i]->value( 1 );
	};
}

void LangSwitch::cb_lang( lang_button_T* b, void*)
{
	LangSwitch *s = (LangSwitch *)b->parent();
	s->heart->set_language( (int)b->argument() );

	//s->refresh_layout();
	//s->activate_button( b );
}
