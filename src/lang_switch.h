/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _LangSwitch_h
#define _LangSwitch_h

#include "control_panel.h"
#include "heart.h"

#include <FL/Fl_Radio_Light_Button.H>

class LangSwitch : public ControlPanel
{
private:

	typedef Fl_Radio_Light_Button lang_button_T;

	std::vector<lang_button_T *> lang_buttons;
	
	static const int check_width = 10;
	static const int label_border_w = 0;
	static const int label_border_h = 0;

	static const int button_space = 4;

	static const int border_w = 4;
	static const int border_h = 4;

public:
	LangSwitch( Heart *hrt, int x, int y, int w, int h );
	~LangSwitch() {};
	
	void refresh_layout();
	bool get_size( int &W, int &H );

private:
	int create_lang_buttons();
	void measure_lang_button( lang_button_T *b, int &W, int &H );
	void activate_button( lang_button_T* b );

	static void cb_lang( lang_button_T* b, void*);
};

#endif //_LangSwitch_h
