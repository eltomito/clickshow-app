/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Text_Display.H>

#include "layout_engine.h"

//======================= class LayoutEngine ================================

	/*
	 * constructor
	 */
	LayoutEngine::LayoutEngine()
	{
		Geoms.clear();
	};

	/*
	 * destructor
	 */
	LayoutEngine::~LayoutEngine()
	{
		remove_all();
	};

	/*
	 * add a widget with a geometry
	 */
	int LayoutEngine::add( Fl_Widget *w, LayoutGeometry *g, int clonegeom )
	{
		LayoutGeometry *lg;

		if( clonegeom )
			lg = g->clone();
		else
			lg = g;
		Geoms.insert( T_WG_Pair( w, lg ) );
		return( true );
	};

	/*
	 * remove a widget
	 */
	int LayoutEngine::remove( Fl_Widget *w )
	{
		T_WG_Map::iterator it = Geoms.find( w );
		if( it != Geoms.end() )
		{
			delete it->second;
			Geoms.erase( it );
			return true;
		}
		else
			return false;
	};

	/*
	 * remove all widgets
	 */
	void LayoutEngine::remove_all()
	{
		T_WG_Map::iterator it;

		for (it=Geoms.begin(); it!=Geoms.end(); ++it)
			delete it->second;
		Geoms.clear();
	};

	/*
	 * find the geometry of a widget
	 */
	LayoutGeometry *LayoutEngine::find( Fl_Widget *w )
	{
		T_WG_Map::iterator it;

		it = Geoms.find( w );
		if( it == Geoms.end() )
			return( NULL );
		else
			return( it->second );
	};

	/*
	 * find the iterator of a widget
	 */
	LayoutEngine::T_WG_Map::iterator LayoutEngine::find_it( Fl_Widget *w )
	{
		return Geoms.find( w );
	};

	//-- refreshing! --
	int LayoutEngine::refresh_layout( int px, int py, int pw, int ph )
	{
		T_WG_Map::iterator it;
		int x, y, w, h;

		for (it=Geoms.begin(); it!=Geoms.end(); ++it)
		{
			it->second->plot( pw, ph, x, y, w, h );
			it->first->resize( px+x, py+y, w, h );
		};
		return 1;
	};

	int LayoutEngine::refresh_layout( Fl_Widget *parent )
	{
		return refresh_layout( parent->x(), parent->y(), parent->w(), parent->h() );
	};

