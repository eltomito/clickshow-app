/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Layout_Engine_h
#define _Layout_Engine_h

#include <map>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>

#include "layout_geometry.h"

class LayoutEngine
{
public:
	typedef std::map<Fl_Widget*, LayoutGeometry*> T_WG_Map;
	typedef std::pair<Fl_Widget*, LayoutGeometry*> T_WG_Pair;

	T_WG_Map	Geoms;

	/*
	 * constructor
	 */
	LayoutEngine();

	/*
	 * destructor
	 */
	~LayoutEngine();

	/*
	 * add a widget with a geometry
	 */
	int add( Fl_Widget *w, LayoutGeometry *g, int clonegeom=false );

	/*
	 * remove a widget
	 */
	int remove( Fl_Widget *w );

	/*
	 * remove all widgets
	 */
	void remove_all();

	/*
	 * find the geometry of a widget
	 */
	LayoutGeometry *find( Fl_Widget *w );

	/*
	 * find the iterator of a widget
	 */
	T_WG_Map::iterator find_it( Fl_Widget *w );

	//-- refreshing! --
	virtual int refresh_layout( int px, int py, int pw, int ph );

	virtual int refresh_layout( Fl_Widget *parent );
};

#endif //_Layout_Engine_h
