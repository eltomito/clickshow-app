/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Markup_Edit_h
#define _Markup_Edit_h

#include <string>
#include <vector>
#include <algorithm>
//#include <memory>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>

#include "../rainbow.h"

class ComposedMEdit;

class MEdit {
public:
	enum Kind {
		UNKNOWN = 0,
		SIMPLE,
		COMPOSED
	};

protected:
	MEdit( Kind _kind = UNKNOWN ) : kind(_kind) {};
	~MEdit() {};
	Kind kind;

public:
	Kind type() const { return kind; };
	virtual void apply( std::string &str ) = 0;
	virtual void apply( RainbowMarkup &rm ) = 0;
	virtual void unapply( RainbowMarkup &rm ) = 0;
	virtual void applyToMe( int at, int insCnt ) = 0;
	virtual void apply( MEdit &ed ) = 0;
};

typedef boost::shared_ptr<MEdit> MEditPtr;
typedef std::vector<MEditPtr> MEdits;

class ComposedMEdit : public MEdit{
public:
	ComposedMEdit() : MEdit( MEdit::COMPOSED ) {};
	ComposedMEdit( MEditPtr outer, MEditPtr inner ) : MEdit( MEdit::COMPOSED ) {
		addOuter( inner );
		addOuter( outer );
	};
	~ComposedMEdit() {};
/*
	void addInner( MEditPtr ed ) {
		if( ed->type() == MEdit::COMPOSED ) {
			addInners( ((ComposedMEdit*)ed.get())->edits );
		} else {
			edits.insert( edits.begin(), ed );
		}
	};
*/
	void addOuter( MEditPtr ed ) {
		if( ed->type() == MEdit::COMPOSED ) {
			addOuters( ((ComposedMEdit*)ed.get())->edits );
		} else {
			apply( *ed );
			edits.push_back( ed );
		}
	};

	void applyOnMe( MEditPtr ed ) { addOuter( ed ); };

/*
	void addInners( MEdits &e ) {
		std::for_each( e.rbegin(), e.rend(), std::bind1st( std::mem_fun(&ComposedMEdit::addInner), this ) );
	};
*/
	void addOuters( MEdits &e ) {
		std::for_each( e.begin(), e.end(), std::bind1st( std::mem_fun(&ComposedMEdit::addOuter), this ) );
	};

	void apply( std::string &str ) {
		int l = edits.size();
		for( int i = 0; i < l; ++i ) {
			edits[ i ]->apply( str );
		}
	};

	void apply( RainbowMarkup &rm ) {
		int l = edits.size();
		for( int i = 0; i < l; ++i ) {
			edits[ i ]->apply( rm );
		}
	};

	void unapply( RainbowMarkup &rm ) {
		for( int i = edits.size() - 1; i >= 0; --i ) {
			edits[ i ]->unapply( rm );
		}
	};

	void applyToMe( int pos, int insCnt ) {
		int l = edits.size();
		for( int i = 0; i < l; ++i ) {
			edits[ i ]->applyToMe( pos, insCnt );
		}
	};

	void apply( MEdit &ed ) {
		int l = edits.size();
		for( int i = 0; i < l; ++i ) {
			edits[ i ]->apply( ed );
		}
	};

	void clear() { edits.clear(); };

protected:
	MEdits edits;
};

typedef boost::shared_ptr<MEdit> ComposedMEditPtr;

class SimpleMEdit : public MEdit {
public:
	SimpleMEdit( int _pos, int _eraseLen, std::string &_replaceWith )
		: MEdit( SIMPLE ), pos(_pos), eraseLen(_eraseLen), replaceWith(_replaceWith) {};

	SimpleMEdit( int _pos, int _eraseLen, const char *_replaceWith )
		: MEdit( SIMPLE ), pos(_pos), eraseLen(_eraseLen), replaceWith(_replaceWith) {};

	SimpleMEdit( int _pos, int _eraseLen )
		: MEdit( SIMPLE ), pos(_pos), eraseLen(_eraseLen), replaceWith("") {};

	SimpleMEdit() {};

	void apply( std::string &str ) { str.replace( pos, eraseLen, replaceWith ); };

	void apply( RainbowMarkup &rm ) {
		int remLen = eraseLen - replaceWith.size();
		int s;
		RainbowMarkup::iterator it = rm.begin();
		RainbowMarkup::iterator endit = rm.end();
		while( it != endit ) {
			s = (*it).start();
			if( s >= ( pos + eraseLen ) ) {
				(*it).start( s - remLen );
			}
			++it;
		}
	};

	void unapply( RainbowMarkup &rm ) {
		int addLen = eraseLen - replaceWith.size();
		int s;
		RainbowMarkup::iterator it = rm.begin();
		RainbowMarkup::iterator endit = rm.end();
		while( it != endit ) {
			s = (*it).start();
			if( s >= (pos + addLen) ) {
				(*it).start( s + addLen );
			}
			++it;
		}
	};

	int extraLen() { return replaceWith.size() - eraseLen; };

	void applyToMe( int at, int insCnt ) {
		//TODO: adapt to handle overlapping edits
		if( at <= pos ) {
			pos += insCnt;
			return;
		}
	};

	void apply( MEdit &ed ) {
		ed.applyToMe( pos, extraLen() );
	};

private:
	int pos;
	int eraseLen;
	std::string replaceWith;
};

#endif //_Markup_Edit_h
