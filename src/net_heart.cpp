/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "net_heart.h"
#include "heart.h"
#include "net_panel.h"
#include "file_panel.h"
#include "panels.h"
#include "shared/src/net/net_types.h"
#include "player/auto_player.h"

#include <cstdlib>
#include <ctime>

#define LOCK_FL	LOG_LOCKME("FL LOCK ON\n"); Fl::lock();
#define UNLOCK_FL	LOG_LOCKME("FL LOCK OFF\n"); Fl::unlock();

//=============== class PanicSub =====================

void PanicSub::Set( int num, const std::string &text )
{
	if( num >= 0 ) {
		subNum = num;
		subText = text;
		visible = true;
	} else {
		visible = false;
	};
}

// ================= class ClickyDrill ==================

void ClickyDrill::holeOpen()
{
	LOG_HOLE_INFO("The HOLE is OPEN!\n");
	std::string srv;
	StrUtils::Printf( srv, "localhost:%u", GetUdpPort() );
	LOG_HOLE_INFO("Connecting through this tunnel: \"%s\"...\n", srv.c_str() );
	netHeart.ConnectDirect( srv.c_str(), directUser.c_str(), directPwd.c_str(), directId.c_str() );
}

void ClickyDrill::holeClosed()
{
	LOG_HOLE_INFO("The HOLE is CLOSED!\n");
	if( signalDisconnect ) {
		netHeart.SetConnectionState( DISCONNECTED );
	}
}

// ================ class ReconnectTimer =========================

void ReconnectTimer::Enable( bool _state )
{
	LOCK_ME;
	autoReconnect = _state;
}

bool ReconnectTimer::Start()
{
	LOCK_ME;
	if( !autoReconnect ) { return false; }
	stop();
	LOG_GUI_INFO("Starting reconnect timeout = %lf seconds.\n", (double)milliSeconds / 1000.0 );
	Fl::add_timeout( (double)milliSeconds / 1000.0, &ReconnectTimer::staticTimeoutHandler, (void*)this );
	return true;
}

void ReconnectTimer::Stop()
{
	LOCK_ME;
	stop();
}

void ReconnectTimer::SetLoginInfo( const char *_server, const char *_user, const char *_pwd, const char *_id )
{
	server = _server;
	user = _user;
	pwd = _pwd;
	id = _id;
	LOG_GUI_DETAIL("Storing login info - server: \"%s\", user: \"%s\", pwd: \"%s\", id: \"%s\"\n",
		server.c_str(),
		user.c_str(),
		pwd.c_str(),
		id.c_str() );
}

void ReconnectTimer::stop()
{
	Fl::remove_timeout( &ReconnectTimer::staticTimeoutHandler, (void*)this );
}

void ReconnectTimer::staticTimeoutHandler( void *_reconnectTimer )
{
	((ReconnectTimer*)_reconnectTimer)->timeoutHandler();
}

void ReconnectTimer::timeoutHandler()
{
	LOCK_ME;
	stop();
	LOG_NET_INFO("Reconnecting to stored login info - server: \"%s\", user: \"%s\", pwd: \"%s\", id: \"%s\"\n",
		server.c_str(),
		user.c_str(),
		pwd.c_str(),
		id.c_str() );
	netHeart.Connect( server.c_str(), user.c_str(), pwd.c_str(), id.c_str() );
}

//============== class SubScheduler ===============

SubScheduler::~SubScheduler()
{
	LOCK_ME;
	subs.Clear();
	Fl::remove_timeout( &SubScheduler::timerHandler, (void*)this );
}

void SubScheduler::ScheduleSub( int num, const std::string &text, const ClickyTime &when )
{
	LOCK_ME;
	TSQEntry	sub( num, text, when );
	subs.Push( sub );
	if( !isTimerRunning ) { startTimer(); }
}

void SubScheduler::startTimer()
{
	Fl::add_timeout( SUB_SCHEDULER_FLOAT_TICK, &SubScheduler::timerHandler, (void*)this ); 
	isTimerRunning = true;
	Fl::awake();
}

void SubScheduler::timerHandler( void *data ) {
	SubScheduler	*ss = (SubScheduler *)data;
	TSQEntry		sub;
	ClickyTime	now( true );
	if( ss->subs.Pop( sub, now - ss->delayMS ) ) {
		ss->curNum = sub.GetNum();
		ss->curText = sub.GetText();
		Fl::awake( &SubScheduler::awakeAndShowSub, data );
	}
	if( !ss->subs.IsEmpty() ) {
		Fl::repeat_timeout( SUB_SCHEDULER_FLOAT_TICK, &SubScheduler::timerHandler, data );
	} else {
		ss->isTimerRunning = false;
	}
}

void SubScheduler::awakeAndShowSub( void *data )
{
	SubScheduler *ss= (SubScheduler *)data;
	ss->nh.heart->show_incoming_sub( ss->curNum, ss->curText.c_str() );
	Fl::flush();
}

//============== class NetHeart ===================

void NetHeart::setStatus( const char *msg )
{
	LOCK_FL
	heart->get_net_panel()->SetStatus(msg);
	UNLOCK_FL
}

void NetHeart::setAnnc( const char *msg )
{
	LOCK_FL
	heart->get_net_panel()->SetAnnc(msg);
	UNLOCK_FL
}

void NetHeart::Connect( const char *server, const char *user, const char *pwd, const char *id )
{
	LOCK_ME;

	reconnectTimer.SetLoginInfo( server, user, pwd, id );

	setConnectionState( CONNECTING );

	bool useTunnel = false;

	if( server[0] == '#' ) {
		useTunnel = false;
		++server;
	} else if( server[0] == ':' ) {
		useTunnel = true;
		++server;
	}

	std::string serverAddr = server;
	std::string proxyAddr = "";
	size_t pluspos =  serverAddr.find('+');
	if( pluspos != std::string::npos ) {
		proxyAddr = serverAddr.substr( pluspos + 1 );
		serverAddr.erase( pluspos );
	}

	StrUtils::TrimInPlace( serverAddr );
	StrUtils::TrimInPlace( proxyAddr );

	std::string login( user );
	if( heart->get_prefs()->protocol_version == 2 ) {
		login.insert(0,"{2}");
	}

	if( !useTunnel ) {
		deleteDrill();
		connectDirect( serverAddr.c_str(), login.c_str(), pwd, id );
	} else {
		connectTunneled( serverAddr.c_str(), login.c_str(), pwd, id, proxyAddr.c_str() );
	}
}

//XXX
void NetHeart::connectTunneled(	const char *server,
											const char *user,
											const char *pwd,
											const char *id,
											const char *proxy )
{
	if( !createDrill() ) {
		setStatus(_("Failed to create web tunnel :(\n") );
		setConnectionState( DISCONNECTED );
		return;
	}

	std::string localServer;
	unsigned short tunnelHttpPort = 80;
	unsigned short tunnelUdpPort = 0;

	drill->SetDirectLogin( user, pwd, id );
	LOG_HOLE_INFO("Connecting through a tunnel...\n" );
	if( !drill->Start( tunnelUdpPort, server, tunnelHttpPort, (proxy != NULL) ? proxy : "" ) ) {
		setStatus(_("Web tunnel failed to start :(\n") );
		setConnectionState( DISCONNECTED );
	}
}

void NetHeart::connectDirect( const char *server, const char *user, const char *pwd, const char *id )
{
	if( client != NULL ) {
		deleteClient();
	}
	if( !createClient() ) {
		setStatus( _("Failed to create network node :(\n") );
		setConnectionState( DISCONNECTED );
		return;
	}

	client->SetId( id );

	lastSentNum = -3;
	lastSentText = "";

	setConnectionState( CONNECTING );

	std::string srv(server);
	StrUtils::TrimInPlace(srv);
	client->Connect( srv, user, pwd );
}

void NetHeart::disconnect( bool immediately )
{
	if( client == NULL ) {
		setConnectionState( DISCONNECTED );
		return;
	};

	if( immediately || IsConnecting() || IsDisconnecting() ) {
		setConnectionState( DISCONNECTED );
		deleteClient();
		deleteDrill();
		return;
	}
	setConnectionState( DISCONNECTING );
	client->Disconnect();
}

ConnT NetHeart::GetConnectionState()
{
	LOG_GUI_INFO("GetConnectionState() returns %ld\n", connState );
	return connState;
}

/** Sends a subtitle to be shown to the server.
 */
void NetHeart::sendSubtitle( int subNum, const std::string &text )
{
	if( !IsConnected() ) {
		LOG_GUI_INFO("Not sending subtitle #%d, because we're not connected.\n", subNum );
		return;
	}
	if( !CanClickSubtitles() ) {
		LOG_GUI_INFO("Not sending subtitle #%d, because we're not allowed to click subtitles.\n", subNum );
		return;
	}
	if( ( subNum == lastSentNum )&&( lastSentText.compare( text ) == 0 ) ) {
		LOG_GUI_INFO("Not sending subtitle #%d \"%s\", because it's the same as last time.\n", subNum, text.c_str() );
		return;
	}
	lastSentNum = subNum;
	lastSentText = text;
	client->SendShow( subNum, text.c_str() );
};

/** Sends the player state to the server.
 */
void NetHeart::sendPlayerState( const PlayerState &ps )
{
	if( !IsConnected() ) {
		LOG_GUI_INFO("Not sending player state, because we're not connected.\n" );
		return;
	}
	if( !CanClickSubtitles() ) {
		LOG_GUI_INFO("Not sending player state, because we're not allowed to click subtitles.\n" );
		return;
	}
	LOG_NET_DETAIL("Sending player state: %s.\n", ps.ToString().c_str() );
	client->SendPlayerState( ps );
};

void NetHeart::UploadSubtitles( SubtitleDoc *subDoc )
{
	LOCK_ME;

	if( subDoc == NULL ) { return; };
	if( !IsConnected() ) {
		LOG_GUI_INFO("NOT CONNECTED - cannot upload subtitles to remote server.\n");
		return;
	};
	if( !CanLoadSubtitles() ) {
		LOG_GUI_INFO("NOT ALLOWED to upload subtitles to remote server.\n");
		return;
	};
	std::string subs;
	int res = subDoc->save_to_string( subs );
	if( res <= 0 ) {
		LOG_GUI_INFO("NO SUBTITLES - nothing to upload ( save_to_string() returned %d.\n", res );
		return;
	};
	panicInfo.Invalidate();
	LOG_GUI_INFO("Sending %d subtitles to remote server.\n", res );
	client->SendSubtitles( subs );
}

bool NetHeart::createClient()
{
	unsigned short port = 0;		//2048 + rand() % 4096;
	try {
		client = new ClickyClient( _ioService, "Clicky!", port );
	} catch(...) {
		LOG_NET_ERROR("ClickyClient crashed in constructor, perhaps due to a port conflict. Just try again :)\n");
	}
	if( client == NULL ) {
		LOG_NET_ERROR("FAILED to create a new ClientNode!\n");
		return false;
	}
	client->SetEmitter( this );
	client->Start();
	return true;
}

void NetHeart::deleteClient()
{
	if( client == NULL ) { return; }
	delete client;
	client = NULL;
}

bool NetHeart::createDrill()
{
	if( drill != NULL ) {
		deleteDrill();
	}
	drill = new ClickyDrill( *this );
	return drill != NULL;
}

void NetHeart::deleteDrill()
{
	if( drill != NULL ) {
		drill->EnableSignalDisconnect( false );
		delete drill;
		drill = NULL;
	}
}

void NetHeart::ClickedPanic()
{
	LOCK_ME;
	if( !panicInfo.IsValid() ) {
		LOG_GUI_INFO("Yaiks! The clicker wanted to pAnIc! but no panic info is available.");
		return;
	}
	LOCK_FL;

	const PanicSub *panicSub = NULL;

	if( panicInfo.IsSubValid() ) {
		panicSub = &panicInfo.GetSub();
		if( panicSub->IsVisible() ) {
			heart->show_incoming_sub( panicSub->GetNum(), panicSub->GetText().c_str() );
		} else {
			heart->go_to_sub( panicSub->GetNum() + 1 );
		}
	} else { //use PanicPlayerState
		const PlayerState &ps = panicInfo.GetPlayerState();
		double local_delay = (double)subScheduler.GetDelay() / (double)1000.0;
		heart->get_auto_player()->SetPlayerStateFromNet( ps, local_delay );
	}
	UNLOCK_FL;

	//this tells the server I know which subtitle is being displayed,
	//so that the server will allow me to hide it.
	if( panicInfo.IsSubValid() && panicSub->IsVisible() ) {
		int n = panicSub->GetNum();
		if( n >= 0 ) {
			sendSubtitle( n, panicSub->GetText() );
		}
	}
}

//LOCK_ME wrappers

void NetHeart::SendPlayerState( const PlayerState &ps )
{
	LOCK_ME;
	sendPlayerState( ps );
}

void NetHeart::SendSubtitle( int subNum, const std::string &text )
{
	LOCK_ME;
	sendSubtitle( subNum, text );
}

void NetHeart::SetStatus( const char *msg )
{
		LOCK_ME;
		setStatus( msg );
}

void NetHeart::SetAnnc( const char *msg )
{
	LOCK_ME;
	setAnnc( msg );
}

void NetHeart::ConnectDirect( const char *server, const char *user, const char *pwd, const char *id )
{
	LOCK_ME;
	connectDirect( server, user, pwd, id );
}

void NetHeart::Disconnect( bool immediately )
{
	LOCK_ME;
	reconnectTimer.Enable( false );
	disconnect( immediately );
}

//-------------- emitter methods -----------------

void NetHeart::FileProgress( netFileRoleT fileRole, netFileIdT fileId, netFileStateT state, size_t _recvd, size_t _total )
{
	const char *states[] = {"NEW","CONT","DONE","ABORTED"};
	LOG_NET_INFO("Receiving file: %s, role=%li, id=%li, recvd=%li, total=%li\n",
		states[state], (long)fileRole, (long)fileId, (long)_recvd, (long)_total );
}

/** Shows a subtitle that has just arrived from the server on the screen.
 */
void NetHeart::ShowSubtitle( int num, const std::string &text )
{
	if( !CanClickSubtitles() ) {
		if( subScheduler.GetDelay() == 0 ) {
			LOCK_FL
			heart->show_incoming_sub( num, text.c_str() );
			UNLOCK_FL
		} else {
			subScheduler.ScheduleSub( num, text, new ClickyTime( true ) );
		}
	}
	panicInfo.SetSub( num, text );
}

void NetHeart::SetPlayerState( const PlayerState &ps )
{
	if( !CanClickSubtitles() ) {
		AutoPlayer *ap = heart->get_auto_player();
		if( ap ) {
			double local_delay = (double)subScheduler.GetDelay() / (double)1000.0;
			LOCK_FL
			ap->SetPlayerStateFromNet( ps, local_delay );
			UNLOCK_FL
		}
	}
	panicInfo.SetPlayerState( ps );
}

void NetHeart::SetConnectionState( ConnT _state )
{
	LOG_GUI_INFO("emitter asked to setConnectionState() to %ld\n", (long)_state );

	switch( _state ) {
		case CONNECTED:
			reconnectTimer.Enable( true );
		break;
		case DISCONNECTED:
			if( reconnectTimer.Start() ) {
				_state = CONNECTING;
			}
		break;
		default:
		break;
	}

	setConnectionState( _state );
}

void NetHeart::setConnectionState( ConnT _state )
{
	LOCK_FL
	connState = _state;
	if( IsDisconnected() ) { myPerms = PERMS_ALL; };
	heart->update_gui_mode();
	UNLOCK_FL
}

void NetHeart::setPerms( UserPermsT perms )
{
	LOCK_FL
	myPerms = perms;
	heart->update_gui_mode();
	UNLOCK_FL
}

void NetHeart::setSubtitles( const std::string &subs )
{
	panicInfo.Invalidate();
	LOCK_FL
	heart->load_subtitles_from_string( subs, "downloaded_subtitles" );
	UNLOCK_FL
}

//LOCK_ME wrappers

void NetHeart::SetSubtitles( const std::string &subs )
{
	LOCK_ME;
	setSubtitles( subs );
}

void NetHeart::SetPerms( UserPermsT perms )
{
	LOCK_ME;
	setPerms( perms );
};
