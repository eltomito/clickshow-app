/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Net_Heart_h
#define _Net_Heart_h 1

#include "shared/src/net/node/node_con.h"
#include "client/clicky_client.h"
#include "client/emitter.h"
#include "subtitles/subtitle_doc.h"
#include "shared/src/net/hole/drill/easy_drill.h"
#include "shared/src/semaph.h"
#include "client/timed_sub_queue.h"
#include "shared/src/player/player_state.h"

class Heart;

// ============ class PanicSub =================

class PanicSub
{
public:
	PanicSub( int _subNum = -1, const std::string &_subText = "", bool _visible = false )
	:	subNum(_subNum),
		subText(_subText),
		visible(_visible)
	{};
	~PanicSub() {};
	
	bool	IsValid() const { return subNum >= 0; };
	bool	IsVisible() const { return visible; };
	int	GetNum() const { return subNum; };
	void	Set( int num, const std::string &text );
	void	Invalidate() { subNum = -1; subText = ""; visible = false; };

	const std::string	&GetText() const { return subText; };

protected:
	int			subNum;
	std::string	subText;
	bool			visible;
};

class PanicPlayerState
{
public:
	PanicPlayerState() : playerState(), valid(false) {};
	~PanicPlayerState() {};

	void Set( const PlayerState &ps ) { playerState = ps; valid = true; };
	const PlayerState &Get() const { return playerState; };
	bool IsValid() const { return valid; };
	void Invalidate() { valid = false; };

protected:
	PlayerState playerState;
	bool			valid;
};

class PanicInfo
{
public:
	PanicInfo() : panicSub(), panicState() {};
	~PanicInfo() {};

	void SetSub( int num, const std::string &text ) { panicSub.Set( num, text ); panicState.Invalidate(); };
	void SetPlayerState( const PlayerState &ps ) { panicState.Set( ps ); panicSub.Invalidate(); };

	const PanicSub &GetSub() const { return panicSub; };
	const PlayerState &GetPlayerState() const { return panicState.Get(); };

	void Invalidate() { panicSub.Invalidate(); panicState.Invalidate(); };

	bool IsValid() const { return panicSub.IsValid() || panicState.IsValid(); };
	bool IsSubValid() const { return panicSub.IsValid(); };
	bool IsPlayerStateValid() const { return panicState.IsValid(); };

protected:
	PanicSub				panicSub;
	PanicPlayerState	panicState;
};

class NetHeart;

// ============= class ClickyDrill ===========

class ClickyDrill : public EasyDrill
{
public:
	ClickyDrill( NetHeart &_netHeart )
	:	netHeart(_netHeart),
		signalDisconnect(true)
	{};
	~ClickyDrill() {};

	void EnableSignalDisconnect( bool _state = true ) { signalDisconnect = _state; };

	void SetDirectLogin( const std::string &_user, const std::string &_pwd, const std::string &_id )
	{
		directUser = _user;
		directPwd = _pwd;
		directId = _id;
	};

	void holeOpen();
	void holeClosed();
protected:
	NetHeart 	&netHeart;

	std::string	directUser;
	std::string	directPwd;
	std::string	directId;

	bool signalDisconnect;
};

// =============== class ReconnectTimer =================

class ReconnectTimer : public Lockable
{
public:
	ReconnectTimer( NetHeart &_netHeart, unsigned _milliSeconds = 1000 )
	:	netHeart(_netHeart),
		milliSeconds(_milliSeconds),
		autoReconnect(false)
	{};
	~ReconnectTimer() { stop(); };

	void Enable( bool _state );
	bool IsEnabled() const { return autoReconnect; };
	bool Start();
	void Stop();
	void SetLoginInfo( const char *_server, const char *_user, const char *_pwd, const char *_id );

protected:
	void stop();
	void timeoutHandler();
	static void staticTimeoutHandler( void *_reconnectTimer );

protected:
	NetHeart		&netHeart;

	bool			autoReconnect;
	unsigned		milliSeconds;

	std::string	server;
	std::string	user;
	std::string	pwd;
	std::string	id;
};


#define SUB_SCHEDULER_FLOAT_TICK 0.1

class SubScheduler : Lockable
{
public:
	SubScheduler( NetHeart &_nh, boost::int64_t _delayMS = 0 ) : nh( _nh ), delayMS( _delayMS ), isTimerRunning( false ) {};
	~SubScheduler();

	void				SetDelay( boost::int64_t _delayMS ) { delayMS = _delayMS; };
	boost::int64_t	GetDelay() { return delayMS; };
	void				ScheduleSub( int num, const std::string &text, const ClickyTime &when );
	void				Clear();

protected:
	void				startTimer();
	static void		timerHandler( void *data );
	static void 	awakeAndShowSub( void *data );

protected:
	NetHeart			&nh;
	boost::int64_t	delayMS;
	TimedSubQueue	subs;
	bool				isTimerRunning;
	int 				curNum;
	std::string		curText;
};

// ============ class NetHeart ====================

class NetHeart : public Emitter, public Lockable
{
	friend class ClickyDrill;
public:
	Heart *heart;

	static const long DEFAULT_AUTO_RECONNECT_MS = 1000;

public:
	NetHeart( Heart *_heart )
	:	heart(_heart),
		client(NULL),
		drill(NULL),
		myPerms( PERMS_ALL ),
		connState( DISCONNECTED ),
		lastSentNum( -3 ),
		lastSentText( "" ),
		reconnectTimer( *this, DEFAULT_AUTO_RECONNECT_MS ),
		subScheduler( *this, 0 )
		{
			srand (time(NULL));
		};
	~NetHeart() { deleteClient(); };

	void SetStatus( const char *msg );
	void SetAnnc( const char *msg );
	void Connect( const char *server, const char *user, const char *pwd, const char *id );
	void ConnectDirect( const char *server, const char *user, const char *pwd, const char *id );
	void Disconnect( bool immediately = false );

	ConnT GetConnectionState();
	UserPermsT GetPerms() { return myPerms; };

	bool IsConnected() { return CONNECTED == GetConnectionState(); };
	bool IsConnecting() { return CONNECTING == GetConnectionState(); };
	bool IsDisconnecting() { return DISCONNECTING == GetConnectionState(); };
	bool IsDisconnected() { return DISCONNECTED == GetConnectionState(); };

	bool IsDrilling() { return drill != NULL; };

	bool CanLoadSubtitles() { return 0 != (myPerms & PERM_LOAD_SUBS); };
	bool CanClickSubtitles() { return 0 != (myPerms & PERM_CLICK_SUBS); };

	void ClickedPanic();

	void SetSubDelay( float seconds ) { subScheduler.SetDelay( (boost::int64_t)( seconds * (float)1000 ) ); };

//--- outgoing
	void SendSubtitle( int subNum, const std::string &text );
	void UploadSubtitles( SubtitleDoc *subDoc );
	void SendPlayerState( const PlayerState &ps );

//--- incoming (emitter)

	void ShowSubtitle( int num, const std::string &text );
	void SetPlayerState( const PlayerState &ps );
	void SetConnectionState( ConnT _state );
	void SetAnnouncement( const std::string &text ) {};
	void SetSubtitles( const std::string &subs );
	void SetPerms( UserPermsT perms );
	void NetMsg( NetMsgT code ) {};
	void FileProgress( netFileRoleT fileRole, netFileIdT fileId, netFileStateT state, size_t _recvd, size_t _total );

protected:
	bool createClient();
	void deleteClient();

	bool createDrill();
	void deleteDrill();

	//internal variants of public LOCK_ME functions
	void disconnect( bool immediately = false );
	void connectDirect( const char *server, const char *user, const char *pwd, const char *id );
	void connectTunneled( const char *server, const char *user, const char *pwd, const char *id, const char *proxy = NULL );
	void setConnectionState( ConnT _state );
	void setSubtitles( const std::string &subs );
	void setPerms( UserPermsT perms );
	void setStatus( const char *msg );
	void setAnnc( const char *msg );
	void sendSubtitle( int subNum, const std::string &text );
	void sendPlayerState( const PlayerState &ps );

	ClickyClient	*client;
	ClickyDrill		*drill;

	ReconnectTimer	reconnectTimer;

	UserPermsT	myPerms;
	ConnT			connState;

	PanicInfo	panicInfo;

	int			lastSentNum;
	std::string	lastSentText;

	SubScheduler	subScheduler;

	boost::asio::io_service _ioService;
};

#endif //_Net_Heart_h
