/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "net_panel.h"

#include "clicky_debug.h"

#include <FL/names.h>
#include <FL/Fl_Native_File_Chooser.H>
#include "body_layout.h"
#include "brain_mlinput.h"
#include "panels.h"

// ============= class ImageButton ===============

ImageButton::ImageButton( QuickImage *_image, bool _own, int x, int y, int w, int h )
	: ClickyButton( x, y, w, h, NULL )
{
	images.push_back( _image );
	ShowImage();
}

/** Takes a vector of pointer to QuickImage structures and uses them as the images.
 *
 * The images are freed when this ImageButton is destroyed.
 */
ImageButton::ImageButton( const ImageVecT & _images, int x, int y, int w, int h )
	: ClickyButton( x, y, w, h, NULL ), images(_images)
{
	ShowImage();
}

ImageButton::~ImageButton()
{
	for( int i = 0; i < images.size(); i ++ ) {
		if( images[i] != NULL ) { delete images[i]; }
	}
}

void ImageButton::ShowImage( int index )
{
	align( FL_ALIGN_IMAGE_BACKDROP );
	index = (index < 0) ? current : index;
	if( index < 0 ) {	return; }
	image( images[index] );
}

void ImageButton::HideImage()
{
	image( NULL );
	current = -1;
}

void ImageButton::SetImageColor( Fl_Color clr, int index )
{
	index = (index < 0) ? current : index;
	if( ( index < 0 )||( index >= images.size() )) {
		return;
	}
	if( images[index] == NULL ) { return; }
	images[index]->set_color( clr );
}

// ================ class DelaySpinner ===============

DelaySpinner::DelaySpinner( NetPanel *_np, const char *_label ) : ClickySpinner( _np->heart, _label, true, 4 ), np( _np )
{
		Fl_Spinner *sp = GetSpinner();

		sp->color( FL_BLACK );
		sp->textcolor( FL_WHITE );

		sp->type( FL_FLOAT_INPUT );
		sp->range( 0, 60 );
		sp->value( 0 );
		sp->align( FL_ALIGN_LEFT );
		sp->step( 0.1 );

		sp->cursor_color( FL_WHITE );

		SetTooltip( _("Received subtitles are delayed by this number of seconds.") );
};

void DelaySpinner::spinner_callback()
{
	np->delay_changed( GetValue() );
};

// =================== class NetPanel =========================

	NetPanel::NetPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, (const char *)"FILE" )
	{
		heart = hrt;
		//mode = DISCONNECTED;

		//layout = new SimpLE();

		main_pack = new BodyPack( true );
		main_pack->align( CL_ALIGN_FILL );
		main_pack->spacing( 5 );

		end();

		vert_pack = new BodyPack( false );
		vert_pack->align( CL_ALIGN_FILL );
		vert_pack->spacing( 5 );

		top_pack = new BodyPack( true );
		top_pack->align( CL_ALIGN_FILL | CL_ALIGN_EVEN );
		top_pack->spacing( 5 );
		mid_pack = new BodyPack( true );
		mid_pack->align( CL_ALIGN_FILL | CL_ALIGN_EVEN );
		mid_pack->spacing( 5 );
		bot_pack = new BodyPack( true );
		bot_pack->align( CL_ALIGN_FILL | CL_ALIGN_EVEN );
		bot_pack->spacing( 5 );

		indicator_pack = new BodyPack( false );
		indicator_pack->align( CL_ALIGN_FILL );
		indicator_pack->spacing( 5 );

		end();

		button_status = new ClickyButton( 0, 0, 0, 0, "" );

		ImageButton::ImageVecT clicker_images;
		clicker_images.push_back( new StockImage( StockImage::CLICKER_DEAD, 32, 24, FL_RED, 0, 0, QuickImage::QI_CENTER ) );
		clicker_images.push_back( new StockImage( StockImage::CLICKER_ALIVE, 32, 24, FL_GREEN, 0, 0, QuickImage::QI_CENTER ) );
		//imgb_clicker = new ImageButton( clicker_images );

		ImageButton::ImageVecT plug_images;
		plug_images.push_back( new StockImage( StockImage::PLUG_DISCONNECTED, 32, 24, FL_RED, 0, 0, QuickImage::QI_CENTER ) );
		plug_images.push_back( new StockImage( StockImage::PLUG_CONNECTING, 32, 24, FL_YELLOW, 0, 0, QuickImage::QI_CENTER ) );
		plug_images.push_back( new StockImage( StockImage::PLUG_CONNECTED, 32, 24, FL_GREEN, 0, 0, QuickImage::QI_CENTER ) );
		plug_images.push_back( new StockImage( StockImage::PLUG_DISCONNECTING, 32, 24, FL_MAGENTA, 0, 0, QuickImage::QI_CENTER ) );
		imgb_plug = new ImageButton( plug_images );

		indicator_pack->add( imgb_plug );
		//indicator_pack->add( imgb_clicker );

		button_connect = new ClickyButton( 0, 0, 0, 0, _("Connect") );

		spinner_delay = new DelaySpinner( this, _("Delay subtitles:") );

		clinput_server = new ClickyInputNonEmpty( heart, 10, _("Server:"), true );
		clinput_login = new ClickyInputNonEmpty( heart, 5, _("Login:"), true );
		clinput_pwd = new ClickyInput( heart, 5, _("Password:"), true );
		clinput_venue = new ClickyInputNonEmpty( heart, 10, _("Venue:"), true );

		button_panic = new ClickyButton( 0, 0, 0, 0, _("Panic!") );
		button_annc = new ClickyButton( 0, 0, 0, 0, "" );

		//packs
		vert_pack->add( top_pack );
		vert_pack->add( mid_pack );
		vert_pack->add( bot_pack );

		top_pack->add( button_status );
		top_pack->add( clinput_venue );

		mid_pack->add( button_panic );
		mid_pack->add( clinput_login );
		mid_pack->add( clinput_pwd );
		mid_pack->add( spinner_delay );

		bot_pack->add( clinput_server );
		bot_pack->add( button_connect );
		bot_pack->add( button_annc );

		main_pack->add( indicator_pack );
		main_pack->add( vert_pack );

		//tooltips
		button_status->tooltip( "" );
		button_connect->tooltip( "" );
		button_panic->tooltip( _("Find current subtitle from other clickers if there are any.") );
		button_annc->tooltip( "" );
		clinput_server->tooltip( _("Address of the server to connect to.") );
		clinput_venue->tooltip( _("Short description of where you are connecting from.") );

		//init button states
		//input_server->when( FL_WHEN_ENTER_KEY|FL_WHEN_NOT_CHANGED );
		clinput_server->SetValue( heart->get_prefs()->net_default_server.c_str() );
		clinput_login->SetValue( heart->get_prefs()->net_default_login.c_str() );
		clinput_pwd->SetValue( heart->get_prefs()->net_default_pwd.c_str() );
		clinput_venue->SetValue( heart->get_prefs()->net_default_venue.c_str() );

		//visibility
		imgb_plug->show();
		UpdateMode( false );

		//callbacks
		button_connect->callback((Fl_Callback*)cb_connect);
		button_panic->callback((Fl_Callback*)cb_panic);
		//input_server->callback((Fl_Callback*)cb_server);

		//-- style --
		Fl_Widget* most_buttons[] = {	button_connect,
												button_panic,
												clinput_server,
												clinput_login,
												clinput_pwd,
												clinput_venue,
												NULL };
		heart->get_style_engine()->assign_style( StylePrefs::FILE_BUTTON, &most_buttons[0] );

		Fl_Widget* nolabel_buttons[] = {
												//imgb_clicker,
												imgb_plug,
												NULL };
		heart->get_style_engine()->assign_style( StylePrefs::NO_LABEL_BUTTON, &nolabel_buttons[0] );

		Fl_Widget* netlabel_buttons[] = {
												button_status,
												button_annc,
												NULL };
		heart->get_style_engine()->assign_style( StylePrefs::NET_LABEL_BUTTON, &netlabel_buttons[0] );
		heart->get_style_engine()->assign_style( StylePrefs::NET_LABEL_BUTTON, (Fl_Button *)spinner_delay );

		refresh_style();
		refresh_layout();
	};

    NetPanel::~NetPanel()
    {
       // delete layout;
    };

	void NetPanel::SetStatus( const char *msg )
	{
		button_status->label( msg );
	}

	void NetPanel::SetAnnc( const char *msg )
	{
		button_annc->label( msg );
	}

	void NetPanel::UpdateMode( bool refresh )
	{
		ConnT state = heart->get_net_heart()->GetConnectionState();
		UserPermsT perms = heart->get_net_heart()->GetPerms();

		switch( state ) {
			case DISCONNECTED:
				//mid_pack->show();
				//imgb_clicker->hide();
				imgb_plug->ShowImage( 0 );
				button_status->hide();
				button_annc->hide();
				button_panic->hide();
				clinput_server->show();
				clinput_login->show();
				clinput_pwd->show();
				clinput_venue->show();
				button_connect->label(_("Connect"));
				button_connect->tooltip( _("Connect to a server.") );
				spinner_delay->hide();
			break;
			case CONNECTING:
				//mid_pack->hide();
				//imgb_clicker->hide();
				imgb_plug->ShowImage( 1 );
				button_status->show();
				button_annc->hide();
				button_panic->hide();
				clinput_server->hide();
				clinput_login->hide();
				clinput_pwd->hide();
				clinput_venue->hide();
				button_connect->label(_("Cancel"));
				button_connect->tooltip( _("Give up trying to connect.") );
				SetStatus( heart->get_net_heart()->IsDrilling() ? _("Connecting thru WWW...") : _("Connecting...") );
				spinner_delay->hide();
			break;
			case CONNECTED:
				heart->get_prefs()->net_default_server.assign( clinput_server->GetValue() );
				heart->get_prefs()->net_default_login.assign( clinput_login->GetValue() );
				heart->get_prefs()->net_default_pwd.assign( clinput_pwd->GetValue() );
				heart->get_prefs()->net_default_venue.assign( clinput_venue->GetValue() );

				//mid_pack->hide();
				//imgb_clicker->show();
				imgb_plug->ShowImage( 2 );
				button_status->show();
				button_annc->show();

				LOG_NET_INFO("My perms are: %ld\n", (long)perms );

				if( 0 != (perms & PERM_CLICK_SUBS ) ) {
					button_panic->show();
					spinner_delay->hide();
				} else {
					button_panic->hide();
					spinner_delay->show();
				}

				clinput_server->hide();
				clinput_login->hide();
				clinput_pwd->hide();
				clinput_venue->hide();
				button_connect->label(_("Disconnect"));
				button_connect->tooltip( _("Disconnect from the server.") );
				//SetStatus( _("Connected") );
				SetStatus( heart->get_net_heart()->IsDrilling() ? _("Connected thru WWW") : _("Connected") );
			break;
			case DISCONNECTING:
				//mid_pack->hide();
				//imgb_clicker->hide();
				imgb_plug->ShowImage( 3 );
				button_status->show();
				button_annc->hide();
				button_panic->hide();
				clinput_server->hide();
				clinput_login->hide();
				clinput_pwd->hide();
				clinput_venue->hide();
				button_connect->label(_("Don't wait for server"));
				button_connect->tooltip( _("Don't wait for the server and disconnect now.") );
				SetStatus( _("Disconnecting...") );
				spinner_delay->hide();
			break;
			default:
				LOG_GEN_INFO("Asked to go to a mode based on this UNKNOWN STATE: %d !\n", state ); 
			break;
		};
		if( refresh ) {
			heart->get_main_panel()->refresh_layout();
			heart->get_main_panel()->redraw();
		}
	}

	void NetPanel::tryConnect() {
		ConnT state = heart->get_net_heart()->GetConnectionState();
		bool valid; 
		switch( state ) {
			case DISCONNECTED:
				valid = clinput_server->Validate();
				valid = clinput_login->Validate() && valid;
				valid = clinput_venue->Validate() && valid;
				if( !valid ) { return; }
				heart->get_net_heart()->Connect(	clinput_server->GetValue(),
															clinput_login->GetValue(),
															clinput_pwd->GetValue(),
															clinput_venue->GetValue() );
			break;
			case CONNECTING:
				heart->get_net_heart()->Disconnect( true );
			break;
			case CONNECTED:
				heart->get_net_heart()->Disconnect();
			break;
			case DISCONNECTING:
				heart->get_net_heart()->Disconnect( true );
			break;
			default:
				LOG_GEN_INFO("Asked to connect / disconnect from a weird state: %d\n", state );
			break;
		}
	}

	bool NetPanel::get_size( int &ww, int &hh )
	{
		main_pack->natural_size( ww, hh );
		return true;
	}

	void NetPanel::refresh_layout()
	{
		main_pack->resize( x(), y(), w(), h() );
	}

	void NetPanel::cb_connect(Fl_Button* b, void*)
	{
		NetPanel *p = (NetPanel *)b->parent()->parent()->parent()->parent();
		LOG_GEN_INFO("We'll be connecting...!\n");
		p->tryConnect();
	}

	void NetPanel::cb_panic(Fl_Button* b, void*)
	{
		NetPanel *p = (NetPanel *)b->parent()->parent()->parent()->parent();
		LOG_GEN_INFO("Let's panic! Yeeeeaaaaaah!!\n" );
		p->heart->get_net_heart()->ClickedPanic();
	}

	void NetPanel::cb_server(Fl_Button* b, void*)
	{
		NetPanel *p = (NetPanel *)b->parent()->parent()->parent()->parent()->parent();
		//p->heart->clicked_connect( p->input_server->value() );
		//p->heart->get_prefs()->net_default_server.assign( p->input_server->value() );
		LOG_GEN_INFO("Clicked in da server!\n");
	}

	void NetPanel::delay_changed( double value )
	{
		heart->get_net_heart()->SetSubDelay( (float)value );
		LOG_RENDER_DETAIL2("Subtitle delay changed!\n" ); 
	};
