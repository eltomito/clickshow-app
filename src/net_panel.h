/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Net_Panel_h
#define _Net_Panel_h

//#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Multiline_Input.H>

//#include <FL/Fl_Check_Button.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "heart.h"
#include "ask_dialog.h"
#include "clicky_button.h"
#include "clicky_input.h"
#include "body_pack.h"
#include "shared/src/net/node/node_con.h"	//for ConnT
#include "clicky_spinner.h"

class ImageButton : public ClickyButton
{
public:
	typedef std::vector<QuickImage*> ImageVecT;

	ImageButton( QuickImage *_image, bool _own = true, int x=0, int y=0, int w=0, int h=0 );
	ImageButton( const ImageVecT & _images, int x=0, int y=0, int w=0, int h=0 );
	~ImageButton();

	void ShowImage( int index = 0 );
	void HideImage();
	void SetImageColor( Fl_Color clr, int index = -1 );

protected:
	ImageVecT	images;
	int 			current;		//index in images, -1 means none shown.
};

class NetPanel;

class DelaySpinner : public ClickySpinner
{
public:
	DelaySpinner( NetPanel *_np, const char *_label );
	~DelaySpinner() {};
protected:
	void spinner_callback();

	NetPanel *np;
};

/*
 * the file panel class
 */
class NetPanel : public ControlPanel
{
	friend class DelaySpinner;
	//SimpLE	*layout;

	BodyPack *main_pack;
	BodyPack *vert_pack;
	BodyPack *top_pack;
	BodyPack *mid_pack;
	BodyPack *bot_pack;
	BodyPack *indicator_pack;

	ClickyButton	*button_status;
	ClickyButton	*button_connect;

	//ImageButton		*imgb_clicker;
	ImageButton		*imgb_plug;

	ClickyInput		*clinput_server;
	ClickyInput		*clinput_login;
	ClickyInput		*clinput_pwd;
	ClickyInput		*clinput_venue;

	DelaySpinner	*spinner_delay;

	ClickyButton	*button_panic;
	ClickyButton	*button_annc;

	static	void cb_connect(Fl_Button* b, void*);
	static	void cb_panic(Fl_Button* b, void*);
	static	void cb_server(Fl_Button* b, void*);

public:
	NetPanel(Heart *hrt, int x, int y, int w, int h);
	~NetPanel();

	void SetStatus( const char *msg );
	void SetAnnc( const char *msg );
	void UpdateMode( bool refresh = true );

	//-- these functions should be generalized and probably go in the ControlPanel class --

	bool get_size( int &w, int &h );
	void refresh_layout();
	
protected:
	void delay_changed( double value );

	void tryConnect();
};

#endif //_Net_Panel_h
