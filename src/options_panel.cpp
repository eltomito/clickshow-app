/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "options_panel.h"

#include "clicky_debug.h"

OptionsPanel::OptionsPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel( hrt, x, y, w, h, "OPTIONS" )
{
	layout = new SimpLE();

	check_ontop = new Fl_Check_Button(0,0,0,0,_("On Top") );
	check_fullscreen = new Fl_Check_Button(0,0,0,0,_("Fullscreen") );

	button_close = new Fl_Button(0,0,0,0,"");
	lang_switch = new LangSwitch(heart, 0,0,0,0);

	end();

	//init button states
	check_fullscreen->value( heart->get_prefs()->full_screen_mode );
	check_ontop->value( heart->get_prefs()->on_top );

	static const int num_cols = 10;
	static const int num_rows = 3;

	static const int closeb_size = 21;
	static const int closeb_margin = 4;

	//layout
	layout->add_cell( check_ontop, 0, 0, 6, 1,	//cx, cy, cw, ch
									num_cols, num_rows,				//columns, rows
									10, 10,			//pixx, pixy
									0.0, 0.0,	//relx, rely
									1.0, 1.0,	//relw, relh
									(closeb_size+closeb_margin), 8,		//margins
									30, 2 );		//spaces

	layout->add_cell( check_fullscreen, 0, 1, 6, 1,	//cx, cy, cw, ch
									num_cols, num_rows,				//columns, rows
									10, 10,			//pixx, pixy
									0.0, 0.0,	//relx, rely
									1.0, 1.0,	//relw, relh
									(closeb_size+closeb_margin), 8,		//margins
									30, 2 );		//spaces

	layout->add_cell( lang_switch, 6, 0, 4, 3,	//cx, cy, cw, ch
									num_cols, num_rows,				//columns, rows
									10, 10,			//pixx, pixy
									0.0, 0.0,	//relx, rely
									1.0, 1.0,	//relw, relh
									(closeb_size+closeb_margin), 20,		//margins
									0, 2 );		//spaces

	layout->add( button_close,
						-1*(closeb_size), 0,			//pixx, pixy
						1.0, 0.0,	//relx, rely
						0.0, 0.0,	//relw, relh
						0, 0,		//margins
						closeb_size, closeb_size );		//min pix

	//style
	heart->get_style_engine()->assign_style( StylePrefs::MISCOPTS_BUTTON, check_ontop );
	heart->get_style_engine()->assign_style( StylePrefs::MISCOPTS_BUTTON, check_fullscreen );
	heart->get_style_engine()->assign_style( StylePrefs::OPCLOSE_BUTTON, button_close );

	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_PANEL, lang_switch );

	//callbacks
	check_ontop->callback((Fl_Callback*)cb_ontop);
	check_fullscreen->callback((Fl_Callback*)cb_fullscreen);
	button_close->callback((Fl_Callback*)cb_close);

	refresh_style();
	refresh_layout();
};

OptionsPanel::~OptionsPanel()
{
	delete layout;
};

void OptionsPanel::cb_ontop(Fl_Check_Button* b, void*)
{
	OptionsPanel *p = (OptionsPanel *)b->parent();
	p->heart->set_ontop( (bool)b->value() );
};

void OptionsPanel::cb_fullscreen(Fl_Check_Button* b, void*)
{
	OptionsPanel *p = (OptionsPanel *)b->parent();

	LOG_GEN_INFO("setting fullscreen to %i\n", b->value() );
	p->heart->set_fullscreen( (bool)b->value() );
};

void OptionsPanel::cb_close(Fl_Button* b, void*)
{
	OptionsPanel *p = (OptionsPanel *)b->parent();
	p->heart->show_misc_options( 0 );
	LOG_GEN_INFO("Close options, man!\n" );  
};

bool OptionsPanel::get_size( int &w, int &h )
{
	int	fh;

	fh = heart->get_style_prefs()->NormalButton->ls.Size;
	h = fh*6;
	w = h * 4;
	return true;
};

void OptionsPanel::refresh_layout()
{
	layout->refresh_layout( this );
};

