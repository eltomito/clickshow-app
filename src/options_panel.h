/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Options_Panel_h
#define _Options_Panel_h

#include <FL/Fl.H>
//#include <FL/Fl_Spinner.H>
#include <FL/Fl_Button.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_prefs.h"
#include "ask_dialog.h"
#include "lang_switch.h"

class OptionsPanel : public ControlPanel
{
	SimpLE	*layout;

	Fl_Check_Button	*check_ontop;
	Fl_Check_Button	*check_fullscreen;
	Fl_Button			*button_close;

	LangSwitch			*lang_switch;

	static void cb_ontop(Fl_Check_Button* b, void*);
	static void cb_fullscreen(Fl_Check_Button* b, void*);
	static void cb_close(Fl_Button* b, void *panel);

public:
	OptionsPanel(Heart *hrt, int x, int y, int w, int h);
	~OptionsPanel();

	void set_fullscreen( bool state ) { check_fullscreen->value( state ); };

	bool get_size( int &w, int &h );
	void refresh_layout();
};

#endif	//_Options_Panel_h