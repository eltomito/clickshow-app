/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Pack_Body_Geom_h
#define _Pack_BodyGeom_h

#include "body_geom.h"

class PackBodyGeom : public BodyGeom
{
public:
		PackBodyGeom( int space_before = 5, int space_after = 5, Cl_Geom_AlignType align = 0, double weight = 1 );
		~BodyGeom() {};
};

#endif //_BodyGeom_h
