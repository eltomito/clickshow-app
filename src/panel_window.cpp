/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "panel_window.h"

#include "FL/names.h"

#include "clicky_debug.h"

#include <cstring>
#include <string>
#include <algorithm>
#include <cmath>

//=== class PanelWindow ===

	/*
	 * paramters:
	 *		heart - the heart of this program
	 *		panel - the control panel to embed
	 *		parent - a widget to anchor to
	 *		x, y -	if >= 1 in absolute value, pixel offsets to the anchor widget
	 *					if < 1 in abs. value, pixel offsets as multiples of anchor widget dimensions
	 *		anc - anchor type (TOP,BOTTOM,LEFT,RIGHT,VCENTER,HCENTER,CENTER )
	 *		winlabel - window label (UNIMPLEMENTED)
	 */
PanelWindow::PanelWindow( Heart *hrt, ControlPanel *pnl, Fl_Widget *parent, Cl_Anchor anc,
								float x, float y, const char *winlabel, unsigned int flags )
			:Fl_Window( 0,0,0,0,NULL )
{
	heart = hrt;

	copy_label( winlabel );

	color( FL_BLACK );
	box( FL_FLAT_BOX );

	//add an experimental frame
	window_frame = new Fl_Box( FL_PLASTIC_UP_FRAME, 0,0,0,0, NULL );
	window_frame->color( FL_GRAY );

	//remember settings
	real_parent = parent;
	anchor_x = x;
	anchor_y = y;
	anchor_type = anc;

	end();

	panel = pnl;
	add( panel );
	panel->clear_visible_focus();

	drag_bar = NULL;
	close_button = NULL;
	deco_flags = 0;
	decorations( flags );

	refresh_layout();
	init_position();
};

PanelWindow::~PanelWindow()
{
};

void PanelWindow::refresh_layout()
{
	int	W, H;
	int	yoff = 0;
	int	xmarg = 0;

	if( deco_flags & TITLEBAR )
	{
		yoff = drag_bar->h();
	};

	if( deco_flags & CLOSEBUTTON )
	{
		yoff = std::max( close_button->h(), yoff );
		xmarg = close_button->w();
	};

	W = 40;
	H = 40;

	if( panel != NULL )
	{
		panel->get_size( W, H );
		panel->resize( 0, yoff, W, H );
	};

	size( W, H + yoff );
	window_frame->resize( 0,0,w(),h() );

	if( deco_flags & TITLEBAR )
	{
		drag_bar->resize( x(), y(), w()-xmarg, drag_bar->h() );
		//drag_bar->show();
	};

	if( deco_flags & CLOSEBUTTON )
	{
		close_button->position( x()+w()-xmarg, y() );
	};

	LOG_RENDER_DETAIL2("drag bar x=%i, y=%i, w=%i, h=%i, yoff=%i\n", drag_bar->x(), drag_bar->y(),drag_bar->w(),drag_bar->h(), yoff );
};

void PanelWindow::init_position()
{
	if( real_parent != NULL )
	{

		int ax, ay;
		
		if( ( fabs( anchor_x ) > 0 )&&( fabs( anchor_x ) < 1 ) )
			ax = (int)( ( anchor_x * (float)real_parent->w() ) );
		else
			ax = (int)anchor_x;

		if( ( fabs( anchor_y ) > 0 )&&( fabs( anchor_y ) < 1 ) )
			ay = (int)( ( anchor_y * (float)real_parent->h() ) );
		else
			ay = (int)anchor_y;

		int	X = real_parent->x() + ax;
		int	Y = real_parent->y() + ay;

		if( 0!=( anchor_type & VCENTER ) )
			Y -= h()/2;

		if( 0!=( anchor_type & HCENTER ) )
			X -= w()/2;

		if( 0!=( anchor_type & BOTTOM ) )
			Y += real_parent->h() - h();

		if( 0!=( anchor_type & RIGHT ) )
			X += real_parent->w() - w();

		//make sure the window is visible
		if( X < 0 )
			X = 0;
		if( Y < 0 )
			Y = 0;

		position( X, Y );
	};
}

/*
 */
void PanelWindow::decorations( unsigned int flags )
{
	if( flags != deco_flags )
	{
		// -- close button
		if(( flags & CLOSEBUTTON )&&( close_button == NULL ))
		{
			close_button = new ClickyButton( 0,0,20,20,NULL );
			if( close_button == NULL )
			{
				LOG_RENDER_DETAIL2("failed to create close button!!\n");
				flags &= (~CLOSEBUTTON);
			}
			else
			{
				heart->get_style_prefs()->PWCloseImgB->apply( close_button );
				close_button->callback( (Fl_Callback*)cb_close );
				add( close_button );
			};
		};

		// -- title bar
		if( ( flags & TITLEBAR) && ( drag_bar == NULL ) )
		{
			drag_bar = new DragBar( this, 0, 0, 0, 20 );
			if( drag_bar == NULL )
			{
				LOG_RENDER_DETAIL2("failed to create DragBar!!\n");
				flags &= (~TITLEBAR);
			}
			else
				add( drag_bar );
		};
		
		if(( 0 == (flags & TITLEBAR ) )&&( drag_bar != NULL ))
		{
			delete drag_bar;
			drag_bar = NULL;
		};

		deco_flags = flags;
		refresh_layout();
	};
}

void PanelWindow::cb_close(ClickyButton* b, void*)
{
	PanelWindow *w = (PanelWindow *)b->parent();
	w->hide();
};


int PanelWindow::handle( int event )
{
#ifdef DEBUG
	LOG_RENDER_DETAIL2("PanelWindow received an EVENT: %s\n", fl_eventnames[ event ] );
	if( event == FL_KEYDOWN ) {
		LOG_RENDER_DETAIL2(" - \"%s\"\n", Fl::event_text() );
	} else {
		LOG_RENDER_DETAIL2("\n" );
	}
#endif //DEBUG

	//-- handle mouse clicks and FOCUS

	if( event == FL_PUSH )
		if( ( !Fl::belowmouse()->visible_focus() )||( Fl::belowmouse()==this ) )
		{
			LOG_RENDER_DETAIL2("PanelWindow: I Am STEALING FOCUS!!!\n");
			Fl::focus( this );
			Fl_Window::handle( event );
			return 1;
		};

	if( 1 == heart->handle_global_shortcuts( event ) )
		return 1;
	else
	{
		Fl_Window::handle( event );
		return 1;
	};
};
