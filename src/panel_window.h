/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Panel_Window_h
#define _Panel_Window_h

#include "clicky_debug.h"

#include "control_panel.h"
#include "drag_bar.h"
#include "clicky_button.h"

#include <FL/Fl_Window.H>

class PanelWindow : public Fl_Window
{
public:
	typedef unsigned char Cl_Anchor;

	static const Cl_Anchor BOTTOM = 1;
	static const Cl_Anchor RIGHT = 1 << 1;
	static const Cl_Anchor TOPLEFT = 0;
	static const Cl_Anchor TOPRIGHT = RIGHT;
	static const Cl_Anchor BOTTOMLEFT = BOTTOM;
	static const Cl_Anchor BOTTOMRIGHT = BOTTOM|RIGHT;

	static const Cl_Anchor VCENTER = 1 << 2;
	static const Cl_Anchor HCENTER = 1 << 3;
	static const Cl_Anchor CENTER = VCENTER|HCENTER;

	//flags
	static const unsigned int TITLEBAR = 1;
	static const unsigned int CLOSEBUTTON = 1 << 1;
	static const unsigned int WINDOWTITLE = 1 << 2;

private:
	Heart 			*heart;
	ControlPanel	*panel;

	float	anchor_x, anchor_y;
	Fl_Widget *real_parent;

	Cl_Anchor	anchor_type;

	unsigned int deco_flags;
	
	DragBar	*drag_bar;
	ClickyButton *close_button;
	Fl_Box *window_frame;

public:

	/*
	 * paramters:
	 *		heart - the heart of this program
	 *		panel - the control panel to embed
	 *		parent - a widget to anchor to
	 *		x, y -	if >= 1 in absolute value, pixel offsets to the anchor widget
	 *					if < 1 in abs. value, pixel offsets as multiples of anchor widget dimensions
	 *		anc - anchor type (TOP,BOTTOM,LEFT,RIGHT,VCENTER,HCENTER,CENTER )
	 *		winlabel - window label (UNIMPLEMENTED)
	 */
	PanelWindow( Heart *hrt, ControlPanel *panel, Fl_Widget *parent, Cl_Anchor anc,
					float x, float y, const char *winlabel, unsigned int flags = TITLEBAR|CLOSEBUTTON );
	~PanelWindow();

	void refresh_layout();
	void init_position();

	void decorations( unsigned int flags );
	unsigned int decorations() { return deco_flags; };

private:

	int handle( int event );
	static void cb_close(ClickyButton* b, void*);

};

#endif //_Panel_Window_h
