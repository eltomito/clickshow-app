/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "panels.h"

#include "clicky_debug.h"

#include <FL/fl_draw.H>
#include <FL/names.h>

//========================== class DashPanel ==================================

DashPanel::DashPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel( hrt, x, y, w, h, (const char *)"DASH" )
{
	file_panel = new FilePanel( hrt, 0, 0, 0, 0 );
	options_panel = new OptionsPanel( hrt, 0, 0, 0, 0);

	attached_fp_cfg = new AttachedFPCfg( hrt );
	detached_fp_cfg = new DetachedFPCfg( hrt );
	font_panel = new FontPanel( hrt, 0, 0, 0, 0,
		heart->get_prefs()->bb_visible ? (FontPanelCfg *)detached_fp_cfg : (FontPanelCfg *)attached_fp_cfg );

	candy_panel = new CandyPanel( hrt, 0, 0, 0, 0 );
	net_panel = new NetPanel( hrt, 0, 0, 0, 0 );
	billboard_panel = new BillboardPanel( hrt, 0, 0, 0, 0 );
	player_panel = new PlayerPanel( hrt, *hrt->get_auto_player() );
	click_panel = new SubClickPanel( hrt, 0, 0, 0, 0 );
	recorder_panel = new RecorderPanel( hrt, *hrt->get_sub_recorder() );
	search_panel = new SearchPanel( hrt, 0, 0, 0, 0 );
	edit_panel = new EditPanel( hrt, 0, 0, 0, 0 );
	info_panel = new InfoPanel( hrt, 0, 0, 0, 0 );

	wdgact.AddWidget( click_panel, WidgetActInfo::METH_SHOW );
	wdgact.AddWidget( search_panel, WidgetActInfo::METH_SHOW );

	//the font, options and edit panels are hidden by default
	font_panel->hide();
	options_panel->hide();
	candy_panel->hide();
	edit_panel->hide();

	//init visibility from prefs
	if( !heart->get_prefs()->net_panel_visible ) net_panel->hide();
	if( !heart->get_prefs()->bb_panel_visible ) billboard_panel->hide();

	//layout
	panel_spacing = 15;
	v_border = 3;

	Fl_Widget* wrow[] = {	file_panel,
									search_panel,
									net_panel,
									billboard_panel,
									click_panel,
									NULL,
									font_panel,
									options_panel,
									candy_panel,
									NULL };

	end();

	//style
	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_PANEL, (Fl_Widget**)wrow );

	heart->get_style_engine()->assign_style( StylePrefs::TEXTOPTS_PANEL, font_panel );
	heart->get_style_engine()->assign_style( StylePrefs::MISCOPTS_PANEL, options_panel );
	heart->get_style_engine()->assign_style( StylePrefs::CANDYOPTS_PANEL, candy_panel );
	heart->get_style_engine()->assign_style( StylePrefs::BBOPTS_PANEL, billboard_panel );	//#756da9
	heart->get_style_engine()->assign_style( StylePrefs::NETOPTS_PANEL, net_panel );

	heart->get_style_engine()->assign_style( StylePrefs::RECORDER_PANEL, recorder_panel );

	heart->get_style_engine()->assign_style( StylePrefs::DASH_PANEL, this );

	//color( FL_GREEN );

	recorder_panel->hide();

	//refresh
	refresh_layout();
	refresh_style();
	wdgact.RefreshEnabled();
};

void DashPanel::font_panel_edits_bb( bool state )
{
	if( state ) {
		font_panel->set_cfg( detached_fp_cfg );
		font_panel->set_max_border( heart->get_bb_height() );
	} else {
		font_panel->set_cfg( attached_fp_cfg );
		font_panel->set_max_border( heart->get_title_panel()->h() );
	}
};

void DashPanel::EnableControls( unsigned long mask, bool enable )
{
	if( !enable && ( mask & SWITCH_SEARCHPANEL != 0 ) ) { edit_mode( 0 ); }
	wdgact.SetActivation( mask, enable );
}

/*
 * Set edit mode
 * parameters:
 *		state - 0..off, 1..on, 2..toggle
 */
void DashPanel::edit_mode( int state )
{
	//toggle
	if( state == 2 ) {
		state = edit_panel->visible() ? 0 : 1;
	}

	if( state == 0 ) {
		edit_panel->hide();
		search_panel->show();	
	} else {
		edit_panel->show();
		search_panel->hide();	
	}
	
	refresh_layout();
};

DashPanel::~DashPanel()
{
	delete attached_fp_cfg;
	delete detached_fp_cfg;
};

bool DashPanel::edit_mode()
{
	return edit_panel->visible();
}

void DashPanel::refresh_layout()
{
	Fl_Widget **warr = (Fl_Widget **)array();
	int warr_size = children();

	BodyLayout::layout_pack( warr, warr_size, x(), y(), w(), h(),
									true, 0,0,0,0,
									10, CL_ALIGN_EVEN );
}


bool DashPanel::get_size( int &W, int &H )
{
	Fl_Widget **warr = (Fl_Widget **)array();
	int warr_size = children();

	BodyLayout::pack_size( warr, warr_size, W, H,
									true, 0,0,0,0,
									10 );
	return true;
}

//===================== class MainPanel ===========================

MainPanel::MainPanel( Heart *hrt, int x, int y, int w, int h ) : ControlPanel( hrt, x, y, w, h, (const char *)"MAIN" )
{
	heart = hrt;;

	//dimensions
	scrollbar_height = 12; //12

	//create the scroll widget
	dash_scroll = new Fl_Scroll( 0, 0, 0, 500 );
	end();

	//setup the scroll widget
	dash_scroll->scrollbar_size( scrollbar_height );
	dash_scroll->clip_children( 1 );
	dash_scroll->type( Fl_Scroll::HORIZONTAL );
	dash_scroll->color( FL_BLACK );
	dash_scroll->box( FL_NO_BOX );
	dash_scroll->selection_color( FL_BLACK );

	//restyle the scrollbar
	Fl_Widget **arr = (Fl_Widget**)dash_scroll->array();
	Fl_Scrollbar *sbar = (Fl_Scrollbar *)arr[1];
	sbar->color( FL_BLACK );
	sbar->slider( FL_PLASTIC_UP_FRAME );
	sbar->labelcolor( FL_GRAY );
	sbar->clear_visible_focus();

	//create the dash and add it to the scroll widget
	dash = new DashPanel( hrt, 0, 0, 0, 0 );

	//add the dash panel
	dash_scroll->add( dash );

	//style
	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_PANEL, this );
	heart->get_style_engine()->assign_style( StylePrefs::SCROLL_PANEL, dash_scroll );

/*
	dash_scroll->color( FL_CYAN );
	dash_scroll->box( FL_NO_BOX );

	color( FL_MAGENTA);
	box( FL_FLAT_BOX );
	clip_children( 1 );
*/

	refresh_style();
	refresh_layout();
};

void MainPanel::refresh_layout()
{
	int W, H;

	dash->get_size( W, H );

	W = std::max( W, w() );

	dash->resize( dash->x(), dash->y(), W, H );

	dash_scroll->resize( x(), y(), w(), h() );

	LOG_RENDER_DETAIL2("W = %i, H = %i, dash->x() = %i, dash->y = %i, x() = %i, y() = %i, w() = %i, h() = %i\n",
								W, H, dash->x(), dash->y(), x(), y(), w(), h() ); 

};

bool MainPanel::get_size( int &W, int &H )
{
	//defaults. hopefully, dash->get_size() returns something sane.
	H = 100;
	W = 100;
	dash->get_size( W, H );

	H += scrollbar_height;

	LOG_RENDER_DETAIL2("W = %i, H = %i\n", W, H );

	return true;
};

//------------------------ class TotalPanel -----------------------------------

TotalPanel::TotalPanel( Heart *hrt, int x, int y, int w, int h ) : ControlPanel ( hrt, x, y, w, h, (const char *)"TOTAL" )
{
	LOG_RENDER_DETAIL2("constructing TotalPanel...\n");

	heart = hrt;;

	TitlePanelCfg	cfg( StylePrefs::TITLE_DISPLAY, &heart->get_prefs()->title_border_size );
	title_panel = new TitlePanel( hrt, 0, 0, 0, 0, cfg, (bool)heart->get_prefs()->on_top );
	main_panel = new MainPanel( hrt, 0, 0, 0, 0 );
	sublist_panel = new SubListPanel( hrt, 0, 0, 0, 0 );

	last_on_top = heart->get_prefs()->on_top;

	drag_bar = new DragLine( (double)heart->get_prefs()->title_panel_height,
										true,
										Fl_Color( 0x55555500 ), FL_RED );

	end();

	//drag-bar callback
	drag_bar->set_move_callback( &TotalPanel::dragbar_moved );

	heart->set_total_panel( this );

	min_mp_h = 50;
	max_mp_h = 200;
	rel_mp_h = 0.3;

	mp_aspect = 5;

	max_mp_w = 1000;
	min_mp_w = 600;

	//style
	heart->get_style_engine()->assign_style( StylePrefs::FOCUSING_PANEL, this );

	//refresh
	heart->refresh_subtitles();

	refresh_style();
	refresh_layout();

	LOG_RENDER_DETAIL2("TotalPanel finished!\n");
};

TotalPanel::~TotalPanel()
{
	//store preferences
	heart->get_prefs()->title_panel_height = drag_bar->get_position();

	//delete stuff
	delete drag_bar;
	delete title_panel;
	delete main_panel;
	delete sublist_panel;
};

void TotalPanel::dragbar_moved( DragLine *l )
{
	LOG_RENDER_DETAIL2("dragbar_moved()\n");
	( (TotalPanel *)(l->parent()) )->refresh_layout();
};

/*
 * This function handles the PUSH event in order to give focus to the TotalPanel,
 * which is useful to steal focus from Input widgets.
 */
int TotalPanel::handle( int event )
{
	LOG_GUI_DETAIL2("TotalPanel received an EVENT: %i (%s) \"%s\"\n", event, fl_eventnames[ event ],
		( event == FL_KEYDOWN ) ? Fl::event_text() : "<no text>");

	if( event == FL_FOCUS ) {
		LOG_GUI_DETAIL2("TOTAL: Yes, I ACCEPT FOCUS!!!\n");
		return 1;
	}

	//-- handle mouse clicks and FOCUS

	if( event == FL_PUSH )
		if( ( !Fl::belowmouse()->visible_focus() )||( Fl::belowmouse()==this ) )
		{
			LOG_GUI_DETAIL2("TOTAL: I Am STEALING FOCUS!!!\n");
			Fl::focus( this );
			return ControlPanel::handle( event );
		};

	//-- handle global keyboard shortcuts --

	if( heart->handle_global_shortcuts( event ) == 1 )
		return 1;
	else
		return ControlPanel::handle( event );
};

void TotalPanel::refresh_style()
{

	LOG_RENDER_DETAIL2("TotalPanel::refresh_style()\n");

	ControlPanel::refresh_style();
};

void TotalPanel::refresh_layout()
{
	int mph, mpw;
	int tph;
	int stph;

	LOG_RENDER_DETAIL2("TotalPanel::refresh_layout() starting...\n");

	//defaults
	mph = std::max( min_mp_h, std::min( (int)( rel_mp_h*(float)h() ), max_mp_h ) );
	mpw = 0;
	//get real size
	main_panel->get_size( mpw, mph );
	mpw = w();

	if( last_on_top != (bool)heart->get_prefs()->on_top )
	{
		//invert the drag line
		LOG_RENDER_DETAIL2(" inverting drag line...\n");
		drag_bar->invert();
		last_on_top = (bool)heart->get_prefs()->on_top;

		//change alignment of the title text
		title_panel->set_position( last_on_top );
	};

	if( heart->get_prefs()->on_top )
	{
		tph = drag_bar->get_abs_position();
		stph = h() - tph - mph - drag_bar->get_bar_width() - 2*border_size;

		title_panel->resize( 0, border_size, w(), tph );
		sublist_panel->resize( 0, border_size + tph + drag_bar->get_bar_width(), w(), stph );
		main_panel->resize( 0, h()-mph - border_size, mpw, mph );

		drag_bar->set_limits( 0.2, 1, 0, h()-mph );
	}
	else
	{
		tph = h() - drag_bar->get_abs_position() - drag_bar->get_bar_width();
		stph = h() - tph - mph - drag_bar->get_bar_width() - 2*border_size;

		main_panel->resize( 0, border_size, mpw, mph );
		sublist_panel->resize( 0, mph + border_size, w(), stph );
		title_panel->resize( 0, h()-tph-border_size, w(), tph );

		drag_bar->set_limits( 0, 0.8, mph, h() );
	};

	drag_bar->resize( 0, 0, 0, 0 );

	//adjust the value of the border slider as a percentage of the new title-panel height
	heart->totalpanel_geom_changed();

	LOG_RENDER_DETAIL2("mpw = %i, mph = %i, tph = %i, stph = %i\n", mpw, mph, tph, stph );

	redraw();
};
