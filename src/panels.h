/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Panels_h
#define _Panels_h

#include <map>
#include <list>
#include <climits>
#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Multiline_Input.H>
#include <FL/Fl_Scroll.H>
#include <FL/Fl_Spinner.H>
#include <FL/Fl_Color_Chooser.H>

#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "search_panel.h"
#include "title_panel.h"
#include "sub_list/sub_list_panel2.h"
#include "sub_click_panel.h"
#include "file_panel.h"
#include "font_panel.h"
#include "candy_panel.h"
#include "options_panel.h"
#include "edit_panel.h"
#include "net_panel.h"
#include "drag_line.h"
#include "info_panel.h"
#include	"billboard_panel.h"
#include "body_layout.h"
#include "rainbow.h"
#include "player/player_panel.h"
#include "recorder/recorder_panel.h"

class SubClickPanel;
class	SubListPanel;
class	ControlPanel;
class	Heart;

/*
 * the dashboard panel
 */
class DashPanel : public ControlPanel
{
	int	panel_spacing;
	int	v_border;

	WidgetActivator wdgact;

public:
	SubClickPanel	*click_panel;
	PlayerPanel		*player_panel;
	RecorderPanel	*recorder_panel;
	NetPanel			*net_panel;
	BillboardPanel	*billboard_panel;
	SearchPanel		*search_panel;
	FilePanel		*file_panel;

	AttachedFPCfg	*attached_fp_cfg;
	DetachedFPCfg	*detached_fp_cfg;
	FontPanel		*font_panel;

	OptionsPanel	*options_panel;
	EditPanel		*edit_panel;
	InfoPanel		*info_panel;
	CandyPanel		*candy_panel;

	DashPanel(Heart *hrt, int x, int y, int w, int h);
	~DashPanel();

	static const int SWITCH_CLICKPANEL = 1;
	static const int SWITCH_SEARCHPANEL = 2;

	void EnableControls( unsigned long mask, bool enable = true );

	void font_panel_edits_bb( bool state );

	/*
	 * Set edit mode
	 * parameters:
	 *		state - 0..off, 1..on, 2..toggle
	 */
	void edit_mode( int state );

	bool edit_mode();

	//virtual void resize (int x, int y, int w, int h);
	virtual void refresh_layout();
	bool get_size( int &W, int &H );
};

/*
 * the MainPanel class
 */
class MainPanel : public ControlPanel
{
	//SimpLE		*layout;

	int scrollbar_height;

public:
	Fl_Scroll	*dash_scroll;

	DashPanel	*dash;

	MainPanel( Heart *hrt, int x, int y, int w, int h );
	void refresh_layout();
	bool get_size( int &W, int &H );
};

/*
 * the TotalPanel class
 */
class TotalPanel : public ControlPanel
{
public:
	MainPanel		*main_panel;
	TitlePanel		*title_panel;
	SubListPanel	*sublist_panel;
	DragLine			*drag_bar;

private:
	static	const int	border_size = 4;

	bool	last_on_top;

	int min_mp_h;
	int max_mp_h;
	float rel_mp_h;

	float mp_aspect;
	int max_mp_w;
	int min_mp_w;

	void refresh_style()	;

	//callbacks
	static void dragbar_moved( DragLine *l );

public:
	TotalPanel( Heart *hrt, int x, int y, int w, int h );
	~TotalPanel();
	void refresh_layout();

	int handle( int event );
};

#endif //_Panels_h
