/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "parser.h"

#include "shared/src/default_log.h"
#include "../utf8_tools.h"

//============== PHARes =============

PHARes::PHARes( const char *str, int skipUtf8Count, bool _pop ) : pop(_pop)
{
	int charlen;
	int res = 0;

	while(( str[0] != 0 )&&( skipUtf8Count > 0 )) {
		charlen = fl_utf8len( str[0] );
		res += charlen;
		str += charlen;
		--skipUtf8Count;
	}
	skip = res;
};

//================= PHSet ====================

PHARes PHSet::act( const char *str, int pos ) {
	PHARes res;
	int matchLen;
	int l = handlers.size();
	int i;
	LOG_CANDY_DETAIL("Matching handlers at \"%s\"...\n", str);
	for( i = 0; i < l; ++i ) {
		matchLen =  handlers[ i ]->isMine( str, pos, *this );
		if( matchLen >= 0 ) {
			LOG_CANDY_DETAIL("Handler %i matches! pos = %i\n", i, pos);
			res = handlers[ i ]->action( str, pos, matchLen, *this );
			break;
		}
	}
	if( i == l ) {
		LOG_CANDY_DETAIL("No handler matched -> defaultAction().\n");
		res = defaultAction( str, pos, *this );
	}
	return res;
};

PHARes PHSet::defaultAction( const char *str, int pos, PHSet &phSet )
{
	throw std::invalid_argument( "no handler matches." );
	return PHARes();
};

// ============== Parser =================

void Parser::parse( const char *str, PHSetPtr handlerSet ) {
	PHStack phstack;
	PHSetPtr curSet = handlerSet;
	PHPtr handler;
	int matchLen;
	PHARes phaction;

	const char *orig_str = str;

	while( str && (str[0] != 0) && curSet ) {

		phaction = curSet->act( str, (int)(str - orig_str) );

		if( phaction.pop ) {
			if( phstack.size() ) {
				curSet = phstack.back();
				phstack.pop_back();
				LOG_CANDY_DETAIL("Popped one handler set.\n");
			} else {
				curSet.reset();
			}
		}

		if( phaction.setToPush ) {
			if( curSet ) {
				phstack.push_back( curSet );
			}
			curSet = phaction.setToPush;
			LOG_CANDY_DETAIL("Pushed one handler set.\n");
		}
		str += phaction.skip;
	};
};
