/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Parser_h_
#define _Parser_h_ 1

#include <vector>
#include <stdexcept>

//#include <memory>
#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>

class PHSet;
class PHARes;

typedef boost::shared_ptr<PHSet> PHSetPtr;
typedef std::vector<PHSetPtr> PHStack;

class PHARes {
public:
	PHARes( int _skip = -1, bool _pop = false ) : pop(_pop), skip(_skip) {}; //, setToPush(NULL)
	PHARes( const char *str, int skipCount, bool _pop = false );
	PHARes( PHSetPtr set, int _skip, bool _pop = false ) : setToPush(set), pop(_pop), skip(_skip) {};
	~PHARes() {};

	bool none() { return !pop && (skip < 0) && !setToPush; };
	void reset() { skip = -1; pop = false; setToPush.reset(); }; 

public:
	PHSetPtr setToPush;
	bool		pop;
	int		skip;
};

class PHandler {
protected:
	PHandler() {};
public:
	virtual ~PHandler() {};
	virtual int isMine( const char *str, int pos, PHSet &phSet ) = 0;
	virtual PHARes action( const char *str, int pos, int matchLen, PHSet &phSet ) = 0;
};

typedef boost::shared_ptr<PHandler> PHPtr;

#define PHACTIONDEF(_name,_parent) \
class _name : public _parent { \
public: \
	_name () {}; \
	~ _name () {}; \
	virtual PHARes action( const char *str, int pos, int matchLen, PHSet &set ); \
};

#define PHDEF(_name) \
class _name : public PHandler { \
public: \
	_name () {}; \
	~ _name () {}; \
	virtual int isMine( const char *str, int pos, PHSet &phSet ); \
	virtual PHARes action( const char *str, int pos, int matchLen, PHSet &set ); \
};

#define PHDEF1(_name,_vartype,_varname) \
class _name : public PHandler { \
public: \
	_name () {}; \
	~ _name () {}; \
	virtual int isMine( const char *str, int pos, PHSet &phSet ); \
	virtual PHARes action( const char *str, int pos, int matchLen, PHSet &set ); \
protected: \
	_vartype _varname ; \
};

class PHSet {
protected:
	PHSet() {};
	~PHSet() {};
protected:
	virtual PHARes defaultAction( const char *str, int pos, PHSet &phSet );
public:
	PHARes act( const char *str, int pos );
	int size() const { return handlers.size(); };
protected:
	std::vector<PHPtr> handlers;
};


class Parser {
public:
	Parser() {};
	~Parser() {};
	void parse( const char *str, PHSetPtr handlerSet );
};

#endif //_Parser_h_