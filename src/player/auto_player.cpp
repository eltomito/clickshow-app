/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../heart.h"
#include "../subtitles/subtitles.h"
#include "../subtitles/subtitle.h"
#include "auto_player.h"
#include "player_panel.h"
#include "player_timer.h"
#include "../clicky_debug.h"
#include "../panels.h"
#include <cfloat>

AutoPlayer::AutoPlayer( Heart &_heart ): heart(_heart), delay(0.0), new_delay(0.0), sendNextChangeToNet(false)
{
	timer = new SubtitlePlayerTimer( 0.1 );
	timer->SetPlayer( this );
}

bool AutoPlayer::Seek( double sec )
{
	if( !timer ) return false;
	if( !timer->Seek( sec ) ) return false;
	if( player_panel ) player_panel->set_tc( (long)(sec * (double)1000.0) );
	return true;
}

bool AutoPlayer::SeekToSub( const Subtitle &sub )
{
	if( IsRunning() ) return false;
	const SubTiming &t = sub.get_timing();
	if( !t.IsSet() ) {
		LOG_PLAYER_DETAIL("Seeking to na untimed subtitle.\n");
		if( player_panel ) player_panel->enablePlay( false );
		return false;
	}
	double s = (double)t.GetStart() / (double)1000.0;
	LOG_PLAYER_DETAIL("Seeking to a subtitle that starts at %lf sec.\n", s);
	if( player_panel ) player_panel->enablePlay( true );
	return Seek( s );
}

void AutoPlayer::SetTimer( const ClickyTime &_startTimestamp, double sec, bool sendToNet )
{
	if( timer ) {
		LOG_PLAYER_DETAIL("startTimestamp=%s, sec=%lf\n",_startTimestamp.ToString().c_str(), sec);
		timer->Set( _startTimestamp, sec );
		if( sendToNet ) sendMyStateToNet();
		if( player_panel ) player_panel->set_tc( (long)(timer->GetTimecode() * (double)1000.0) );
	}
}

bool AutoPlayer::IsRunning()
{
	return timer && timer->IsRunning();
}

/*
 * Can the player start playing from the current subtitle?
 * If so, what time code would it start at?
 */
bool AutoPlayer::CanStart( double *start_time )
{
	if( !timer ) return false;
	bool set_time = false;
	int sub_num = heart.get_shown_sub();

	LOG_PLAYER_DETAIL("Can I start from sub %i?\n", sub_num);

	if( !heart.is_valid_sub_num( sub_num ) || heart.get_subdoc()->get_subtitles()->get_subtitle( sub_num )->is_comment() ) {
		sub_num = heart.get_next_sub();
		if( !heart.is_valid_sub_num( sub_num ) ) {
			LOG_PLAYER_DETAIL("No, even the next sub %i is invalid.\n", sub_num);
			return false;
		}
		set_time = true;
	}
	Subtitles *subs = heart.get_subtitles();
	if( !subs ) {
		return false;
		LOG_PLAYER_DETAIL("No, no subtitles.\n");
	}
	Subtitle *sub = subs->get_subtitle( sub_num );
	if( !sub || !sub->get_timing().IsSet() ) return false;
	if( start_time ) {
		if( set_time ) {
			*start_time = (double)sub->get_timing().GetStart() / (double)1000.0;
			LOG_PLAYER_DETAIL("Yes! I can start from %lf!\n", *start_time );
		} else {
			*start_time = DBL_MAX;
			LOG_PLAYER_DETAIL("Yes! I can start from where I was!\n" );
		}
	} else {
		LOG_PLAYER_DETAIL("Yes!\n");
	}
	return true;
}

bool AutoPlayer::Start( bool sendToNet )
{
	if( IsRunning() ) return true;
	double start_time;
	if( !CanStart( &start_time ) ) return false;
	delay = 0.0;
	new_delay = 0.0;
	if( start_time != DBL_MAX ) {
		LOG_PLAYER_DETAIL("Seeking to new start time: %lf!\n", start_time );
		if( !timer->Seek( start_time ) ) {
			LOG_PLAYER_DETAIL("Failed to seek to %lf!\n", start_time );
			return false;
		}
	}
	sendNextChangeToNet = sendToNet;
	return timer->Start();
}

bool AutoPlayer::Pause( bool sendToNet )
{
	if( !timer ) return false;
	sendNextChangeToNet = sendToNet;
	return timer->Pause();
}

bool AutoPlayer::StartOrPause( bool sendToNet )
{
	if( !timer->IsRunning() ) {
		return Start( sendToNet );
	}
	return Pause( sendToNet );
}

void AutoPlayer::SetDelay( double sec, bool sendToNet, bool flash )
{
	LOG_PLAYER_DETAIL("Delay: %lf seconds.\n");
	new_delay = sec;
	if( flash && player_panel ) player_panel->flash_delay( (-1)*new_delay );
	if( sendToNet ) sendMyStateToNet();
}

void AutoPlayer::AddToDelay( double sec, bool sendToNet )
{
	new_delay += sec;
	if( player_panel ) player_panel->flash_delay( (-1)*sec );
	if( sendToNet ) sendMyStateToNet();
}

void AutoPlayer::SetSpeed( double factor, bool show, bool sendToNet )
{
	timer->SetSpeed( factor );
	if( show && player_panel ) player_panel->show_speed( factor );
	if( sendToNet ) sendMyStateToNet();
}

double AutoPlayer::GetSpeed()
{
	return timer->GetSpeed();
}

double AutoPlayer::GetTimecode()
{
	return timer->GetTimecode() - delay;
}

void AutoPlayer::tick( double sec )
{
	Subtitles *subs;
	Subtitle *sub;
	bool clear = false;

	bool search_back = new_delay > delay;
	delay = new_delay;
	sec -= delay;

	long tc = (long)( sec * (double)1000.0);
	if( player_panel ) player_panel->set_tc( tc );

	subs = heart.get_subtitles();
	if( !subs ) return;

	long rounded_tc = (long)( (sec + (timer->GetResolution() / 2 )) * (double)1000.0 );
	int shown_sub = heart.get_shown_sub();

	SubStatusT shown_ss = get_sub_status( *subs, shown_sub, rounded_tc );
	if(( shown_ss == PAST )||( shown_ss == FUTURE )) {
		clear = true;
		LOG_PLAYER_DETAIL("The displayed subtitle will be hidden: %i\n", shown_sub );
	}

	SubStatusT ss = UNKNOWN;

	int next_sub = heart.get_next_sub();
	int old_next_sub = next_sub;

	if( search_back ) {
		LOG_PLAYER_DETAIL2("backtracking from subtitle %i...\n", next_sub);
		next_sub = backtrack( *subs, next_sub, rounded_tc, ss );
		LOG_PLAYER_DETAIL2("backtracked to subtitle %i.\n", next_sub);
	}

	if(( !search_back )||( ss == PRESENT )) {
		LOG_PLAYER_DETAIL2("fast forwarding from subtitle %i...\n", next_sub);
		next_sub = fast_forward( *subs, next_sub, rounded_tc, ss );
		LOG_PLAYER_DETAIL2("fast forwarded to subtitle %i.\n", next_sub);
	}

	if(( ss == UNTIMED )&&( shown_ss != PRESENT )) {
		LOG_PLAYER_DETAIL2("Hit an untimed subtitle! %i\n", next_sub);
		Pause();
		if( player_panel ) {
			player_panel->warn_untimed();
			player_panel->enablePlay( false );
		}
		heart.show_manual_player( true );
		heart.enable_manual_player( true );
	}

	if(( ss == PRESENT )&&( next_sub != shown_sub )) {
		LOG_PLAYER_DETAIL("Showing new subtitle: %i\n", next_sub);
		clear = false;
		heart.show_list_sub( next_sub );
		Fl::awake();
	} else if( next_sub != old_next_sub ) {
		heart.go_to_sub( next_sub, false );
	}

	if( clear ) {
		LOG_PLAYER_DETAIL("Auto hiding subtitle.\n" );
		heart.hide_list_sub();
		if( next_sub >= subs->size() ) {
			Pause();
		}
	}
	Fl::awake();
}

AutoPlayer::SubStatusT AutoPlayer::get_sub_status( const Subtitle &sub, long tc )
{
	const SubTiming &st = sub.get_timing();
	if( !st.IsSet() ) { return UNTIMED; }
	if( st.GetEnd() <= tc ) { return PAST; }
	if( st.GetStart() <= tc ) { return PRESENT; }
	return FUTURE;
}

AutoPlayer::SubStatusT AutoPlayer::get_sub_status( Subtitles &subs, int n, long tc )
{
	if( n < 0 ) { return NONEXISTENT; }
	Subtitle *sub = subs.get_subtitle( n );
	if( !sub ) { return NONEXISTENT; }
	return get_sub_status( *sub, tc );
}

/** Backtrack to the lowest-indexed subtitle that's in the future or the present
 * @returns a valid subtitle index. It can be 0 but never out of range, unless start_num was out of range.
 */
int AutoPlayer::backtrack( Subtitles &subs, int start_num, long tc, SubStatusT &ss )
{
	int last_n = start_num;
	int n = last_n;
	SubStatusT last_ss = NONEXISTENT;
	ss = get_sub_status( subs, n, tc );
	while( ss == FUTURE ) {
		last_n = n;
		--n;
		ss = get_sub_status( subs, n, tc );
	}
	if( ss == PRESENT ) {
		return n;
	}
	ss = last_ss;
	return last_n;
}

int AutoPlayer::fast_forward( Subtitles &subs, int start_num, long tc, SubStatusT &ss )
{
	int n = start_num;
	int last_n = n;
	SubStatusT last_ss = NONEXISTENT;
	ss = get_sub_status( subs, n, tc );
	while(( ss == PAST )||( ss == PRESENT )) {
		last_n = n;
		last_ss = ss;
		++n;
		ss = get_sub_status( subs, n, tc );
	}
	if( last_ss == PRESENT ) {
		ss = last_ss;
		return last_n;
	}
	return n;
}

PlayerState &AutoPlayer::GetPlayerState( PlayerState &dst )
{
	dst.startTimestamp = timer->GetStartTimestamp();
	dst.startTimecode = timer->GetStartTimecode();
	dst.delay = GetDelay();
	dst.speed = timer->GetSpeed();
	dst.isPlaying = IsRunning();
	dst.shownSub = heart.get_shown_sub();
	dst.nextSub = heart.get_next_sub();
	return dst;
}

void AutoPlayer::SetPlayerStateFromNet( const PlayerState &ps, double extra_delay )
{
	PlayerState cs;
	GetPlayerState( cs );
	SetTimer( ps.startTimestamp, ps.startTimecode, false );
	SetSpeed( ps.speed, true, false );
	SetDelay( ps.delay + extra_delay, false );

	if( !cs.AlmostSameAs( ps ) ) {
		if( ps.shownSub < 0 ) {
			heart.go_to_sub( ps.nextSub, true );
		} else {
			heart.show_list_sub( ps.shownSub, false );
		}
	}

	if( ps.isPlaying ) {
		Start( false );
	} else {
		Pause( false );
	}
}

void AutoPlayer::timerStarted()
{
	LOG_PLAYER_DETAIL2("timer started!\n");
	if( player_panel ) player_panel->started();
	if( sendNextChangeToNet ) sendMyStateToNet();
	heart.get_dash_panel()->click_panel->deactivate();
}

void AutoPlayer::timerPaused()
{
	if( player_panel ) player_panel->paused();
	if( sendNextChangeToNet ) sendMyStateToNet();
	heart.get_dash_panel()->click_panel->activate();
}

void AutoPlayer::sendMyStateToNet()
{
	PlayerState state;
	GetPlayerState( state );
	heart.get_net_heart()->SendPlayerState( state );
}
