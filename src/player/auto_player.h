/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Auto_Player_h_
#define _Auto_Player_h_ 1

#include "timered_player.h"
#include "shared/src/player/player_state.h"

class Heart;
class SubtitlePlayerTimer;
class PlayerPanel;

class AutoPlayer : public TimeredPlayer
{
friend class Tape;
friend class SubtitlePlayerTimer;

public:
	AutoPlayer( Heart &_heart );
	~AutoPlayer() {};

//--- auto player

	void SetPlayerPanel( PlayerPanel *p ) { player_panel = p; };
	bool Start( bool sendToNet = true );
	bool Pause( bool sendToNet = true );
	bool StartOrPause( bool sendToNet = true );
	bool Seek( double sec );
	bool SeekToSub( const Subtitle &sub );
	void SetTimer( const ClickyTime &_startTimestamp, double sec, bool sendToNet = true );
	bool IsRunning();
	bool CanStart( double *start_time = NULL );
	void SetDelay( double sec = 0, bool sendToNet = true, bool flash = false );
	void AddToDelay( double sec, bool sendToNet = true );
	double GetDelay() { return new_delay; };

	void SetSpeed( double factor = 1.0, bool show = true, bool sendToNet = true );
	double GetSpeed();
	double GetTimecode();

	PlayerState &GetPlayerState( PlayerState &dst );
	void SetPlayerStateFromNet( const PlayerState &ps, double extra_delay = 0.0 );

	void tick( double sec );
	void timerStarted();
	void timerPaused();

protected:

	enum SubStatusT {
		UNKNOWN = -3,
		NONEXISTENT = -2,
		UNTIMED = -1,
		PAST = 0,
		PRESENT,
		FUTURE
	};

	SubStatusT get_sub_status( const Subtitle &sub, long tc );
	SubStatusT get_sub_status( Subtitles &subs, int n, long tc );
	int backtrack( Subtitles &subs, int start_num, long tc, SubStatusT &ss );
	int fast_forward( Subtitles &subs, int start_num, long tc, SubStatusT &ss );
	void sendMyStateToNet();

protected:
	Heart		&heart;
	double	delay;
	double	new_delay;

	bool		sendNextChangeToNet;

	SubtitlePlayerTimer	*timer;
	PlayerPanel				*player_panel;
};

#endif //_Player_h_
