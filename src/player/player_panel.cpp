/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../heart.h"
#include "player_panel.h"
#include "auto_player.h"
#include "../clicky_debug.h"
#include "../subtitle_formats/timing/srt.h"

void PTShifter::action( double inc )
{
	panel.delay( inc );
}

//---- button callbacks ----

void PlayerPanel::cb_playpause(Fl_Button* b, void *)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->clicked_playpause();
	LOG_PLAYER_DETAIL("Clicked Play/Pause!\n" );
};

void PlayerPanel::cb_up(Fl_Button* b, void *panel)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->heart->clicked_go( -1 );
	p->heart->focus_default_widget();
	LOG_GEN_DETAIL2("Clicked up!!\n" );
};

void PlayerPanel::cb_down(Fl_Button* b, void*)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->heart->clicked_go( 1 );
	p->heart->focus_default_widget();
	LOG_GEN_DETAIL2("Clicked down!\n" );
};

void PlayerPanel::cb_manual(Fl_Button* b, void*)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->heart->show_sub_click_panel( 2 );
};

void PlayerPanel::cb_expand(Fl_Button* b, void*)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->toggle_expand();
};

void PlayerPanel::cb_speed(Fl_Button* b, void*)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->heart->get_auto_player()->SetSpeed( ((Fl_Spinner *)b)->value(), false, true );
	p->heart->focus_default_widget();
};

void PlayerPanel::cb_speed1(Fl_Button* b, void*)
{
	PlayerPanel *p = (PlayerPanel *)b->parent();
	p->spinner_speed->value( 1.0 );
	p->heart->get_auto_player()->SetSpeed( 1.0, false, true );
};

//---- constructor ----

PlayerPanel::PlayerPanel(Heart *hrt, AutoPlayer &_player, int x, int y, int w, int h)
: ControlPanel ( hrt, x, y, w, h, (const char *)"PLAYER" ), player(_player), flash_countdown(0), expanded(false)
{
	layout_basic = new SimpLE();
	layout_expanded = new SimpLE();

	//size_range( 150, 100, 450, 300, 1, 1, 0 );

	tc_label[0] = 0;
	button_tc = new ClickyButton( 0,0,0,0, tc_label );
	button_playpause = new ClickyButton( 0,0,0,0, _("Play") );
	shifter = new PTShifter( *this, heart );
	button_up = new ClickyRepeatButton( 0,0,0,0, NULL);		//"@2<-"
	button_down = new ClickyRepeatButton( 0,0,0,0, NULL);		//"@2<-"
	button_manual = new ClickyButton( 0,0,0,0, NULL);
	button_expand = new ClickyButton( 0,0,0,0, ">");
	button_speedlabel = new ClickyButton( 0,0,0,0, _("Speed") );
	spinner_speed = new Fl_Spinner( 0,0,0,0, NULL );
	button_speed1 = new ClickyButton( 0,0,0,0, "1" );
	end();	//to stop adding widgets to this group

	spinner_speed->type( FL_FLOAT_INPUT );
	spinner_speed->range( 0.00000001, 1000000 );
	spinner_speed->value( 1.0 );
	spinner_speed->align( FL_ALIGN_LEFT );
	spinner_speed->step( 0.001 );
	spinner_speed->format("%.4f");

	//tooltips
	button_playpause->tooltip( _("Play or pause timed subtitles.") );
	button_up->tooltip( _("Browse subtitles up without displaying any.") );
	button_down->tooltip( _("Browse subtitles down without displaying any.") );
	button_manual->tooltip( _("Show or hide manual controls.") );
	button_expand->tooltip( _("Show/hide speed controls.") );
	spinner_speed->tooltip(_("Slow down or speed up the subtitles.\n1.0 is the normal speed."));
	button_speed1->tooltip(_("Set subtitle speed to 1."));

	button_playpause->callback((Fl_Callback*)cb_playpause);
	button_up->callback((Fl_Callback*)cb_up);
	button_down->callback((Fl_Callback*)cb_down);
	button_manual->callback((Fl_Callback*)cb_manual);
	button_expand->callback((Fl_Callback*)cb_expand);
	spinner_speed->callback((Fl_Callback*)cb_speed);
	button_speed1->callback((Fl_Callback*)cb_speed1);

	//-- layout --
	generate_layout( layout_basic, false );
	generate_layout( layout_expanded, true );

	//-- style --
	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_BUTTON, button_tc );
	heart->get_style_engine()->assign_style( StylePrefs::GOUP_BUTTON, button_up );
	heart->get_style_engine()->assign_style( StylePrefs::GODOWN_BUTTON, button_down );
	heart->get_style_engine()->assign_style( StylePrefs::NO_BORDER_BUTTON, shifter );
	heart->get_style_engine()->assign_style( StylePrefs::HAND_BUTTON, button_manual );
	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_BUTTON, button_expand );
	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_BUTTON, spinner_speed );
	heart->get_style_engine()->assign_style( StylePrefs::NO_BORDER_BUTTON, button_speedlabel );
	heart->get_style_engine()->assign_style( StylePrefs::NORMAL_BUTTON, button_speed1 );
	button_speedlabel->align( FL_ALIGN_INSIDE|FL_ALIGN_LEFT );

	//-- init button states --
	set_playing_look( false );
	set_tc( NO_TC );

	player.SetPlayerPanel( this );

	//-- refresh --

	refresh_expansion( false );
	refresh_layout();
	refresh_style();
};

PlayerPanel::~PlayerPanel()
{
 	delete layout_basic;
 	layout_basic = NULL;
	delete layout_expanded;
 	layout_expanded = NULL;
}

void PlayerPanel::toggle_expand()
{
	expanded = !expanded;
	refresh_expansion();
}

void PlayerPanel::refresh_expansion( bool refresh_dash )
{
	button_expand->label( expanded ? "<":">" );
	if( expanded ) {
		button_speedlabel->show();
		spinner_speed->show();
		button_speed1->show();
	} else {
		button_speedlabel->hide();
		spinner_speed->hide();
		button_speed1->hide();
	}
	if( refresh_dash ) heart->refresh_dashboard( true );
}

void PlayerPanel::delay( double inc )
{
	player.AddToDelay( inc );
}

void PlayerPanel::show_speed( double factor )
{
	spinner_speed->value( factor );
}

void PlayerPanel::set_playing_look( bool state )
{
	if( state ) {
		button_playpause->copy_label(_("Pause"));
		heart->get_style_prefs()->PauseImgB->apply( button_playpause );
		button_up->deactivate();
		button_down->deactivate();
		shifter->activate();
		refresh_layout();
	} else {
		button_playpause->copy_label(_("Play"));
		heart->get_style_prefs()->PlayImgB->apply( button_playpause );
		button_up->activate();
		button_down->activate();
		shifter->deactivate();
		refresh_layout();
	}
}

bool PlayerPanel::get_size( int &w, int &h )
{
	int	fh;

	fh = heart->get_style_prefs()->NormalButton->ls.Size;
	h = fh*6;
	w = expanded ? h*3*21/16 : h*3;
	return true;
}


void PlayerPanel::generate_layout( SimpLE *layout, bool wide )
{
	int cols = wide ? 21 : 16;

	static const int pixx = 5;
	static const int pixy = 0;
	static const int marginw = 0;
	static const int marginh = 0;
	static const int spacex = 4;
	static const int spacey = 4;

	layout->add_cell( button_tc, 4, 0, 8, 2,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_up, 0, 0, 2, 2,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_down, 2, 0, 2, 2,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_playpause, 0, 2, 4, 4,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( shifter, 4, 2, 12, 4,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_manual, 12, 0, 2, 2,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_expand, 14, 0, 2, 2,	//cx, cy, cw, ch
										cols, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces
	if( wide ) {
		layout->add_cell( button_speedlabel, 16*4+2, 0, 5*4-2, 2,	//cx, cy, cw, ch
											cols*4, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces
	
		layout->add_cell( spinner_speed, 16*4+2, 2, 5*4-2, 2,	//cx, cy, cw, ch
											cols*4, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces
	
		layout->add_cell( button_speed1, 16*4+2, 4, 2*4-2, 2,	//cx, cy, cw, ch
											cols*4, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces
	}
}

void PlayerPanel::refresh_layout()
{
	if( !expanded ) {
		layout_basic->refresh_layout( this );
	} else {
		layout_expanded->refresh_layout( this );
	}
}

void PlayerPanel::clicked_playpause()
{
	player.StartOrPause();
}

void PlayerPanel::set_tc( long milli )
{
	LOG_PLAYER_DETAIL2("TC: %li\n", milli);

	if( milli == NO_TC ) {
		sprintf( tc_label, "--:--:--,---" );
	} else {
		int h, m, s, mm;
		bool plus = true;
		if( milli < 0 ) {
			plus = false;
			milli = (-1)*milli;
		}
		SrtParser::TimeToParts( (int)milli, h, m, s, mm );
		sprintf( tc_label, "%s%02i:%02i:%02i,%03i", plus ? "" : "-", h, m, s, mm );
	}
	if( flash_countdown > 0 ) {
		--flash_countdown;
		return;
	}
	button_tc->label( tc_label );
}

void PlayerPanel::flash_delay( double sec )
{
	LOG_PLAYER_DETAIL2("delay change: %lf\n", sec);

	sprintf( flash_label, "%+.1lf", sec );
	button_tc->label( flash_label );
	flash_countdown = 2;
}

void PlayerPanel::started()
{
	LOG_PLAYER_INFO("Player STARTED!\n");
	set_playing_look( true );
}
void PlayerPanel::paused()
{
	LOG_PLAYER_INFO("Player PAUSED!\n");
	if( flash_countdown > 0 ) {
		flash_countdown = 0;
		button_tc->label( tc_label );
	}
	set_playing_look( false );
}
void PlayerPanel::enablePlay( bool state )
{
	LOG_PLAYER_INFO("Player %s\n", state ? "ENABLED!" : "DISABLED :(");
	if( state ) {
		button_playpause->activate();
	} else {
		button_playpause->deactivate();
	}
}
void PlayerPanel::warn_untimed( bool state )
{
	LOG_PLAYER_INFO("Warning! UNTIMED subtitles! :? :O :x\n");
}
