/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Player_Panel_h_
#define _Player_Panel_h_ 1

#include <FL/Fl_Repeat_Button.H>

#include "../clicky_button.h"
#include "../control_panel.h"
#include "../simple_layout_engine.h"
#include "../controls/time_shifter.h"
#include <climits>
#include <FL/Fl_Repeat_Button.H>
#include <FL/Fl_Spinner.H>

class Heart;
class AutoPlayer;
class PlayerPanel;

class PTShifter : public TimeShifter
{
private:
	PlayerPanel &panel;
public:
	PTShifter( PlayerPanel &tp, Heart *_heart, int style_paddle = StylePrefs::NORMAL_BUTTON, int style_label = StylePrefs::NO_BORDER_BUTTON )
	: TimeShifter(_heart,style_paddle,style_label), panel( tp ) {};
	~PTShifter() {};

	void action( double inc );
};

/*
 * the clicker panel class
 */
class PlayerPanel : public ControlPanel
{
	SimpLE	*layout_basic;
	SimpLE	*layout_expanded;

	AutoPlayer &player;

	ClickyButton			*button_tc;
	ClickyButton			*button_playpause;
	PTShifter				*shifter;
	ClickyRepeatButton	*button_up;
	ClickyRepeatButton	*button_down;
	ClickyButton			*button_manual;
	ClickyButton			*button_expand;
	Fl_Spinner				*spinner_speed;
	ClickyButton			*button_speedlabel;
	ClickyButton			*button_speed1;

	const static int margin_v = 2;
	const static int margin_h = 2;
	const static int space_h = 1;
	const static int space_v = 5;

	static void cb_playpause(Fl_Button* b, void*);
	static void cb_up(Fl_Button* b, void*);
	static void cb_down(Fl_Button* b, void*);
	static void cb_manual(Fl_Button* b, void*);
	static void cb_expand(Fl_Button* b, void*);
	static void cb_speed(Fl_Button* b, void*);
	static void cb_speed1(Fl_Button* b, void*);

	void generate_layout( SimpLE *layout, bool wide = false );
	void toggle_expand();
	void refresh_expansion( bool refresh_dash = true );

public:
	static const long NO_TC = LONG_MIN;

	PlayerPanel(Heart *hrt, AutoPlayer &_player, int x = 0, int y = 0, int w = 0, int h = 0);
    ~PlayerPanel();

	void set_tc( long milli );
	void flash_delay( double sec );
	void show_speed( double factor );
	void started();
	void paused();
	void enablePlay( bool state = true );
	void warn_untimed( bool state = true );

	void refresh_layout();
	bool get_size( int &w, int &h );

	void delay( double sec );

private:
	char tc_label[64];
	char flash_label[64];
	void set_playing_look( bool state );
	void clicked_playpause();
	int flash_countdown;
	bool expanded;
};

#endif //_Player_Panel_h_
