/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Player_Timer_h_
#define _Player_Timer_h_ 1

#include "../subtitles/subtitles.h"
#include <ctime>
#include "shared/src/clicky_time.h"

class Tape
{
public:
	enum Type {
		MANAGED = 0,
		UNMANAGED
	};

	virtual bool IsRunning() { return false; };
	virtual double GetResolution() { return 0.0; };
	Type GetType() { return type; };
	virtual double GetTimecode() { return 0; }; 

protected:
	Tape( Type t ) : type( t ) {};
	~Tape() {};

protected:
	Type type;
	virtual void callbackTick( double timecode ) {};
	virtual void callbackStarted() {};
	virtual void callbackPaused() {};
};

class ManagedTape : public Tape
{
public:
	virtual bool Seek( double sec = 0 ) { return false; };
	virtual bool Start() { return false; };
	virtual bool Pause() { return false; };
	virtual void Reset() {};

protected:
	ManagedTape() : Tape( Type::MANAGED ) {};
	~ManagedTape() {};
};

class TimerTape : public ManagedTape
{
public:
	TimerTape( double _resolution = 0.1 ) : resolution(_resolution), isTimerRunning(false), speed(1.0) { reset(); };
	~TimerTape() {}

	bool IsRunning() { return isTimerRunning; };

	void Reset() {
		if( IsRunning() ) { Pause(); };
		reset();
	};

	bool Seek( double sec = 0 ) { start_timestamp.SetToNow(); start_timecode = sec; return true; };
	void Set( const ClickyTime &_startTimestamp, double sec ) { start_timestamp = _startTimestamp; start_timecode = sec; };

	bool Start() {
		if( !isTimerRunning ) {
			start_timestamp.SetToNow();
			isTimerRunning = true;
			Fl::add_timeout( resolution, &TimerTape::timerHandler, (void*)this );
			Fl::awake( &TimerTape::awakeStartHandler, (void *)this );
		}
		return true;
	};

	bool Pause() {
		if( isTimerRunning ) {
			start_timecode = GetTimecode();
			start_timestamp.SetToNow();
			isTimerRunning = false;
			Fl::awake( &TimerTape::awakePauseHandler, (void *)this );
		}
		return true;
	};

	double GetResolution() { return resolution; };

	double GetTimecode() {
		ClickyTime now_time( true );
		double tc = start_timecode + ((double)(now_time - start_timestamp) / (double)1000.0) * speed;
		return tc;
	};

	void SetSpeed( double factor ) { Seek( GetTimecode() ); speed = factor; };
	double GetSpeed() { return speed; };

	const ClickyTime &GetStartTimestamp() const { return start_timestamp; };
	double GetStartTimecode() const { return start_timecode; };

protected:
	static void timerHandler( void *data ) {
		TimerTape *tt = (TimerTape *)data;
		if( tt->isTimerRunning ) {
			Fl::repeat_timeout( tt->resolution, &TimerTape::timerHandler, data );
		}
		Fl::awake( &TimerTape::awakeTickHandler, data );
	};

	static void awakeTickHandler( void *data ) {
		TimerTape *tt = (TimerTape *)data;
		tt->callbackTick( tt->GetTimecode() );
	};

	static void awakeStartHandler( void *data ) {
		TimerTape *tt = (TimerTape *)data;
		tt->callbackStarted();
	};

	static void awakePauseHandler( void *data ) {
		TimerTape *tt = (TimerTape *)data;
		tt->callbackPaused();
	};

	void reset() {
		start_timestamp.SetToInvalid();
		start_timecode = 0.0;
	};

protected:
	ClickyTime start_timestamp;
	double start_timecode;
	double resolution;
	double speed;
	bool isTimerRunning;
};

class AutoPlayer;

class SubtitlePlayerTimer : public TimerTape
{
public:
	SubtitlePlayerTimer( double _resolution ) : TimerTape( _resolution ), player(NULL) {};
	~SubtitlePlayerTimer() {};

	void SetPlayer( TimeredPlayer *p ) { player = p; };

protected:
	void callbackTick( double timecode ) {
		if( player ) { player->tick( timecode ); }
	};
	void callbackStarted() {
		if( player ) { player->timerStarted(); }
	};
	void callbackPaused() {
		if( player ) { player->timerPaused(); }
	};

protected:
	TimeredPlayer *player;
};

#endif //_Player_Timer_h_
