/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "prefs.h"

#include "clicky_debug.h"
#include "config.h"
#include <locale>

//const char Prefs::AboutText[] = ABOUT_CLICKY_SHORT;

#define LOAD_PREFS_INT( PREF_NAME, VAR_NAME ) cnt += ( 0!= flp->get( PREF_NAME, (int &)VAR_NAME, (int)VAR_NAME ) );
#define SAVE_PREFS_INT( PREF_NAME, VAR_NAME ) cnt += ( 0!= flp->set( PREF_NAME, (int)VAR_NAME ) );

Prefs::Prefs( const char *vendor, const char *app )
{
	flp = new Fl_Preferences( Fl_Preferences::USER, vendor, app );
	reset_to_defaults();
};

Prefs::~Prefs()
{
	delete flp;
};

void Prefs::reset_to_defaults()
{
	protocol_version = 2;

	load_path = "";

	with_spaces = true;
	title_panel_height = 0.3;

	on_top = true;
	full_screen_mode = true;
	subtitle_numbers = false;
	allow_blank_subs = true;
	
	parse_candy_colors = 3;	//strict
	candy_colors[0] = FL_GREEN;
	candy_colors[1] = FL_YELLOW;
	candy_colors[2] = FL_CYAN;
	candy_colors[3] = FL_WHITE;
	candy_colors[4] = FL_GRAY;
	candy_colors[5] = FL_MAGENTA;
	candy_colors[6] = FL_RED;
	candy_colors[7] = FL_BLUE;

	title_font_size = 30;
	title_font_color = (unsigned long)0xffff0000;
	title_font = FL_HELVETICA;
	title_border_size = 0;

	bb_title_font_size = 40;
	bb_title_font_color = (unsigned long)0xffffff00;
	bb_title_font = FL_HELVETICA;
	bb_title_border_size = 0;

	list_font_size = 24;
	list_font_color = (unsigned long)FL_GREEN;
	list_font_comment_color = (unsigned long)FL_MAGENTA;

	ui_font_size = 16;

	get_default_window_dims( main_window_x, main_window_y, main_window_width, main_window_height );

	//-- directories
	load_path = "";

	//-- language
	language_name = "";
	std::string sysLangName = std::locale("").name();
	if(	( 0 == sysLangName.compare(0, 2, "cs", 2) )
	  	||	( 0 == sysLangName.compare(0, 5, "Czech", 5 ) )
	  	) {
		language_name = "Česky";
  	}

	//-- net
	net_default_server = "";
	net_default_login = "";
	net_default_pwd = "";
	net_default_venue = "";
	
	//-- bb
	bb_visible = false;
	bb_fullscreen = true;
	bb_screen_num = -1;
	bb_window_x = 0;
	bb_window_y = 0;
	bb_window_width = -1;
	bb_window_height = -1;
	bb_ontop = true;

	//-- panel visibility
	net_panel_visible = -1;
	bb_panel_visible = false;

	//-- version
	version_major = -1;
	version_minor = -1;

};

//--load, save --

/*
 * returns: 0 for failure, 1 for success
 */
int Prefs::load_prefs_string( const char *key_name, std::string &str )
{
	char	*value;

	if( 0 != flp->get( key_name, value, str.c_str() ) )
	{
		str = value;
		free( value );
		return 1;
	};
	return 0;
};

int Prefs::read_candy_colors( int how_many )
{
	int cnt = 0;
	char prefname[ 32 ];
	for( int i = 0; i < how_many; ++i ) {
		sprintf( prefname, "candy-color-%i", i + 1 );
		cnt += ( 0!= flp->get( prefname, (int &)candy_colors[i], (int)candy_colors[i] ) );
	}
	return cnt;
}

int Prefs::write_candy_colors( int how_many )
{
	int cnt = 0;
	char prefname[ 32 ];
	for( int i = 0; i < how_many; ++i ) {
		sprintf( prefname, "candy-color-%i", i + 1 );
		cnt += ( 0!= flp->set( prefname, (int)candy_colors[i] ) );
	}
	return cnt;
}

/*
 * returns: the number of preference values actually loaded
 */
int Prefs::load()
{
	int cnt = 0;

	reset_to_defaults();

	cnt += load_prefs_string( "load-path", load_path );

	cnt += ( 0!= flp->get( "use-candy-colors", (int &)parse_candy_colors, (int)parse_candy_colors ) );

	int candy_color_count = def_candy_color_count;
	cnt += ( 0!= flp->get( "candy-color-count", (int &)candy_color_count, (int)candy_color_count ) );
	candy_color_count = (candy_color_count > def_candy_color_count) ? def_candy_color_count : candy_color_count;
	read_candy_colors( candy_color_count );

	cnt += ( 0!= flp->get( "with-spaces", (int &)with_spaces, (int)with_spaces ) );
	cnt += ( 0!= flp->get( "title-height", (double &)title_panel_height, (double )title_panel_height ) );

	cnt += ( 0!= flp->get( "title-font-size", (int &)title_font_size, (int)title_font_size ) );
	cnt += ( 0!= flp->get( "title-font-color", (int &)title_font_color, (int)title_font_color ) );
	cnt += ( 0!= flp->get( "title-font", (int &)title_font, (int)title_font ) );
	cnt += ( 0!= flp->get( "title-border-size", (int &)title_border_size, (int)title_border_size ) );

	cnt += ( 0!= flp->get( "bb-title-font-size", (int &)bb_title_font_size, (int)bb_title_font_size ) );
	cnt += ( 0!= flp->get( "bb-title-font-color", (int &)bb_title_font_color, (int)bb_title_font_color ) );
	cnt += ( 0!= flp->get( "bb-title-font", (int &)bb_title_font, (int)bb_title_font ) );
	cnt += ( 0!= flp->get( "bb-title-border-size", (int &)bb_title_border_size, (int)bb_title_border_size ) );

	cnt += ( 0!= flp->get( "list-font-size", (int &)list_font_size, (int)list_font_size ) );
	cnt += ( 0!= flp->get( "list-font-color", (int &)list_font_color, (int)list_font_color ) );
	cnt += ( 0!= flp->get( "list-font-comment-color", (int &)list_font_comment_color, (int)list_font_comment_color ) );

	cnt += ( 0!= flp->get( "ui-font-size", (int &)ui_font_size, (int)ui_font_size ) );

	cnt += ( 0!= flp->get( "main-window-x", (int &)main_window_x, (int)main_window_x ) );
	cnt += ( 0!= flp->get( "main-window-y", (int &)main_window_y, (int)main_window_y ) );
	cnt += ( 0!= flp->get( "main-window-width", (int &)main_window_width, (int)main_window_width ) );
	cnt += ( 0!= flp->get( "main-window-height", (int &)main_window_height, (int)main_window_height ) );

	cnt += ( 0!= flp->get( "on-top", (int &)on_top, (int)on_top ) );
	cnt += ( 0!= flp->get( "subtitle-numbers", (int &)subtitle_numbers, (int)subtitle_numbers ) );
	cnt += ( 0!= flp->get( "allow-blank-subs", (int &)allow_blank_subs, (int)allow_blank_subs ) );

	cnt += load_prefs_string( "language", language_name );

	cnt += load_prefs_string( "net-default-server", net_default_server );
	cnt += load_prefs_string( "net-default-login", net_default_login );
	cnt += load_prefs_string( "net-default-pwd", net_default_pwd );
	cnt += load_prefs_string( "net-default-venue", net_default_venue );

	LOAD_PREFS_INT( "bb-visible", bb_visible )
	LOAD_PREFS_INT( "bb-fullscreen", bb_fullscreen )
	LOAD_PREFS_INT( "bb-ontop", bb_ontop )
	LOAD_PREFS_INT( "bb-screen-num", bb_screen_num )
	LOAD_PREFS_INT( "bb-window-x", bb_window_x )
	LOAD_PREFS_INT( "bb-window-y", bb_window_y )
	LOAD_PREFS_INT( "bb-window-width", bb_window_width )
	LOAD_PREFS_INT( "bb-window-height", bb_window_height )
	LOAD_PREFS_INT( "net-panel-visible", net_panel_visible )
	LOAD_PREFS_INT( "bb-panel-visible", bb_panel_visible )
	LOAD_PREFS_INT( "version-major", version_major )
	LOAD_PREFS_INT( "version-minor", version_minor )

	//updates from old versions
	if(( version_major < VERSION_MAJOR )
		||( ( version_major == VERSION_MAJOR )&&( version_minor < VERSION_MINOR ) ))
	{
		update_from_old_version();
	}
	version_major = VERSION_MAJOR;
	version_minor = VERSION_MINOR;

	//corrections
	if( net_panel_visible == -1 ) {
		if(	!net_default_server.empty() ||
				!net_default_login.empty() ||
				!net_default_pwd.empty() )
		{
			net_panel_visible = 1;
		} else {
			net_panel_visible = 0;
		}
	}

	return cnt;
};

/*
 * Things to do if the preferences store on this system
 * come from an older version of ClickShow than this one.
 * The exact version should be in this->version_major and this->version_minor.
 * These two values can also be -1 if the stored prefs are unversioned,
 * i.e., very old.
 */
void Prefs::update_from_old_version()
{
	LOG_GEN_INFO("The stored preferences come from an %s version (%i.%i).\n",
	((version_major < 0 ) ? "unknown, probably older":"older"), version_major, version_minor);
	//change the AUTO candy color setting to STRICT
	if( parse_candy_colors == 2 ) {
		parse_candy_colors = 3;
		LOG_GEN_INFO("Changed candy color settings from AUTO to STRICT.\n");
	}
}

/*
 * returns the number of save attempts failed.
 * i.e., 0 indicates success.
 */
int Prefs::save()
{
	int cnt = 0;

	cnt += ( 0 == flp->set( "load-path", load_path.c_str() ) );

	cnt += ( 0 == flp->set( "use-candy-colors", (int)parse_candy_colors ) );
	cnt += ( 0 == flp->set( "candy-color-count", (int)def_candy_color_count ) );
	write_candy_colors( def_candy_color_count );

	cnt += ( 0 == flp->set( "with-spaces", (int)with_spaces ) );
	cnt += ( 0 == flp->set( "title-height", (double)title_panel_height ) );

	cnt += ( 0 == flp->set( "title-font-size", (int)title_font_size ) );
	cnt += ( 0 == flp->set( "title-font-color", (int)title_font_color ) );
	cnt += ( 0 == flp->set( "title-font", (int)title_font ) );
	cnt += ( 0 == flp->set( "title-border-size", (int)title_border_size ) );

	cnt += ( 0 == flp->set( "bb-title-font-size", (int)bb_title_font_size ) );
	cnt += ( 0 == flp->set( "bb-title-font-color", (int)bb_title_font_color ) );
	cnt += ( 0 == flp->set( "bb-title-font", (int)bb_title_font ) );
	cnt += ( 0 == flp->set( "bb-title-border-size", (int)bb_title_border_size ) );

	cnt += ( 0!= flp->set( "list-font-size", (int)list_font_size ) );
	cnt += ( 0!= flp->set( "list-font-color", (int)list_font_color ) );
	cnt += ( 0!= flp->set( "list-font-comment-color", (int)list_font_comment_color ) );

	cnt += ( 0!= flp->set( "ui-font-size", (int)ui_font_size ) );

	cnt += ( 0!= flp->set( "main-window-x", (int)main_window_x ) );
	cnt += ( 0!= flp->set( "main-window-y", (int)main_window_y ) );
	cnt += ( 0!= flp->set( "main-window-width", (int)main_window_width ) );
	cnt += ( 0!= flp->set( "main-window-height", (int)main_window_height ) );

	cnt += ( 0!= flp->set( "on-top", (int)on_top ) );
	cnt += ( 0!= flp->set( "subtitle-numbers", (int)subtitle_numbers ) );
	cnt += ( 0!= flp->set( "allow-blank-subs", (int)allow_blank_subs ) );

	cnt += ( 0 == flp->set( "language", language_name.c_str() ) );
	cnt += ( 0 == flp->set( "net-default-server", net_default_server.c_str() ) );
	cnt += ( 0 == flp->set( "net-default-login", net_default_login.c_str() ) );
	cnt += ( 0 == flp->set( "net-default-pwd", net_default_pwd.c_str() ) );
	cnt += ( 0 == flp->set( "net-default-venue", net_default_venue.c_str() ) );

	SAVE_PREFS_INT( "bb-visible", bb_visible )
	SAVE_PREFS_INT( "bb-fullscreen", bb_fullscreen )
	SAVE_PREFS_INT( "bb-ontop", bb_ontop )
	SAVE_PREFS_INT( "bb-screen-num", bb_screen_num )
	SAVE_PREFS_INT( "bb-window-x", bb_window_x )
	SAVE_PREFS_INT( "bb-window-y", bb_window_y )
	SAVE_PREFS_INT( "bb-window-width", bb_window_width )
	SAVE_PREFS_INT( "bb-window-height", bb_window_height )
	SAVE_PREFS_INT( "net-panel-visible", net_panel_visible )
	SAVE_PREFS_INT( "bb-panel-visible", bb_panel_visible )
	SAVE_PREFS_INT( "version-major", version_major )
	SAVE_PREFS_INT( "version-minor", version_minor )

	return cnt;
};

//-- helper functions --

void Prefs::get_default_window_dims( int &X, int &Y, int &W, int &H )
{
	int	sx, sy, sw, sh;

	Fl::screen_work_area( sx, sy, sw, sh );

	W = std::min( (9*sw/10), 800 );
	H = std::min( (9*sh/10), 600 );

	X = sx + (sw - W)/2;
	Y = sy + (sh - H)/2;
};

/*
 * returns true if the dimensions were okay
 *	returns false if the dimensions needed a correction.
 */
bool Prefs::fix_window_dims( int &X, int &Y, int &W, int &H )
{
	int	sx, sy, sw, sh;
	bool retval = true;

	Fl::screen_work_area( sx, sy, sw, sh );

	LOG_GEN_INFO("screen area: x %i, y %i, w %i, h %i\n", sx, sy, sw, sh );
	LOG_GEN_INFO("window: x %i, y %i, w %i, h %i\n", X, Y, W, H );

	if( X < sx )
	{
		retval = false;
		X = sx;
	};
	if( Y < sy )
	{
		retval = false;
		Y = sy;
	};
	if( W > sw )
	{
		retval = false;
		W = sw;
	};
	if( H > sh )
	{
		retval = false;
		H = sh;
	};
	if( (X+W) > (sx+sw) )
	{
		retval = false;
		X = sx + (sw-W)/2;
	};
	if( (Y+H) > (sy+sh) )
	{
		retval = false;
		Y = sy + (sh-H)/2;
	};
	return retval;
};

void Prefs::store_window_dims( Fl_Window *w )
{
	main_window_x = w->x();
	main_window_y = w->y();
	main_window_width = w->w();
	main_window_height = w->h();

	LOG_GEN_INFO("Prefs stored window dimensions x %i, y %i, w %i, h %i\n", main_window_x, main_window_y, main_window_width, main_window_height );

};

void Prefs::recall_window_dims( int &X, int &Y, int &W, int &H )
{

	X = main_window_x;
	Y = main_window_y;
	W = main_window_width;
	H = main_window_height;

	LOG_GEN_INFO("Prefs recalled window dimensions x %i, y %i, w %i, h %i\n", X, Y, W, H );

};
