/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Prefs_h
#define _Prefs_h

#include <FL/Fl.H>
#include	<FL/Fl_Preferences.H>
#include <FL/Fl_Window.H>

#include <string>

#include "candy_color_constants.h"
#include "clicky_version.h"

class Prefs
{
	Fl_Preferences	*flp;

	public:

	//static const char AboutText[];

	static const int def_candy_color_count	= CLICKY_CANDY_COLOR_COUNT;

	//-- preference values --		

	int protocol_version; //never saved, never loaded. Only changed from the cmdline.

	//-- behavior
	int			with_spaces;
	int			on_top;
	int			full_screen_mode;
	int			subtitle_numbers;
	int			allow_blank_subs;

	//subtitle rendering
	int				parse_candy_colors;		//0 - off, 1 - on, 2 - auto
	Fl_Color			candy_colors[ def_candy_color_count ];

	//-- dimensions
	double		title_panel_height;

	//-- fonts and sizes
	int					title_font_size;
	unsigned int		title_font_color;
	int					title_font;
	int					title_border_size;

	int					bb_title_font_size;
	unsigned int		bb_title_font_color;
	int					bb_title_font;
	int					bb_title_border_size;

	int					list_font_size;
	unsigned long		list_font_color;
	unsigned long		list_font_comment_color;

	int					ui_font_size;

	int					main_window_x;
	int					main_window_y;
	int					main_window_width;
	int					main_window_height;

	//-- language
	std::string		language_name;

	//-- directories
	std::string			load_path;
	
	//-- net
	std::string			net_default_server;
	std::string			net_default_login;
	std::string			net_default_pwd;
	std::string			net_default_venue;

	//-- billboard
	int	bb_visible;
	int	bb_fullscreen;
	int	bb_screen_num;
	int	bb_window_x;
	int	bb_window_y;
	int	bb_window_width;
	int	bb_window_height;
	int	bb_ontop;

	//-- default panel visibility
	int	net_panel_visible;
	int	bb_panel_visible;

	//-- version
	int	version_major;
	int	version_minor;

	//-- constructor, destructor --
	Prefs( const char *vendor, const char *app );
	~Prefs();

	//-- defaults --
	void reset_to_defaults();

	//--load, save --
	int load_prefs_string( const char *key_name, std::string &str );

	int load();
	int save();

	//-- helper functions --
	void get_default_window_dims( int &X, int &Y, int &W, int &H );
	bool fix_window_dims( int &X, int &Y, int &W, int &H );
	void store_window_dims( Fl_Window *w );
	void recall_window_dims( int &X, int &Y, int &W, int &H );

private:

	void update_from_old_version();
	int read_candy_colors( int how_many );
	int write_candy_colors( int how_many );

/*
	int read_prefs( PrefsEntry *e );
	int write_prefs( PrefsEntry *e );
*/
};

#endif //_Prefs_h
