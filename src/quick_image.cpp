/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "quick_image.h"

#include <FL/fl_draw.H>

void QuickImage::make_line( QINTCmd &ic, int xoff, int yoff, double rw, double rh,
								double x1, double y1, double x2, double y2 ) 
{
	double X1, Y1, X2, Y2;

	rough_rotate_xy( rot_quarters, x1, y1, X1, Y1 );
	rough_rotate_xy( rot_quarters, x2, y2, X2, Y2 );

	ic.x1 = xoff + (int)( rw*X1 );
	ic.y1 = yoff + (int)( rh*Y1 );
	ic.x2 = xoff + (int)( rw*X2 );
	ic.y2 = yoff + (int)( rh*Y2 );
};

void QuickImage::rough_rotate_xy( int quarters, double x, double y, double &X, double &Y )
{
	switch( quarters )
	{
		case 0:
			X = x;
			Y = y;
		break;
		
		case 1:
			X = (double)1 - y;
			Y = x;
		break;

		case 2:
			X = (double)1 -x;
			Y = (double)1 -y;
		break;

		case 3:
			X = (double)1 - y;
			Y = (double)1 - x;
		break;

		default:
			X = x;
			Y = y;
		};
}

void QuickImage::findAutoScale( const QICmd *f, double *shift_x, double *shift_y, double *scale_x, double *scale_y )
{
	double min_x = MAX_X;
	double max_x = MIN_X;
	double min_y = MAX_Y;
	double max_y = MIN_Y;
	double left = f->x1;
	double right = f->y1;
	double top = f->x2;
	double bottom = f->y2;

	++f;
	while( f->cmd != END ) {
		switch( f->cmd )
		{
			case LINE:
				min_x = f->x1 < min_x ? f->x1 : min_x;
				min_x = f->x2 < min_x ? f->x2 : min_x;
				min_y = f->y1 < min_y ? f->y1 : min_y;
				min_y = f->y2 < min_y ? f->y2 : min_y;
				max_x = f->x1 > max_x ? f->x1 : max_x;
				max_x = f->x2 > max_x ? f->x2 : max_x;
				max_y = f->y1 > max_y ? f->y1 : max_y;
				max_y = f->y2 > max_y ? f->y2 : max_y;
			break;
		}
		++f;		
	}

	double prescale_x = (max_x == min_x) ? 1 : ( 1 / ( max_x - min_x ) );
	double prescale_y = (max_y == min_y) ? 1 : ( 1 / ( max_y - min_y ) );
	double scale = (prescale_x < prescale_y) ? prescale_x : prescale_y;
	*scale_x = scale;
	*scale_y = scale;
	*shift_x = min_x * (*scale_x) * (-1.0);
	*shift_y = min_y * (*scale_y) * (-1.0);
}

void QuickImage::intize_formula( const QICmd *f )
{
	QINTCmd	ic;
	int	xoff, yoff;
	double rw, rh;

	bool	mirror_v = false;
	bool	mirror_h = false;
	bool	mirror_nw = false;
	bool	mirror_ne = false;

	double shift_x = 0;
	double shift_y = 0;
	double scale_x = 1; //scale is applied first
	double scale_y = 1;

	double fx1, fy1, fx2, fy2;

	if( alignment&QI_RIGHT )
		xoff = space_h;
	else if( alignment&QI_HCENTER )
		xoff = space_h / 2;
	else
		xoff = 0;

	if( alignment&QI_BOTTOM )
		yoff = space_v;
	else if( alignment&QI_VCENTER )
		yoff = space_v / 2;
	else
		yoff = 0;

	rw = w() - space_h;
	rh = h() - space_v;

	formula_int.clear();
	
	if( f!= NULL )
	{
		while( f->cmd != END )
		{
			switch( f->cmd )
			{
				case LINE:
					fx1 = f->x1 * scale_x + shift_x;
					fy1 = f->y1 * scale_y + shift_y;
					fx2 = f->x2 * scale_x + shift_x;
					fy2 = f->y2 * scale_y + shift_y;

					ic.cmd = LINE;
					make_line( ic, xoff, yoff, rw, rh, fx1, fy1, fx2, fy2 );
					formula_int.push_back( ic );
					if( mirror_v )
					{
						make_line( ic, xoff, yoff, rw, rh, fx1, (double)1 - fy1, fx2,  (double)1 - fy2 );
						formula_int.push_back( ic );
					};
					if( mirror_h )
					{
						make_line( ic, xoff, yoff, rw, rh, (double)1 - fx1, fy1, (double)1 - fx2, fy2 );
						formula_int.push_back( ic );
					};
					if( mirror_nw )
					{
						make_line( ic, xoff, yoff, rw, rh, (double)1 - fy1, (double)1 - fx1, (double)1 - fy2, (double)1 - fx2 );
						formula_int.push_back( ic );
					};
					if( mirror_ne )
					{
						make_line( ic, xoff, yoff, rw, rh, fy1, fx1, fy2,  fx2 );
						formula_int.push_back( ic );
					};

				break;
				
				case MIRROR:
					mirror_v = (f->x1 >= 1);
					mirror_h = (f->y1 >= 1);
					mirror_nw = (f->x2 >= 1);
					mirror_ne = (f->y2 >= 1);
				break;
				
				case SCALE:
					shift_x = f->x1;
					shift_y = f->y1;
					scale_x = f->x2;
					scale_y = f->y2;
				break;

				case AUTOSCALE:
					findAutoScale( f, &shift_x, &shift_y, &scale_x, &scale_y );
				break;

			};
			++f;
		};
	};
};

QuickImage::QuickImage( int w, int h, Fl_Color clr, const QICmd *f,
								int hspace, int vspace, int align, int rotate )
							: Fl_Image( w+hspace, h+vspace, 4 )
{
	color = clr;
	space_h = hspace;
	space_v = vspace;
	alignment = align;

	rot_quarters = rotate;

	if( f != NULL )
		intize_formula( f );
	else
		formula_int.clear();
};

QuickImage::QuickImage( const QuickImage &other )
	: Fl_Image( other.w(), other.h(), 4 )
{
	color = other.color;
	space_h = other.space_h;
	space_v = other.space_v;
	alignment = other.alignment;
	rot_quarters = other.rot_quarters;
	formula_int = other.formula_int;
}

Fl_Image *QuickImage::copy( int W, int H )
{
	return new QuickImage( *this );
}

void QuickImage::draw( int X,
		int  	Y,
		int  	W,
		int  	H,
		int  	cx,
		int  	cy )
{
	Fl_Color	last_color;
	
	last_color = fl_color();
	fl_color( color );

//-- draw the int formula --
	QINTCmd *qc;

	for( int i=0; i < (int)formula_int.size(); i++ )
	{
		qc = &formula_int[i];

		if( qc->cmd == LINE )
			fl_line( X+cx+qc->x1, Y+cy+qc->y1, X+cx+qc->x2, Y+cy+qc->y2 );
	};

// restore original color

	fl_color( last_color );
};
