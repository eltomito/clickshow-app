/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _QuickImage_h
#define _QuickImage_h

#include <FL/Fl.H>
#include <FL/Fl_Image.H>

#include <vector>

struct QICmd
{
	int		cmd;
	double		x1, y1, x2, y2;
};

struct QINTCmd
{
	int		cmd;
	int	x1, y1, x2, y2;
};

class QuickImage : public Fl_Image
{
	typedef std::vector<QINTCmd>	QIC_T;

	Fl_Color	color;
	QIC_T		formula_int;

	int	space_h;
	int	space_v;
	int	alignment;

	int	rot_quarters;

	void make_line( QINTCmd &ic, int xoff, int yoff, double rw, double rh,
								double x1, double y1, double x2, double y2 );

	void rough_rotate_xy( int quarters, double x, double y, double &X, double &Y );

	void findAutoScale( const QICmd *f, double *shift_x, double *shift_y, double *scale_x, double *scale_y );

#define MIN_X -10000
#define MAX_X 10000
#define MIN_Y -10000
#define MAX_Y 10000

	public:

	void intize_formula( const QICmd *f );

	QuickImage( int w, int h, Fl_Color clr, const QICmd *f,
					int hspace=0, int vspace=0, int align=0, int rotate=0 );

	QuickImage( const QuickImage &other );

	void draw( int X,
		int  	Y,
		int  	W,
		int  	H,
		int  	cx = 0,
		int  	cy = 0 );

	void set_color( Fl_Color clr ) { color = clr; };
	Fl_Color get_color() { return color; };

	Fl_Image *copy( int W, int H );
	void desaturate() { color = FL_GRAY; };

	static	const int	END = 0;
	static	const int	LINE = 1;
	static	const int	MIRROR = 2;	//vertical, horizontal, NW/SE, NE/SW (if x1, y1, x2, y2 >= 1 respectively )
	static	const int	SCALE = 3; //shift_x, shift_y, scale_x, scale_y. Scale is applied first.
	static	const int	AUTOSCALE = 4; //borders: left, right, top, bottom

	static	const int QI_TOP = 0;
	static	const int QI_BOTTOM = 1;
	static	const int QI_VCENTER = 2;

	static	const int QI_LEFT = 0;
	static	const int QI_RIGHT = 4;
	static	const int QI_HCENTER = 8;
	
	static	const int QI_CENTER = QI_VCENTER|QI_HCENTER;
};

#endif	//_QuickImage_h