/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "rainbow.h"

#include <cassert>
#include <algorithm>

#include <FL/fl_utf8.h>
#include <FL/fl_draw.H>

#include "utf8_tools.h"
#include "clicky_debug.h"
#include "rainbow_converter.h"

void RainbowWalker::start( int _startPos, int _endPos, const RainbowStyle &initStyle ) {
	startPos = _startPos;
	endPos = _endPos;
	style = initStyle;
	markupIndex = -1;
}

bool RainbowWalker::next() {
	if( markupIndex >= (int)markup.size() ) {
		//markup index past the end means we've already done everything
		return false;
	}

	if( markupIndex < 0 ) {
		//we're working out the very first markup span
		style = RainbowMarkupUtils::StyleAtPos( startPos, markup, style );
		style.start( 0 );
		markupIndex = RainbowMarkupUtils::FindFirstAfterPos( startPos, markup );
		if( markupIndex < 0 ) {
			//there is no markup after startPos, so return startPos to endPos.
			spanLen = endPos - startPos;
			markupIndex = markup.size(); //no more spans after this one.
			return true;
		}
		if( markup[ markupIndex ].start() == startPos ) {
			//markup starts right at startPos.
			//The span we return is from startPos to the beginning of the next markup
			//if there is one.
			if( (markupIndex + 1) < markup.size() ) {
				//there's at least one more span after this one
				spanLen = std::min( markup[ markupIndex + 1 ].start(), endPos ) - startPos;
			} else {
				//this will be the last span returned
				spanLen = endPos - startPos;
			}
			++markupIndex;
			return true;
		}
		//the markup starts after startPos,
		//so let's return the span up to the beginning of this markup
		spanLen =  std::min( markup[ markupIndex ].start(), endPos ) - startPos;
		//don't increment markupIndex.
		//The next span will start right at markup[ markupIndex ].start().
		return true;
	}

	//markupIndex points to the markup span we should return now.
	int s = markup[ markupIndex ].start();
	if( s > endPos ) {
		//the markup starts past the end of the requested range.
		//There's nothing to return.
		markupIndex = markup.size();
		return false;
	}
	style.start( s - startPos );
	//apply all the styles that are stacked at this position
	while(( markupIndex < markup.size() )&&( markup[ markupIndex ].start() == s )) {
		style.applyToMe( markup[ markupIndex ] );
		++markupIndex;
	}
	//spanLen is either to the beginning of the next style
	//or to endPos
	if( markupIndex < markup.size() ) {
		spanLen = std::min( markup[ markupIndex ].start(), endPos ) - s;
	} else {
		spanLen = endPos - s;
	}
	return true;
}

/** same as normal strchr() but returns the pointer
 * to the terminating 0 if c not found.
 */
const char *rainbow_strchr2( const char *str, char c ) {
	while(( str[0] != c )&&( str[0] != 0 )) {
		++str;
	}
	return str;
};

/** Set fl_font() to the inital value before calling this
 * @param markup_pos - the offset of str in markup or in other words.
 *                     the length of the string before str if markup
 *                     refers to the whole string.
 * @param initStyle - is in and out. It's left set to the final style.
 * @returns pointer to the '\n' or 0 byte at the end of the measured string.
 */
const char *rainbow_measure_to_eol2(	int &w, int &h, const char *str, const RainbowMarkup &markup, RainbowStyle &initStyle,
																			int markup_pos, const Fl_Font *faces ) {
	w = 0;
	h = 0;
	int pW;
	int pH = 0;
	if( str == NULL ) return NULL;
	std::string tmp;
	const char *eolpos = rainbow_strchr2( str, '\n' );
	int maxLen = eolpos - str;

	LOG_CANDY_DETAIL("- - - measuring to eol - - - str =\n\"%s\"\npos = %i\nmarkup:\n%s\n", str, markup_pos, RainbowMarkupUtils::toString( markup ).c_str() );

	RainbowWalker walk( markup );
	LOG_CANDY_DETAIL("Walking the markup: startPos: %i, endPos: %i \n", markup_pos, markup_pos + maxLen );
	walk.start( markup_pos, markup_pos + maxLen, initStyle );
	while( walk.next() ) {

		LOG_CANDY_DETAIL("Next: start: %i, len: %i, style: %s\n", walk.style.start(), walk.spanLen, walk.style.toString().c_str() );

		walk.style.applyToFL( NULL, 0, faces );
		assert( walk.spanLen >= 0 );
		tmp.assign( str + walk.style.start(), walk.spanLen );
		pW = 0;
		fl_measure( tmp.c_str(), pW, pH, 0 );
		w += pW;
		h = (pH > h) ? pH : h;
	}

	LOG_CANDY_DETAIL("Done walking the markup!\n\n- - - done measuring to eol - - -\n");

	initStyle = walk.style;
	return eolpos;
};

/**
 * @param width, height - are output only
 * @param initStyle is in and out
 */
const char *rainbow_draw_to_eol(	const char *str, const RainbowMarkup &markup,
										int left_x, int top_y, int &width, int &height,
										const Fl_Color *palette, Fl_Color color255, int startPos,
										const Fl_Font *faces, RainbowStyle &initStyle )
{
	int w = 0;
	int h = 0;
	int pW;
	int pH = 0;
	if( str == NULL ) return NULL;
	std::string tmp;
	const char *eolpos = rainbow_strchr2( str, '\n' );
	int maxLen = eolpos - str;

	LOG_CANDY_DETAIL("- - - drawing to eol - - - str =\n\"%s\"\nmaxlen = %i\nmarkup:\n%s\n", str, maxLen, RainbowMarkupUtils::toString( markup ).c_str() );

	int x = left_x;
	int y = top_y;

	RainbowWalker walk( markup );
	walk.start( startPos, startPos + maxLen, initStyle );
	while( walk.next() ) {
		walk.style.applyToFL( palette, color255, faces );
		assert( walk.spanLen >= 0 );
		tmp.assign( str + walk.style.start(), walk.spanLen );
		pW = 0;
		fl_measure( tmp.c_str(), pW, pH, 0 );
		w += pW;
		h = (pH > h) ? pH : h;

		fl_draw( tmp.c_str(), x, top_y );

		if( walk.style.underline() || walk.style.strike() ) {
			int lineWidth = 1 + fl_size() / 32;
			fl_line_style( FL_SOLID, lineWidth );
			if( walk.style.underline() ) {
				int underY = top_y + lineWidth;
				fl_line( x, underY, x + pW, underY );
			};
			if( walk.style.strike() ) {
				int strikeY = top_y + fl_descent() - pH/2; 
				fl_line( x, strikeY, x + pW, strikeY );
			};
		}

		x += pW;
	}
	
	LOG_CANDY_DETAIL("- - - done drawing to eol - - -\n");

	initStyle = walk.style;
	width = w;
	height = h;
	return eolpos;
};

void rainbow_draw(	const char *str, const RainbowMarkup &markup,
										int left_x, int top_y, int w, int h, Fl_Align alignment,
										const Fl_Color *palette, Fl_Color color255, const Fl_Font *faces,
										RainbowStyle &initStyle )
{
	int x = left_x;
	int y = top_y;
	int yoff;
	int rowW = 0;
	int rowH = 0;
	int totalW = 0;
	int totalH = 0;

	LOG_CANDY_DETAIL("-- drawing -- str:\n\"%s\"\nmarkup:\n%s\n", str, RainbowMarkupUtils::toString( markup ).c_str() );

	RainbowStyle startStyle = initStyle;

	rainbow_measure( str, markup, totalW, totalH, faces, startStyle );
	startStyle = initStyle;
	rainbow_measure_to_eol2( rowW, rowH, str, markup, startStyle, 0, faces );
	yoff = rowH - fl_descent(); //if different font sizes are used,
	                            //maximum fl_descent will be needed.

	if( alignment & FL_ALIGN_BOTTOM ) {
		y = top_y + h - totalH;
	}

	if( alignment & FL_ALIGN_RIGHT ) {
		x = left_x + w - rowW;
	} else if( 0 == (alignment & FL_ALIGN_LEFT) ) { //center
		x = left_x + (w - rowW) / 2;
	}

	//initStyle can finally be updated to the style at the end of the 1st line.
	const char *s = rainbow_draw_to_eol( str, markup, x, y + yoff, rowW, rowH, palette, color255, 0, faces, initStyle );
	while( s[0] != 0 ) {
		++s;
		y += rowH;
		rowH = 0;
		rowW = 0;

		startStyle = initStyle;
		rainbow_measure_to_eol2( rowW, rowH, s, markup, startStyle, s - str, faces );
		if( alignment & FL_ALIGN_RIGHT ) {
			x = left_x + w - rowW;
		} else if( 0 == (alignment & FL_ALIGN_LEFT) ) {
			x = left_x + (w - rowW) / 2;
		}

		rowH = 0;
		rowW = 0;
		//updating initStyle again
		s = rainbow_draw_to_eol( s, markup, x, y + yoff, rowW, rowH, palette, color255, s - str, faces, initStyle );
	}
	LOG_CANDY_DETAIL("-- done drawing --\n");
};

/**
 * @param initStyle is in and out
 */
const char *rainbow_convert_to_eol(	const char *str, const RainbowMarkup &markup,
										int startPos, RainbowStyle &initStyle, RainbowConverter &rc )
{
	if( str == NULL ) return NULL;
	std::string tmp;
	const char *eolpos = rainbow_strchr2( str, '\n' );
	int maxLen = eolpos - str;

	LOG_CANDY_DETAIL("drawing to eol str =\n\"%s\"\nmaxlen = %i\nmarkup:\n%s\n", str, maxLen, RainbowMarkupUtils::toString( markup ).c_str() );

	RainbowWalker walk( markup );
	walk.start( startPos, startPos + maxLen, initStyle );
	while( walk.next() ) {
		rc.applyStyle( walk.style );
		assert( walk.spanLen >= 0 );
		tmp.assign( str + walk.style.start(), walk.spanLen );
		rc.appendText( tmp.c_str() );
	}
	initStyle = walk.style;
	return eolpos;
};

void rainbow_convert(	const char *str, const RainbowMarkup &markup,
											RainbowStyle &initStyle, RainbowConverter &rc )
{
	RainbowStyle startStyle = initStyle;
	//initStyle can finally be updated to the style at the end of the 1st line.
	rc.startLine();
	const char *s = rainbow_convert_to_eol( str, markup, 0, initStyle, rc );
	rc.endLine();
	while( s[0] != 0 ) {
		++s;
		startStyle = initStyle;
		//updating initStyle again
		rc.startLine();
		s = rainbow_convert_to_eol( s, markup, s - str, initStyle, rc );
		rc.endLine();
	}
};

void rainbow_measure( const char *str, const RainbowMarkup &markup, int &w, int &h,
											const Fl_Font *faces, RainbowStyle &initStyle )
{
	w = 0;
	h = 0;
	int linew, lineh;
	const char *s = str;

	LOG_CANDY_DETAIL("-- measuring -- str:\n\"%s\"\nmarkup:\n%s\n", str, RainbowMarkupUtils::toString( markup ).c_str() );

	while( s[0] != 0 ) {
		s = rainbow_measure_to_eol2( linew, lineh, s, markup, initStyle, s - str, faces );
		if( s[0] == '\n' ) { ++s; }
		w = (linew > w) ? linew : w;
		h += lineh;
	}

	LOG_CANDY_DETAIL("-- done measuring --\n");
}

void RainbowStyle::AppendToColorSet( ColorSet &dst, Fl_Color *src, int len, bool resolve_fl_colors )
{
	Fl_Color clr;
	for( int i = 0; i < len; ++i ) {
		clr = src[0];
		if( resolve_fl_colors && (clr < 256 ) ) {
			clr = Fl::get_color( clr );
		}
		dst.push_back( clr );
		++src;
	}
};
