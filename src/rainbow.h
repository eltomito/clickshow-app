/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Rainbow_h
#define _Rainbow_h 1

#include <cstdlib>
#include <vector>
#include <cstring>

#include <FL/Fl.H>
#include <FL/fl_utf8.h>

#include "rainbow_style.h"
#include "rainbow_markup.h"
class RainbowConverter;

void rainbow_draw(	const char *str, const RainbowMarkup &markup,
										int left_x, int top_y, int w, int h, Fl_Align alignment,
										const Fl_Color *palette, Fl_Color color255,
										const Fl_Font *faces, RainbowStyle &initStyle );

void rainbow_measure( const char *str, const RainbowMarkup &markup, int &w, int &h,
											const Fl_Font *faces, RainbowStyle &initStyle );

void rainbow_convert(	const char *str, const RainbowMarkup &markup,
											RainbowStyle &initStyle, RainbowConverter &rc );


class RainbowWalker {
public:
	RainbowWalker( const RainbowMarkup &m ) : markup(m) {};
	~RainbowWalker() {};

	void start( int _startPos, int _endPos, const RainbowStyle &initStyle );
	bool next();

public:
 	RainbowStyle style;
	int spanLen;
	const RainbowMarkup &markup;

protected:
	int startPos;
	int endPos;
	int markupIndex;
};

#endif //_Rainbow_h
