#ifndef _rainbow_converter_h_
#define _rainbow_converter_h_ 1

#include "rainbow_style.h"

#include "../../shared/src/str_utils.h"

/** Objects of this class are passed to rainbow_convert methods
 * and represent converters to particular formats, e.g., srt tags.
 */
class RainbowConverter {
protected:
	RainbowConverter() {};
public:
	virtual ~RainbowConverter() {};
	virtual void appendText( const char *str ) {};
	virtual void applyStyle( const RainbowStyle &style ) {};
	virtual void startSub() {};
	virtual void startLine() {};
	virtual void endLine() {};
};

class SrtTagsRC : public RainbowConverter {
public:
	SrtTagsRC( const ColorSet &_palette, Fl_Color _color255 )
	: palette(_palette), color255(_color255), curStyle(0,0,0,0) {};
	~SrtTagsRC() {};

	void appendText( const char *str ) {
		text.append( str );
	};
	
	void applyStyle( const RainbowStyle &newStyle ) {
		if( ( !newStyle.useColor() &&( curStyle.useColor() ))
			||( newStyle.useColor() && curStyle.useColor() && ( newStyle.color() != curStyle.color() )))
		{
			text.append("</font>");
		}
		if( newStyle.useColor() && ( !curStyle.useColor() || ( curStyle.color() != newStyle.color() )) )
		{
			StrUtils::Printf( text, "<font color=\"#%s\">", colorToString( newStyle.color() ).c_str() );
		}
		if( newStyle.italic() != curStyle.italic() ) {
			StrUtils::Printf( text, "<%si>", newStyle.italic() ? "" : "/" );
		}
		if( newStyle.bold() != curStyle.bold() ) {
			StrUtils::Printf( text, "<%sb>", newStyle.bold() ? "" : "/" );
		}
		if( newStyle.underline() != curStyle.underline() ) {
			StrUtils::Printf( text, "<%su>", newStyle.underline() ? "" : "/" );
		}
		if( newStyle.strike() != curStyle.strike() ) {
			StrUtils::Printf( text, "<%ss>", newStyle.strike() ? "" : "/" );
		}
		curStyle.applyToMe( newStyle );
	};

	void startSub() { text.clear(); };
	virtual void startLine() {
		if( !text.empty() ) { text.push_back('\n'); }
	};
	virtual void endLine() {
		if( text.empty() ) { return; }
		if( curStyle.useColor() ) { text.append("</font>"); }
		if( curStyle.italic() ) { text.append("</i>"); }
		if( curStyle.bold() ) { text.append("</b>"); }
		if( curStyle.underline() ) { text.append("</u>"); }
		if( curStyle.strike() ) { text.append("</s>"); }
		curStyle.use( 0 );
	};

	const std::string &getText() const { return text; };

	std::string colorToString( Fl_Color c ) {
		std::string res;

		if( c == 255 ) {
			c = color255;
		} else if( c < palette.size() ) {
			c = palette[ c ];
		}
		c = c >> 8;
		StrUtils::Printf(res, "%06x", c);
		return res;
	};

protected:
	std::string text;
	ColorSet palette;
	Fl_Color color255;
	RainbowStyle curStyle;
};

#endif //_rainbow_converter_h_
