/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Rainbow_Markup_h
#define _Rainbow_Markup_h 1

#include "rainbow_style.h"
#include "../../shared/src/str_utils.h"

typedef std::vector<RainbowStyle> RainbowMarkup;

class RainbowMarkupUtils
{
public:

	static bool AreSame( const RainbowMarkup &m, const RainbowMarkup &n )
	{
		if( m.size() != n.size() ) { return false; }
		int len = m.size();
		for( int i = 0; i < len; ++i ) {
			if( m[i] != n[i] ) { return false; }
		}
		return true;
	};

	/*
	 * @param start_index - start searching m from this index.
	 * @returns The index at which the new style was inserted
	 */
	static int Insert( RainbowMarkup &m, const RainbowStyle &s, int start_index = 0 )
	{
		int size = m.size();
		int start = s.start();
		for( int i = start_index; i < size; ++i ) {
			if( m[i].start() > start ) {
				RainbowMarkup::iterator it = m.begin() + i;
				m.insert( it, s );
				return i;
			}
		}
		m.push_back( s );
		return size;
	};

	/** finds the index of the first style that starts at or after pos.
	 * @returns style index in markup or -1 if none found.
	 */
	static int FindFirstAfterPos( int pos, const RainbowMarkup &m ) {
		int i = 0;
		int l = m.size();
		while(( i < l )&&( m[i].start() < pos )) {
			++i;
		}
		return ( i == l ) ? -1 : i;
	};

	static RainbowStyle FinalStyle( const RainbowMarkup &m, const RainbowStyle &defaultStyle ) {
		size_t i = m.size();
		RainbowStyle style;
		while(( i > 0 )&&( !style.isComplete() )) {
			--i;
			style.setDefaults( m[i] );
		}
		style.setDefaults( defaultStyle );
		return style;
	}

	static RainbowStyle StyleAtPos( size_t pos, const RainbowMarkup &m, const RainbowStyle &defaultStyle ) {
		size_t l = m.size();
		size_t i = 0;
		RainbowStyle style;
		while(( i < l )&&( m[ i ].start() <= pos )) {
			style.applyToMe( m[i] );
			++i;
		}
		style.setDefaults( defaultStyle );
		return style;
	}

	/*
	 */
	static void Merge( RainbowMarkup &dst, const RainbowMarkup &src )
	{
		int len = src.size();
		int i;
		int pos = 0;
		for( i = 0; i < len; ++i ) {
			pos = Insert( dst, src[i], pos );
		}
	}

	static int InsertString( RainbowMarkup &m, int pos, int len, int start_index = 0 )
	{
		int insert_index = -1;
		int size = m.size();
		int i = start_index;
		while(( i < size )&&( m[i].start() < pos )) {
			++i;
		}
		insert_index = i;
		while( i < size ) {
			m[i].add_to_start( len );
			++i;
		}
		return insert_index;
	};

	static int EraseString( RainbowMarkup &m, int pos, int len, int start_index = 0 )
	{
		int size = m.size();
		int i = start_index;
		int end = pos + len;
		while(( i < size )&&( m[i].start() < pos )) {
			++i;
		}
		start_index = i;
		int squash_start = i;
		int squash_end = i;
		while(( i < size )&&( m[i].start() < end )) {
			m[i].start( pos );
			squash_end = i;
			++i;
		}

		//Several styles could've ended up starting at the same offset.
		//Erase all of them except the last one.
		if( squash_end > squash_start ) {
			RainbowMarkup::iterator sit = m.begin() + squash_start;
			RainbowMarkup::iterator eit = m.begin() + squash_end;
			m.erase( sit, eit );
			i = squash_start + 1;
		}

		while( i < size ) {
			m[i].add_to_start( (-1)*len );
			++i;
		}

		return start_index;
	};

	static int ReplaceString( RainbowMarkup &m, int pos, int orig_len, int new_len ) {
		int start_index = EraseString( m, pos, orig_len );
		if( start_index >= 0 ) {
			InsertString( m, pos, new_len, start_index );
		}
		return start_index;
	};

	static std::string toString( const RainbowMarkup &m ) {
		std::string res;
		size_t l = m.size();
		for( size_t i = 0; i < l; ++i ) {
			StrUtils::Printf( res, "%li ", i );
			res.append( m[i].toString() );
			res.push_back('\n');
		};
		return res;
	};

private:
	RainbowMarkupUtils() {};
	~RainbowMarkupUtils() {};
};

#endif //_Rainbow_Markup_h
