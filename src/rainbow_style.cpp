/*
 * This file is part of ClickShow
 * Copyright 2013-2021 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "rainbow_style.h"

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#include "font_faces.h"
#include "clicky_debug.h"
#include "../../shared/src/str_utils.h"

void RainbowStyle::set( int start_offset, Fl_Color flcolor, unsigned int _features, unsigned int _use ) {
	m_start = start_offset;
	m_color = flcolor;
	m_features = _features;
	m_use = _use;
};

void RainbowStyle::applyToMe( const RainbowStyle &other ) {
	m_use |= other.m_use;
	m_features = (m_features & ( ~other.m_use )) | ( other.m_features & other.m_use );
	if( other.useColor() ) {
		color( other.color() );
	}
};

void RainbowStyle::setDefaults( const RainbowStyle &def ) {
	unsigned int defMask = (~m_use & M_ALL) & def.m_use;
	m_features |= defMask & def.m_features;
	m_use |= defMask;
};

void RainbowStyle::fromFL() {
	unsigned int c = fl_color();
	color( (c < 256) ? Fl::get_color( c ) : c );
	int attr;
	Fl::get_font_name( fl_font(), &attr );
	bold( attr & FL_BOLD );
	italic( attr & FL_ITALIC );
};

void RainbowStyle::applyToFL( const Fl_Color *palette, Fl_Color color255, const Fl_Font *faces ) const {
	if( useColor() ) {
		Fl_Color c = color();
		if( c == 255 ) {
			c = color255;
		} else if(( palette !=NULL )&&( c < 8 )) {
			c = palette[c];
		}
		fl_color( c );
	}
	if( use_italic() || use_bold() ) {
		int newfont;
		int attr = m_features & m_use & (FL_ITALIC|FL_BOLD);
		if( faces != NULL ) {
			newfont = faces[ attr ];
			LOG_CANDY_DETAIL("setting font (from faces[%i]) to %i\n", attr, newfont );
		} else {
			int oldfont = fl_font();
			LOG_CANDY_WARN("Slow font face lookup! font: %i, attr: %i\n", oldfont, attr );
			newfont = FontFaces::find_font_face_fail_free( oldfont, attr );
			LOG_CANDY_DETAIL("setting font to %i\n", newfont );
		}
		fl_font( newfont, fl_size() );
	}
};

std::string RainbowStyle::toString() const {
	std::string res;
	StrUtils::Printf( res, "start: %i, use: %u, features: %u, color: %x", m_start, m_use, m_features, m_color );
	return res;
};
