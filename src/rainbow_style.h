/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Rainbow_Style_h
#define _Rainbow_Style_h 1

#include <vector>
#include <string>

#include <FL/Fl.H>
#include <FL/fl_draw.H>

#define FONT_FEATURE( name, capname, bitnum ) \
static const unsigned int M_ ## capname = 1 << bitnum; \
bool name () const { return m_use & m_features & M_## capname; }; \
void name ( bool state ) { m_features = (m_features & ~M_ ## capname)|( state ? M_ ## capname : 0 ); m_use |= M_ ## capname; }; \
bool use_ ## name () const { return m_use & M_ ## capname; };

typedef std::vector<Fl_Color> ColorSet;

class RainbowStyle {
public:
	// these macros define methods like this
	//for every font feature. E.e., for italic..
	//bool italic() const;
	//void italic( bool state );
	//bool use_italic() const;
	FONT_FEATURE( bold, BOLD, 0 )
	FONT_FEATURE( italic, ITALIC, 1 )
	FONT_FEATURE( underline, UNDERLINE, 2 )
	FONT_FEATURE( strike, STRIKE, 3 )
	static const unsigned int M_COLOR = 1 << 4;

	static const unsigned int M_ALL = M_COLOR | M_ITALIC | M_BOLD | M_UNDERLINE | M_STRIKE;

	RainbowStyle( int start_offset = 0, Fl_Color flcolor = FL_WHITE, unsigned int _features = 0, unsigned int _use = 0 )
		: m_start(start_offset), m_color(flcolor), m_features(_features), m_use(_use) {};
	RainbowStyle( const RainbowStyle &s ) { m_start = s.m_start; m_color = s.m_color; m_features = s.m_features; m_use = s.m_use;  };
	~RainbowStyle() {};

	static void AppendToColorSet( ColorSet &dst, Fl_Color *src, int len, bool resolve_fl_colors = true );

	void set( int start_offset = 0, Fl_Color flcolor = 0, unsigned int _features = 0, unsigned int _use = 0 );

	void use( unsigned int mask ) { m_use = mask; };

	int start() const { return m_start; };
	void start( int start_offset ) { m_start = start_offset; };
	int add_to_start( int len ) { m_start += len; return m_start; };
	Fl_Color color() const { return m_color; };
	void color( Fl_Color clr ) { m_color = clr; m_use |= M_COLOR; };

	bool useColor() const { return m_use & M_COLOR; };

	void applyToMe( const RainbowStyle &other );
	void setDefaults( const RainbowStyle &def );

	bool isComplete() const {
		return (m_use & M_ALL) == M_ALL;
	};

	bool operator==( const RainbowStyle &other ) const {
		return ((m_start == other.m_start )
			&&(m_use == other.m_use )
			&&( (!useColor())||(m_color == other.m_color) )
			&&( (m_features & m_use) == (other.m_features & other.m_use) ));
	};
	bool operator!=( const RainbowStyle &other ) const {
		return !( *this == other );
	};

	void fromFL();
	void applyToFL( const Fl_Color *palette = NULL, Fl_Color color255 = 0xFFFFFF00, const Fl_Font *faces = NULL ) const;
	std::string toString() const;

protected:
	int m_start;
	unsigned int m_features;
	unsigned int m_use;
	Fl_Color m_color;
};

#endif //_Rainbow_Style_h
