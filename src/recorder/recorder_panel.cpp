/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "../heart.h"
#include "recorder_panel.h"
#include "sub_recorder.h"
#include "../clicky_debug.h"
#include "../subtitle_formats/timing/srt.h"

//---- button callbacks ----

void RecorderPanel::cb_recpause(Fl_Button* b, void *)
{
	RecorderPanel *p = (RecorderPanel *)b->parent();
	p->clicked_recpause();
	LOG_PLAYER_DETAIL("Clicked Rec/Pause!\n" );
}

void RecorderPanel::cb_save(Fl_Button* b, void *)
{
	RecorderPanel *p = (RecorderPanel *)b->parent();
	p->recorder.clicked_save();
	LOG_PLAYER_DETAIL("Clicked Save!\n" );
}

void RecorderPanel::cb_reset(Fl_Button* b, void *)
{
	RecorderPanel *p = (RecorderPanel *)b->parent();
	p->recorder.clicked_reset();
	LOG_PLAYER_DETAIL("Clicked Reset!\n" );
}

void RecorderPanel::cb_stop(Fl_Button* b, void *)
{
	RecorderPanel *p = (RecorderPanel *)b->parent();
	p->recorder.clicked_stop();
	LOG_PLAYER_DETAIL("Clicked Stop!\n" );
}

void RecorderPanel::cb_close(Fl_Button* b, void*)
{
	RecorderPanel *p = (RecorderPanel *)b->parent();
	p->heart->show_recorder( 0 );
	LOG_RENDER_DETAIL2("Closing the recorder panel, man!\n" );  
};

//---- constructor ----

RecorderPanel::RecorderPanel(Heart *hrt, SubRecorder &_recorder)
: ControlPanel ( hrt, 0, 0, 0, 0, (const char *)"RECORDER" ), recorder(_recorder)
{
	layout = new SimpLE();

	//size_range( 150, 100, 450, 300, 1, 1, 0 );

	tc_label[0] = 0;
	button_tc = new ClickyButton( 0,0,0,0, tc_label );
	button_count = new ClickyButton( 0,0,0,0, "" );
	button_recpause = new ClickyButton( 0,0,0,0, _("Rec") );
	button_save = new ClickyButton( 0,0,0,0, _("Save") );
	button_reset = new ClickyButton( 0,0,0,0, _("Clear") );
	button_stop = new ClickyButton( 0,0,0,0, "◻" );
	button_close = new ClickyButton( 0,0,0,0, "" );

	end();	//to stop adding widgets to this group

	button_count->align(FL_ALIGN_LEFT|FL_ALIGN_INSIDE);

	//tooltips
	button_recpause->tooltip( _("Start or pause recording timed subtitles.") );
	button_save->tooltip( _("Save recorded timed subtitles.") );
	button_reset->tooltip( _("Forget what's been recorded.") );
	button_count->tooltip( _("Number of recorded subtitles.") );
	button_stop->tooltip( _("Reset the timecode to 00:00:00,000.") );

	//callbacks
	button_recpause->callback((Fl_Callback*)cb_recpause);
	button_save->callback((Fl_Callback*)cb_save);
	button_reset->callback((Fl_Callback*)cb_reset);
	button_stop->callback((Fl_Callback*)cb_stop);
	button_close->callback((Fl_Callback*)cb_close);

	//-- layout --

	static const int pixx = 5;
	static const int pixy = 5;
	static const int marginw = 4;
	static const int marginh = 2;
	static const int spacex = 10;
	static const int spacey = 10;
	static const int closeb_size = 21;

	layout->add_cell( button_tc, 4, 0, 8, 2,	//cx, cy, cw, ch
										16, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_stop, 12, 0, 2, 2,	//cx, cy, cw, ch
										16, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_recpause, 0, 0, 4, 4,	//cx, cy, cw, ch
										16, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_count, 4, 2, 12, 2,	//cx, cy, cw, ch
										16, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_save, 0, 4, 8, 2,	//cx, cy, cw, ch
										16, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add_cell( button_reset, 8, 4, 8, 2,	//cx, cy, cw, ch
										16, 6,				//columns, rows
										pixx, pixy,			//pixx, pixy
										0, 0,	//relx, rely
										1.0, 1.0,	//relw, relh
										marginw, marginh,		//margins
										spacex, spacey );		//spaces

	layout->add( button_close,
							-1*(closeb_size), 0,			//pixx, pixy
							1.0, 0.0,	//relx, rely
							0.0, 0.0,	//relw, relh
							0, 0,		//margins
							closeb_size, closeb_size );		//min pix

	//-- style --
	heart->get_style_engine()->assign_style( StylePrefs::RECPANEL_BUTTON, button_tc );
	heart->get_style_engine()->assign_style( StylePrefs::RECPANEL_BUTTON, button_recpause );
	heart->get_style_engine()->assign_style( StylePrefs::RECPANEL_BUTTON, button_save );
	heart->get_style_engine()->assign_style( StylePrefs::RECPANEL_BUTTON, button_reset );
	heart->get_style_engine()->assign_style( StylePrefs::RECPANEL_BUTTON, button_stop );
	heart->get_style_engine()->assign_style( StylePrefs::NO_BORDER_BUTTON, button_count );
	heart->get_style_engine()->assign_style( StylePrefs::RECCLOSE_BUTTON, (Fl_Button *)button_close );

	//--- activators
	wac_save.AddWidget( button_save, WidgetActInfo::METH_ACTIVATE );
	wac_save.AddWidget( button_reset, WidgetActInfo::METH_ACTIVATE );

	//-- init button states --
	set_recording_look( false );
	set_tc( NO_TC );
	set_rec_count( 0 );

	recorder.SetRecorderPanel( this );

	//-- refresh --

	refresh_layout();
	refresh_style();
};

RecorderPanel::~RecorderPanel()
{
 	delete layout;
}

void RecorderPanel::set_recording_look( bool state )
{
	if( state ) {
		button_recpause->copy_label(_("Pause"));
		heart->get_style_prefs()->RecPauseImgB->apply( button_recpause );
		button_stop->deactivate();
	} else {
		button_recpause->copy_label(_("Rec"));
		heart->get_style_prefs()->RecImgB->apply( button_recpause );
		button_stop->activate();
	}
}

bool RecorderPanel::get_size( int &w, int &h )
{
	int	fh;

	fh = heart->get_style_prefs()->NormalButton->ls.Size;
	h = fh*6;
	w = h*3;
	return true;
}

void RecorderPanel::set_rec_count( int count )
{
	count_string.clear();
	StrUtils::Printf( count_string,"%s %i", _("Recorded:"), count );
	button_count->copy_label( count_string.c_str() );
	LOG_RENDER_DETAIL2("count label: %s\n", count_string.c_str() );
}

void RecorderPanel::enable_save( bool state )
{
	LOG_RENDER_DETAIL2("wac_save: %x, %s\n", wac_save.GetSwitchableMask(), state ? "true" : "false" );
	wac_save.SetActivation( wac_save.GetSwitchableMask(), state );
	parent()->redraw();
}

void RecorderPanel::refresh_layout()
{
	layout->refresh_layout( this );
}

void RecorderPanel::clicked_recpause()
{
	recorder.StartOrPause();
}

void RecorderPanel::set_tc( long milli )
{
	LOG_PLAYER_INFO("TC: %li\n", milli);

	if( milli == NO_TC ) {
		sprintf( tc_label, "--:--:--,---" );
	} else {
		int h, m, s, mm;
		SrtParser::TimeToParts( (int)milli, h, m, s, mm );
		sprintf( tc_label, "%02i:%02i:%02i,%03i", h, m, s, mm );
	}
	button_tc->label( tc_label );
}

void RecorderPanel::started()
{
	LOG_PLAYER_INFO("Recording STARTED!\n");
	set_recording_look( true );
}
void RecorderPanel::paused()
{
	LOG_PLAYER_INFO("Recording PAUSED!\n");
	set_recording_look( false );
}
