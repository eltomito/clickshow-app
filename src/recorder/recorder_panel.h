/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Recorder_Panel_h_
#define _Recorder_Panel_h_ 1

#include <FL/Fl_Repeat_Button.H>

#include "../clicky_button.h"
#include "../control_panel.h"
#include "../simple_layout_engine.h"
#include "../controls/time_shifter.h"
#include "shared/src/str_utils.h"
#include <climits>
#include <FL/Fl_Repeat_Button.H>

class Heart;
class SubRecorder;

/*
 * the clicker panel class
 */
class RecorderPanel : public ControlPanel
{
	SimpLE	*layout;

	SubRecorder &recorder;

	ClickyButton	*button_tc;
	ClickyButton	*button_recpause;
	ClickyButton	*button_count;
	ClickyButton	*button_save;
	ClickyButton	*button_reset;
	ClickyButton	*button_stop;
	ClickyButton	*button_close;

	const static int margin_v = 2;
	const static int margin_h = 2;
	const static int space_h = 1;
	const static int space_v = 5;

	static void cb_recpause(Fl_Button* b, void*);
	static void cb_save(Fl_Button* b, void*);
	static void cb_reset(Fl_Button* b, void*);
	static void cb_stop(Fl_Button* b, void*);
	static void cb_close(Fl_Button* b, void*);

	std::string count_string;

	WidgetActivator wac_save;

public:
	static const long NO_TC = LONG_MIN;

	RecorderPanel(Heart *hrt, SubRecorder &_recorder );
    ~RecorderPanel();

	void set_tc( long milli );
	void set_rec_count( int count );

	void paused();
	void started();

	void enable_save( bool state = true );

	void refresh_layout();
	bool get_size( int &w, int &h );

private:
	char tc_label[64];
	void set_recording_look( bool state );
	void clicked_recpause();
};

#endif //_Recorder_Panel_h_
