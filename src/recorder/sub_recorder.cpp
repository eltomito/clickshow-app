#include "sub_recorder.h"
#include "recorder_panel.h"
#include <climits>

SubRecorder::SubRecorder( Heart &_heart )
	: heart(&_heart), rec_panel(NULL), subs(NULL), min_sub_num(INT_MAX), max_sub_num(-1),
	unsaved(false), unclosed_sub(false),start_shift(LONG_MIN),end_shift(0)
{
	timer = new SubtitlePlayerTimer( 0.1 );
	timer->SetPlayer( this );
}

void SubRecorder::tick( double sec )
{
	if( rec_panel ) rec_panel->set_tc( (long)( sec * (double)1000.0) );
}

void SubRecorder::timerStarted()
{
	if(rec_panel) {
		rec_panel->started();
		update_saveability();
	}
}

void SubRecorder::timerPaused()
{
	if(rec_panel) {
		rec_panel->paused();
		update_saveability();
	}
}

bool SubRecorder::Start()
{
	if( !timer ) return false;
	if( !subs || !subs->size() ) {
		start_shift = LONG_MIN;
		end_shift = 0;
	}
	return timer->Start();
}

bool SubRecorder::Pause()
{
	if( !timer ) return false;
	return timer->Pause();
}

bool SubRecorder::StartOrPause()
{
	if( !timer->IsRunning() ) {
		return Start();
	}
	return Pause();
}

bool SubRecorder::Seek( double sec )
{
	if( !timer ) return false;
	if( !timer->Seek( sec ) ) return false;
	if( rec_panel ) rec_panel->set_tc( (long)(sec * (double)1000.0) );
	return true;
}

bool SubRecorder::SeekToSub( const Subtitle &sub, const Subtitle *prev_sub )
{
	if( IsRecording() ) return false;
	const SubTiming &t = sub.get_timing();
	long seek_time = 0;
	if( t.IsSet() ) {
		seek_time = t.GetStart();
		LOG_PLAYER_DETAIL("Seeking to a timed subtitle. Using start = %li\n", seek_time);
	} else {
		if( prev_sub ) {
			const SubTiming &pt = prev_sub->get_timing();
			if( pt.IsSet() ) {
				seek_time = pt.GetEnd();
				LOG_PLAYER_DETAIL("Seeking to an untimed subtitle with a timed predecessor. Using end = %li\n", seek_time);
			} else {
				LOG_PLAYER_DETAIL("Both the sub and its predecessor are untimed.\n");
				return false;
			}
		} else {
			LOG_PLAYER_DETAIL("The sub is untimed and it has no predecessor.\n");
			return false;
		}
	}
	double s = (double)seek_time / (double)1000.0;
	return Seek( s );
}

void SubRecorder::record_show( const Subtitle &sub, int sub_num )
{
	if( !IsRecording() ) { return; }
	if( !subs ) {
		subs = new Subtitles();
		subs->set_timed( true );
	}
	if( unclosed_sub ) {
		record_hide();
	}
	Subtitle newsub( sub );
	unclosed_sub = true;
	newsub.mutget_timing().SetStart( (long)( timer->GetTimecode() * 1000 ) );
	newsub.mutget_timing().SetDur( 1000 );
	subs->append( newsub );

	if( sub.get_timing().IsSet() ) {
		end_shift = newsub.get_timing().GetStart() - sub.get_timing().GetStart();
		if( start_shift == LONG_MIN ) {
			start_shift = end_shift;
		}
	}

	if( sub_num >= 0 ) {
		if( sub_num < min_sub_num ) min_sub_num = sub_num;
		if( sub_num > max_sub_num ) max_sub_num = sub_num;
	}

	if( rec_panel ) {
		rec_panel->set_rec_count( subs->size() );
	}
	unsaved = true;
}

void SubRecorder::record_hide()
{
	if( !IsRecording() ) { return; }
	if( !subs ) {
		LOG_GEN_ERROR("Dude, you're trying to hide a subtitle with no subtitles being recorded!");
		return;
	}
	unclosed_sub = false;
	Subtitle *sub = subs->get_subtitle( subs->size() - 1 );
	if( !sub ) return; //the showing was off the record and now we're trying to record the hiding.
	sub->mutget_timing().SetEnd( (long)( timer->GetTimecode() * 1000 ) );
	unsaved = true;
}

/*
 * @returns true if the save has completed, false if it's been canceled.
 */
bool SubRecorder::clicked_save()
{
	if( !subs || (subs->size() == 0) ) {
		LOG_GEN_ERROR("You shouldn't be able to click 'save recorded subs' with no subs recorded!\n");
		return true;
	}

	Subtitles *savesubs = new Subtitles( *subs );
	Subtitles *fullsubs = heart->get_subtitles();
	if( fullsubs ) {
		if( min_sub_num > 0 ) {
			savesubs->insert( 0, fullsubs, 0, min_sub_num  );
			if( start_shift != LONG_MIN ) {
				savesubs->shift_timing( start_shift, 0, min_sub_num );
			}
		}
		if( max_sub_num < (fullsubs->size() - 1) ) {
			int oldsize = savesubs->size();
			savesubs->insert( oldsize, fullsubs, max_sub_num + 1, fullsubs->size() );
			if( end_shift != 0 ) {
				savesubs->shift_timing( end_shift, oldsize, savesubs->size() );
			}
		}
	}

	SubtitleDoc newdoc;
	newdoc.set_subtitles( savesubs );

	SubtitleDoc *olddoc = heart->get_subdoc();
	if( olddoc ) {
		newdoc.set_encinfo( olddoc->get_encinfo() );
	}
	newdoc.generate_savename( (olddoc && !olddoc->save_name.empty() ) ? olddoc->save_name.c_str() : NULL );

	std::vector<std::string> fnames( 1, newdoc.save_name );
	if( heart->show_file_dialog( fnames, true ) > 0 )
	{
		std::string &fname = fnames[fnames.size() - 1];
		int retval = newdoc.save_to_file( fname.c_str() );
		LOG_GEN_INFO("SubtitleDoc::save_to_file returned %i\n", retval );
		newdoc.set_filename( fname.c_str() );
		unsaved = false;
		return true;
	}
	return false;
}

bool SubRecorder::clicked_stop()
{
	if( IsRecording() ) return false;
	return Seek( 0 );
}

/*
 * @returns true if the reset has completed, false if it's been canceled.
 */
bool SubRecorder::clicked_reset()
{
	if( unsaved ) {
		int action = heart->ask( _("Attention!"), _("There are unsaved recorded subtitles!"),
                     _("#0 #c0xff000000 Discard|#1 #c0x00ff0000 Save|#2 #c0x0000ff00 Cancel") );		
		switch( action ) {
			case 0:
				LOG_PLAYER_INFO("Discarding recorded subtitles.\n");
			break;
			case 1:
				LOG_PLAYER_INFO("Saving recorded subtitles before discarding them...\n");
				if( !clicked_save() ) {
					LOG_PLAYER_INFO("Saving was canceled by the user in the file chooser dialog.\n");
					return false;
				}
			break;
			case 2:
				LOG_PLAYER_INFO("Saving was canceled by the user in the save? dialog.\n");
				return false;
			break;
			default:
				LOG_PLAYER_ERROR("WEIRD! A dialog with options 0-2 returned %i!\n", action); 
			break;
		}
	}
	if( subs ) subs->clear();
	unsaved = false;
	unclosed_sub = false;
	min_sub_num = INT_MAX;
	max_sub_num = -1;
	timer->Reset();
	if( rec_panel ) {
		rec_panel->set_tc( 0 );
		rec_panel->set_rec_count( 0 );
		update_saveability();
	}
	return true;
}

void SubRecorder::update_saveability()
{
	if( !rec_panel ) return;
	rec_panel->enable_save( timer && !timer->IsRunning() && subs && subs->size() );
}
