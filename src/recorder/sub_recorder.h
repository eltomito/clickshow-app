/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Sub_Recorder_h_
#define _Sub_Recorder_h_ 1

#include "../player/timered_player.h"
#include "../player/player_timer.h"

class Heart;
class Subtitle;
class RecorderPanel;

class SubRecorder : public TimeredPlayer
{
private:
	Heart						*heart;
	RecorderPanel			*rec_panel;
	SubtitlePlayerTimer	*timer;

	bool unsaved;
	bool unclosed_sub;

	long start_shift, end_shift;	//added to the old TC to get the new TC

	Subtitles *subs;
	int min_sub_num, max_sub_num;

public:
	SubRecorder( Heart &_heart );
	~SubRecorder() {};

	void record_show( const Subtitle &sub, int sub_num );
	void record_hide();

	bool Start();
	bool Pause();
	bool StartOrPause();
	void SetRecorderPanel( RecorderPanel *rp ) { rec_panel = rp; update_saveability(); };
	bool IsRecording() { return timer->IsRunning(); };

	bool SeekToSub( const Subtitle &sub, const Subtitle *prev_sub = NULL );
	bool Seek( double sec );

	bool clicked_save();
	bool clicked_reset();
	bool clicked_stop();

	void tick( double sec );
	void timerStarted();
	void timerPaused();

private:
	void update_saveability();
};

#endif //_Sub_Recorder_h_
