/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Row_Geom_h
#define _Row_Geom_h

class RowGeom : LayoutGeometry
{
public:

	RowGeom( float aspect )
	~RowGeom() {};

	RowGeom *clone();

	int plot( int parent_w, int parent_h, int &x, int &y, int &w, int &h );

};

#endif //_Row_Geom_h

RowGeom::plot( int parent_w, int parent_h, int &x, int &y, int &w, int &h, int horizontal = 1 )
{
	
	
	
	
}
