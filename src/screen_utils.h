/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Screen_Utils_h
#define _Screen_Utils_h 1

#include <FL/Fl.H>
#include <FL/Fl_Window.H>

class ScreenUtils {
	public:

	static int screen_num( int x, int y ) {
		int X, Y, W, H;
		int cnt = Fl::screen_count();
		for( int i = 0; i < cnt; ++i ) {
			Fl::screen_work_area( X, Y, W, H, i );
			if(( X <= x )&&( (X+W) > x )&&( Y <= y )&&( (Y+H) > y )) {
				return i;
			} 
		}
		return -1;
	}

	static int screen_num( Fl_Window *w ) {
		return screen_num( w->x(), w->y() );
	}

	static bool window_to_screen( Fl_Window *win, int n ) {
		int X, Y, W, H;
		if( !win ) {
			return false;
		}
		if( Fl::screen_count() <= n ) {
			return false;
		}
		int cur_n = screen_num( win );
		if( cur_n == n ) {
			return true;
		}
		Fl::screen_work_area( X, Y, W, H, n );
		int neww = (win->w() < W) ? win->w() : W;
		int newh = (win->h() < H) ? win->h() : H;
		win->resize( X, Y, neww, newh );
		return true;
	}

	static void shrink_frame_by_percent( int &X, int &Y, int &W, int &H, int hpc = 20, int vpc = 20 ) {
		int border_x = hpc * W / 200;
		int border_y = vpc * H / 200;
		X += border_x;
		Y += border_y;
		W -= 2*border_x;
		H -= 2*border_y;
	}

	/**
	 * @returns {bool} Whether any values were actually changed.
	 */
	static bool fit_frame_in_frame(	int &x, int &y, int &w, int &h, int X, int Y, int W, int H ) {
		bool res = false;
		if(( w > W )||( w < 0 )) {
			w = W;
			x = X;
			res = true;
		}
		if(( h > H )||( h < 0 )) {
			h = H;
			y = Y;
			res = true;
		}
		if(( x < X )||( x >= (X+W) )) {
			x = X;
			res = true;
		}
		if(( y < Y )||( y >= (Y+H) )) {
			y = Y;
			res = true;
		}
		return res;
	}
};

#endif //Screen_Utils_h
