/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "search_panel.h"

#include "brain_mlinput.h"

//================== class SearchPanel =========================

	SearchPanel::SearchPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, "SEARCH" )
	{
		heart = hrt;;

		//layout = new SimpLE();

		vert_pack = new BodyPack( false );
		vert_pack->align( CL_ALIGN_FILL );
		vert_pack->spacing( 5 );

		//let's try to sit mlinput_text inside a BodyPack to give it a frame
		input_pack = new BodyPack();
		input_pack->align( CL_ALIGN_FILL );
		input_pack->borders( 2,2,2,2 );

		mlinput_text = new Fl_Multiline_Input( 0, 0, 150, 40 );

		ctrl_pack = new BodyPack( true );
		ctrl_pack->align( CL_ALIGN_EVEN|CL_ALIGN_FILL );
		ctrl_pack->spacing( 10 );

		button_find = new ClickyButton( 0, 0, 0, 0, _("Find @2->") );
		button_find_bwd = new ClickyButton( 0, 0, 0, 0, _("@2<-") );
		button_asterisk = new ClickyButton( 0, 0, 0, 0, "*" );
		button_show = new ClickyButton( 0, 0, 0, 0, _("Show") );
		//button_test = new ClickyButton( 0, 0, 0, 0, _("Test") );
		button_goto = new ClickyButton( 0, 0, 0, 0, _("Go to #") );

		vert_pack->add( ctrl_pack );

		end();

		WidgetBrain::set_widget_brain( mlinput_text, new MLInputBrain( mlinput_text, 20, 2 ) );

		WidgetBrain::set_user_data( button_find, (void *)this );
		WidgetBrain::set_user_data( button_find_bwd, (void *)this );
		WidgetBrain::set_user_data( button_asterisk, (void *)this );
		WidgetBrain::set_user_data( button_show, (void *)this );
		//WidgetBrain::set_user_data( button_test, (void *)this );
		WidgetBrain::set_user_data( button_goto, (void *)this );

		mlinput_text->cursor_color( FL_WHITE );

		//tooltips
		button_find->tooltip( _("Search for text in the subtitles.") );
		button_find_bwd->tooltip( _("Backward search for text in the subtitles.") );
		button_asterisk->tooltip( _("Press this button and then \"Find\"\nto look for marked subtitles.\nPress this button twice to clear the text box.") );
		button_show->tooltip( _("Shows the text in the box above as a subtitle.") );
		//button_test->tooltip( _("Shows a sample text as a subtitle.") );
		button_goto->tooltip( _("Go to subtitle of the number typed in the text box.") );

		//callbacks
		button_find->callback((Fl_Callback*)cb_find);
		button_find_bwd->callback((Fl_Callback*)cb_find_bwd);
		button_asterisk->callback((Fl_Callback*)cb_asterisk);
		button_show->callback((Fl_Callback*)cb_show);
		//button_test->callback((Fl_Callback*)cb_test);
		button_goto->callback((Fl_Callback*)cb_goto);

		//-- style --
		Fl_Widget* all_buttons[] = { button_find, button_find_bwd, button_asterisk, button_show, button_goto, NULL };
		heart->get_style_engine()->assign_style( StylePrefs::FILE_BUTTON, &all_buttons[0] );
		heart->get_style_engine()->assign_style( StylePrefs::TEXT_INPUT, mlinput_text );
		heart->get_style_engine()->assign_style( StylePrefs::SEARCH_WIDGET, input_pack );

		refresh_layout();
		refresh_style();
	};

    SearchPanel::~SearchPanel()
    {
        //delete layout;
    };

	bool SearchPanel::get_size( int &w, int &h )
	{
		LOG_RENDER_DETAIL2( "* * * * starting...\n", w, h );

		vert_pack->natural_size( w, h );

		LOG_RENDER_DETAIL2( "* * * * w=%i, h=%i\n", w, h );
		return true;
/*
		int	fh;

		fh = heart->get_style_prefs()->NormalButton->ls.Size;
		h = fh*6;
		w = ( 380 * h ) / 100;
		return true;
*/
	};

	void SearchPanel::refresh_layout()
	{
		vert_pack->resize( x(), y(), w(), h() );
		//vert_pack->refresh_layout();
		//layout->refresh_layout( this );
	};

//-- callbacks --

void SearchPanel::cb_find(Fl_Button* b, void*)
{
	//SearchPanel *p = (SearchPanel *)b->parent();
	SearchPanel *p = (SearchPanel *)WidgetBrain::get_user_data( (Fl_Widget*)b );

	p->heart->find_sub( p->mlinput_text->value(), true );
	p->heart->focus_default_widget();
};

void SearchPanel::cb_find_bwd(Fl_Button* b, void*)
{
	//SearchPanel *p = (SearchPanel *)b->parent();
	SearchPanel *p = (SearchPanel *)WidgetBrain::get_user_data( (Fl_Widget*)b );

	p->heart->find_sub( p->mlinput_text->value(), false );
	p->heart->focus_default_widget();
};

void SearchPanel::cb_asterisk(Fl_Button* b, void*)
{
	//SearchPanel *p = (SearchPanel *)b->parent();
	SearchPanel *p = (SearchPanel *)WidgetBrain::get_user_data( (Fl_Widget*)b );

	if( 0 == strcmp( (const char *)p->mlinput_text->value(), (const char *)SubtitleDoc::MARK_STRING ) )
		p->mlinput_text->value( (const char *)"" );
	else
		p->mlinput_text->value( (const char *)SubtitleDoc::MARK_STRING );
};

void SearchPanel::cb_goto(Fl_Button* b, void*)
{
	//SearchPanel *p = (SearchPanel *)b->parent();
	SearchPanel *p = (SearchPanel *)WidgetBrain::get_user_data( (Fl_Widget*)b );

	if( p->heart->go_to_sub( p->mlinput_text->value() ) ) {
		p->mlinput_text->value( NULL );
		p->heart->focus_default_widget();
	}
};

void SearchPanel::cb_show(Fl_Button* b, void*)
{
	//SearchPanel *p = (SearchPanel *)b->parent();
	SearchPanel *p = (SearchPanel *)WidgetBrain::get_user_data( (Fl_Widget*)b );

	p->heart->show_text( p->mlinput_text->value() );
};
