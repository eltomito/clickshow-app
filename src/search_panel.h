/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Search_Panel_h
#define _Search_Panel_h

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include <FL/Fl_Multiline_Input.H>

#include "heart.h"
#include "control_panel.h"
#include "simple_layout_engine.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "clicky_button.h"
#include "body_pack.h"

/*
 * the search panel class
 */
class SearchPanel : public ControlPanel
{
	//SimpLE	*layout;
	
	BodyPack *vert_pack;
	BodyPack *ctrl_pack;
	BodyPack *input_pack;

	Fl_Multiline_Input *mlinput_text;
	ClickyButton	*button_find;
	ClickyButton	*button_find_bwd;
	ClickyButton	*button_asterisk;
	ClickyButton	*button_show;
	//ClickyButton	*button_test;
	ClickyButton	*button_goto;

	static void cb_find(Fl_Button* b, void*);
	static void cb_find_bwd(Fl_Button* b, void*);
	static void cb_asterisk(Fl_Button* b, void*);
	static void cb_show(Fl_Button* b, void*);
	//static void cb_test(Fl_Button* b, void*);
	static void cb_goto(Fl_Button* b, void*);

public:
	SearchPanel(Heart *hrt, int x, int y, int w, int h);
    ~SearchPanel();

	void refresh_layout();
	bool get_size( int &w, int &h );
};


#endif //_Search_Panel_h
