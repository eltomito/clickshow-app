/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <sstream>
#include <string>
using std::endl;

#include "sim_geom.h"


//============== class SimGeom ====================
	SimGeom::SimGeom( int pixx, int pixy, float relx, float rely, float relw, float relh, int marginw, int marginh, int minpixw, int minpixh )
	{
		pix_x = pixx;
		pix_y = pixy;
		rel_x = relx;
		rel_y = rely;
		rel_w = relw;
		rel_h = relh;
		margin_w = marginw;
		margin_h = marginh;
		min_pix_w = minpixw;
		min_pix_h = minpixh;
	};

	 SimGeom::SimGeom( SimGeom *sg )
	{
		pix_x = sg->pix_x;
		pix_y = sg->pix_y;
		rel_x = sg->rel_x;
		rel_y = sg->rel_y;
		rel_w = sg->rel_w;
		rel_h = sg->rel_h;
		margin_w = sg->margin_w;
		margin_h = sg->margin_h;
		min_pix_w = sg->min_pix_w;
		min_pix_h = sg->min_pix_h;
	};

	LayoutGeometry * SimGeom::clone()
	{
		SimGeom *sg = new SimGeom( this );
		return (LayoutGeometry*)sg;
	};

	int  SimGeom::plot( int parent_w, int parent_h, int &x, int &y, int &w, int &h )
	{
		x = (int)( rel_x * (float)parent_w ) + pix_x;
		y = (int)( rel_y * (float)parent_h ) + pix_y;
		w = (int)( rel_w * (float)parent_w ) - margin_w;
		h = (int)( rel_h * (float)parent_h ) - margin_h;
		if( w < min_pix_w )
			w = min_pix_w;
		if( h < min_pix_h )
			h = min_pix_h;

		return true;
	};

	std::string&  SimGeom::to_string( std::string &dst )
	{
		std::ostringstream res;

		res << "pix_xy = ( " << pix_x << ", " << pix_y << " ) " << endl
			<< "rel_xy = ( " << rel_x << ", " << rel_y << " ) " << endl
			<< "rel_wh = ( " << rel_w << ", " << rel_h << " ) " << endl
			<< "margin_wh = ( " << margin_w << ", " << margin_h << " ) " << endl
			<< "min_pix_wh = ( " << min_pix_w << ", " << min_pix_h << " ) " << endl;
		dst = res.str();

		return dst;
	}

