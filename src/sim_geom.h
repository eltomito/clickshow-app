/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Sim_Geom_h
#define _Sim_Geom_h

#include <string>

#include "layout_geometry.h"

class SimGeom : public LayoutGeometry
{
public:
	int	pix_x, pix_y;
	float	rel_x, rel_y;
	float	rel_w, rel_h;
	int	margin_w, margin_h;
	int	min_pix_w, min_pix_h;

	SimGeom( int pixx, int pixy, float relx, float rely, float relw, float relh, int marginw, int marginh, int minpixw, int minpixh );
	SimGeom( SimGeom *sg );
	~SimGeom()	{};;

	LayoutGeometry *clone();

	int plot( int parent_w, int parent_h, int &x, int &y, int &w, int &h );

	std::string& to_string( std::string &dst );
};

#endif //_Sim_Geom_h
