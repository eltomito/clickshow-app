/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "simple_layout_engine.h"
#include "sim_geom.h"

//=================== class SimpLE =====================
	int SimpLE::add( Fl_Widget *w,
			int pixx, int pixy,
			float relx, float rely,
			float relw, float relh,
			int marginw, int marginh,
			int minpixw, int minpixh )
	{
			SimGeom *g = new SimGeom( pixx, pixy, relx, rely, relw, relh, marginw, marginh, minpixw, minpixh );
			LayoutEngine::add( w, g, false );
			return 1;
	};

	int SimpLE::add_row( Fl_Widget **ws,
						int pixx, int pixy,
						float relx, float rely,
						float relw, float relh,
						int marginw, int marginh,
						int spacex, int spacey )
	{
		return add_column( ws, pixx, pixy, relx, rely, relw, relh, marginw, marginh, spacex, spacey, true );
	}

	int SimpLE::add_column( Fl_Widget **ws,
						int pixx, int pixy,
						float relx, float rely,
						float relw, float relh,
						int marginw, int marginh,
						int spacex, int spacey, int horizontal )
	{
		Fl_Widget **t;

		if( ws == NULL )
			return true;

		//count widgets
		int numw = 0;
		t = ws;
		while( t[0] != NULL )
		{
			t++;
			numw++;
		};

		//add widgets
		int i = 0;
		t = ws;
		while( i < numw )
		{
			if( !horizontal )
				//column
				add_cell( t[0],
								0, i, 1, 1,
								1, numw,
								pixx, pixy, relx, rely, relw, relh, marginw, marginh, spacex, spacey );
			else
				//row
				add_cell( t[0],
								i, 0, 1, 1,
								numw, 1,
								pixx, pixy, relx, rely, relw, relh, marginw, marginh, spacex, spacey );
			t++;
			i++;
		}
		return true;
	};

	int SimpLE::add_cell(	Fl_Widget *w,
						int cx, int cy, int cw, int ch,
						int columns, int rows,
						int pixx, int pixy,
						float relx, float rely,
						float relw, float relh,
						int marginw, int marginh,
						int spacex, int spacey )
	{
		//calculate grid constants
		float cell_relw = relw / (float)columns;
		float cell_relh = relh / (float)rows;

		float final_relx = (float)cx * cell_relw + relx;
		float final_rely = (float)cy * cell_relh + rely;

		float final_relw = (float)cw * cell_relw;
		float final_relh = (float)ch * cell_relh;

//---

	int final_pixx = pixx;
	int final_pixy = pixy;

//apply margins
		int final_marginw = ( marginw * (cx+cw) ) / columns;
		final_pixx -= ( marginw * cx ) / columns;

		int final_marginh = ( marginh * (cy+ch) ) / rows;
		final_pixy -= ( marginh * cy ) / rows;

//apply spacing
		final_pixx += (( cx + cw - 1)/columns ) * ( (columns -1 ) * spacex / columns );
		final_marginw += (columns -1 ) * spacex / columns;

		final_pixy += (( cy + ch - 1)/rows ) * ( (rows -1 ) * spacey / rows );
		final_marginh += (rows -1 ) * spacey / rows;

/*
int SimpLE::add( Fl_Widget *w,
			int pixx, int pixy,
			float relx, float rely,
			float relw, float relh,
			int marginw, int marginh,
			int minpixw, int minpixh )
*/

		add( w, final_pixx, final_pixy,			//pixx, pixy
					final_relx, final_rely,			//relx, rely
					final_relw, final_relh,			//relw, relh
					final_marginw, final_marginh,	//marginw, marginh
					0, 0 );								//minpixw, minpixh

		return 1;
	};
