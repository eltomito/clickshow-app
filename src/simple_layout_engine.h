/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Simple_Layout_Engine_h
#define _Simple_Layout_Engine_h

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>

#include "layout_engine.h"
#include	"control_panel.h"

class SimpLE : public LayoutEngine
{
public:
	/*
	 * add one widget to the layout
	 * parameters:
	 *		pixx, pixy - pixel offsets of the top-left corner
	 *		relx, rely - offsets of the top-left corner as proportions of the container size
	 *		relw, relh - width and height as proportions of the container size
	 *		marginw, marginh - right and bottom margins in pixels
	 *		minpixw, minpixh - minimum height and width of the widget in pixels 
	 */
	int add( Fl_Widget *w,
			int pixx, int pixy,
			float relx, float rely,
			float relw, float relh,
			int marginw, int marginh,
			int minpixw, int minpixh );

	int add_column( Fl_Widget **ws,
						int pixx, int pixy,
						float relx, float rely,
						float relw, float relh,
						int marginw, int marginh,
						int spacex, int spacey, int horizontal=false );

	int add_row( Fl_Widget **ws,
						int pixx, int pixy,
						float relx, float rely,
						float relw, float relh,
						int marginw, int marginh,
						int spacex, int spacey );

	int add_cell(	Fl_Widget *w,
						int cx, int cy, int cw, int ch,
						int columns, int rows,
						int pixx, int pixy,
						float relx, float rely,
						float relw, float relh,
						int marginw, int marginh,
						int spacex, int spacey );
};

#endif //_Simple_Layout_Engine_h
