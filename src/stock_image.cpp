/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "stock_image.h"

//#include <FL/fl_draw.H>

const QICmd StockImage::star_formula[5] = {
	{ QuickImage::LINE, 0.1, 0.1, 0.9, 0.9 },
	{ QuickImage::LINE, 0.9, 0.1, 0.1, 0.9 },
	{ QuickImage::LINE, 0.5, 0, 0.5, 1, },
	{ QuickImage::LINE, 0, 0.5, 1, 0.5 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::cross_formula[3] = {
	{ QuickImage::LINE, 0.1, 0.1, 0.9, 0.9 },
	{ QuickImage::LINE, 0.9, 0.1, 0.1, 0.9 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

static const double	raf_b = 0.1;
static const double	raf_sw = 0.4;
static const double	raf_sl = 0.3;

const QICmd StockImage::right_arrow_formula[6] = {
	{ QuickImage::MIRROR, 1, 0, 0, 0, },
	{ QuickImage::LINE, raf_b, 0.5, raf_b, 0.5-(raf_sw/2.0) },
	{ QuickImage::LINE, raf_b, 0.5-(raf_sw/2.0), (raf_b + raf_sl), 0.5-(raf_sw/2.0) },
	{ QuickImage::LINE, raf_sl+raf_b, 0.5-(raf_sw/2.0), raf_sl+raf_b, raf_b },
	{ QuickImage::LINE, raf_sl+raf_b, raf_b, 1.0-raf_b, 0.5 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::symbol_play_formula[4] = {
	{ QuickImage::LINE, 0.1, 0.1, 0.9, 0.5 },
	{ QuickImage::LINE, 0.9, 0.5, 0.1, 0.9 },
	{ QuickImage::LINE, 0.1, 0.9, 0.1, 0.1 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::symbol_pause_formula[6] = {
	{ QuickImage::MIRROR, 0, 1, 0, 0, },
	{ QuickImage::LINE, 0.2, 0.1, 0.2, 0.9 },
	{ QuickImage::LINE, 0.4, 0.1, 0.4, 0.9 },
	{ QuickImage::LINE, 0.2, 0.1, 0.4, 0.1 },
	{ QuickImage::LINE, 0.2, 0.9, 0.4, 0.9 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::symbol_record_formula[5] = {
	{ QuickImage::MIRROR, 1, 1, 1, 0, },
	{ QuickImage::LINE, 0.1, 0.5, 0.1, 0.3 },
	{ QuickImage::LINE, 0.1, 0.3, 0.3, 0.1 },
	{ QuickImage::LINE, 0.3, 0.1, 0.5, 0.1 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

/*
const QICmd StockImage::clicker_alive_old_formula[18] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 118.38, 157.38, 118.56, 164.38},
	{ QuickImage::LINE, 118.56, 164.38, 114.50, 164.44},
	{ QuickImage::LINE, 127.38, 158.38, 128.31, 164.44},
	{ QuickImage::LINE, 128.31, 164.44, 123.25, 164.38},
	{ QuickImage::LINE, 110.36, 147.36, 112.27, 147.36},
	{ QuickImage::LINE, 111.44, 146.38, 111.38, 148.25},
	{ QuickImage::LINE, 114.18, 150.91, 111.64, 155.09},
	{ QuickImage::LINE, 109.45, 143.45, 106.73, 134.18},
	{ QuickImage::LINE, 106.73, 134.18, 112.55, 140.91},
	{ QuickImage::LINE, 115.64, 141.27, 111.64, 131.45},
	{ QuickImage::LINE, 111.64, 131.45, 112.55, 142.36},
	{ QuickImage::LINE, 145.45, 139.82, 135.09, 148.73},
	{ QuickImage::LINE, 90.00, 150.55, 127.09, 158.36},
	{ QuickImage::LINE, 127.09, 158.36, 135.82, 149.09},
	{ QuickImage::LINE, 135.82, 149.09, 122.18, 139.64},
	{ QuickImage::LINE, 122.18, 139.64, 90.00, 150.55},
	{ QuickImage::END, 0, 0, 0, 0, }
};
*/

const QICmd StockImage::clicker_alive_formula[26] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 310.36, 159.82, 350.91, 159.45,},
	{ QuickImage::LINE, 350.91, 159.45, 344.18, 144.36,},
	{ QuickImage::LINE, 344.18, 144.36, 310.36, 159.82,},
	{ QuickImage::LINE, 326.91, 145.09, 317.82, 141.45},
	{ QuickImage::LINE, 317.82, 141.45, 326.91, 152.18},
	{ QuickImage::LINE, 326.91, 152.18, 327.45, 139.45},
	{ QuickImage::LINE, 327.45, 139.45, 332.55, 149.64},
	{ QuickImage::LINE, 364.18, 162.55, 350.91, 159.82},
	{ QuickImage::LINE, 326.55, 165.27, 330.73, 165.27},
	{ QuickImage::LINE, 330.73, 165.27, 330.73, 160.00},
	{ QuickImage::LINE, 340.18, 165.45, 344.36, 165.64},
	{ QuickImage::LINE, 344.36, 165.64, 344.18, 160.00},
	{ QuickImage::LINE, 313.45, 157.82, 312.45, 157.09},
	{ QuickImage::LINE, 312.45, 157.09, 310.82, 156.73},
	{ QuickImage::LINE, 310.82, 156.73, 309.09, 156.82},
	{ QuickImage::LINE, 309.09, 156.82, 307.64, 158.55},
	{ QuickImage::LINE, 307.64, 158.55, 308.45, 160.64},
	{ QuickImage::LINE, 308.45, 160.64, 311.64, 161.09},
	{ QuickImage::LINE, 311.64, 161.09, 313.36, 159.91},
	{ QuickImage::LINE, 326.64, 153.45, 324.91, 154.82},
	{ QuickImage::LINE, 324.91, 154.82, 326.73, 156.64},
	{ QuickImage::LINE, 326.73, 156.64, 328.55, 155.00},
	{ QuickImage::LINE, 328.55, 155.00, 326.64, 153.45},
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::clicker_dead_formula[25] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 445.82,147.27,398.91,156.36},
	{ QuickImage::LINE, 398.91,156.36,435.27,164.73},
	{ QuickImage::LINE, 435.27,164.73,445.82,147.27},
	{ QuickImage::LINE, 448.00,164.73,444.55,159.09},
	{ QuickImage::LINE, 444.55,159.09,449.64,153.64},
	{ QuickImage::LINE, 449.64,153.64,445.64,147.27},
	{ QuickImage::LINE, 420.73,161.45,406.91,166.36},
	{ QuickImage::LINE, 406.91,166.36,412.55,159.82},
	{ QuickImage::LINE, 431.64,140.36,438.00,138.91},
	{ QuickImage::LINE, 438.00,138.91,437.64,148.91},
	{ QuickImage::LINE, 419.45,142.55,425.82,138.00},
	{ QuickImage::LINE, 425.82,138.00,426.73,150.91},
	{ QuickImage::LINE, 422.73,154.73,428.00,159.82},
	{ QuickImage::LINE, 428.73,155.45,423.09,158.91},
	{ QuickImage::LINE, 402.56,157.62,401.00,158.75},
	{ QuickImage::LINE, 401.00,158.75,398.88,158.94},
	{ QuickImage::LINE, 398.88,158.94,396.25,158.25},
	{ QuickImage::LINE, 396.25,158.25,395.56,156.00},
	{ QuickImage::LINE, 395.56,156.00,395.81,154.19},
	{ QuickImage::LINE, 395.81,154.19,397.25,152.38},
	{ QuickImage::LINE, 397.25,152.38,399.62,152.62},
	{ QuickImage::LINE, 399.62,152.62,401.38,153.44},
	{ QuickImage::LINE, 401.38,153.44,402.25,154.81},
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::plug_connected_formula[11] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 125.38,248.75, 140.25,248.88 },
	{ QuickImage::LINE, 110.38,241.50, 125.62,241.62 },
	{ QuickImage::LINE, 125.62,241.62, 125.50,255.38 },
	{ QuickImage::LINE, 125.50,255.38, 109.38,255.50 },
	{ QuickImage::LINE, 110.25,251.12, 110.25,245.38 },
	{ QuickImage::LINE, 110.25,245.38, 121.62,245.38 },
	{ QuickImage::LINE, 121.62,245.38, 121.50,251.12 },
	{ QuickImage::LINE, 121.50,251.12, 110.25,251.12 },
	{ QuickImage::LINE, 90.25,248.50, 121.25,248.50 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::plug_connecting_formula[11] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 183.25,248.50, 190.09,248.73 },
	{ QuickImage::LINE, 190.25,251.12, 190.25,245.38 },
	{ QuickImage::LINE, 190.25,245.38, 201.62,245.38 },
	{ QuickImage::LINE, 201.62,245.38, 201.50,251.12 },
	{ QuickImage::LINE, 201.50,251.12, 190.25,251.12 },
	{ QuickImage::LINE, 218.38,248.75, 233.25,248.88 },
	{ QuickImage::LINE, 203.38,241.50, 218.62,241.62 },
	{ QuickImage::LINE, 218.62,241.62, 218.50,255.38 },
	{ QuickImage::LINE, 218.50,255.38, 202.38,255.50 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

//-- disconnected
const QICmd StockImage::plug_disconnected_formula[12] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 270.18,249.64, 290.73,249.64 },
	{ QuickImage::LINE, 290.73,249.64, 290.75,253.12 },
	{ QuickImage::LINE, 290.75,253.12, 279.25,252.88 },
	{ QuickImage::LINE, 279.25,252.88, 279.25,246.38 },
	{ QuickImage::LINE, 279.25,246.38, 290.50,246.25 },
	{ QuickImage::LINE, 290.50,246.25, 290.62,249.62 },
	{ QuickImage::LINE, 303.00,256.38, 318.62,256.38 },
	{ QuickImage::LINE, 318.62,256.38, 318.50,242.12 },
	{ QuickImage::LINE, 318.50,242.12, 303.50,242.25 },
	{ QuickImage::LINE, 324.50,249.75, 318.50,249.62 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

//-- disconnecting
const QICmd StockImage::plug_disconnecting_formula[12] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 369.12,248.62, 389.26,248.43 },
	{ QuickImage::LINE, 389.26,248.43, 389.22,245.35 },
	{ QuickImage::LINE, 389.22,245.35, 378.12,245.50 },
	{ QuickImage::LINE, 378.12,245.50, 378.12,251.00 },
	{ QuickImage::LINE, 378.12,251.00, 389.30,251.09 },
	{ QuickImage::LINE, 389.30,251.09, 389.22,248.65 },
	{ QuickImage::LINE, 385.22,255.48, 400.39,255.43 },
	{ QuickImage::LINE, 400.39,255.43, 400.30,241.35 },
	{ QuickImage::LINE, 400.30,241.35, 385.48,241.43 },
	{ QuickImage::LINE, 406.52,248.87, 400.39,248.87 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd StockImage::hand_formula[21] = {
	{ QuickImage::AUTOSCALE, 0, 0, 0, 0 },
	{ QuickImage::LINE, 778.00, 698.67, 776.67, 820.00 },
	{ QuickImage::LINE, 776.67, 820.00, 749.33, 802.00 },
	{ QuickImage::LINE, 749.33, 802.00, 735.33, 766.00 },
	{ QuickImage::LINE, 735.33, 766.00, 714.67, 771.33 },
	{ QuickImage::LINE, 714.67, 771.33, 720.00, 822.67 },
	{ QuickImage::LINE, 720.00, 822.67, 761.33, 860.67 },
	{ QuickImage::LINE, 761.33, 860.67, 778.00, 867.33 },
	{ QuickImage::LINE, 778.00, 867.33, 849.33, 868.67 },
	{ QuickImage::LINE, 849.33, 868.67, 870.00, 841.33 },
	{ QuickImage::LINE, 870.00, 841.33, 868.67, 754.67 },
	{ QuickImage::LINE, 868.67, 754.67, 847.33, 754.00 },
	{ QuickImage::LINE, 847.33, 754.00, 846.00, 777.33 },
	{ QuickImage::LINE, 846.00, 777.33, 840.00, 778.67 },
	{ QuickImage::LINE, 840.00, 778.67, 837.33, 752.67 },
	{ QuickImage::LINE, 837.33, 752.67, 814.00, 752.00 },
	{ QuickImage::LINE, 814.00, 752.00, 814.00, 778.67 },
	{ QuickImage::LINE, 814.00, 778.67, 802.67, 778.00 },
	{ QuickImage::LINE, 802.67, 778.00, 802.00, 698.67 },
	{ QuickImage::LINE, 802.00, 698.67, 778.00, 698.67 },
	{ QuickImage::END, 0, 0, 0, 0, }
};

const QICmd *StockImage::stock_formulas[] = { &star_formula[0],
												&cross_formula[0],
												&right_arrow_formula[0],
												&clicker_alive_formula[0],
												&clicker_dead_formula[0],
												&plug_connected_formula[0],
												&plug_connecting_formula[0],
												&plug_disconnected_formula[0],
												&plug_disconnecting_formula[0],
												&symbol_play_formula[0],
												&symbol_pause_formula[0],
												&symbol_record_formula[0],
												&hand_formula[0]
											};

StockImage::StockImage( int id, int w, int h, Fl_Color clr, int hspace, int vspace, int align, int rotate )
							: QuickImage( w, h, clr, NULL, hspace, vspace, align, rotate )
{
	if( ( id < 0 ) || ( id >= (int)sizeof( stock_formulas ) ) )
		return;	//the id is out of range

	intize_formula( stock_formulas[ id ] );
};
