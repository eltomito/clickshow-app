/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Stock_Image_h
#define _Stock_Image_h

#include <FL/Fl.H>
#include <FL/Fl_Image.H>

#include "quick_image.h"

class StockImage : public QuickImage
{
static const QICmd star_formula[5];
static const QICmd cross_formula[3];
static const QICmd right_arrow_formula[6];
static const QICmd clicker_alive_formula[26];
static const QICmd clicker_dead_formula[25];
static const QICmd plug_connected_formula[11];
static const QICmd plug_connecting_formula[11];
static const QICmd plug_disconnected_formula[12];
static const QICmd plug_disconnecting_formula[12];
static const QICmd symbol_play_formula[4];
static const QICmd symbol_pause_formula[6];
static const QICmd symbol_record_formula[5];
static const QICmd hand_formula[21];

static const QICmd *stock_formulas[13];

public:
	static const int		STAR = 0;
	static const int		CROSS = 1;
	static const int		RIGHT_ARROW = 2;
	static const int		CLICKER_ALIVE = 3;
	static const int		CLICKER_DEAD = 4;
	static const int		PLUG_CONNECTED = 5;
	static const int		PLUG_CONNECTING = 6;
	static const int		PLUG_DISCONNECTED = 7;
	static const int		PLUG_DISCONNECTING = 8;
	static const int		SYMBOL_PLAY = 9;
	static const int		SYMBOL_PAUSE = 10;
	static const int		SYMBOL_REC = 11;
	static const int		SYMBOL_HAND = 12;

	StockImage( int id, int w, int h, Fl_Color clr, int hspace=0, int vspace=0, int align=0, int rotate=0 );
};

#endif //_Stock_Image_h
