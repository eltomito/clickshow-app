/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <map>
#include <sstream>
#include <string>
#include <climits>
using std::endl;

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Text_Display.H>

#include "style_engine.h"

/*
 * ======================== StyleWidgetList class ================================
 *
 *		a helper class holding a WidgetStyle and a list of associated widgets
 */
	StyleWidgetList::StyleWidgetList( WidgetStyle *s, unsigned int user_flags )
	{
		widgets.clear();
		//clone style definition object if necessary
		flags = user_flags;
		style = s;
	};

	StyleWidgetList::~StyleWidgetList()
	{
		widgets.clear();
		if( flags && DELETE_STYLE )
			delete style;
	};

	/*
	 * returns the number of widgets the style has been applied to.
	 */
	int StyleWidgetList::apply_to_all()
	{
		T_Widget_List::iterator it;
		int cnt;
		
		it = widgets.begin();
		cnt = 0;

		while( it != widgets.end() )
		{
			style->apply( *it );
			it++;
			cnt++;
		};

		return cnt;
	};

	bool StyleWidgetList::remove_widget( Fl_Widget *w ) {
		widgets.remove( w );
		return true;	//TODO: get rid of this kludge! Nobody actually uses it, AFAIK.
	};

	int StyleWidgetList::remove_widgets( Fl_Widget **w, int size ) {
		int cnt = 0;
		if( size >= 0 ) {
			for( int i = 0; i < size; ++i ) {
				cnt += remove_widget( w[i] ) ? 1:0;
			}
		} else {
			while( w[0] ) {
				cnt += remove_widget( w[0] ) ? 1:0;
				++w;
			}
		}
		return cnt;
	};


/*
 * ========================= StyleEngine class =================================
 */
		/*
		 */
		int StyleEngine::get_next_style_id()
		{
			return next_style_id++;
		};

		/*
		 * returns true if the if is available, false otherwise
		 */
		int StyleEngine::id_available( int id )
		{
			T_Style_Map::iterator it;
			
			it =	styles.find( id );
			if( it == styles.end() )
				return true;

			return false;
		};

		StyleEngine::StyleEngine()
		{
			styles.clear();
			next_style_id = 0;
		};

		StyleEngine::~StyleEngine()
		{
			clear_all_makeup();
			delete_all_styles();
		};

		/*
		 * adds a style object to the list of known styles
		 * under a requested or automatically generated id.
		 * parameters:
		 * 	s - the style definition
		 * 	clonestyle - true if StyleEngine should create its own copy od s.
		 *						The destructor of StyleEngine always frees s.
		 * 	id - requested id for this style or -1 for automatic assignment
		 *	returns:
		 *		the id assigned to the object or -1 if unsuccessful
		 *		(possibly because the requested id is not available)
		 */
		int	StyleEngine::add_style( WidgetStyle *s, int delete_style, int id )
		{
			//sort out the id
			if( id == -1 )
				id = get_next_style_id();
			else
				//check if this id is available	
				if( !id_available( id ) )
					return -1;

			unsigned int flags = 0;
			if( delete_style )
				flags = StyleWidgetList::DELETE_STYLE;

			styles.insert( T_Style_Pair( id, new StyleWidgetList( s, flags ) ) );

			return id;
		}
/*
		WidgetStyle *get_style( int id ) {
			T_Style_Map::iterator it = styles.find( id );
			if( it == styles.end() ) {
				return NULL;
			}
			return it->second;
		};
*/
		/*
		 * delete a style from the list of styles
		 * after clearing it from all widgets and deleting its definition object first.
		 */
		int	StyleEngine::delete_style( int s )
		{
			T_Style_Map::iterator it;

			it = styles.find( s );
			if( it != styles.end() )
			{
				clear_style_from_all_widgets( it );
				delete it->second;
				styles.erase( it );
				return true;
			}
			return false;
		};

		/*
		 * destroy all styles clearing them from all widgets first
		 */
		void	StyleEngine::delete_all_styles()
		{
			T_Style_Map::iterator it;

			it = styles.begin();
			while( it != styles.end() )
			{
				clear_style_from_all_widgets( it );
				delete it->second;
				it++;
			};
			styles.clear();
		};

		/*
		 * assign a known style to a widget
		 * returns true or false indicating success or failure
		 *
		 * NOTE: !!! The method doesn't check whether this widget
		 * already has a different style. If so, it will have 2 or several
		 * styles at once and anything can happen.
		 */
		int	StyleEngine::assign_style( int s, Fl_Widget *w )
		{
			T_Style_Map::iterator it;

			it = styles.find( s );
			if( it != styles.end() )
			{
				it->second->widgets.push_back( w );
				return true;
			}

			return false;
		};

		/*
		 * assign a known style to several widgets
		 * returns true or false indicating success or failure
		 *
		 * NOTE: !!! The method doesn't check whether this widget
		 * already has a different style. If so, it will have 2 or several
		 * styles at once and anything can happen.
		 */
		int	StyleEngine::assign_style( int s, Fl_Widget **w, int cnt )
		{
			T_Style_Map::iterator it;

			it = styles.find( s );
			if( it != styles.end() )
			{
				while(( cnt != 0 )&&( w[0] != NULL ))
				{
					it->second->widgets.push_back( w[0] );
					cnt--;
					w++;
				};
				return true;
			}
			return false;
		};

		/*
		 * clear style from this widget.
		 * returns the id of the previously assigned style (-1 if none)
		 */
		int	StyleEngine::clear_style_from_widget( Fl_Widget *w )
		{
			//TODO: implement?
			return -1;
		}

		/*
		 * clear style from several widgets.
		 * returns the number of styles that were actually removed from widgets.
		 */
		int	StyleEngine::clear_style_from_widgets( Fl_Widget **w, int size )
		{
			T_Style_Map::iterator it;
			int cnt = 0;

			it = styles.begin();
			while( it != styles.end() )
			{
				cnt += it->second->remove_widgets( w, size );
				it++;
			};
			return cnt;
		}

		/*
		 * remove this style from all widgets
		 * returns true if the style exists, false otherwise
		 */
		int	StyleEngine::clear_style_from_all_widgets( int s )
		{
			T_Style_Map::iterator it;

			it = styles.find( s );
			if( it != styles.end() )
			{
				it->second->widgets.clear();
				return true;
			};
			return false;
		};

		/*
		 * remove this style from all widgets
		 */
		void	StyleEngine::clear_style_from_all_widgets( T_Style_Map::iterator it )
		{
				it->second->widgets.clear();
		};			

		/*
		 * clear all widgets of all styles
		 */
		void	StyleEngine::clear_all_makeup()
		{
			T_Style_Map::iterator it;

			it = styles.begin();
			while( it != styles.end() )
			{
				it->second->widgets.clear();
				it++;
			};
		};

		/*
		 * reapply this style to all widgets it is assigned to
		 * returns the number of widgets affected or -1 if style doesn't exist.
		 */
		int	StyleEngine::refresh_style( int s )
		{
			T_Style_Map::iterator	it;

			it = styles.find( s );
			if( it != styles.end() )
				return it->second->apply_to_all();

			return -1;
		};


		/*
		 * reapply all styles to all widgets they are assigned to.
		 * returns the number of styles refreshed.
		 */
		int	StyleEngine::refresh_all_styles()
		{
			T_Style_Map::iterator it;
			int cnt;

			cnt = 0;
			it = styles.begin();
			while( it != styles.end() )
			{
				it->second->apply_to_all();
				it++;
				cnt++;
			};

			return cnt;
		};
