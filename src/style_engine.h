/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Style_Engine_h
#define _Style_Engine_h

#include <map>
#include <list>
#include <climits>

#include <FL/Fl.H>
#include <FL/Fl_Widget.H>

#include "styles.h"

/*
 * a helper class holding a WidgetStyle and a list of associated widgets
 */
class StyleWidgetList
{
public:
	static const unsigned int		DELETE_STYLE	=	1;

	typedef std::list<Fl_Widget *> T_Widget_List;	
	T_Widget_List	widgets;

	unsigned int flags;
	WidgetStyle *style;

	/*
	 * create a list of widgets associated with a style
	 *		s - the style these widgets have applied
	 *		user_flags - an orred awway of flags:
	 *						DELETE_STYLE - delete the style object in the destructor
	 */
	StyleWidgetList( WidgetStyle *s, unsigned int user_flags=0 );
	~StyleWidgetList();

	/*
	 * returns the number of widgets the style has been applied to.
	 */
	int apply_to_all();

	bool	remove_widget( Fl_Widget *w );
	int	remove_widgets( Fl_Widget **w, int size = -1 );
};

/*
 * StyleEngine class
 */
class StyleEngine
{
private:
	typedef std::map<int, StyleWidgetList*> T_Style_Map;
	typedef std::pair<int, StyleWidgetList*> T_Style_Pair;

	T_Style_Map	styles;
	int	next_style_id;

	public:
		StyleEngine();
		~StyleEngine();

		/*
		 * adds a style object to the list of known styles
		 * under a requested or automatically generated id.
		 * parameters:
		 * 	s - the style definition
		 * 	clonestyle - true if StyleEngine should create its own copy od s.
		 *						The destructor of StyleEngine always frees s.
		 * 	id - requested id for this style or -1 for automatic assignment
		 *	returns:
		 *		the id assigned to the object or -1 if unsuccessful
		 *		(possibly because the requested id is not available)
		 */
		int	add_style( WidgetStyle *s, int delete_style = false, int id=-1 );
//		WidgetStyle *get_style( int id );

		/*
		 * delete a style from the list of styles
		 * after clearing it from all widgets and deleting its definition object first.
		 */
		int	delete_style( int s );

		/*
		 * destroy all styles clearing them from all widgets first
		 */
		void	delete_all_styles();

		/*
		 * assign a known style to a widget
		 * returns true or false indicating success or failure
		 *
		 * NOTE: !!! The method doesn't check whether this widget
		 * already has a different style. If so, it will have 2 or several
		 * styles at once and anything can happen.
		 */
		int	assign_style( int s, Fl_Widget *w );

		/*
		 * assign a known style to several widgets
		 * returns true or false indicating success or failure
		 *
		 * NOTE: !!! The method doesn't check whether this widget
		 * already has a different style. If so, it will have 2 or several
		 * styles at once and anything can happen.
		 */
		int	assign_style( int s, Fl_Widget **w, int cnt=-1 );

		/*
		 * clear style from this widget.
		 * returns the id of the previously assigned style (-1 if none)
		 */
		int	clear_style_from_widget( Fl_Widget *w );

		/*
		 * clear style from several widgets.
		 * returns the number of widgets that were actually found to have a style assigned.
		 */
		int	clear_style_from_widgets( Fl_Widget **w, int size = -1 );

		/*
		 * remove this style from all widgets
		 * returns true if the style exists, false otherwise
		 */
		int	clear_style_from_all_widgets( int s );

		/*
		 * remove this style from all widgets
		 */
		void	clear_style_from_all_widgets( T_Style_Map::iterator it );

		/*
		 * clear all widgets of all styles
		 */
		void	clear_all_makeup();

		/*
		 * reapply this style to all widgets it is assigned to
		 * returns the number of widgets affected or -1 if style doesn't exist.
		 */
		int	refresh_style( int s );

		/*
		 * reapply all styles to all widgets they are assigned to.
		 * returns the number of styles refreshed.
		 */
		int	refresh_all_styles();

private:
		int get_next_style_id();

		/*
		 * returns true if the if is available, false otherwise
		 */
		int id_available( int id );
};

#endif	//_Style_Engine_h
