/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "style_prefs.h"
#include "cl_enums.h"

//============================= class StylePrefs ==================================

	StylePrefs::StylePrefs( Prefs *p )
	{
		prefs = p;

		NormalButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );
		FileButton = new ButtonStyle(		FL_PLASTIC_UP_FRAME,	//CLICKY_BLOCK_UP_BOX,	
													FL_GRAY,
													FL_WHITE,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_GRAY );
		FileCButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_GRAY,
													FL_SELECTION_COLOR,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_GRAY,
													FL_HELVETICA,
													false,
													false );
		SearchButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_GRAY,
													FL_SELECTION_COLOR,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_BLUE );
		SearchWidget = new WidgetStyle( FL_PLASTIC_UP_FRAME,
													FL_GRAY,
													FL_GRAY );
		MarkButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_YELLOW,
													FL_SELECTION_COLOR,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		OptionButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_MAGENTA,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		MiscOptsButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_CYAN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE,
													FL_HELVETICA,
													false,
													false );

		CandyOptsButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													(Fl_Color)0xd4ff0000,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE,
													FL_HELVETICA,
													false,
													false );

		BBOptsButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_BLUE,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE,
													FL_HELVETICA,
													false,
													false );

		NetOptsButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE,
													FL_HELVETICA,
													false,
													false );

		EditButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_YELLOW,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		RecPanelButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_RED,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		HelpOkButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_WHITE,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		HelpKeyButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_YELLOW,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		HelpDescButton = new ButtonStyle( FL_NO_BOX,
													FL_GRAY,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		HelpLabelButton = new ButtonStyle( FL_NO_BOX,
													FL_GRAY,
													FL_BLACK,
													FL_NORMAL_LABEL,
													(3*prefs->ui_font_size)/2,
													FL_WHITE );

		NoLabelButton = new ButtonStyle( FL_NO_BOX,
													FL_BLACK,
													FL_BLACK,
													FL_NORMAL_LABEL,
													0,
													FL_BLACK );

		NetLabelButton = new ButtonStyle( FL_NO_BOX,
													FL_BLACK,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		NoBorderButton = new ButtonStyle( FL_NO_BOX,
													FL_BLACK,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		TitleDisplay = new ButtonStyle( FL_FLAT_BOX,
													FL_BLACK,		//Fl_Color( 0x22000000 )
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->title_font_size,
													prefs->title_font_color,
													prefs->title_font );

		BBTitleDisplay = new ButtonStyle( FL_FLAT_BOX,
													FL_BLACK,		//Fl_Color( 0x22000000 )
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->bb_title_font_size,
													prefs->bb_title_font_color,
													prefs->bb_title_font );

		AskButton = new ButtonStyle( FL_PLASTIC_UP_FRAME,
													FL_BLUE,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE );

		AskLabel = new ButtonStyle( FL_NO_BOX,
												FL_BLACK,		//Fl_Color( 0x22000000 )
												FL_BLACK,
												FL_NORMAL_LABEL,
												prefs->ui_font_size,
												Fl_Color( 0xffff0000 )
												);

		InputLabel = new ButtonStyle( FL_NO_BOX,
												FL_BLACK,		//Fl_Color( 0x22000000 )
												FL_BLACK,
												FL_NORMAL_LABEL,
												prefs->ui_font_size,
												Fl_Color( 0xffff0000 )
												);

		InputLabelInvalid = new ButtonStyle( FL_NO_BOX,
												FL_BLACK,		//Fl_Color( 0x22000000 )
												FL_BLACK,
												FL_NORMAL_LABEL,
												prefs->ui_font_size,
												Fl_Color( 0xff220000 )
												);

		LangButton = new ButtonStyle( FL_FLAT_BOX,
													FL_BLACK,
													FL_CYAN,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_GRAY,
													FL_HELVETICA,
													false,
													false );

		NormalList = new BrowserStyle( FL_FLAT_BOX,
													FL_BLACK,		//Fl_Color( 0x22000000 )
													FL_MAGENTA,
													prefs->ui_font_size,
													FL_WHITE );

		//group styles
		GroupBorderNone = new WidgetStyle(
							FL_NO_BOX, //Fl_Boxtype box 
							FL_BLACK, //Fl_Color bg
							FL_SELECTION_COLOR, //Fl_Color sel 
							FL_NO_LABEL, //Fl_Labeltype label
							false ); //bool can_focus 

		GroupBorderDefault = new WidgetStyle(
							FL_PLASTIC_UP_FRAME, //Fl_Boxtype box 
							FL_WHITE, //Fl_Color bg
							FL_SELECTION_COLOR, //Fl_Color sel 
							FL_NO_LABEL, //Fl_Labeltype label
							false ); //bool can_focus 

		//list panel images
		star_image = new StockImage( StockImage::STAR, 11, 11, FL_YELLOW, 10, 0, QuickImage::QI_RIGHT );
		big_star_image = new StockImage( StockImage::STAR, 30, 30, prefs->title_font_color );

		SubtitleImgB = new ImgBStyle( FL_NO_BOX,
													FL_BLACK,
													FL_SELECTION_COLOR,
													FL_NORMAL_LABEL,
													prefs->list_font_size,
													prefs->list_font_color, FL_HELVETICA,
													FL_ALIGN_TEXT_NEXT_TO_IMAGE,
													NULL );

		StarImgB = new ImgBStyle( FL_NO_BOX,
											FL_BLACK,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											big_star_image );

		//show next, up and down image buttons

		shownext_image = new StockImage( StockImage::RIGHT_ARROW, 25, 25, FL_WHITE, 0, 4, QuickImage::QI_TOP);
		ShowNextImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													shownext_image );

		goup_image = new StockImage( StockImage::RIGHT_ARROW, 20, 20, FL_WHITE, 0, 4, QuickImage::QI_TOP, 3);
		GoUpImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													goup_image );

		godown_image = new StockImage( StockImage::RIGHT_ARROW, 20, 20, FL_WHITE, 0, 4, QuickImage::QI_TOP, 1);
		GoDownImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													godown_image );

		play_image = new StockImage( StockImage::SYMBOL_PLAY, 25, 25, FL_WHITE, 0, 4, QuickImage::QI_TOP);
		PlayImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													play_image );

		pause_image = new StockImage( StockImage::SYMBOL_PAUSE, 25, 25, FL_WHITE, 0, 4, QuickImage::QI_TOP);
		PauseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													pause_image );

		rec_image = new StockImage( StockImage::SYMBOL_REC, 25, 25, FL_RED, 0, 4, QuickImage::QI_TOP);
		RecImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_RED,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_RED, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													rec_image );

		recpause_image = new StockImage( StockImage::SYMBOL_PAUSE, 25, 25, FL_RED, 0, 4, QuickImage::QI_TOP);
		RecPauseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_RED,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_RED, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													recpause_image );

		hand_image = new StockImage( StockImage::SYMBOL_HAND, 16, 16, FL_WHITE, 2, 2, QuickImage::QI_TOP );
		HandImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
													FL_GREEN,
													FL_BLACK,
													FL_NORMAL_LABEL,
													prefs->ui_font_size,
													FL_WHITE, FL_HELVETICA,
													FL_ALIGN_IMAGE_OVER_TEXT,
													hand_image );

		//close button image and style
		fpclose_image = new StockImage( StockImage::CROSS, 10, 10, FL_MAGENTA, 0, 0, QuickImage::QI_CENTER );
		bbclose_image = new StockImage( StockImage::CROSS, 10, 10, FL_BLUE, 0, 0, QuickImage::QI_CENTER );
		opclose_image = new StockImage( StockImage::CROSS, 10, 10, FL_CYAN, 0, 0, QuickImage::QI_CENTER );
		cpclose_image = new StockImage( StockImage::CROSS, 10, 10, Fl_Color(0xd4ff0000), 0, 0, QuickImage::QI_CENTER );
		licclose_image = new StockImage( StockImage::CROSS, 10, 10, FL_WHITE, 0, 0, QuickImage::QI_CENTER );
		pwclose_image = new StockImage( StockImage::CROSS, 10, 10, FL_GRAY, 0, 0, QuickImage::QI_CENTER );
		recclose_image = new StockImage( StockImage::CROSS, 10, 10, FL_RED, 0, 0, QuickImage::QI_CENTER );

		RECCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											FL_RED,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											recclose_image );

		FPCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											FL_MAGENTA,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											fpclose_image );

		BBCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											FL_BLUE,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											bbclose_image );

		OPCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											FL_CYAN,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											opclose_image );

		CPCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											(Fl_Color)0xd6ff0000,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											cpclose_image );

		LicCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											FL_WHITE,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											licclose_image );

		PWCloseImgB = new ImgBStyle( FL_PLASTIC_UP_FRAME,
											FL_WHITE,
											FL_BLACK,
											FL_NORMAL_LABEL,
											16, FL_BLACK, FL_HELVETICA,
											FL_ALIGN_IMAGE_BACKDROP,
											pwclose_image );

		DebugPanel = new WidgetStyle( FL_UP_FRAME, Fl_Color( 0x33110000 ) );
		DashPanel = new WidgetStyle();
		NormalPanel = new WidgetStyle( FL_FLAT_BOX, FL_BLACK );	//(Fl_Color)0x11111100
		FocusingPanel = new WidgetStyle( FL_FLAT_BOX, FL_BLACK, FL_SELECTION_COLOR, FL_NO_LABEL, true );

		ScrollPanel = new WidgetStyle( FL_FLAT_BOX, FL_BLACK );

		ScrollTextPanel = new WidgetStyle( FL_FLAT_BOX, FL_BLACK );	//Fl_Color( 0x11110000 )
		TextOptsPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, FL_MAGENTA );
		CandyOptsPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, (Fl_Color)0xd4ff0000 );
		BBOptsPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, FL_BLUE );
		NetOptsPanel = new WidgetStyle( FL_FLAT_BOX, FL_BLACK );	//(Fl_Color)0x84aa9100
		MiscOptsPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, FL_CYAN );
		AskPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, Fl_Color( 0xffff0000 ) );
		HelpPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, FL_WHITE );
		RecorderPanel = new WidgetStyle( FL_PLASTIC_UP_FRAME, FL_RED );

		TextInput = new InputStyle( FL_FLAT_BOX,
												FL_BLACK,		//(Fl_Color)0x0c0c0c,							//bg
												(Fl_Color)0xbbbbff,			//selection
												16,								//font size
												FL_YELLOW						//font color
												);

		LicenseKeyInput = new InputStyle( FL_FLAT_BOX,
												(Fl_Color)0x0c0c0c,							//bg
												(Fl_Color)0xbbbbff,			//selection
												16,								//font size
												FL_GREEN						//font color
												);

		HelpTextDisplay  = new InputStyle( FL_PLASTIC_UP_FRAME,
												FL_GRAY,							//bg
												(Fl_Color)0x05050500,			//selection
												16,								//font size
												FL_WHITE						//font color
												);

		//-- add styles into the the style engine

		Makeup = new StyleEngine();

		Makeup->add_style( NormalButton, true, NORMAL_BUTTON );
		Makeup->add_style( FileButton, true, FILE_BUTTON );
		Makeup->add_style( FileCButton, true, FILE_CBUTTON );
		Makeup->add_style( MarkButton, true, MARK_BUTTON );
		Makeup->add_style( StarImgB, true, STAR_BUTTON );
		Makeup->add_style( SearchButton, true, SEARCH_BUTTON );
		Makeup->add_style( SearchWidget, true, SEARCH_WIDGET );
		Makeup->add_style( OptionButton, true, OPTION_BUTTON );
		Makeup->add_style( MiscOptsButton, true, MISCOPTS_BUTTON );
		Makeup->add_style( CandyOptsButton, true, CANDYOPTS_BUTTON );
		Makeup->add_style( BBOptsButton, true, BBOPTS_BUTTON );
		Makeup->add_style( NetOptsButton, true, NETOPTS_BUTTON );
		Makeup->add_style( EditButton, true, EDIT_BUTTON );
		Makeup->add_style( HelpOkButton, true, HELP_OK_BUTTON );
		Makeup->add_style( InputLabel, true, INPUT_LABEL );
		Makeup->add_style( InputLabelInvalid, true, INPUT_LABEL_INVALID );
		Makeup->add_style( RecPanelButton, true, RECPANEL_BUTTON );

		Makeup->add_style( NoLabelButton, true, NO_LABEL_BUTTON );
		Makeup->add_style( NetLabelButton, true, NET_LABEL_BUTTON );
		Makeup->add_style( NoBorderButton, true, NO_BORDER_BUTTON );

		Makeup->add_style( FPCloseImgB, true, FPCLOSE_BUTTON );
		Makeup->add_style( BBCloseImgB, true, BBCLOSE_BUTTON );
		Makeup->add_style( OPCloseImgB, true, OPCLOSE_BUTTON );
		Makeup->add_style( CPCloseImgB, true, CPCLOSE_BUTTON );
		Makeup->add_style( RECCloseImgB, true, RECCLOSE_BUTTON );

		Makeup->add_style( ShowNextImgB, true, SHOWNEXT_BUTTON );
		Makeup->add_style( GoUpImgB, true, GOUP_BUTTON );
		Makeup->add_style( GoDownImgB, true, GODOWN_BUTTON );
		Makeup->add_style( HandImgB, true, HAND_BUTTON );

		Makeup->add_style( PauseImgB, true, PAUSE_BUTTON );

		Makeup->add_style( TitleDisplay, true, TITLE_DISPLAY );
		Makeup->add_style( BBTitleDisplay, true, BB_TITLE_DISPLAY );

		Makeup->add_style( DebugPanel, true, DEBUG_PANEL );
		Makeup->add_style( NormalPanel, true, NORMAL_PANEL );
		Makeup->add_style( FocusingPanel, true, FOCUSING_PANEL );
		Makeup->add_style( DashPanel, true, DASH_PANEL );
		Makeup->add_style( ScrollPanel, true, SCROLL_PANEL );
		Makeup->add_style( ScrollTextPanel, true, SCROLLTEXT_PANEL );
		Makeup->add_style( TextOptsPanel, true, TEXTOPTS_PANEL );
		Makeup->add_style( CandyOptsPanel, true, CANDYOPTS_PANEL );
		Makeup->add_style( BBOptsPanel, true, BBOPTS_PANEL );
		Makeup->add_style( NetOptsPanel, true, NETOPTS_PANEL );
		Makeup->add_style( MiscOptsPanel, true, MISCOPTS_PANEL );
		Makeup->add_style( RecorderPanel, true, RECORDER_PANEL );

		Makeup->add_style( TextInput, true, TEXT_INPUT );

		Makeup->add_style( NormalList, true, NORMAL_LIST );

		Makeup->add_style( GroupBorderNone, true, GROUP_BORDER_NONE );
		Makeup->add_style( GroupBorderDefault, true, GROUP_BORDER_DEFAULT );
	};

	StylePrefs::~StylePrefs()
	{
		//style engine
		delete Makeup;

		//delete stock images
		delete pause_image;
		delete recclose_image;
		delete star_image;
		delete big_star_image;
		delete fpclose_image;
		delete bbclose_image;
		delete opclose_image;
		delete cpclose_image;
		delete shownext_image;
		delete goup_image;
		delete godown_image;
		delete hand_image;

		//delete styles that were never added to the style engine
		delete AskButton;
        delete AskLabel;
        delete AskPanel;
        delete SubtitleImgB;
	};

	StyleEngine *StylePrefs::get_engine()
	{
		return Makeup;
	};

	void StylePrefs::set_title_font_size( int size )
	{
		TitleDisplay->ls.set_size( size );
		prefs->title_font_size = size;
		Makeup->refresh_style( TITLE_DISPLAY );
	};

	void StylePrefs::set_title_font_color( Fl_Color clr )
	{
		TitleDisplay->ls.set_color( clr );
		big_star_image->set_color( clr );

		prefs->title_font_color = clr;
		Makeup->refresh_style( TITLE_DISPLAY );
		Makeup->refresh_style( STAR_BUTTON );
	};

	void StylePrefs::set_title_font( Fl_Font font )
	{
		TitleDisplay->ls.set_font( font );
		prefs->title_font = font;
		Makeup->refresh_style( TITLE_DISPLAY );
	};

	void StylePrefs::set_bb_title_font_size( int size )
	{
		BBTitleDisplay->ls.set_size( size );
		prefs->bb_title_font_size = size;
		Makeup->refresh_style( BB_TITLE_DISPLAY );
	};

	void StylePrefs::set_bb_title_font_color( Fl_Color clr )
	{
		BBTitleDisplay->ls.set_color( clr );

		prefs->bb_title_font_color = clr;
		Makeup->refresh_style( BB_TITLE_DISPLAY );
	};

	void StylePrefs::set_bb_title_font( Fl_Font font )
	{
		BBTitleDisplay->ls.set_font( font );
		prefs->bb_title_font = font;
		Makeup->refresh_style( BB_TITLE_DISPLAY );
	};
