/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Style_Prefs_h
#define _Style_Prefs_h

#include "style_engine.h"
#include "styles.h"
#include "prefs.h"
#include "stock_image.h"
#include "clicky_button.h"

class StylePrefs
{
	Prefs	*prefs;

public:
	static const int NORMAL_BUTTON = 1;
	static const int FILE_BUTTON = 2;
	static const int SEARCH_BUTTON = 3;
	static const int MARK_BUTTON = 4;
	static const int STAR_BUTTON = 5;
	static const int OPTION_BUTTON = 6;
	static const int MISCOPTS_BUTTON = 7;
	static const int CANDYOPTS_BUTTON = 18;
	static const int BBOPTS_BUTTON = 22;
	static const int NETOPTS_BUTTON = 23;
	static const int RECPANEL_BUTTON = 34;

	static const int FPCLOSE_BUTTON = 8;
	static const int OPCLOSE_BUTTON = 9;
	static const int CPCLOSE_BUTTON = 19;
	static const int BBCLOSE_BUTTON = 24;
	static const int RECCLOSE_BUTTON = 33;

	static const int SHOWNEXT_BUTTON = 10;
	static const int GOUP_BUTTON = 11;
	static const int GODOWN_BUTTON = 35;
	static const int HAND_BUTTON = 12;

	static const int EDIT_BUTTON = 13;

	static const int HELP_OK_BUTTON = 14;
	static const int HELP_KEY_BUTTON = 15;

	static const int FILE_CBUTTON = 16;

	static const int SEARCH_WIDGET = 17;

	static const int INPUT_LABEL = 20;
	static const int INPUT_LABEL_INVALID = 21;

	static const int PLAY_BUTTON = 25;
	static const int PAUSE_BUTTON = 26;

	static const int NO_LABEL_BUTTON = 30;
	static const int NET_LABEL_BUTTON = 31;
	static const int NO_BORDER_BUTTON = 32;

	static const int TEXT_INPUT = 50;
	static const int TITLE_DISPLAY = 51;
	static const int BB_TITLE_DISPLAY = 52;
	static const int NORMAL_LIST = 60;

	static const int DEBUG_PANEL = 99;

	static const int NORMAL_PANEL = 100;
	static const int FOCUSING_PANEL = 109;
	static const int DASH_PANEL = 101;
	static const int SCROLL_PANEL = 102;
	static const int SCROLLTEXT_PANEL = 103;
	static const int TEXTOPTS_PANEL = 104;
	static const int MISCOPTS_PANEL = 105;
	static const int CANDYOPTS_PANEL = 106;
	static const int BBOPTS_PANEL = 107;
	static const int NETOPTS_PANEL = 108;
	static const int RECORDER_PANEL = 110;

	static const int GROUP_BORDER_NONE = 200;
	static const int GROUP_BORDER_DEFAULT = 201;

	ButtonStyle	*NormalButton;
	ButtonStyle	*FileButton;
	ButtonStyle	*FileCButton;
	ButtonStyle	*SearchButton;
	ButtonStyle	*MarkButton;
	ButtonStyle	*OptionButton;
	ButtonStyle	*MiscOptsButton;
	ButtonStyle	*CandyOptsButton;
	ButtonStyle	*BBOptsButton;
	ButtonStyle	*NetOptsButton;
	ButtonStyle	*AskButton;
	ButtonStyle	*EditButton;
	ButtonStyle	*RecPanelButton;

	ButtonStyle	*NoLabelButton;
	ButtonStyle	*NetLabelButton;
	ButtonStyle	*NoBorderButton;

	WidgetStyle *SearchWidget;

	ButtonStyle	*HelpOkButton;
	ButtonStyle	*HelpKeyButton;
	ButtonStyle	*HelpDescButton;
	ButtonStyle	*HelpLabelButton;
	ButtonStyle	*LangButton;

	ButtonStyle	*TitleDisplay;
	ButtonStyle	*BBTitleDisplay;

	ButtonStyle	*AskLabel;
	ButtonStyle	*InputLabel;
	ButtonStyle	*InputLabelInvalid;

	BrowserStyle	*NormalList;

	WidgetStyle *GroupBorderNone;
	WidgetStyle *GroupBorderDefault;

	WidgetStyle	*DebugPanel;
	WidgetStyle	*NormalPanel;
	WidgetStyle	*FocusingPanel;
	WidgetStyle	*DashPanel;
	WidgetStyle	*ScrollPanel;
	WidgetStyle	*ScrollTextPanel;
	WidgetStyle	*TextOptsPanel;
	WidgetStyle	*MiscOptsPanel;
	WidgetStyle	*CandyOptsPanel;
	WidgetStyle	*BBOptsPanel;
	WidgetStyle	*NetOptsPanel;
	WidgetStyle	*AskPanel;
	WidgetStyle	*HelpPanel;
	WidgetStyle	*RecorderPanel;

	InputStyle	*TextInput;
	InputStyle	*HelpTextDisplay;
	InputStyle	*LicenseKeyInput;

	StockImage	*star_image;
	StockImage	*big_star_image;
	ImgBStyle	*SubtitleImgB;
	ImgBStyle	*StarImgB;

	StockImage	*recclose_image;
	ImgBStyle	*RECCloseImgB;

	StockImage	*fpclose_image;
	ImgBStyle	*FPCloseImgB;

	StockImage	*bbclose_image;
	ImgBStyle	*BBCloseImgB;

	StockImage	*opclose_image;
	ImgBStyle	*OPCloseImgB;

	StockImage	*cpclose_image;
	ImgBStyle	*CPCloseImgB;

	StockImage	*licclose_image;
	ImgBStyle	*LicCloseImgB;

	StockImage	*pwclose_image;
	ImgBStyle	*PWCloseImgB;

	StockImage	*shownext_image;
	ImgBStyle	*ShowNextImgB;

	StockImage	*goup_image;
	ImgBStyle	*GoUpImgB;

	StockImage	*godown_image;
	ImgBStyle	*GoDownImgB;

	StockImage	*hand_image;
	ImgBStyle	*HandImgB;

	StockImage	*play_image;
	ImgBStyle	*PlayImgB;

	StockImage	*pause_image;
	ImgBStyle	*PauseImgB;

	StockImage	*rec_image;
	ImgBStyle	*RecImgB;

	StockImage	*recpause_image;
	ImgBStyle	*RecPauseImgB;

	StyleEngine	*Makeup;

	StylePrefs( Prefs *p );
	~StylePrefs();

	StyleEngine *get_engine();

	void set_title_font_size( int size );
	void set_title_font_color( Fl_Color clr );
	void set_title_font( Fl_Font font );

	void set_bb_title_font_size( int size );
	void set_bb_title_font_color( Fl_Color clr );
	void set_bb_title_font( Fl_Font font );

};

#endif //Style_Prefs
