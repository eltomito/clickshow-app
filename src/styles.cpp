/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "styles.h"
#include "rainbow_style.h"

// ================ WidgetStyle class =============================

	WidgetStyle::WidgetStyle(
						Fl_Boxtype box,
						Fl_Color bg,
						Fl_Color sel,
						Fl_Labeltype label,
						bool can_focus )
	{
		BGColor = bg;
		SelColor = sel;
		BoxType = box;
		LabelType = label;
		CanFocus = can_focus;
	};

	WidgetStyle::WidgetStyle( WidgetStyle *w )
	{
		copy_from( w );
	};

	void WidgetStyle::copy_from( WidgetStyle *w )
	{
		BGColor = w->BGColor;
		SelColor = w->SelColor;
		BoxType = w->BoxType;
		LabelType = w->LabelType;
		CanFocus = w->CanFocus;
	};

	void WidgetStyle::apply( Fl_Widget *w )
	{
		w->color( BGColor, SelColor );
		w->box( BoxType );
		w->labeltype( LabelType );
		if( CanFocus )
			w->set_visible_focus();
		else
			w->clear_visible_focus();
	};

	WidgetStyle *WidgetStyle::clone()
	{
		return new WidgetStyle( this );
	};

// ==================== class TextStyle ==============================

	TextStyle::TextStyle( Fl_Fontsize size, Fl_Color color, Fl_Font font )
	{
		Color= color;
		Size = size;
		set_font( font );
	};

	void TextStyle::set_size( Fl_Fontsize size )
	{
		Size = size;
	};
	
	void TextStyle::set_color( Fl_Color clr )
	{
		Color = clr;
	};

	void TextStyle::set_font( Fl_Font font )
	{
		Font = font;
	};

	void TextStyle::set( Fl_Fontsize size, Fl_Color clr, Fl_Font font )
	{
		Size = size;
		Color = clr;
		set_font( font );
	};
	
	Fl_Fontsize TextStyle::size()
	{
		return Size;	
	}

	void TextStyle::apply_to_label( Fl_Widget *w )
	{
		w->labelcolor( Color );
		w->labelfont( Font );
		w->labelsize( Size );
	};
	
	void TextStyle::apply_to_text( Fl_Text_Display *t )
	{
		t->textcolor( Color );
		t->textfont( Font );
		t->textsize( Size );
	};

	void TextStyle::apply_to_input( Fl_Input *t )
	{
		t->textcolor( Color );
		t->textfont( Font );
		t->textsize( Size );
	};

	void TextStyle::apply_to_browser( Fl_Browser *t )
	{
		t->textcolor( Color );
		t->textfont( Font );
		t->textsize( Size );
	};

//====================== class ButtonStyle ========================

	ButtonStyle::ButtonStyle( Fl_Boxtype box,
						Fl_Color bg,
						Fl_Color sel,
						Fl_Labeltype label,
						Fl_Fontsize size,
						Fl_Color color,
						Fl_Font font,
						bool can_focus, bool setdownbox ) : WidgetStyle( box, bg, sel, label, can_focus )
	{
		ls.set( size, color, font );
		SetDownBox = setdownbox;
	};

	ButtonStyle::ButtonStyle( ButtonStyle *b )
	{
		copy_from( b );
	};
	
	void ButtonStyle::copy_from( ButtonStyle *b )
	{
		WidgetStyle::copy_from( (WidgetStyle *)b );
		ls = b->ls;
		SetDownBox = b->SetDownBox;
	};

	void ButtonStyle::apply( Fl_Widget *w )
	{
		WidgetStyle::apply( w );
		ls.apply_to_label( w );
		if( SetDownBox )
			((Fl_Button *)w)->down_box( FL_NO_BOX ); //w->box()
	};

	ButtonStyle *ButtonStyle::clone()
	{
		return new ButtonStyle( this );
	};

//=============== class ImgBStyle =============================

	ImgBStyle::ImgBStyle( Fl_Boxtype box,
						Fl_Color bg,
						Fl_Color sel,
						Fl_Labeltype label,
						Fl_Fontsize size,
						Fl_Color color,
						Fl_Font font,
						Fl_Align align,
						Fl_Image *img,
						bool can_focus,
						Fl_Image *deimg )  : ButtonStyle( box, bg, sel, label, size, color, font, can_focus )
	{
		alignment = align;
		image = img;
		owned_deimage = false;
		if( deimg ) {
			deimage = deimg;
		} else if( img ) {
			deimage = img->copy();
			deimage->desaturate();
			owned_deimage = true;
		} else {
			deimage = NULL;
		}
	};

	ImgBStyle::ImgBStyle( ImgBStyle *b )
	{
		copy_from( b );
	}

	ImgBStyle::~ImgBStyle()
	{
		if( owned_deimage ) {
			delete deimage;
			deimage = NULL;
		}
	}

	void ImgBStyle::copy_from( ImgBStyle *b )
	{
		ButtonStyle::copy_from( (ButtonStyle *)b );
		alignment = b->alignment;
		image = b->image;
		deimage = b->deimage;
		owned_deimage = b->owned_deimage;
	};

	void ImgBStyle::apply( Fl_Widget *w )
	{
		WidgetStyle::apply( w );
		ls.apply_to_label( w );
		w->image( image );
		w->deimage( deimage );
		w->align( alignment );
	};

	ImgBStyle *ImgBStyle::clone()
	{
		return new ImgBStyle( this );
	};
	
	//====================== class InputStyle ========================

	InputStyle::InputStyle( Fl_Boxtype box,
						Fl_Color bg,
						Fl_Color sel,
						Fl_Fontsize size,
						Fl_Color color,
						Fl_Font font,
						Fl_Color curcol,
						bool can_focus ) : WidgetStyle( box, bg, sel, FL_NO_LABEL, can_focus )
	{
		ls.set( size, color, font );
		cursor_color = curcol;
	};

	InputStyle::InputStyle( InputStyle *b )
	{
		copy_from( b );
	};
	
	void InputStyle::copy_from( InputStyle *b )
	{
		WidgetStyle::copy_from( (WidgetStyle *)b );
		ls = b->ls;
		cursor_color = b->cursor_color;
	};

	void InputStyle::apply( Fl_Widget *w )
	{
		WidgetStyle::apply( w );
		ls.apply_to_input( (Fl_Input*)w );
		((Fl_Input*)w)->cursor_color( cursor_color );
	};

	InputStyle *InputStyle::clone()
	{
		return new InputStyle( this );
	};
	
	//====================== class BrowserStyle ========================

	BrowserStyle::BrowserStyle( Fl_Boxtype box,
						Fl_Color bg,
						Fl_Color sel,
						Fl_Fontsize size,
						Fl_Color color,
						Fl_Font font,
						bool can_focus ) : WidgetStyle( box, bg, sel, FL_NO_LABEL, can_focus )
	{
		ls.set( size, color, font );
	};

	BrowserStyle::BrowserStyle( BrowserStyle *b )
	{
		copy_from( b );
	};
	
	void BrowserStyle::copy_from( BrowserStyle *b )
	{
		WidgetStyle::copy_from( (WidgetStyle *)b );
		ls = b->ls;
	};

	void BrowserStyle::apply( Fl_Widget *w )
	{
		WidgetStyle::apply( w );
		ls.apply_to_browser( (Fl_Browser*)w );
		//w->redraw();
	};

	BrowserStyle *BrowserStyle::clone()
	{
		return new BrowserStyle( this );
	};
	