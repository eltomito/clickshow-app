/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Styles_h
#define _Styles_h

#include <map>
#include <list>
#include <climits>

#include <FL/Fl.H>
#include <FL/Fl_Window.H>
#include <FL/Fl_Box.H>
#include <FL/Fl_Check_Button.H>
#include <FL/Fl_Text_Display.H>
#include <FL/Fl_Browser.H>
#include <FL/Fl_Input.H>

#include "clicky_debug.h"


/*
 * a style applicable to any widget
 */
class WidgetStyle
{
public:
	Fl_Color		BGColor;
	Fl_Color		SelColor;
	Fl_Boxtype	BoxType;
	Fl_Labeltype LabelType;
	bool			CanFocus;
		
	WidgetStyle(
						Fl_Boxtype box = FL_FLAT_BOX,
						Fl_Color bg = FL_BLACK,
						Fl_Color sel = FL_SELECTION_COLOR,
						Fl_Labeltype label = FL_NO_LABEL,
						bool can_focus = false );
	WidgetStyle( WidgetStyle *w );

	virtual ~WidgetStyle()
	{ LOG_RENDER_DETAIL2("a widget style is being deleted FOREVER!!!\n"); };

	virtual void copy_from( WidgetStyle *w );
	virtual void apply( Fl_Widget *w );
	virtual WidgetStyle *clone();
};

/*
 * style applicable to any text
 */
class TextStyle
{
public:
	Fl_Color		Color;
	Fl_Font			Font;
	Fl_Fontsize	Size;

	TextStyle( Fl_Fontsize size = 16, Fl_Color color = FL_WHITE, Fl_Font font = FL_HELVETICA);
	~TextStyle() {};

	void set_size( Fl_Fontsize size );
	void set_color( Fl_Color clr );
	void set_font( Fl_Font font );
	void set( Fl_Fontsize size, Fl_Color clr, Fl_Font font );
	Fl_Fontsize size();
	void apply_to_label( Fl_Widget *w );
	void apply_to_text( Fl_Text_Display *t );
	void apply_to_input( Fl_Input *t );
	void apply_to_browser( Fl_Browser *t );
};

/*
 * a style applicable to any widget with a label
 */
class ButtonStyle  : public WidgetStyle
{
	bool	SetDownBox;

public:
	TextStyle	ls;

	ButtonStyle( Fl_Boxtype box = FL_PLASTIC_UP_FRAME,
						Fl_Color bg = FL_GREEN,
						Fl_Color sel = FL_SELECTION_COLOR,
						Fl_Labeltype label = FL_NORMAL_LABEL,
						Fl_Fontsize size = 16,
						Fl_Color color = FL_WHITE,
						Fl_Font font = FL_HELVETICA,
						bool can_focus = false,
						bool setdownbox = true );	//set to false for check buttons
	ButtonStyle( ButtonStyle *b );

	virtual ~ButtonStyle() {};

	virtual void copy_from( ButtonStyle *b );
	virtual void apply( Fl_Widget *w );
	virtual ButtonStyle *clone();
};

/*
 * a style applicable to any widget with a label and an image
 */
class ImgBStyle  : public ButtonStyle
{
public:
	Fl_Align	alignment;
	Fl_Image	*image;
	Fl_Image	*deimage;
	bool owned_deimage;

	/*
	 * NOTE: ImgBStyle doesn't make a copy of the image
	 *			and doesn't delete the image when deleted.
	 *			It only keeps a pointer to the image structure.
	 */
	ImgBStyle( Fl_Boxtype box = FL_PLASTIC_UP_FRAME,
						Fl_Color bg = FL_GREEN,
						Fl_Color sel = FL_SELECTION_COLOR,
						Fl_Labeltype label = FL_NORMAL_LABEL,
						Fl_Fontsize size = 16,
						Fl_Color color = FL_WHITE,
						Fl_Font font = FL_HELVETICA,
						Fl_Align align = FL_ALIGN_IMAGE_NEXT_TO_TEXT,
						Fl_Image *img = NULL,
						bool can_focus = false,
						Fl_Image *deimg = NULL );
	ImgBStyle( ImgBStyle *b );

	virtual ~ImgBStyle();

	virtual void copy_from( ImgBStyle *b );
	virtual void apply( Fl_Widget *w );
	virtual ImgBStyle *clone();
};

/*
 * a style applicable to any widget containing text
 */
class InputStyle  : public WidgetStyle
{
public:
	TextStyle	ls;
	Fl_Color		cursor_color;

	InputStyle( Fl_Boxtype box = FL_PLASTIC_UP_FRAME,
						Fl_Color bg = FL_GRAY,
						Fl_Color sel = FL_SELECTION_COLOR,
						Fl_Fontsize size = 16,
						Fl_Color color = FL_WHITE,
						Fl_Font font = FL_HELVETICA,
						Fl_Color curcol = FL_WHITE,
						bool can_focus = true );
	InputStyle( InputStyle *b );

	virtual ~InputStyle() {};

	virtual void copy_from( InputStyle *b );
	virtual void apply( Fl_Widget *w );
	virtual InputStyle *clone();
};

/*
 * a style applicable to Fl_Text_Display and Fl_Text_Editor
 */
/*

TODO: this is the future...

class TextDisplayStyle  : public WidgetStyle
{
public:
	TextStyle	ls;
	Fl_Color		cursor_color;

	TextDisplay( Fl_Boxtype box = FL_PLASTIC_UP_FRAME,
						Fl_Color bg = FL_GRAY,
						Fl_Color sel = FL_SELECTION_COLOR,
						Fl_Fontsize size = 16,
						Fl_Color color = FL_WHITE,
						Fl_Font font = FL_HELVETICA,
						Fl_Color curcol = FL_WHITE,
						bool can_focus = true );
	TextDisplay( TextDisplay *b );

	virtual ~TextDisplay() {};

	virtual void copy_from( TextDisplay *b );
	virtual void apply( Fl_Widget *w );
	virtual TextDisplay *clone();
};
*/

/*
 * a style applicable to a Fl_Browser and its derivatives
 */
class BrowserStyle  : public WidgetStyle
{
public:
	TextStyle	ls;

	BrowserStyle( Fl_Boxtype box = FL_NO_BOX,
						Fl_Color bg = FL_BLACK,
						Fl_Color sel = FL_BLACK,
						Fl_Fontsize size = 16,
						Fl_Color color = FL_WHITE,
						Fl_Font font = FL_HELVETICA,
						bool can_focus = true );
	BrowserStyle( BrowserStyle *b );

	virtual ~BrowserStyle() {};

	virtual void copy_from( BrowserStyle *b );
	virtual void apply( Fl_Widget *w );
	virtual BrowserStyle *clone();
};

#endif //_Styles_h
