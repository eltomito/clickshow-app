/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sub_click_panel.h"

#include "heart.h"

//============================== class SubClickPanel ===============================


//---- button callbacks ----

	void SubClickPanel::cb_next(Fl_Button* b, void *)
	{
		SubClickPanel *p = (SubClickPanel *)b->parent();
		p->heart->clicked_show_next();
		p->heart->focus_default_widget();
		LOG_RENDER_DETAIL2("Huhehe!\n" );
	};

	void SubClickPanel::cb_up(Fl_Button* b, void *panel)
	{
		SubClickPanel *p = (SubClickPanel *)b->parent();
		p->heart->clicked_go( -1 );
		p->heart->focus_default_widget();
		LOG_RENDER_DETAIL2("Hohuhu!\n" );
	};

	void SubClickPanel::cb_down(Fl_Button* b, void*)
	{
		SubClickPanel *p = (SubClickPanel *)b->parent();
		p->heart->clicked_go( 1 );
		p->heart->focus_default_widget();
		LOG_RENDER_DETAIL2("Hohuhu!\n" );
	};

	void SubClickPanel::cb_clear(Fl_Button* b, void*)
	{
		SubClickPanel *p = (SubClickPanel *)b->parent();
		p->heart->clicked_clear();
		p->heart->focus_default_widget();
		LOG_RENDER_DETAIL2("Clear, dude!\n" );
	};

	void SubClickPanel::cb_mark(Fl_Button* b, void*)
	{
		SubClickPanel *p = (SubClickPanel *)b->parent();
		p->heart->clicked_mark();
		p->heart->focus_default_widget();
	};

//---- constructor ----

	SubClickPanel::SubClickPanel(Heart *hrt, int x, int y, int w, int h) : ControlPanel ( hrt, x, y, w, h, (const char *)"SUBCLICK" )
	{
		heart = hrt;;

		layout = new SimpLE();

		//size_range( 150, 100, 450, 300, 1, 1, 0 );

		button_next = new ClickyButton( 0,0,0,0, _("Show\nNext") );
		button_up = new ClickyRepeatButton( 0,0,0,0, NULL);		//"@2<-"
		button_down = new ClickyRepeatButton( 0,0,0,0, NULL);		//"@2->"
		button_clear = new ClickyButton( 0, 0, 0, 0, _("Clear\nScreen") );
		button_mark = new ClickyButton( 0, 0, 0, 0, _("* Mark") );

		end();	//to stop adding widgets to this group

		//tooltips
		button_next->tooltip( _("Display the next subtitle or a blank screen\ndepending on whether \"Spaces\" are checked.") );
		button_up->tooltip( _("Browse subtitles up without displaying any.") );
		button_down->tooltip( _("Browse subtitles down without displaying any.") );
		button_clear->tooltip( _("Clear the subtitle view.") );
		button_mark->tooltip( _("Mark the currently displayed subtitle\nwith an asterisk (*).") );

		//callbacks
		button_next->callback((Fl_Callback*)cb_next);
		button_up->callback((Fl_Callback*)cb_up);
		button_down->callback((Fl_Callback*)cb_down);
		button_clear->callback((Fl_Callback*)cb_clear);
		button_mark->callback((Fl_Callback*)cb_mark);

		//-- layout --

		static const int pixx = 5;
		static const int pixy = 0;
		static const int marginw = 0;
		static const int marginh = 0;
		static const int spacex = 10;
		static const int spacey = 10;

		layout->add_cell( button_up, 0, 0, 1, 3,	//cx, cy, cw, ch
											5, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces

		layout->add_cell( button_down, 0, 3, 1, 3,	//cx, cy, cw, ch
											5, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces

		layout->add_cell( button_next, 1, 0, 2, 6,	//cx, cy, cw, ch
											5, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces

		layout->add_cell( button_mark, 3, 0, 2, 2,	//cx, cy, cw, ch
											5, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces

		layout->add_cell( button_clear, 3, 2, 2, 4,	//cx, cy, cw, ch
											5, 6,				//columns, rows
											pixx, pixy,			//pixx, pixy
											0, 0,	//relx, rely
											1.0, 1.0,	//relw, relh
											marginw, marginh,		//margins
											spacex, spacey );		//spaces

		//-- style --
		heart->get_style_engine()->assign_style( StylePrefs::NORMAL_BUTTON, button_clear );

		heart->get_style_engine()->assign_style( StylePrefs::MARK_BUTTON, button_mark );
		heart->get_style_engine()->assign_style( StylePrefs::SHOWNEXT_BUTTON, button_next );
		heart->get_style_engine()->assign_style( StylePrefs::GOUP_BUTTON, button_up );
		heart->get_style_engine()->assign_style( StylePrefs::GODOWN_BUTTON, button_down );

		//heart->get_style_engine()->assign_style( StylePrefs::NORMAL_PANEL, this );

		//-- refresh --

		refresh_layout();
		refresh_style();

	};

    SubClickPanel::~SubClickPanel()
    {
        delete layout;
    }

	bool SubClickPanel::get_size( int &w, int &h )
	{
		int	fh;

		fh = heart->get_style_prefs()->NormalButton->ls.Size;
		h = fh*6;
		w = h*3;
		return true;
	};

	void SubClickPanel::refresh_layout()
	{
		layout->refresh_layout( this );
	};
