/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SUB_CLICK_PANEL_H_
#define _SUB_CLICK_PANEL_H_ 1

#include <FL/Fl_Repeat_Button.H>

#include "clicky_button.h"
#include "control_panel.h"
#include "simple_layout_engine.h"

class Heart;

/*
 * the clicker panel class
 */
class SubClickPanel : public ControlPanel
{
	SimpLE	*layout;

	ClickyButton *button_next;
	ClickyRepeatButton *button_up;
	ClickyRepeatButton *button_down;
	ClickyButton *button_clear;
	ClickyButton *button_mark;

	const static int margin_v = 2;
	const static int margin_h = 2;
	const static int space_h = 1;
	const static int space_v = 5;

	static void cb_next(Fl_Button* b, void*);
	static void cb_up(Fl_Button* b, void *panel);
	static void cb_down(Fl_Button* b, void*);
	static	void cb_clear(Fl_Button* b, void*);
	static	void cb_mark(Fl_Button* b, void*);

public:
	SubClickPanel(Heart *hrt, int x, int y, int w, int h);
    ~SubClickPanel();

	void refresh_layout();
	bool get_size( int &w, int &h );
};

#endif //_SUB_CLICK_PANEL_H_
