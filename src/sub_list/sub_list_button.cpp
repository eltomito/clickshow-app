/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sub_list_button.h"
#include "sub_list_panel2.h"

SubListButton::SubListButton( SubListPanel *_list, Subtitle *_sub ) : Fl_Widget( 0,0,0,0,NULL ), list(_list)
{
	if(_sub) {
		sub = *_sub;
		LOG_RENDER_DETAIL2("Creating SubtitleButton \"%s\"\n", sub.get_disp_text().c_str() );
	}
}

SubListButton::~SubListButton()
{
#ifdef DEBUG
 LOG_RENDER_DETAIL2("Destroying SubtitleButton \"%s\", x = %i, y = %i, w = %i, w = %i\n",
									sub.get_disp_text().c_str(), x(), y(), w(), h() );
#endif //DEBUG
}

int SubListButton::handle( int event )
{
	return list ? list->handle_item_event( *this, event ) : 0;
}

void SubListButton::print()
{
	LOG_RENDER_DETAIL2("SubListButton \"%s\", x = %i, y = %i, w = %i, w = %i\n", sub.get_disp_text().c_str(), x(), y(), w(), h() );
}

SubListButton::SubListButton( const SubListButton &sb ) : Fl_Widget( sb.x(),sb.y(),sb.w(),sb.h(), NULL )
{
	list = sb.list;
	sub = sb.sub;
}

SubListButton& SubListButton::operator=(const SubListButton& sb)
{
	resize( sb.x(), sb.y(), sb.w(), sb.h() );
	sub = sb.sub;
	list = sb.list;
	return *this;
}

void SubListButton::draw()
{
	LOG_RENDER_DETAIL2("UNIMPLEMENTED virtual method called!");
}
