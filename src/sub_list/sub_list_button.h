/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SubListButton_h_
#define _SubListButton_h_

#include "../clicky_debug.h"
#include "../subtitles/subtitle.h"

#include <FL/Fl_Widget.H>

class SubListPanel;

class SubListButton : public Fl_Widget
{
protected:
	Subtitle sub;
	SubListPanel *list;

protected:
	SubListButton( SubListPanel *_list, Subtitle *_sub = NULL );
	virtual ~SubListButton();

public:
	SubListButton( const SubListButton &sb );
	SubListButton &operator=(const SubListButton& sb);

	virtual void print();

	virtual bool natural_size( int &W, int &H ) { return false; };
	virtual void draw();
	virtual int handle( int event );
	virtual void set_sub( Subtitle &_sub ) { sub = _sub; };
};

#endif //_SubListButton_h_

