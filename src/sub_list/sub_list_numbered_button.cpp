/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sub_list_numbered_button.h"

#include <FL/fl_draw.H>
#include <cstdlib>
#include <cstdio>

#include "shared/src/str_utils.h"
#include "sub_list_panel2.h"
#include "../subtitle_formats/timing/srt.h"

SubListNumberedButton::SubListNumberedButton( const SubListNumberedButton &sb )
	: SubListButton( sb )
{
	sub_num = sb.sub_num;
	sub_num_str = sb.sub_num_str;
}

SubListNumberedButton& SubListNumberedButton::operator=(const SubListNumberedButton& sb)
{
	sub_num = sb.sub_num;
	sub_num_str = sb.sub_num_str;
	*((SubListButton *)this) = sb;
	return *this;
}

void SubListNumberedButton::set_sub_num( int n )
{
	sub_num = n;
	sub_num_str.clear();
	StrUtils::Printf( sub_num_str, "%i", n + 1 );
}

bool SubListNumberedButton::natural_size( int &W, int &H )
{
	int numw = 0;
	int markw = 0;
	int textw = 0;
	int texth = 0;
	int minh = 0;

	const Fl_Font *faces = ((SubListPanel*)parent())->get_font_faces( labelfont() );

	fl_font( labelfont(), labelsize() );
	RainbowStyle initStyle( 0, labelcolor(), 0, RainbowStyle::M_ALL );

	const char *measured_str = sub.get_disp_text().c_str();
	if(( measured_str == NULL )||( measured_str[0] == 0 )) {
		measured_str = "A";
	}
	rainbow_measure( measured_str, sub.get_markup(), textw, texth, faces, initStyle );

	if( list ) {
		numw = list->get_num_col_width();
		markw = list->get_mark_col_width();
	}

	W = numw + markw + textw;
	H = texth;

	LOG_RENDER_DETAIL2("sub_num: %s minh %i textw %i texth %i\n", sub_num_str.c_str(), minh, textw, texth );

	return true;
}

void SubListNumberedButton::draw()
{
	int numcolor = FL_GRAY;
	int textcolor = FL_GREEN;
	int markx = 0;
	int textx = 0;
	int startx = 0;
	int durx = 0;
	int numx = 0;

	//fl_rectf( x(), y(), w(), h(), (sub_num % 2) ? 0x08081000 : 0x00000000 ); //debug only!

	const Fl_Font *faces = ((SubListPanel*)parent())->get_font_faces( labelfont() );

	if(!list) {
		LOG_RENDER_WARN("NOT DRAWING LIST BUTTON, because it belongs to no list.\n");
		return;
	}

	LOG_RENDER_DETAIL2("DRAWING LIST BUTTON: \"%s\" x=%i, y=%i, w=%i, h=%i\n", sub.get_disp_text().c_str(),x(),y(),w(),h());

	if (type() == FL_HIDDEN_BUTTON)
  		return;

	if( sub.is_comment() ) {
		numcolor = list->get_comment_color();
	} else {
		numcolor = list->get_sub_num_color();
	}

	Fl_Align textalign = FL_ALIGN_LEFT;
	fl_font( labelfont(), labelsize() );
//	fl_color( labelcolor() );

	int textw = 0;
	int texth = 0;
	RainbowStyle initStyle( 0, labelcolor(), 0, RainbowStyle::M_ALL );
	const char *measured_str = sub.get_disp_text().c_str();
	if(( measured_str == NULL )||( measured_str[0] == 0 )) {
		measured_str = "A";
	}
	rainbow_measure( measured_str, sub.get_markup(), textw, texth, faces, initStyle );

	fl_font( labelfont(), labelsize() );

	if( list->show_timing() ) {
		startx = numx + list->get_num_col_width();
		durx = startx + list->get_start_col_width();
		markx = durx + list->get_dur_col_width();
	} else {
		markx = numx + list->get_num_col_width();
	}
	textx = markx + list->get_mark_col_width();

	//render number
	fl_color( numcolor );
	fl_draw( sub_num_str.c_str(), x() + numx, y(), list->get_num_col_width(), texth, FL_ALIGN_LEFT, NULL, 0 );

	//draw the mark image, if any
	if( sub.is_marked() ) {
		const Fl_Image *img = list->get_mark_image();
		if( img ) {
			int yoff = (h() - img->h()) / 2;
			((Fl_Image*)img)->draw( x() + markx, y() + yoff, img->w(), img->h() );
		}
	};

	//timing
	if( list->show_timing() ) {
		fl_color( list->get_timing_color() );

		char startstr[16];
		char durstr[16];
		FormatSubStart( startstr );
		FormatSubDur( durstr );
		fl_draw( startstr, x() + startx, y(), list->get_num_col_width(), texth, FL_ALIGN_LEFT, NULL, 0 ); 
		fl_draw( durstr, x() + durx, y(), list->get_num_col_width(), texth, FL_ALIGN_LEFT, NULL, 0 ); 
	}

	//draw the text
	if( !sub.is_comment() ) {
		fl_color( labelcolor() );
		initStyle.set( 0, labelcolor(), 0, RainbowStyle::M_ALL );
		rainbow_draw( (char *)sub.get_disp_text().c_str(), sub.get_markup(), x() + textx, y(), textw, texth, textalign, &list->get_rainbow_colors()[0], labelcolor(), faces, initStyle );
	} else {
		RainbowMarkup tmpmarkup;
		ColorSet tmpcolors( 8, list->get_comment_color() );
		initStyle.set( 0, list->get_comment_color(), 0, RainbowStyle::M_ALL );
		tmpmarkup.push_back( initStyle );
		rainbow_draw( (char *)sub.get_disp_text().c_str(), tmpmarkup, x() + textx, y(), textw, texth, textalign, &tmpcolors[0], list->get_comment_color(), faces, initStyle );
	}
}

int SubListNumberedButton::FormatSubStart( char *str )
{
	if( str ) {
		if( !sub.get_timing().IsSet() ) {
			sprintf( str, "--:--:--.---" );
		} else {
			int h, m, s, mm;
			SrtParser::TimeToParts( sub.get_timing().GetStart(), h, m, s, mm );
			sprintf( str, "%02i:%02i:%02i,%03i", h, m, s, mm );
		}
	}
	return 12;
}

int SubListNumberedButton::FormatSubDur( char *str )
{
	if( str ) {
		if( !sub.get_timing().IsSet() ) {
			sprintf( str, "--.---" );
		} else {
			long dur = sub.get_timing().GetDur();
			if( dur >= 100000 ) {
				sprintf( str, "99.999" );
			} else {
				int h, m, s, mm;
				SrtParser::TimeToParts( dur, h, m, s, mm );
				sprintf( str, "%02i:%03i", s, mm );
			}
		}
	}
	return 6;
}
