/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SubListNumberedButton_h_
#define _SubListNumberedButton_h_

#include "sub_list_button.h"
#include "../clicky_debug.h"
#include "FL/fl_types.h"
#include "../rainbow_style.h"

class SubListPanel;

/*
 * SubListNumberedButton class - a wrapper for Fl_Button with a argument-free constructor
 */
class SubListNumberedButton : public SubListButton
{
private:
	int sub_num;
	std::string sub_num_str;

public:
	SubListNumberedButton( SubListPanel *_list = NULL, Subtitle *_sub = NULL, int _sub_num = -1 )
	: SubListButton( _list, _sub ) { set_sub_num(_sub_num); };

	~SubListNumberedButton() {};

	SubListNumberedButton( const SubListNumberedButton &sb );
	SubListNumberedButton& operator=(const SubListNumberedButton& sb);

	void set( SubListPanel *_list = NULL, Subtitle *_sub = NULL, int _sub_num = -1 ) {
		list = _list;
		sub = *_sub;
		set_sub_num( _sub_num );
	};

	void set_sub_num( int n );
	int get_sub_num() { return sub_num; };

	bool natural_size( int &W, int &H );
	void draw();

private:
	int FormatSubStart( char *str );
	int FormatSubDur( char *str );
};

#endif //_SubListNumberedButton_h_
