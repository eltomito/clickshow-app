/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "sub_list_panel2.h"
#include "sub_list_numbered_button.h"

#include <FL/fl_draw.H>
#include <cstdlib>
#include <cstdio>

//================== SubListPanel ======================

SubListPanel::SubListPanel(Heart *hrt, int x, int y, int w, int h ) : ControlPanel( hrt, x, y, w, h ), top_sub(0), subs_shown(0)
{
	heart = hrt;

	load_candy_colors();

	//init
	subtitle_space = ( 2*(heart->get_style_prefs()->SubtitleImgB->ls.Size) )/3;
	subtitle_border = 0;

	font_faces.setFont( 0 );	//create all the font faces of font 0 (SANS)
	last_font = 0;

	//tooltip
	tooltip_text = (char *) _("Left Mouse Button - Mark subtitle\nRight Mouse Button - Edit subtitle\nCtrl+Left Mouse Button - Display subtitle");

	//mark button
	button_mark = new Fl_Button( 0, 0, 30, 30, "" );
	heart->get_style_engine()->assign_style( StylePrefs::STAR_BUTTON, button_mark );

	//the slider
	slider_pos = new ClickySlider( 0,0,slider_width,0,NULL );
	slider_pos->color( Fl_Color( 0x55555500 ) );
	slider_pos->box( FL_NO_BOX );	//FL_PLASTIC_UP_FRAME
	slider_pos->slider( FL_PLASTIC_UP_FRAME );
	slider_pos->clear_visible_focus();
	slider_pos->min_slider_size( min_slider_height );

	end();

	start_col_width = 0;
	dur_col_width = 0;
	num_col_width = 0;
	fl_font( heart->get_style_prefs()->SubtitleImgB->ls.Font, heart->get_style_prefs()->SubtitleImgB->ls.Size );
	fl_measure( "-00:00:00.000|", start_col_width, min_row_height, 0 );
	fl_measure( "-00.000|", dur_col_width, min_row_height, 0 );
	fl_measure( "00000|", num_col_width, min_row_height, 0 );

	button_mark->hide();

	//callbacks
	slider_pos->callback((Fl_Callback*)cb_slider);

	//-- style --
	heart->get_style_engine()->assign_style( StylePrefs::SCROLLTEXT_PANEL, this );

	clear_flag(CLIP_CHILDREN);

	//subtitle buttons
	sub_buttons.clear();
};

SubListPanel::~SubListPanel()
{
	for( int i = 0; i < sub_buttons.size(); ++i ) {
		delete sub_buttons[i];
		sub_buttons[i] = NULL;
	}
	sub_buttons.clear();
}

void SubListPanel::set_candy_color( int i, Fl_Color clr )
{
	if( i >= colors.size() ) {
		LOG_RENDER_ERROR("Cannot set color #%i when there are just %i colors!\n", i, colors.size() );
		return;
	}
	if( clr < 256 ) clr = Fl::get_color( clr );
	colors[i] = clr;
}

void SubListPanel::load_candy_colors()
{
	colors.clear();
	RainbowStyle::AppendToColorSet( colors, &heart->get_prefs()->candy_colors[0], sizeof(heart->get_prefs()->candy_colors), true );
}

const Fl_Image *SubListPanel::get_mark_image()
{
	return heart->get_style_prefs()->star_image;
}

int SubListPanel::get_mark_col_width() { return 3 * heart->get_style_prefs()->star_image->w() / 2; };

void SubListPanel::cb_slider(Fl_Slider* s, void*)
{
	SubListPanel *p = (SubListPanel *)s->parent();
	int n = (int)s->value();
	p->heart->go_to_sub( n );
};

void SubListPanel::update_slider()
{
	if(( top_sub != -1 )&&( subs_shown > 0 )&&( subtitles != NULL ))
	{
		double ss = (double)subs_shown / (double)subtitles->size();

		slider_pos->minimum( 0 );
		slider_pos->maximum( subtitles->size() -1 );
		slider_pos->slider_size( ss );
		slider_pos->show();
		slider_pos->value( top_sub );
	}
	else
	{
		slider_pos->hide();
	};
}

void SubListPanel::set_list_font( Fl_Font font )
{
	if( last_font == font ) { return; }
	last_font = font;
	font_faces.setFont( font );
}

void SubListPanel::set_mark( bool mark )
{
	if( mark )
		button_mark->show();
	else
		button_mark->hide();
};

void SubListPanel::draw()
{
	LOG_RENDER_DETAIL2( "DRAWW x y w h %i %i %i %i\n", x(), y(), w(), h());
	fl_push_no_clip();
	fl_rectf( x(), y(), w(), h(), FL_BLACK );
	fl_push_clip( x(), y(), w(), h() );
	Fl_Group::draw();
	fl_pop_clip();
	fl_pop_clip();
};

void SubListPanel::refresh_layout()
{
	int sliderx;
	int markx;

	layout_subs();

	sliderx = x() + w() - slider_pos->w();
	markx = sliderx - button_mark->w() - 10;

	//position the slider
	slider_pos->resize( sliderx, y(), slider_pos->w(), h() );
	update_slider();

	//LOG_RENDER_DETAIL2("resizing mark button to %i, %i, %i, %i\n", x(), y(), 50, 50 );
	button_mark->resize( markx, y()+5, 30, 30 ); //y()/2 - (button_mark->h()/2)
};

void SubListPanel::refresh_style()
{
	ImgBStyle *sib = heart->get_style_prefs()->SubtitleImgB;

	for( int i=0; i < (int)sub_buttons.size(); ++i )
	{
		sib->apply( sub_buttons[i] );
	};
	ControlPanel::refresh_style();
};

void SubListPanel::set_subtitles( Subtitles *subs )
{
	subtitles = subs;
	if( subs != NULL )
		top_sub = 0;
	else
		top_sub = -1;
};

void SubListPanel::refresh_subs()
{
	refresh_layout();
	redraw();
};

void SubListPanel::refresh_mark_button()
{
	button_mark->redraw();
	refresh_subs();
};

void SubListPanel::set_first_sub( int n )
{
	top_sub = n;
	layout_subs();
	update_slider();
	redraw();
	Fl::awake();
};

void	SubListPanel::hide_buttons( int start, int end )
{
	int i;
	if( end == -1 )
		end = sub_buttons.size();

	for( i = start; i < end; i++ )
	{
		sub_buttons[i]->hide();
	}
}

/*
 * returns the number of subtitles shown
 */
int SubListPanel::layout_subs()
{
	if(( top_sub < 0 )||( subtitles && ( top_sub >= subtitles->size() ) )) {
		hide_buttons( 0 );
		return 0;
	}

	int sub_num = top_sub;

	int X = x() + subtitle_border;
	int Y = y() + subtitle_border;
	int W = w() - 2*subtitle_border - slider_width;
	int botY = y() + h() - subtitle_border;

	int natW, natH;

	int list_i = 0;
	Subtitle *sub = subtitles->get_subtitle( sub_num );

	while( sub && ( Y < botY )) {

		LOG_RENDER_DETAIL2("list_i=%i, sub_num=%i, Y=%i\n", list_i, sub_num, Y);

		if( list_i == sub_buttons.size() ) {
			sub_buttons.push_back( new SubListNumberedButton( this, sub, sub_num ) );
			add( sub_buttons[ list_i ] );
			heart->get_style_prefs()->SubtitleImgB->apply( sub_buttons[ list_i ] );
		} else {
			sub_buttons[ list_i ]->set( this, sub, sub_num );
		}
		SubListNumberedButton &but = *sub_buttons[ list_i ];
		but.natural_size( natW, natH );
		but.resize( X, Y, W, natH );
		but.show();
		but.redraw();

		Y += natH + subtitle_space;
		++list_i;
		++sub_num;
		sub = subtitles->get_subtitle( sub_num );
	};
	subs_shown = list_i;
	if( list_i < sub_buttons.size() ) {
		LOG_RENDER_DETAIL2("hiding buttons from %i\n", list_i);
		hide_buttons( list_i );
	}
	return list_i;
}

/*
 * handle the mouse wheel
 */
/*
 TODO!!! It's broken. It gets all events even when the cursor is over the dash panel.
*/
int SubListPanel::handle( int event )
{
    //LOG_RENDER_DETAIL2("traveling through SubtitleButton::handle!!\n");

	if( event == FL_MOUSEWHEEL )
	{
       //LOG_RENDER_DETAIL2("Riders on the mouse!!! dy=%i\n", Fl::event_dy() );

		int mx = Fl::event_x();
		int my = Fl::event_y();

		if( (mx>=x()) && (mx<(x()+w())) && (my>=y()) && (my<(y()+h())) )
		{
	       //LOG_RENDER_DETAIL2("Are inside our house!!!\n" );
	       heart->clicked_go( Fl::event_dy() );
	       return 1;
		};
	};
	return ControlPanel::handle( event );
}

int SubListPanel::handle_item_event( SubListButton &button, int event )
{
	if(( event == FL_ENTER )||( event == FL_LEAVE ))
	{
	 	//LOG_RENDER_DETAIL2("SubtitleButton::handle received FL_ENTER or FL_LEAVE!!\n");
		return 1;
	};

	if( event == FL_PUSH )
	{
		int sub_num = ((SubListNumberedButton &)button).get_sub_num();
		switch( Fl::event_button() )
		{
			case FL_LEFT_MOUSE :
		   	LOG_RENDER_DETAIL2("Left-clicked subtitle %i!\n", sub_num );
				if( Fl::event_ctrl() )
				{
					//ctrl + left click means jump directly to subtitle
					heart->go_to_sub( sub_num );
					heart->clicked_show_next();
					return 1;
				};
		   	heart->mark_sub( sub_num );
				return 1;
				break;
			case FL_RIGHT_MOUSE :
		   	LOG_RENDER_DETAIL2("Right-clicked subtitle %i!\n", sub_num );
		   	heart->edit_subtitle( sub_num );
				return 1;
				break;
		};
	};
	return 0;
}
