/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SubListPanel_h
#define _SubListPanel_h

#include <deque>
#include <queue>

#include <FL/Fl.H>

#include "../control_panel.h"
#include "../heart.h"
#include "../subtitles/subtitles.h"
#include "../stock_image.h"
#include "../clicky_slider.h"
#include "../rainbow_style.h"
#include "../font_faces.h"

#include "../clicky_debug.h"

class	ControlPanel;
class	Heart;
class SubListButton;
class SubListNumberedButton;

/*
 * the subtitle list panel class
 */
class SubListPanel : public ControlPanel
{
	Subtitles	*subtitles;

	int	subtitle_space;
	int	subtitle_border;

	char *tooltip_text;

	FontFaces	font_faces;
	Fl_Font		last_font;

	//-- subtitle buttons
	typedef std::vector<SubListNumberedButton *> ItemQueueT;
	ItemQueueT	sub_buttons;
	int	top_sub;
	int	subs_shown;

	int start_col_width;
	int dur_col_width;
	int num_col_width;
	int min_row_height;

	//side slider
	static const int min_slider_height = 25;
	static const int slider_width = 10;
	ClickySlider *slider_pos;
	char slider_tooltip[20];

	//star button
	Fl_Button	*button_mark;

	void hide_buttons( int start = 0, int end = -1);
	int layout_subs();
	void update_slider();

	//callback	
	static void cb_slider(Fl_Slider* s, void*);

public:

	SubListPanel(Heart *hrt, int x, int y, int w, int h );
	~SubListPanel();

	const Fl_Font *get_font_faces() { return font_faces.getFaces(); };
	const Fl_Font *get_font_faces( Fl_Font font ) { set_list_font( font ); return font_faces.getFaces(); };
	void set_list_font( Fl_Font font );

	void set_first_sub( int n );
	void set_subtitles( Subtitles *subs = NULL );
	void set_mark( bool mark );

	void refresh_mark_button();
	void refresh_subs();
	void draw();

	void refresh_layout();
	void refresh_style();

	int handle( int event );
	int handle_item_event( SubListButton &button, int event );

	bool show_timing() { return subtitles && subtitles->is_timed(); };

	//-- item properties
	int get_num_col_width() { return num_col_width; };
	int get_mark_col_width();
	int get_start_col_width() { return start_col_width; };
	int get_dur_col_width() { return dur_col_width; };

	const Fl_Image *get_mark_image();

	Fl_Color get_sub_num_color() { return FL_GRAY; };
	Fl_Color get_comment_color() { return FL_MAGENTA; };
	Fl_Color get_timing_color() { return FL_GRAY; };

	void set_candy_color( int i, Fl_Color clr );
	void load_candy_colors();
	const ColorSet &get_rainbow_colors() { return colors; };

	ColorSet colors;
};

#endif	//_SubListPanel_h
