/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <climits>

class SubTiming {
public:
	SubTiming( int _start = INT_MIN, int _len = 0 ) : start( _start ), len( _len ) {};
	~SubTiming() {};

	int Start() const { return start; };
	int Length() const { return len; };
	int End() const { return start + len; };

	void Start( int s ) { start = s; };
	void Length( int l ) { len = l; };
	void End( int e ) { len = e - start; };

	bool IsVoid() const { return start == INT_MIN; };
	void Reset() { start = INT_MIN; };

private:
	int start;
	int len;
};
