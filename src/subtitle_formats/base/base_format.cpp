/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "base_format.h"
#include "../../utf8_tools.h"

/*
 * returns an error code.
 */
int BaseFormat::Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run )
{
	if( dry_run ) { return Errr::OK; }

	std::string &str = sub.mutget_disp_text();

	int charlen;
	unsigned int ucs = utf8_getchar( str.c_str(), &charlen );

	if(( ucs != '\\' )&&( ucs != 0xa0 )&&( ucs != 0x200B )) {
		//The input string doesn't start with an escape character
		size_t start = 0;
		if( 0 == str.compare( 0, 3, "(*)" ) ) {
			sub.set_mark( true );
			start = 3;
		}

		if(( str.size() > start )&&( str[start] == '#' )) {
			sub.set_comment( true );
		}
		str.erase( 0, start );
		RainbowMarkupUtils::EraseString( sub.mutget_markup(), 0, start );

		return Errr::OK;
	} else {
		//the string starts with an escape character
		if( str[ charlen ] == 0 ) { return Errr::OK; }
		if(( str[ charlen ] == '#' )||( 0 == str.compare( 1, 3, "(*)" ) )) {
			//the escape character makes sense, so let's delete it.
			str.erase( 0, charlen );
			RainbowMarkupUtils::EraseString( sub.mutget_markup(), 0, charlen );
			return Errr::OK;
		}
	}
	return Errr::OK;
}
