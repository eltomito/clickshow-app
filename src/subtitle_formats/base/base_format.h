/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Base_Format_h_
#define _Base_Format_h_ 1

#include "shared/src/default_log.h"
#include "shared/src/err.h"
#include "../../subtitles/subtitle.h"
#include "../../subtitles/subtitles.h"
#include "../sub_morph.h"

typedef unsigned int ucsT;

//======= BaseFormat =====

class BaseFormat : public SubMorph
{
public:
	BaseFormat() {};
	~BaseFormat() {};

	/*
	 * returns an error code.
	 */
	int Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run = false );
	bool Detect( Subtitles &subs, ErrDescsT &errs ) { return true; };
};

#endif //_Base_Format_h_
