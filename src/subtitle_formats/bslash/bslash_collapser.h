/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _BSlash_Collpaser_h_
#define _BSlash_Collpaser_h_ 1

#include "shared/src/default_log.h"
#include "shared/src/err.h"
#include "../../subtitles/subtitle.h"
#include "../../subtitles/subtitles.h"
#include "../sub_morph.h"
#include "../../markup/markup_edit.h"
#include "boost/regex.hpp"

//======= BSlashCollapser =====

bool find_bslashes( const char *str, int &bslashPos, int &bslashCnt ) {
	const char *s = str;
	while(( s[0] != 0 )&&( s[0] != '\\' )) { ++s; }
	if( s[0] == 0 ) { return false; }
	bslashPos = s - str;
	while( s[0] == '\\' ) { ++s; }
	bslashCnt = s - str - bslashPos;
	return true;
};

class BSlashCollapser : public SubMorph
{
public:
	BSlashCollapser() {};
	~BSlashCollapser() {};

	/*
	 * returns an error code.
	 */
	int Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run = false ) {
		ComposedMEdit edit;

		const char *str = sub.mutget_disp_text().c_str();
		const char *s = str;
		int bslashCnt = 0;
		int bslashPos = -1;
		while( find_bslashes( s, bslashPos, bslashCnt ) ) {
			int leftover = bslashCnt % 2;
			edit.applyOnMe( boost::make_shared<SimpleMEdit>( s - str + bslashPos, (bslashCnt / 2) + leftover ) );
			s += bslashPos + bslashCnt;
		}
		if( !dry_run ) {
			edit.apply( sub.mutget_disp_text() );
			edit.apply( sub.mutget_markup() );
		}
		return Errr::OK;
	};
	void Reset() {};

};

#endif //_BSlash_Collpaser_h_
