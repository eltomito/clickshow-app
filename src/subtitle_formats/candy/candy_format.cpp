/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "candy_format.h"
#include "../../candy/candy_recipe.h"
#include "../../candy/candy_syntax.h"

/*
 * returns an error code.
 */
int CandyFormat::Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run )
{
	CandyRecipe cr;
	if( sub.is_comment() ) {
		//don't morph comment subs
		return Errr::OK;
	}
	cr = CandySyntax::parse( sub.get_disp_text(), last_color );
	if( collect_stats ) {
		stats.insertMarkup( cr.markup, last_color );
	}
	last_color = cr.final_color;
	cr.edit.apply( cr.markup );
	if( !dry_run ) {
		sub.set_markup( cr.markup );
		cr.edit.apply( sub.mutget_disp_text() );
	}
	return Errr::OK;
}

bool CandyFormat::Detect( Subtitles &subs, ErrDescsT &errs )
{
	Reset();
	collect_stats = true;
	int err = MorphSubs( subs, errs, true );
	collect_stats = false;
	if( err ) { return false; }
	return stats.areCandy();
}

void CandyStats::insertMarkup( RainbowMarkup &rm, int initial_color )
{
	++markup_count;

	int s = stats.size();
	bool changedColor = false;
	RainbowMarkup::iterator it = rm.begin();
	RainbowMarkup::iterator eit = rm.end();

	while( it != eit ) {
		if(( it->color() != initial_color ) || changedColor ) {
			if( it->color() < s ) {
				++stats[ it->color() ];
			}
			changedColor = true;
		}
		++it;
	}
}

std::string CandyStats::toString()
{
	std::string str;
	char tmpstr[64];

	int l = stats.size();
	for( int i = 0; i < l; ++i ) {
		sprintf(tmpstr, "%i, ", stats[i]);
		str.append( tmpstr );
	}
	if( l > 0 ) {
		str.erase( str.size() - 2, 2 );
	}
	return str;
}

bool CandyStats::areCandy()
{
	int sum = 0;
	int l = (MARKS_THAT_MATTER >= stats.size()) ? stats.size() : MARKS_THAT_MATTER;

	int frequent_marks = 0;
	int frequent_threshold = markup_count / REL_FREQUENT_THRESHOLD;
	if( frequent_threshold < MIN_FREQUENT_THRESHOLD ) {
		frequent_threshold = MIN_FREQUENT_THRESHOLD;
	}

	LOG_CANDY_DETAIL("stats: %s\n", toString().c_str() );
	LOG_CANDY_DETAIL("marks that matter: %i, freq thresh: %i\n", l, frequent_threshold );

	for( int i = 0; i < l; ++i ) {
		if( i != NON_CANDY_INDEX ) {
			if( stats[i] >= frequent_threshold ) {
				++frequent_marks;
			}
			sum += stats[i];
		}
	}

	LOG_CANDY_DETAIL("frequent marks: %i (min: %i)\n", frequent_marks, MIN_FREQUENT_MARKS );

	if( frequent_marks < MIN_FREQUENT_MARKS ) {
		return false;
	}

	int candy_mark_threshold = markup_count / REL_MIN_CANDY_MARKS;
	if( candy_mark_threshold < MIN_CANDY_MARKS ) {
		candy_mark_threshold = MIN_CANDY_MARKS;
	}

	LOG_CANDY_DETAIL("candy marks: %i (min: %i)\n", sum, candy_mark_threshold );

	return ( sum >= candy_mark_threshold );
}

void CandyStats::clear() {
	markup_count = 0;
	int l = stats.size();
	for( int i = 0; i < l; ++i ) {
		stats[i] = 0;
	}
};
