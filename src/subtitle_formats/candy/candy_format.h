/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Candy_Format_h_
#define _Candy_Format_h_ 1

#include "shared/src/default_log.h"
#include "shared/src/err.h"
#include "../../subtitles/subtitle.h"
#include "../../subtitles/subtitles.h"
#include "../sub_morph.h"

//======== CandyStats =======

class CandyStats
{
public:
	//the number of marks to consider relevant
	//to deciding whether the stats are candy.
	static const int MARKS_THAT_MATTER = 3;
	//the number of markups divided by this number
	//is the threshold to consider a mark frequent.
	static const int REL_FREQUENT_THRESHOLD = 20;
	//the absolute minimum number of mark occurences
	//to consider it frequent
	static const int MIN_FREQUENT_THRESHOLD = 10;
	//the minimum number of marks that must be frequent
	//in order to consider these stats candy.
	static const int MIN_FREQUENT_MARKS = 2;
	//the minimum number of occurences
	//of all marks that matter combined
	//to consider these stats candy
	//is the number of markups divided by this.
	static const int REL_MIN_CANDY_MARKS = 10;
	//the absolute minimum for the number
	//of occurences of marks that matter.
	static const int MIN_CANDY_MARKS = 10;
	static const int NON_CANDY_INDEX = 3;

	CandyStats( int num_colors ) : stats( num_colors, 0 ), markup_count(0) {};
	~CandyStats() {};

	void insertMarkup( RainbowMarkup &rm, int initial_color );
	bool areCandy();
	void clear();

	std::string toString();
private:
	std::vector<int> stats;
	int markup_count;
};

//======= CandyFormat =====

class CandyFormat : public SubMorph
{
public:
	CandyFormat( int num_colors = 8, int _initial_color = 0 )
		: stats( num_colors ), initial_color(_initial_color), last_color(_initial_color), collect_stats(false) {};
	~CandyFormat() {};

	/*
	 * returns an error code.
	 */
	int Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run = false );
	bool Detect( Subtitles &subs, ErrDescsT &errs );
	void Reset() { last_color = initial_color; stats.clear(); };

protected:
	int initial_color;
	int last_color;
	CandyStats stats;
	bool collect_stats;
};

#endif //_Candy_Format_h_
