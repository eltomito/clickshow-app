/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "multi_format.h"
#include "../../clicky_debug.h"
#include "shared/src/default_log.h"
#include "shared/src/str_line_iterator.h"

#include <boost/smart_ptr/shared_ptr.hpp>
#include <boost/smart_ptr/make_shared.hpp>

bool MultiStats::isMulti()
{
	LOG_CANDY_DETAIL("MMStats: total=%i, split subs=%i, splits=%i\n", total_subs, split_subs, total_splits );
	return ( split_subs > ( total_subs / 10 ) );
}

MultiFormat::MultiFormat( const ColorSet *_colors )
{
	split_mark = "===";
	if( _colors ) {
		colors = *_colors;
	}  else {
		colors.push_back( (Fl_Color)0 );
		colors.push_back( (Fl_Color)1 );
		colors.push_back( (Fl_Color)2 );
		colors.push_back( (Fl_Color)3 );
		colors.push_back( (Fl_Color)4 );
		colors.push_back( (Fl_Color)5 );
		colors.push_back( (Fl_Color)6 );
		colors.push_back( (Fl_Color)7 );
	}
}

/*
 * Ignores comment subtitles.
 */
int MultiFormat::Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run )
{
	stats.total_subs += 1;
	if( sub.is_comment() ) { return Errr::OK; }

	const std::string &src = sub.get_disp_text();
	RainbowMarkup rm = sub.get_markup();
	std::string dst;
	RainbowStyle tmpstyle;

	int colorindex = 0;
	tmpstyle.start( 0 );
	tmpstyle.color( colors[ colorindex ] );
	rm.push_back( tmpstyle );
	colorindex++;

	str_line_iterator it( (char *)(src.c_str()) );
	str_line_iterator endit;
	while(( it != endit )&&( colorindex < colors.size() )) {
		if( *it != split_mark ) {
			dst.append( *it );
			dst.push_back('\n');
		} else {
			if( !dst.size() ) { dst.push_back('\n'); }
			tmpstyle.start( dst.size() );
			tmpstyle.color( colors[ colorindex ] );
			RainbowMarkupUtils::EraseString( rm, it.offset(), split_mark.size() + ( dst.size() ? 0 : 1 ) );
			RainbowMarkupUtils::Insert( rm, tmpstyle );
			rm.push_back( tmpstyle );
			colorindex++;
		}
		++it;
	}
	if( dst.size() > 0 ) {
		dst.erase( dst.size() - 1 );
	}
	if( !dry_run ) {
		sub.set_disp_text( dst );
		sub.set_markup( rm );
	}

	stats.total_splits += colorindex - 1;
	stats.split_subs += ( colorindex > 1 ) ? 1 : 0;

	return Errr::OK;
}

bool MultiFormat::Detect( Subtitles &subs, ErrDescsT &errs )
{
	Reset();
	int err = MorphSubs( subs, errs, true );
	if( err ) { return false; }
	return stats.isMulti();
}

int MultiFormat::MergeSubtitles( Subtitles &dst, const std::vector<Subtitles*> sublist )
{
	int listlen = sublist.size();
	if( !listlen ) { return Errr::MISSING_DATA; }

	//make sure all the subtitles have the same number of, you know, subtitles.
	int numsubs = sublist[0]->size();
	for( int i = 1; i < listlen; ++i ) {
		if( sublist[i]->size() != numsubs ) {
			return Errr::BAD_COUNT;
		}
	}

	std::string mergedstr;
	std::string commentstr;
	Subtitle tmpsub;
	const Subtitle *sub;
	bool has_noncomment;
	
	for( int subi = 0; subi < numsubs; ++subi ) {
		mergedstr.clear();
		commentstr.clear();
		tmpsub.reset();
		has_noncomment = false;
		for( int i = 0; i < listlen; ++i ) {
			sub = sublist[i]->get_subtitle( subi );
			if( !sub->is_comment() ) {
				mergedstr.append( sub->get_disp_text() );
				has_noncomment = true;
			} else {
				commentstr.append( sub->get_disp_text() );
			}
			if( i != (listlen -1) ) {
				mergedstr.append("\n===\n");
				commentstr.append("\n===\n");
			}
		}
		if( has_noncomment ) {
			tmpsub.set_raw_text( mergedstr );
		} else {
			tmpsub.set_raw_text( commentstr );
			tmpsub.set_comment( true );
		}
		dst.append( tmpsub );
	}
	return Errr::OK;
}
