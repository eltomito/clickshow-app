/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Multi_Format_h_
#define _Multi_Format_h_ 1

#include "shared/src/default_log.h"
#include "shared/src/err.h"
#include "../../subtitles/subtitle.h"
#include "../../subtitles/subtitles.h"
#include "../sub_morph.h"
#include "../../rainbow_style.h" //just for ColorSet

//======== MultiStats =======

class MultiStats {
	public:
		int total_subs;
		int total_splits;
		int split_subs;

		MultiStats() { clear(); };
		bool isMulti();
		void clear() { total_subs = 0; total_splits = 0; split_subs = 0; };
};

//======= MultiFormat =====

class MultiFormat : public SubMorph
{
public:
	MultiFormat( const ColorSet *_colors = NULL );
	~MultiFormat() {};

	/*
	 * returns an error code.
	 */
	int Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run = false );
	bool Detect( Subtitles &subs, ErrDescsT &errs );
	void Reset() { stats.clear(); };

	/*
	 * Merges several sets of subtitles sub by sub
	 * into one set where every subtitle consists
	 * of subtitles with the same index separated by \n===\n.
	 * Only the raw_text of the dst subtitles is set. Their disp_text is left blank.
	 *
	 * CAUTION! .srt subtitles lose timing and are merged as plain subtitles!
	 *
	 * @returns an error code
	 */
	int MergeSubtitles( Subtitles &dst, const std::vector<Subtitles*> sublist );

protected:
	ColorSet		colors;
	MultiStats	stats;
	bool 			collect_stats;
	std::string split_mark;
};

#endif //_Multi_Format_h_
