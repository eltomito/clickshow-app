/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "srttag_syntax.h"

#include "../../rainbow_style.h"
#include "../../utf8_tools.h"
#include "shared/src/default_log.h"
#include "../../markup/markup_edit.h"
#include "../../rainbow_markup.h"

#include "boost/regex.hpp"
#include "boost/smart_ptr/shared_ptr.hpp"
#include "boost/smart_ptr/make_shared.hpp"
//#include <boost/enable_shared_from_this.hpp>

#include <map>
#include <string>

/**
 * NOTE: _initStyle members start and len are never used.
 */
bool SrtTagSyntax::parse( const std::string &text, const RainbowStyle &_initStyle, const RainbowMarkup *baseMarkup ) {
	boost::cmatch match;

	markup.clear();
	if( baseMarkup ) { markup = *baseMarkup; }
	edit.clear();
	RainbowStyle style;

	//stores the markup index of the styles
	//that correspond to opening <font> tags,
	//so that the correct color can be found
	//whan that tag closes.
	std::vector<size_t> markupIndexStack;

	const char *str = text.c_str();
	const char *pos = str;
	int matchPos;

	while( boost::regex_search( pos, match, rgxTag ) ) {
		style.set();
		matchPos = match.position();
		bool pushMarkupIndex = false;
		if( match[1].length() > 0 ) {

			//handle a run of backslashes
			size_t backslashCnt = match[1].length();
			if( backslashCnt % 2 ) {
				//the tag is escaped. Skip the whole tag
				//and erase the last backslash responsible for the escaping.
				edit.applyOnMe( boost::make_shared<SimpleMEdit>( pos - str + matchPos + backslashCnt - 1, 1 ) );
				pos += matchPos + match.length(0);
			} else {
				//just skip the backslashes
				pos += matchPos + backslashCnt;
			}
			continue;

		} else {

			//this is a tag!
			bool endTag = ( match[2].length() > 0 );

			switch( match[3].str()[0] ) {
				//FIXME! do tag nesting for <i>,<b>,<s>,<u>
				case 'i':
					style.set( pos - str + matchPos, 0, endTag ? 0: RainbowStyle::M_ITALIC, RainbowStyle::M_ITALIC );
				break;
				case 'b':
					style.set( pos - str + matchPos, 0, endTag ? 0: RainbowStyle::M_BOLD, RainbowStyle::M_BOLD );
				break;
				case 'u':
					style.set( pos - str + matchPos, 0, endTag ? 0: RainbowStyle::M_UNDERLINE, RainbowStyle::M_UNDERLINE );
				break;
				case 's':
					style.set( pos - str + matchPos, 0, endTag ? 0: RainbowStyle::M_STRIKE, RainbowStyle::M_STRIKE );
				break;
				default:
					//it's a font tag, process the content
					Fl_Color c;
					bool useColor = true;
					if( endTag ) {
						if( !markupIndexStack.empty() ) {
							int searchStart = markupIndexStack[ markupIndexStack.size() - 1 ];
							markupIndexStack.pop_back();
							c = findPrevColor( searchStart, markup, _initStyle.color() );
						} else {
							//There's no matching <font color...> tag.
							//Either this is a stray </font> tag
							//or the matching <font> tag had no color attribute.
							useColor = false;
						}
					} else { //it's a start tag - <font ...>
						if( !parseColorAttr( c, match[4].str() ) ) {
							LOG_CANDY_ERROR("Bad or no font color: %s!\n", match[4].str().c_str() );
							useColor = false;
						} else {
							pushMarkupIndex = true;
						}
					}
					if( useColor ) {
						style.set( pos - str + matchPos, c, RainbowStyle::M_COLOR, RainbowStyle::M_COLOR );
					}
				break;
			}
		}
		int insertionIndex = RainbowMarkupUtils::Insert( markup, style );
		if( pushMarkupIndex ) {
			markupIndexStack.push_back( insertionIndex );
		}
		//markup.push_back( style );
		edit.applyOnMe( boost::make_shared<SimpleMEdit>( pos - str + matchPos, match.length(0) ) );
		pos += matchPos + match.length();
	}
	return true;
}

bool SrtTagSyntax::parseColorAttr( Fl_Color &dst, const std::string tagContent ) {
	boost::cmatch match;
	if( !boost::regex_search( tagContent.c_str(), match, rgxColorAttr ) ) {
		return false;
	}
	if( match.length(1) > 0 ) {
		//const char *str = match[1].str().c_str();
		//the above line didn't work on VS2008 on Windows
		//but the below works for some reason.
		std::string clrString = match[1].str();
		const char *str = clrString.c_str();
		dst = (Fl_Color)strtol( str, NULL, 16 ) << 8;
		return true;
	}
	if( match.length(2) == 0 ) {
		return false;
	}
	std::string cname( match[2].str().c_str() );
	naiveLowercase( cname );
	ColorMapT::const_iterator it = cmap.find( cname );
	if( it == cmap.end() ) { return false; }
	dst = it->second;
	return true;
}

void SrtTagSyntax::naiveLowercase( std::string &str ) {
	int l = str.size();
	for( int i = 0; i < l; ++i ) {
		if( str[i] < 'a' ) { str.replace( i, 1, 1, str[i] + ('a'-'A') ); }
	}
}

/** Finds a style that sets color before the given markup index
 * (non inclusive) and returns that color.
 */
Fl_Color SrtTagSyntax::findPrevColor( int startMarkupIndex, const RainbowMarkup &markup, Fl_Color defaultColor ) {
	int i = startMarkupIndex - 1;
	while( i >= 0 ) {
		if( markup[i].useColor() ) {
			return markup[i].color();
		}
		--i;
	}
	return defaultColor;
}

void SrtTagSyntax::fillColorMap() {
	cmap["aliceblue"] = 0xf0f8ff00;
	cmap["antiquewhite"] = 0xfaebd700;
	cmap["aqua"] = 0x00ffff00;
	cmap["aquamarine"] = 0x7fffd400;
	cmap["azure"] = 0xf0ffff00;
	cmap["beige"] = 0xf5f5dc00;
	cmap["bisque"] = 0xffe4c400;
	cmap["black"] = 0x00000000;
	cmap["blanchedalmond"] = 0xffebcd00;
	cmap["blue"] = 0x0000ff00;
	cmap["blueviolet"] = 0x8a2be200;
	cmap["brown"] = 0xa52a2a00;
	cmap["burlywood"] = 0xdeb88700;
	cmap["cadetblue"] = 0x5f9ea000;
	cmap["chartreuse"] = 0x7fff0000;
	cmap["chocolate"] = 0xd2691e00;
	cmap["coral"] = 0xff7f5000;
	cmap["cornflowerblue"] = 0x6495ed00;
	cmap["cornsilk"] = 0xfff8dc00;
	cmap["crimson"] = 0xdc143c00;
	cmap["cyan"] = 0x00ffff00;
	cmap["darkblue"] = 0x00008b00;
	cmap["darkcyan"] = 0x008b8b00;
	cmap["darkgoldenrod"] = 0xb8860b00;
	cmap["darkgray"] = 0xa9a9a900;
	cmap["darkgreen"] = 0x00640000;
	cmap["darkgrey"] = 0xa9a9a900;
	cmap["darkkhaki"] = 0xbdb76b00;
	cmap["darkmagenta"] = 0x8b008b00;
	cmap["darkolivegreen"] = 0x556b2f00;
	cmap["darkorange"] = 0xff8c0000;
	cmap["darkorchid"] = 0x9932cc00;
	cmap["darkred"] = 0x8b000000;
	cmap["darksalmon"] = 0xe9967a00;
	cmap["darkseagreen"] = 0x8fbc8f00;
	cmap["darkslateblue"] = 0x483d8b00;
	cmap["darkslategray"] = 0x2f4f4f00;
	cmap["darkslategrey"] = 0x2f4f4f00;
	cmap["darkturquoise"] = 0x00ced100;
	cmap["darkviolet"] = 0x9400d300;
	cmap["deeppink"] = 0xff149300;
	cmap["deepskyblue"] = 0x00bfff00;
	cmap["dimgray"] = 0x69696900;
	cmap["dimgrey"] = 0x69696900;
	cmap["dodgerblue"] = 0x1e90ff00;
	cmap["firebrick"] = 0xb2222200;
	cmap["floralwhite"] = 0xfffaf000;
	cmap["forestgreen"] = 0x228b2200;
	cmap["fuchsia"] = 0xff00ff00;
	cmap["gainsboro"] = 0xdcdcdc00;
	cmap["ghostwhite"] = 0xf8f8ff00;
	cmap["gold"] = 0xffd70000;
	cmap["goldenrod"] = 0xdaa52000;
	cmap["gray"] = 0x80808000;
	cmap["green"] = 0x00800000;
	cmap["greenyellow"] = 0xadff2f00;
	cmap["grey"] = 0x80808000;
	cmap["honeydew"] = 0xf0fff000;
	cmap["hotpink"] = 0xff69b400;
	cmap["indianred"] = 0xcd5c5c00;
	cmap["indigo"] = 0x4b008200;
	cmap["ivory"] = 0xfffff000;
	cmap["khaki"] = 0xf0e68c00;
	cmap["lavender"] = 0xe6e6fa00;
	cmap["lavenderblush"] = 0xfff0f500;
	cmap["lawngreen"] = 0x7cfc0000;
	cmap["lemonchiffon"] = 0xfffacd00;
	cmap["lightblue"] = 0xadd8e600;
	cmap["lightcoral"] = 0xf0808000;
	cmap["lightcyan"] = 0xe0ffff00;
	cmap["lightgoldenrodyellow"] = 0xfafad200;
	cmap["lightgray"] = 0xd3d3d300;
	cmap["lightgreen"] = 0x90ee9000;
	cmap["lightgrey"] = 0xd3d3d300;
	cmap["lightpink"] = 0xffb6c100;
	cmap["lightsalmon"] = 0xffa07a00;
	cmap["lightseagreen"] = 0x20b2aa00;
	cmap["lightskyblue"] = 0x87cefa00;
	cmap["lightslategray"] = 0x77889900;
	cmap["lightslategrey"] = 0x77889900;
	cmap["lightsteelblue"] = 0xb0c4de00;
	cmap["lightyellow"] = 0xffffe000;
	cmap["lime"] = 0x00ff0000;
	cmap["limegreen"] = 0x32cd3200;
	cmap["linen"] = 0xfaf0e600;
	cmap["magenta"] = 0xff00ff00;
	cmap["maroon"] = 0x80000000;
	cmap["mediumaquamarine"] = 0x66cdaa00;
	cmap["mediumblue"] = 0x0000cd00;
	cmap["mediumorchid"] = 0xba55d300;
	cmap["mediumpurple"] = 0x9370db00;
	cmap["mediumseagreen"] = 0x3cb37100;
	cmap["mediumslateblue"] = 0x7b68ee00;
	cmap["mediumspringgreen"] = 0x00fa9a00;
	cmap["mediumturquoise"] = 0x48d1cc00;
	cmap["mediumvioletred"] = 0xc7158500;
	cmap["midnightblue"] = 0x19197000;
	cmap["mintcream"] = 0xf5fffa00;
	cmap["mistyrose"] = 0xffe4e100;
	cmap["moccasin"] = 0xffe4b500;
	cmap["navajowhite"] = 0xffdead00;
	cmap["navy"] = 0x00008000;
	cmap["oldlace"] = 0xfdf5e600;
	cmap["olive"] = 0x80800000;
	cmap["olivedrab"] = 0x6b8e2300;
	cmap["orange"] = 0xffa50000;
	cmap["orangered"] = 0xff450000;
	cmap["orchid"] = 0xda70d600;
	cmap["palegoldenrod"] = 0xeee8aa00;
	cmap["palegreen"] = 0x98fb9800;
	cmap["paleturquoise"] = 0xafeeee00;
	cmap["palevioletred"] = 0xdb709300;
	cmap["papayawhip"] = 0xffefd500;
	cmap["peachpuff"] = 0xffdab900;
	cmap["peru"] = 0xcd853f00;
	cmap["pink"] = 0xffc0cb00;
	cmap["plum"] = 0xdda0dd00;
	cmap["powderblue"] = 0xb0e0e600;
	cmap["purple"] = 0x80008000;
	cmap["rebeccapurple"] = 0x66339900;
	cmap["red"] = 0xff000000;
	cmap["rosybrown"] = 0xbc8f8f00;
	cmap["royalblue"] = 0x4169e100;
	cmap["saddlebrown"] = 0x8b451300;
	cmap["salmon"] = 0xfa807200;
	cmap["sandybrown"] = 0xf4a46000;
	cmap["seagreen"] = 0x2e8b5700;
	cmap["seashell"] = 0xfff5ee00;
	cmap["sienna"] = 0xa0522d00;
	cmap["silver"] = 0xc0c0c000;
	cmap["skyblue"] = 0x87ceeb00;
	cmap["slateblue"] = 0x6a5acd00;
	cmap["slategray"] = 0x70809000;
	cmap["slategrey"] = 0x70809000;
	cmap["snow"] = 0xfffafa00;
	cmap["springgreen"] = 0x00ff7f00;
	cmap["steelblue"] = 0x4682b400;
	cmap["tan"] = 0xd2b48c00;
	cmap["teal"] = 0x00808000;
	cmap["thistle"] = 0xd8bfd800;
	cmap["tomato"] = 0xff634700;
	cmap["turquoise"] = 0x40e0d000;
	cmap["violet"] = 0xee82ee00;
	cmap["wheat"] = 0xf5deb300;
	cmap["white"] = 0xffffff00;
	cmap["whitesmoke"] = 0xf5f5f500;
	cmap["yellow"] = 0xffff0000;
	cmap["yellowgreen"] = 0x9acd3200;
}
