/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Srt_Tag_Syntax_h_
#define _Srt_Tag_Syntax_h_ 1

#include "../../rainbow_style.h"
#include "../../utf8_tools.h"
#include "shared/src/default_log.h"
#include "../../markup/markup_edit.h"
#include "../../rainbow_markup.h"

#include "boost/regex.hpp"
#include "boost/smart_ptr/shared_ptr.hpp"
#include "boost/smart_ptr/make_shared.hpp"
//#include <boost/enable_shared_from_this.hpp>

#include <map>
#include <string>

class SrtTagSyntax {
protected:
	typedef std::map<std::string, Fl_Color> ColorMapT;
	ColorMapT	cmap;

public:
	SrtTagSyntax() {
		rgxTag.assign("(\\\\+)?<(\\/)?([ibsu]|font)([^>]*)>");
		rgxColorAttr.assign("color *= *\"?(?>(?>#([0-9a-fA-F]{6}))|([a-zA-Z]+))\"?");
		fillColorMap();
	};

	void fillColorMap();

	/**
	 * NOTE: _initStyle members start and len are never used.
	 */
	bool parse( const std::string &text, const RainbowStyle &_initStyle, const RainbowMarkup *baseMarkup = NULL );
	bool parseColorAttr( Fl_Color &dst, const std::string tagContent );
	void naiveLowercase( std::string &str );
	Fl_Color findPrevColor( int start, const RainbowMarkup &markup, Fl_Color defaultColor = 255 );

	ComposedMEdit edit;
	RainbowMarkup	markup;

protected:
	boost::regex					rgxTag;
	boost::regex					rgxColorAttr;
};

#endif //_Srt_Tag_Syntax_h_
