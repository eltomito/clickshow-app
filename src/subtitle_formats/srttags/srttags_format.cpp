/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "srttags_format.h"
#include "srttag_syntax.h"
#include "../../markup/markup_edit.h"
#include "../../rainbow_markup.h"

/*
 * returns an error code.
 */
int SrtTagFormat::Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run )
{
	SrtTagSyntax sts;

	if( sub.is_comment() ) {
		//don't morph comment subs
		return Errr::OK;
	}

	sts.parse( sub.get_disp_text(), lastStyle, &sub.get_markup() );
	lastStyle = RainbowMarkupUtils::FinalStyle( sts.markup, lastStyle );
	if( !dry_run ) {
		sts.edit.apply( sub.mutget_disp_text() );
		sts.edit.apply( sts.markup );
		sub.set_markup( sts.markup );
		LOG_GEN_INFO("SrtTag Markup:\n%s", RainbowMarkupUtils::toString( sts.markup ).c_str() );
	}
	return Errr::OK;
}
