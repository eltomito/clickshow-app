/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Sub_Morph_h_
#define _Sub_Morph_h_ 1

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "../subtitles/subtitle.h"
#include "../subtitles/subtitles.h"

class SubMorph
{
protected:
	SubMorph() { Reset(); };
	~SubMorph() {};

public:
	/*
	 * returns an error code
	 */
	virtual int Morph( Subtitle &sub, ErrDescsT &errs, bool dry_run = false ) { return Errr::OK; };

	/*
	 * returns an error code
	 */
	virtual int MorphSubs( Subtitles &subs, ErrDescsT &errs, bool dry_run = false, bool use_raw_text = false )
	{
		Subtitles::iter it = subs.get_sub_iter( 0 );
		Subtitles::iter endit = subs.get_sub_iter( -1 );
		int num = 0;
		int err = Errr::OK;
		while(( it != endit )&&( !err )) {
			if( use_raw_text ) {
				it->set_disp_text( it->get_raw_text() );
				it->mutget_markup().clear();
			}
			err = Morph( *it, errs, dry_run );
			++it;
		}
		return err;
	};

	virtual bool Detect( Subtitles &subs, ErrDescsT &errs ) { return false; };

	virtual void Reset() {};
};

#endif //_Sub_Morph_h_
