/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "subtitle_formats.h"

#include "shared/src/err.h"
#include "timing/plain.h"
#include "timing/srt.h"
#include "base/base_format.h"
#include "candy/candy_format.h"
#include "multi/multi_format.h"
#include "../clicky_debug.h"
#include "../candy/candy_syntax.h"
#include "../candy/candy_recipe.h"
#include "srttags/srttags_format.h"
#include "bslash/bslash_collapser.h"

#include <FL/filename.H>

/*
 * Returns an error codes.
 */
int SubtitleFormats::Parse( const char *utf8, Subtitles &subs, const ParserCfg &cfg )
{
	if( !utf8 ) { return Errr::MISSING_DATA; }

	ErrDescsT error_list;
	int err;

	subs.set_timed( false );
	subs.set_fully_timed( false );

	int fmt = cfg.Format();
	if( fmt == FMT_UNKNOWN ) {
		fmt = GuessFormat( utf8 );
	}

	//parse plain or srt
	switch( fmt ) {
		case FMT_PLAIN:
			err = PlainParser::Parse( utf8, subs, error_list );
		break;
		case FMT_SRT:
			subs.set_timed( true );
			err = SrtParser::Parse( utf8, subs, error_list );
			subs.check_fully_timed();
		break;
		default:
			return Errr::SUB_FMT_UNKNOWN;
		break;
	}
	if( err ) {
		LOG_FILE_ERROR("File parser error: \"%s\",\n%s\n", Errr::Message(err), ErrDesc::DescsToString( error_list ).c_str() );
		return err;
	}

	err = Morph( subs, cfg, error_list );

	if( err ) {
		LOG_FILE_ERROR("Subtitle parser error: \"%s\",\n%s\n", Errr::Message(err), ErrDesc::DescsToString( error_list ).c_str() );
	}
	return err;
};

bool SubtitleFormats::DetectForcedCandy( const Subtitles &subs ) {
	const Subtitle *sub = ((Subtitles &)subs).get_subtitle( 0 );
	return( sub && sub->is_comment() && ( sub->get_raw_text() == "#*@+-" ));
}

int SubtitleFormats::Morph( Subtitles &subs, const ParserCfg &cfg, ErrDescsT &error_list )
{
	BaseFormat bf;
	CandyFormat cf;
	MultiFormat mf;
	SrtTagFormat stf;
	BSlashCollapser bsc;
	bool multi = false;

	//parse comments and marks
	int err = bf.MorphSubs( subs, error_list, false, true );
	if( !err ) {
		//parse multi markup
		multi = mf.Detect( subs, error_list );
		if( multi ) {
			err = mf.MorphSubs( subs, error_list );
		}
		if( !err ) {
			if( !multi ) {
				subs.set_candy( ( cfg.ParseCandy() == PARSE_ON )
				||(( cfg.ParseCandy() == PARSE_AUTO )&&( DetectForcedCandy( subs ) || cf.Detect( subs, error_list ) ))
				||(( cfg.ParseCandy() == PARSE_STRICT )&& DetectForcedCandy( subs ) ) );
			}
			//parse candy markup
			if( subs.is_candy() ) {
				err = cf.MorphSubs( subs, error_list );
			}
			//parse srt tags
			err = err || stf.MorphSubs( subs, error_list );
			//collapse remaining backslashes
			err = err || bsc.MorphSubs( subs, error_list );
		}
	}
	//TODO: parse multiline markup
	//TODO: parse srt tags
	return err;
}

int SubtitleFormats::Serialize( const Subtitles &subs, std::string &dst, int fmt )
{
	int err = Errr::OK;

	switch( fmt ) {
		case FMT_PLAIN:
			err = PlainParser::Serialize( subs, dst );
		break;
		case FMT_SRT:
			err = SrtParser::Serialize( subs, dst );
		break;
		default:
			err = Errr::SUB_FMT_UNKNOWN;
		break;
	}
	return err;
}

int SubtitleFormats::GuessFormatFromFileName( const std::string &fname )
{
	const char *cstr = fname.c_str();
	const char *ext = fl_filename_ext( cstr );
	if( !ext ) {
		return FMT_UNKNOWN;
	}
	size_t pos = ext - cstr;
	size_t len = fname.size() - pos;
	if(( 0 == fname.compare( pos, len, ".txt" ) )||( 0 == fname.compare( pos, len, ".TXT" ) )) {
		return FMT_PLAIN;
	}
	if(( 0 == fname.compare( pos, len, ".srt" ) )||( 0 == fname.compare( pos, len, ".SRT" ) )) {
		return FMT_SRT;
	}
	return FMT_UNKNOWN;
}

int SubtitleFormats::GuessFormat( const char *utf8 )
{
	Subtitles subs;
	ErrDescsT errorlist;
	int err = SrtParser::Parse( utf8, subs, errorlist, 5 );
	return err ? FMT_PLAIN : FMT_SRT;
}

int SubtitleFormats::CandyParseFromPrefs( int candy_pref )
{
	int states[] =  { PARSE_OFF, PARSE_ON, PARSE_AUTO, PARSE_STRICT };
	if(( candy_pref < 0 )||( candy_pref > (sizeof( states ) / sizeof( int )) )) {
		LOG_GEN_ERROR("BAD candy pref (%i). Defaulting to STRICT.\n");
		candy_pref = 3;
	}
	return states[ candy_pref ];
}
