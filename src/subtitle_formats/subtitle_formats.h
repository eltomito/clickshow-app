/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Subtitle_Formats_h_
#define _Subtitle_Formats_h_ 1

#include "../subtitles/subtitle.h"
#include "../subtitles/subtitles.h"
#include "shared/src/err_desc.h"

class SubtitleFormats
{
private:
	SubtitleFormats() {};
	~SubtitleFormats() {};
public:
	static const int FMT_UNKNOWN = 0;
	static const int FMT_PLAIN = 1;
	static const int FMT_SRT = 2;

	static const int PARSE_OFF = 0;
	static const int PARSE_ON = 1;
	static const int PARSE_AUTO = 2;
	static const int PARSE_STRICT = 3;

	class ParserCfg
	{
	public:
		ParserCfg( int _fmt, int _candy, bool _srt_tags ) : fmt(_fmt),candy(_candy),srt_tags(_srt_tags) {};
		~ParserCfg() {};
		void Format( int _fmt ) { fmt = _fmt; };
		int Format() const { return fmt; };
		/*
		 * @param candy (int) PARSE_OFF|PARSE_ON|PARSE_AUTO
		 */
		void ParseCandy( int _candy ) { candy = _candy; };
		int ParseCandy() const { return candy; };
		void ParseSrtTags( bool _srt_tags ) { srt_tags = _srt_tags; };
		bool ParseSrtTags() const { return srt_tags; };

	private:
		int fmt;
		int candy;
		bool srt_tags;
	};

	int static Parse( const char *utf8, Subtitles &subs, const ParserCfg &cfg );
	int static Morph( Subtitles &subs, const ParserCfg &cfg, ErrDescsT &error_list );
	int static Serialize( const Subtitles &subs, std::string &dst, int fmt = FMT_PLAIN );

	static int GuessFormatFromFileName( const std::string &fname );
	static int GuessFormat( const char *utf8 );

	/*
	 * translates the parse_candy preference value to the PARSE_ON|OFF|AUTO constants.
	 */
	static int CandyParseFromPrefs( int candy_pref );

	static bool DetectForcedCandy( const Subtitles &subs );

};

#endif //_Subtitle_Formats_h_
