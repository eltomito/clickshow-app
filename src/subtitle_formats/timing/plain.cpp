#include "plain.h"

#include "shared/src/str_line_iterator.h"
#include "shared/src/subtitles/subtitle_timing.h"

int PlainParser::Parse( const char *utf8, Subtitles &dst, ErrDescsT &errorlist )
{
	if( !utf8 ) { return Errr::MISSING_DATA; }
	if( utf8[0] == 0 ) { return Errr::OK; }

	Subtitle sub;

	std::string line;
	std::string sub_text;
	int state = 0;
	int err;

	str_line_iterator it( utf8 );
	str_line_iterator endit;

	while( it != endit ) {
		line = *it;
		switch( state )
		{
			case 0: //starting a new subtitle
				sub.reset();
				trimCommentLeft( line );	//leave in the blank characters that can escape a #
				trimRight( line );
				if( !line.empty() ) {
					sub_text = line;
					state = 1;
				} else { //line is empty
					sub.set_raw_text( line );
					dst.append( sub );
					state = 0;
				}
			break;

			case 1: //adding lines to a subtitle
				if( !utf8_is_all_space( line ) ) {
					trim( line );
					sub_text.push_back('\n');
					sub_text.append( line );
				} else {
					sub.set_raw_text( sub_text );
					dst.append( sub );
					sub_text.clear();
					state = 0;
				}
			break;
			default:
			break;
		}
		++it;
	}
	if( state == 1 ) {
		sub.set_raw_text( sub_text );
		dst.append( sub );
	}
	return Errr::OK;
};

int PlainParser::Serialize( const Subtitles &subs, std::string &dst )
{
	Subtitles::iter it = ((Subtitles &)subs).get_sub_iter( 0 );
	Subtitles::iter endit = ((Subtitles &)subs).get_sub_iter( -1 );
	while( it != endit ) {
		dst.append( it->get_raw_text() );
		dst.append( it->get_raw_text().empty() ? "\n" : "\n\n");
		++it;
	}
	if( dst.size() ) {
		dst.erase( dst.size() - 1 );
	}
	return Errr::OK;
};

void PlainParser::trimCommentLeft( std::string &str )
{
	size_t pos = 0;
	size_t len = str.size();
	const char *cstr = str.c_str();
	int charlen;
	unsigned int ucs = utf8_getchar( cstr, &charlen );
	while(( isCommentTrimmableUcs( ucs ) )&&( pos < len )) {
		pos += charlen;
		ucs = utf8_getchar( cstr + pos, &charlen );
	};
	str.erase( 0, pos );
};

void PlainParser::trimLeft( std::string &str )
{
	size_t pos = 0;
	size_t len = str.size();
	const char *cstr = str.c_str();
	int charlen;
	unsigned int ucs = utf8_getchar( cstr, &charlen );
	while(( isTrimmableUcs( ucs ) )&&( pos < len )) {
		pos += charlen;
		ucs = utf8_getchar( cstr + pos, &charlen );
	};
	str.erase( 0, pos );
};

void PlainParser::trimRight( std::string &str )
{
	size_t len = str.size();
	if( !len ) { return; }
	const char *cstr = str.c_str();
	const char *end = cstr + len;
	const char *tailp = end;
	const char *p = fl_utf8back( end - 1, cstr, end );
	int charlen;
	unsigned int ucs = utf8_getchar( p, &charlen );
	while(( isTrimmableUcs( ucs ) )&&( p > cstr )) {
		tailp = p;
		p = fl_utf8back( p - 1, cstr, end );
		ucs = utf8_getchar( p, &charlen );
	};
	if( tailp < end ) {
		size_t tailpos = tailp - cstr;
		str.erase( tailpos );
	}
};
