/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _None_h
#define _None_h

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "../../subtitles/subtitles.h"
#include "../../utf8_tools.h"

class PlainParser
{
public:
	/*
	 * returns an error code.
	 */
	static int Parse( const char *utf8, Subtitles &dst, ErrDescsT &errorlist );

	/*
	 * returns an error code.
	 */
	static int Serialize( const Subtitles &subs, std::string &dst );

private:
	static bool isCommentEscapeBlank( unsigned int ucs ) { return ( ucs == (unsigned long)0x00a0 )||( ucs == (unsigned long)0x200B ); };
	static bool isCommentTrimmableUcs( unsigned int ucs ) { return ( !isCommentEscapeBlank( ucs )&&( utf8_is_space( ucs ) )); };
	static bool isTrimmableUcs( unsigned int ucs ) { return (( ucs != (unsigned long)0x00a0 )&&( utf8_is_space( ucs ) )); };
	static void trimLeft( std::string &str );
	static void trimCommentLeft( std::string &str );
	static void trimRight( std::string &str );
	static void trim( std::string &str ) { trimRight( str ); trimLeft( str ); };

	PlainParser() {};
	~PlainParser() {};
};

#endif //_None_h
