#include "srt.h"

#include "shared/src/str_line_iterator.h"

int SrtParser::Parse( const char *utf8, Subtitles &dst, ErrDescsT &errorlist, int max_subs )
{
	if( !utf8 ) { return Errr::MISSING_DATA; }

	Subtitle sub;
	SubTiming timing;

	std::string line;
	std::string sub_text;
	int line_num = 0;
	int sub_cnt = 0;
	int sub_num;
	int state = 0;
	int err = Errr::OK;
	int parsedlen;
	int final_err = Errr::OK;

	str_line_iterator it( utf8 );
	str_line_iterator endit;

	while(( it != endit )&&( sub_cnt <= max_subs )) {
		line = *it;
		switch( state )
		{
			case 0: //looking for a subtitle number
				if( !line.empty() ) {
					sub_num = ParseSubNum( line.c_str(), parsedlen );
					if(( sub_num != BADSUBNUM )&&( parsedlen == line.size() )) {
						sub.reset();
						++state;
					} else {
						err = Errr::SUB_FMT_GARBAGE;
						errorlist.push_back( ErrDesc( err, line_num, line, ErrDesc::LOC_LINE ) );
					}
				}
			break;

			case 1: //expecting timing
				parsedlen = ParseTiming( line.c_str(), timing );
				if( parsedlen == line.size() ) {
					++state;
				} else {
					err = ( parsedlen < 0 ) ? Errr::SUB_FMT_TIMING : Errr::SUB_FMT_GARBAGE;
					errorlist.push_back( ErrDesc( err, line_num, line, ErrDesc::LOC_LINE ) );
					state = 0;
				}
			break;

			case 2: //subtitle text
				if( line.empty() ) {
					sub.set_raw_text( sub_text );
					sub.set_timing( timing );
					dst.append( sub );
					++sub_cnt;
					sub_text.clear();
					state = 0;
				} else {
					if( !sub_text.empty() ) { sub_text.push_back('\n'); }
					sub_text.append( line );
				}
			break;
		}
		if( err && !final_err ) { final_err = err; }
		++line_num;
		++it;
	}
	if( state == 2 ) {
		sub.set_raw_text( sub_text );
		sub.set_timing( timing );
		dst.append( sub );
	}
	return final_err;
};

int SrtParser::Serialize( const Subtitles &subs, std::string &dst )
{
	int err;
	int num = 1;
	Subtitles::iter it = ((Subtitles &)subs).get_sub_iter( 0 );
	Subtitles::iter endit = ((Subtitles &)subs).get_sub_iter( -1 );
	while( it != endit ) {
		err = FormatSubHead( num, it->get_timing(), dst );
		if( err ) { return err; }
		dst.append( it->get_raw_text() );
		dst.append("\n\n");
		++it;
		++num;
	}
	if( num > 1 ) {
		dst.erase( dst.size() - 1 ); //erase the last one of the two final newlines
	}
	return Errr::OK;
}
