/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Srt_h
#define _Srt_h

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "shared/src/subtitle_formats/core/srt_core.h"
#include "shared/src/subtitles/subtitle_timing.h"
#include "../../subtitles/subtitles.h"
#include <climits>

class SrtParser : public SrtCore
{
public:
	static const long BADTIME = LONG_MIN;
	static const long BADSUBNUM = INT_MIN;
	static const long MAX_SUB_NUM_DIGITS = 11;
	static const long MAX_SUB_NUM = INT_MAX;
	static const long UNTIMED_TIME = 359999999; //99:59:59,999
	/*
	 * returns an error code.
	 */
	static int Parse( const char *utf8, Subtitles &dst, ErrDescsT &errorlist, int max_subs = INT_MAX );

	/*
	 * returns an error code.
	 */
	static int Serialize( const Subtitles &subs, std::string &dst );

protected:
	SrtParser() {};
	~SrtParser() {};
};

#endif //_Srt_h
