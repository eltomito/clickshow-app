/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <cstdlib>
#include <string>
#include <cstdio>

#include "../clicky_debug.h"

#include "subtitle.h"

Subtitle::Subtitle( const char *txt, int text_len )
{
	flags = 0;
	set_raw_text( txt, text_len );
}

Subtitle::~Subtitle()
{
	text.clear();
}

Subtitle &Subtitle::operator=( const Subtitle &sub )
{
	raw_text = sub.raw_text;
	disp_text = sub.disp_text;
	flags = sub.flags;
	timing = sub.timing;
	markup = sub.markup;
	return *this;
}

void Subtitle::reset()
{
	raw_text.clear();
	text.clear();
	timing.Reset();
	flags = 0;
	clear_recipe();
};

void Subtitle::set_from_cstr( std::string &dst, const char *txt, int text_len )
{
	if( txt != NULL )
	{
		if( text_len == -1 )
			dst = txt;
		else if( text_len == 0 )
					dst = "";
				else
					dst.assign( txt, text_len );
	}
	else
		dst.erase();
};
	
/*
 * state:	0 - off
 *				1 - on
 *				2 - toggle
 */	
int Subtitle::mark( int state )
{
	switch( state )
	{
		case 2:	flags = flags^MARKED;
					break;
		case 1:	flags = flags|MARKED;
					break;
		case 0:	flags = flags&( ~MARKED );	
					break;
	};
	update_raw_mark();
	return ( flags & MARKED );
};

void Subtitle::update_raw_mark()
{
	bool raw_marked = ( 0 == raw_text.compare( 0, 3, "(*)" ) );
	if( is_marked() == raw_marked ) return;
	if( raw_marked ) {
		raw_text.erase( 0, 3 );
	} else {
		raw_text.insert( 0, "(*)" );
	}
}

void Subtitle::printme()
{
	LOG_FILE_DETAIL("%s\n", text.c_str() );
};

