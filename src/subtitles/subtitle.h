/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Subtitle_h
#define _Subtitle_h

#include <string>

#include "../rainbow.h"
#include "../candy/candy_recipe.h"
#include "shared/src/subtitles/subtitle_timing.h"

class Subtitle
{
	static const unsigned int	MARKED = 1;
	static const unsigned int	COMMENT = 1<<1;

	std::string		raw_text;
	std::string		disp_text;

	std::string		text;		//deprecated
	unsigned int	flags;

	SubTiming		timing;

public:

	Subtitle( const char *txt=NULL, int text_len=-1 );
	//Subtitle( const Subtitle &sub );
	~Subtitle();

	Subtitle &operator=( const Subtitle &sub );

	void reset();

	const std::string &get_raw_text() const { return raw_text; };
	std::string &mutget_raw_text() { return raw_text; };
	void set_raw_text( const char *txt, int text_len=-1 ) { set_from_cstr( raw_text, txt, text_len ); };
	void set_raw_text( const std::string &txt ) { raw_text = txt; }

	const std::string &get_disp_text() const { return disp_text; };
	std::string &mutget_disp_text() { return disp_text; };
	void set_disp_text( const char *txt, int text_len=-1 ) { set_from_cstr( disp_text, txt, text_len ); };
	void set_disp_text( const std::string &txt ) { disp_text = txt; }

	void set_timing( const SubTiming &st ) { timing = st; };
	const SubTiming &get_timing() const { return timing; };
	SubTiming &mutget_timing() { return timing; };

	//-- marking --

	bool is_marked() const { return ( flags & MARKED ); };
	bool is_comment() const { return ( flags & COMMENT ); };
	void set_mark( bool state = true ) {
		flags = (flags&(~MARKED)) | (state ? MARKED : 0 );
		update_raw_mark();
	};
	void set_comment( bool state = true ) { flags = (flags&(~COMMENT)) | (state ? COMMENT : 0 ); };

	// -- markup
	
	void clear_markup() { markup.clear(); };
	void set_markup( const RainbowMarkup &_markup ) { markup = _markup; };
	const RainbowMarkup &get_markup() const { return markup; };
	RainbowMarkup &mutget_markup() { return markup; };

	// -- recipe
private:
	CandyRecipe recipe;	//deprecated
	RainbowMarkup markup;

	void update_raw_mark();
	void set_from_cstr( std::string &dst, const char *txt, int text_len = 0 );

public:
	void set_recipe( CandyRecipe &cr ) { recipe = cr; };	//deprecated
	void clear_recipe() { recipe.clear(); };					//deprecated
	CandyRecipe &get_recipe() { return recipe; };			//deprecated

	/*
	 * state:	0 - off, 1 - on 2 - toggle
	 *	returns: the new state of the subtitle mark (0 or 1)
	 */	
	int mark( int state );

	void printme();
};

#endif //_Subtitle_h
