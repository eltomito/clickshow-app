/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "subtitle_doc.h"

#include "../clicky_debug.h"
#include "../file_utils.h"

#include "../subtitle_formats/base/base_format.h"
#include "../subtitle_formats/candy/candy_format.h"
#include "../subtitle_formats/multi/multi_format.h"
#include "../subtitle_formats/timing/plain.h"
#include "../subtitle_formats/timing/srt.h"

#include "shared/src/str_utils.h"

#include <FL/Fl.H>
#include <FL/fl_utf8.h>

#include <cstring>
#include <locale>
#include <limits>
#include <algorithm>

const char *SubtitleDoc::MARK_STRING = "(*)";

SubtitleDoc::SubtitleDoc()
{
	set_filename( NULL );
	subtitles = new Subtitles();

	clear_modified();
};

SubtitleDoc::~SubtitleDoc()
{
    delete subtitles;
};

void SubtitleDoc::set_filename( const char *fname )
{
	if( fname == NULL )
		file_name.clear();
	else
		file_name.assign( fname );
};

void SubtitleDoc::set_savename( const char *fname )
{
	if( fname == NULL )
		save_name.clear();
	else
		save_name.assign( fname );
};

/*
 * hash - points to SubtitleDoc::HASH_LEN bytes of memory to hold the hash value
 */
void SubtitleDoc::hash_subtitles2( uint8 *hash )
{
	std::string substr;
	const char *s;
	int	pos;
	uint8 	b, h0, h1, h2, h3;
	uint32	l;

		hash[0] = 0;
		hash[1] = 0;
		hash[2] = 0;
		hash[3] = 0;

	if( subtitles == NULL )
		return;

	//put all subtitles into one string
	if( subs_to_utf8( subtitles, substr ) <= 0 )
		return;

	//hash the whole text
	s = substr.c_str();
	pos = 0;
	while( s[0] != 0 )
	{
		b = (uint8)s[0];
		l = (uint32)b * (uint32)pos;
		h0 = hash[0] ^ (l & 0xff);
		h1 = hash[1] ^ ((l >> 8) & 0xff);
		h2 = hash[2] ^ ((l >> 16) & 0xff);
		h3 = hash[3] ^ ((l >> 24) & 0xff);

		h2 ^= b;
		h3 += b;

		hash[0] = h0;
		hash[1] = h1;
		hash[2] = h2;
		hash[3] = h3;

		s++;
		pos++;
	};
}

int SubtitleDoc::load_from_string( const char *utf8subs, const std::string &fileName, const SubtitleFormats::ParserCfg &cfg )
{
	if( subtitles!= NULL ) { subtitles->clear();	}
	subtitles = utf8_to_subs2( utf8subs, subtitles, cfg );

	//set the file name of this subtitle document
	set_filename( fileName.c_str() );

	//set subtitles as unchanged
	clear_modified();

	return (subtitles != NULL ) ? subtitles->size() : -1;
}

int SubtitleDoc::save_to_string( std::string &dst )
{
	return subs_to_utf8( get_subtitles(), dst );
}

int SubtitleDoc::load_from_file( const char *fname, const SubtitleFormats::ParserCfg &cfg )
{
	size_t	utf8size, fsize;
	char		*fdata;
	char		*utf8data;
	int 		err;

	fdata = FileUtils::read_file( fname, fsize, err, true );
	if( !fdata ) { return -1 * err; };

	utf8data = TextEncoder::ToNormalEasy( fdata, fsize, encinfo, err );
	free( fdata );
	if( !utf8data ) {
		return -1 * err;
	}

	int res = load_from_string( utf8data, fname, cfg );
	free( utf8data );
	return res;
};

void SubtitleDoc::clear_modified()
{
	hash_subtitles2( &last_saved_hash2[0] );
	LOG_FILE_DETAIL("saving new hash: %2x%2x%2x%2x\n", last_saved_hash2[0],last_saved_hash2[1],last_saved_hash2[2],last_saved_hash2[3] );
};

bool SubtitleDoc::is_modified()
{
	uint8 hsh[HASH_LENGTH];

	bool retval;

	hash_subtitles2( &hsh[0] );

	LOG_FILE_DETAIL("current hash : %2x%2x%2x%2x\n", hsh[0],hsh[1],hsh[2],hsh[3] );
	LOG_FILE_DETAIL("previous hash: %2x%2x%2x%2x\n", last_saved_hash2[0],last_saved_hash2[1],last_saved_hash2[2],last_saved_hash2[3] );

	retval =  !( ( hsh[0]==last_saved_hash2[0] )&&( hsh[1]==last_saved_hash2[1] )
					&&( hsh[2]==last_saved_hash2[2] )&&( hsh[3]==last_saved_hash2[3] ) );

	LOG_FILE_DETAIL("returning %i\n", (int)retval );

	return retval;
}

/*
 * if subs is NULL, it creates new subtitles
 */
Subtitles *SubtitleDoc::utf8_to_subs2( const char * utf8str, Subtitles *subs, const SubtitleFormats::ParserCfg &cfg )
{
	bool owned_subs = false;

	if( subs==NULL ) {
		subs = new Subtitles();
		owned_subs = true;
	}

	if( utf8str == NULL )
		return subs;

	const char *s = utf8_skip_bom( utf8str );

	int err  = SubtitleFormats::Parse( s, *subs, cfg );

	if( !err ) {
		int overlaps = subs->fix_timing_overlaps();
		LOG_FILE_INFO("Fixed: %i overlapping subtitles\n", overlaps );
		return subs;
	}
	LOG_FILE_ERROR("Failed to parse subtitles! Err: %i\n", err );
	if( owned_subs) {
		delete subs;
	}
	return NULL;
};

bool SubtitleDoc::is_mark_string( const char *utf8str )
{
	return (0 == strncmp( MARK_STRING, utf8str, 3 ) );
};

//====== saving ========

size_t SubtitleDoc::sub_to_utf8( Subtitle &sub, std::string &str )
{
	std::string substr;
	size_t total = 0;

	substr.append( sub.get_raw_text() );

	//strip trailing newlines
	size_t substrlen = substr.size();
	size_t newlines = 0;
	while( (newlines < substrlen ) && (substr.at( substrlen - 1 - newlines ) == '\n' ) ) {
		newlines++;
	}
	if( newlines > 0 ) {
		substr.erase( substrlen - newlines, newlines );
	}

	if( sub.is_marked() ) {
		str.append( MARK_STRING );
		total += strlen( MARK_STRING );
		if( substr.size() > 0 ) {
			str.append( "\n" );
			total++;
		}
	};
	
	str.append( substr );
	total += substr.size();
	
	return total;
};

/*
 * @returns the number of subtitles converted or a negative error code
 */
int SubtitleDoc::subs_to_utf8( Subtitles *subs, std::string &utf8subs )
{
	if( subs == NULL )
		return -1 * Errr::MISSING_DATA;

	int res;

	if( subs->is_timed() ) {
		res = SrtParser::Serialize( *subs, utf8subs );
	} else {
		res = PlainParser::Serialize( *subs, utf8subs );
	}
	if( res ) {
		return (-1)*res;
	}
	return subs->size();
}

/*
int SubtitleDoc::subs_to_utf8( Subtitles *subs, std::string &utf8subs )
{
	if( subs == NULL )
		return -1 * Errr::MISSING_DATA;

	size_t applen;

	int cnt = subs->size();
	for( int i=0; i<cnt; i++ )
	{
		if( subs->is_timed() ) {
			
		}
		applen = sub_to_utf8( *subs->get_subtitle( i ), utf8subs );
		if( applen > 0 ) {
			utf8subs.append( "\n\n" );
		} else {
			utf8subs.append( "\n" );
		}
	};

	//remove the last empty line
	if( cnt > 0 )
		utf8subs.erase( utf8subs.size() - 1, std::string::npos );

	return cnt;
};
*/

/*
 * returns: number of subtitles saved or a negative error code
 */
int SubtitleDoc::save_to_file( const char *fname, bool fix_ext )
{
	std::string		str;
	int 				numsubs;
	char				*data;
	size_t			len, outlen;
	FILE				*f;

	std::string file_name = fname;

	if( fix_ext ) {
		fix_fname_ext( file_name );
	}

	LOG_FILE_INFO("SAVING SUBTITLES as %s\n", file_name.c_str() );

	numsubs = subs_to_utf8( subtitles, str );
	if( numsubs < 0 ) { return -1 * Errr::BAD_COUNT; }

	int err;
	data = TextEncoder::ToOriginal( str, encinfo, len, err );
	if( err != Errr::OK ) {
		if( data ) { free( data ); }
		return -1 * err;
	}

	FileUtils::write_file( file_name.c_str(), data, len );
	free( data );
	if( err != Errr::OK ) { return -1 * err; }

	//the file has just been saved, so we can clear the modified state
	clear_modified();

	return numsubs;
};

void SubtitleDoc::fix_fname_ext( std::string &fname )
{
	std::string dirs, bare, ext;
	FileUtils::parse_path( fname.c_str(), dirs, bare, ext );
	if( subtitles && subtitles->is_timed() ) {
		ext = "srt";
	} else {
		ext = "txt";
	}
	FileUtils::compose_path( fname, dirs, bare, ext );
}

void SubtitleDoc::generate_savename( const char *origname, const char *default_name )
{
	if( origname == NULL ) {
		origname = default_name;
	}
	std::string dirs, bare, ext;
	FileUtils::parse_path( origname, dirs, bare, ext );
	if( bare.empty() ) {
		FileUtils::parse_path( default_name, dirs, bare, ext );
	}
	ext = (subtitles && subtitles->is_timed() ) ? "srt" : "txt";

	//append '-v1' or -'vn' if -v(n-1) is in the file name already
	size_t last_minus = bare.find_last_of("-");
	if( last_minus == std::string::npos )
	{
		//no minus in the bare file name - simply insert the first version mark and done.
		bare.append( "-v1" );
	}
	else
	{
		//is this mimnus mine?
		if(( bare[ last_minus+1 ] == 'v' )&&( last_minus < bare.size()-2 ))
		{
			//read the string following the "-v" as a number
			std::string numstr = bare.substr( last_minus + 2, std::string::npos );
			char *verend = NULL;
			unsigned long ver_num = strtoul( numstr.c_str(), &verend, 10 );
			if( ver_num == 0 )
			{
				//the "-v" at the end wasn't mine
				bare.append( "-v1" );
			}
			else
			{
				ver_num++;
				bare.erase( last_minus );
				StrUtils::Printf( bare, "-v%i", (int)ver_num );
			}
		} else {
			bare.append( "-v1" );
		}
	}
	FileUtils::compose_path( save_name, dirs, bare, ext );
	LOG_GEN_INFO( "Generated save name %s\n", save_name.c_str() );
}

//========= merging ============

int SubtitleDoc::merge_multiple( SubtitleDoc &dst, std::vector<SubtitleDoc*> docs )
{
	std::vector<Subtitles*> list;
	int numdocs = docs.size();
	for( int i = 0; i < numdocs; i++ ) {
		list.push_back( docs[i]->get_subtitles() );
	}

	MultiFormat mf;
	Subtitles *newsubs = new Subtitles();
	int err = mf.MergeSubtitles( *newsubs, list );
	if( err ) {
		delete newsubs;
		return err;
	}
	ErrDescsT errs;
	err = mf.MorphSubs( *newsubs, errs, false, true );
	if( err ) {
		delete newsubs;
		return err;
	}
	dst.set_subtitles( newsubs );
//	dst->set_type( SubtitleType::MULTI_SUBTITLES );
	return Errr::OK;
};
