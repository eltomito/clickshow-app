/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _SubtitleDoc_h
#define _SubtitleDoc_h

#include "subtitles.h"
#include "shared/src/err.h"
#include "../text_encoder.h"
#include "../subtitle_formats/subtitle_formats.h"

#include <string>
#include <climits>

typedef void sub_to_text_fn_type( Subtitle *sub, void *data, std::string *dst );

// -- uint8, uint16 and uint32 definitions --

#if (UCHAR_MAX == 255)
	typedef unsigned char uint8;
#else
#error Unsigned char is something else than 8 bits!!
#endif //uint8

#if (USHRT_MAX == 65535)
	typedef unsigned short uint16;
#else
#error Unsigned short is something else than 16 bits!!
#endif //uint16

#if (UINT_MAX == 4294967295 )
	typedef unsigned int uint32;
#else
#error Unsigned int is something else than 32 bits!!
#endif //uint32

class SubtitleDoc
{
public:
	static const int		HASH_LENGTH = 4;
	static const char *MARK_STRING;

private:
	Subtitles	*subtitles;

	TextEncodingInfo encinfo;

//	bool			this_is_utf8;
//	bool			newline_is_unix;
//	std::string	newline;

	uint8			last_saved_hash2[HASH_LENGTH];
	//long			last_saved_hash;

public:
	static const int		max_sub_len = 1000;

	std::string	file_name;
	std::string save_name;

	SubtitleDoc();
	~SubtitleDoc();
	
	void set_filename( const char *fname );
	void set_savename( const char *fname );

	const TextEncodingInfo &get_encinfo() const { return encinfo; };
	TextEncodingInfo &mutget_encinfo() { return encinfo; };
	void set_encinfo( const TextEncodingInfo &tei ) { encinfo = tei; };

//	bool	is_utf8() { return encinfo.IsUtf8(); };
//	void	is_utf8( bool utf8 ) { this_is_utf8 = utf8; };

	void generate_savename( const char *origname, const char *default_name = "unnamed" );

	/*
	 * returns: the number of subtitles loaded or a negative error code.
	 */
	int load_from_file( const char *fname, const SubtitleFormats::ParserCfg &cfg );

	/*
	 * returns: number of subtitles saved or a negative error code
    */
	int save_to_file( const char *fname, bool fix_ext = true );
	void fix_fname_ext( std::string &fname );

	int load_from_string( const char *utf8subs, const std::string &fileName, const SubtitleFormats::ParserCfg &cfg );
	int save_to_string( std::string &dst );

	static int merge_multiple( SubtitleDoc &dst, std::vector<SubtitleDoc*> docs );

	Subtitles *get_subtitles()	{ return subtitles; };
	void set_subtitles( Subtitles *subs) { subtitles = subs; };

	static Subtitles *utf8_to_subs2( const char * utf8str, Subtitles *subs, const SubtitleFormats::ParserCfg &cfg );

	/*
	 * converts subtitles subs to utf8 and appends the result to utf8subs
	 *
	 * returns: number of subtitles written or -1 if subs == NULL.
	 */
	int subs_to_utf8( Subtitles *subs, std::string &utf8subs );

	/*
	 * converts subtitle sub to utf8 and appends it to str.
	 * Returns the total number of characters appended.
	 */	
	size_t sub_to_utf8( Subtitle &sub, std::string &str );

	//long hash_subtitles();

	/*
	 * hash - points to SubtitleDoc::HASH_LEN bytes of memory to hold the hash value
	 */
	void hash_subtitles2( uint8 *hash );

	void clear_modified();
	bool is_modified();

private:

	static bool is_mark_string( const char *utf8str );
};

#endif //_SubtitleDoc_h
