/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <algorithm>

#include <FL/fl_utf8.h>

#include "subtitles.h"
#include "../clicky_debug.h"
//#include "shared/src/str_utils.h"
#include "../utf8_tools.h"

Subtitles::Subtitles() : flags(0)
{
	clear_iterator_cache();
}

Subtitles::Subtitles( const Subtitles &other )
{
	flags = other.flags;
	subtitle_list = other.subtitle_list;
	clear_iterator_cache();
}

/*
*/
void Subtitles::clear()
{
	subtitle_list.clear();
};

/*
 * appends a new subtitle
 */
void Subtitles::append( char *text, int text_len )
{
	Subtitle sub = *new Subtitle( text, text_len );
	subtitle_list.push_back( sub );
};

/*
 */
void Subtitles::append( const Subtitle &sub )
{
	subtitle_list.push_back( sub );
};

/*
 */
void Subtitles::append_with_mark( bool marked, char *txt, int text_len )
{
	Subtitle sub = *new Subtitle( txt, text_len );
	sub.mark( marked );
	subtitle_list.push_back( sub );
};

/*
 */
Subtitles::iter Subtitles::get_sub_iter( int n )
{
	iter it;

	if( ( n >= (int)subtitle_list.size() ) || ( n < 0 ) )
		return subtitle_list.end();		//n is out of range

	int cache_d = abs( n - last_n );
	int end_d = subtitle_list.size() - n;

	if( cache_d < std::min( end_d, n ) )
	{
		//the cached iterator is the closest
		it = move_it( last_it, n - last_n );
	}
	else
	{
		if( n <= end_d )
		{
			//iterate from the beginning
			it = move_it( subtitle_list.begin(), n );
		}
		else
		{
			//iterate from the end
			it = move_it( subtitle_list.end(), (-1)*end_d );
		};
	};
	add_to_iterator_cache( it, n );
	return it;
}

bool Subtitles::insert( int pos, Subtitle &sub )
{
	if( pos == -1 ) pos = subtitle_list.size();
	if(( pos < 0 )||( pos > subtitle_list.size() )) return false;			//invalid position

	if( pos == subtitle_list.size() )
	{
		append( sub );
	}
	else
	{
		iter it = get_sub_iter( pos );
		subtitle_list.insert( it, sub );
		clear_iterator_cache();
	};
	return true;
}

bool Subtitles::insert( int pos, Subtitles *subs, int from, int to )
{
	if( pos == -1 ) pos = subtitle_list.size();
	if(( pos < 0 )||( pos > subtitle_list.size() )) return false;			//invalid position

	if( to == -1 ) to = subs->size();
	if(( from < 0 )||( from > subs->size() )) return false;
	if(( to < 0 )||( to > subs->size() )) return false;

	iter posit = get_sub_iter( pos );
	iter fromit = subs->get_sub_iter( from );
	iter toit = subs->get_sub_iter( to );

	subtitle_list.insert( posit, fromit, toit );
	return true;
}

int Subtitles::shift_timing( long add_milli, int pos, int to )
{
	if( pos == -1 ) pos = subtitle_list.size();
	if(( pos < 0 )||( pos > subtitle_list.size() )) return 0;			//invalid position

	if( to == -1 ) to = size();
	if(( to < 0 )||( to > size() )) return 0;

	int cnt = 0;
	iter it = get_sub_iter( pos );
	iter endit = get_sub_iter( to );
	SubTiming *st = NULL;
	while( it != endit ) {
		st = &(it->mutget_timing());
		if( st->IsSet() ) {
			st->AddToStart( add_milli );
			++cnt;
		}
		++it;
	}
	return cnt;
}

bool Subtitles::best_fit_timing( long milliStart, long milliEnd, int from, int to )
{
	if( to == -1 ) to = subtitle_list.size();
	if(( from < 0 )||( from > subtitle_list.size() )) return false;
	if(( to < 0 )||( to > subtitle_list.size() )) return false;

	//count all characters
	long chrcnt = 0;
	iter it = get_sub_iter( from );
	iter toit = get_sub_iter( to );
	while( it != toit ) {
		chrcnt += it->get_disp_text().size();
		++it;
	}

	if( !chrcnt ) chrcnt = to - from;

	long totaldur = milliEnd - milliStart;
	long start = milliStart;
	SubTiming st;

	it = get_sub_iter( from );
	while( it != toit ) {
		st.SetStart( start );
		st.SetDur( totaldur * it->get_disp_text().size() / chrcnt ); 
		it->set_timing( st );
		start = st.GetEnd();
		++it;
	}
	return true;
}

/*
 */
void	Subtitles::erase( int n )
{
	iter	it;

	if(( n < 0 )||( n >= size() ))
		return;		//out of range

	it = get_sub_iter( n );
	subtitle_list.erase( it );
	clear_iterator_cache();
};

/*
 */
void	Subtitles::erase( int first, int last )
{
	iter	first_it;
	iter	last_it;

	if(( first < 0 )||( first >= size() ))
		return;		//range start is out of range

	if(( last < 0 )||( last >= size() ))
		return;		//range end is out of range

	if( last < first )
		return;		//the range is reversed -> weird, probably an error.

	first_it = get_sub_iter( first );
	last_it = get_sub_iter( last );
	
	subtitle_list.erase( first_it, last_it );
	clear_iterator_cache();
}

/*
 * returns the number of subtitles
 */
int Subtitles::size()
{
	return subtitle_list.size();
};

/*
 *	find a subtitle by its number
 *	parameters:
 *		n - the number of subtitle to find
 * returns: a pointer to the subtitle or NULL if n is out of range.
 */
Subtitle* Subtitles::get_subtitle( int n )
{
	iter it = get_sub_iter( n );
	if( it != subtitle_list.end() )
		return &(*it);
	
	return NULL;
}

/*
 */
int Subtitles::find_marked_subtitle( int start, bool fwd)
{
	int				dir;
	int				i;
	bool				found;
	Subtitle			*sub;

	if( fwd )
		dir = 1;
	else
		dir = -1;

	found = false;
	i = start + dir;

	sub = get_subtitle( i );

	while	( ( !found )&&( sub!=NULL ) )
	{
		if( sub->is_marked() )
		{
			found = true;
			continue;
		};

		i += dir;
		sub = get_subtitle( i );
	};

	if( found )
		return i;
	
	return -1;	
}


int Subtitles::find_subtitle( const char *txt, int start, bool fwd, bool sensitive )
{
	int						dir;
	int						i;
	bool						found;
	Subtitle					*sub;
	std::string				lower_t;
	std::string	 			&t = lower_t;
	std::string				search_string( txt );

	if( fwd )
		dir = 1;
	else
		dir = -1;

	found = false;
	i = start + dir;

	sub = get_subtitle( i );

	if( !sensitive )
	{
		if( !utf8_to_lower( txt, search_string ) ) {
			//conversion failed. Oh, well...
			search_string.assign( txt );
			sensitive = true;
		}
	}
	LOG_FILE_DETAIL("searching for: %s, case sensitive: %s\n", search_string.c_str(), sensitive ? "yeah" : "nope" );

	while	( ( !found )&&( sub!=NULL ) )
	{
		t = sub->get_disp_text();
		if( !sensitive ) {
			utf8_to_lower( t, lower_t );
			t = lower_t;
		}
		if( t.find( search_string ) != std::string::npos )
		{
			found = true;
			break;
		};
		i += dir;
		sub = get_subtitle( i );
	};

	return found ? i : -1;
};

/*
 * moves the iterator forward n steps.
 * If n is negative, the iterator is moved backward.
 * No bounds checking done.
 */
Subtitles::iter Subtitles::move_it( iter it, int n )
{
	if( n < 0 )
	{
		while( n < 0 )
		{
			it--;
			n++;
		};
	}
	else
	{
		while( n > 0 )
		{
			it++;
			n--;
		};
	};
	return( it );
};

void Subtitles::clear_iterator_cache()
{
	last_n = -1;
};

void	Subtitles::add_to_iterator_cache( iter it, int n )
{
	last_it = it;
	last_n = n;
};

bool Subtitles::check_fully_timed()
{
	if( !is_timed() ) {
		set_fully_timed( false );
		return false;
	}

	Subtitles::iter it = get_sub_iter( 0 );
	Subtitles::iter endit = get_sub_iter( -1 );
	while(( it != endit )&&( it->get_timing().IsSet() )) {
		++it;
	}
	set_fully_timed( it == endit );
	return is_fully_timed();
}

int Subtitles::fix_timing_overlaps()
{
	if( !is_timed() ) { return -1; }
	if( size() < 2 ) { return 0; }
	Subtitles::iter it = get_sub_iter( 0 );
	Subtitles::iter nit = it;
	++nit;
	Subtitles::iter endit = get_sub_iter( -1 );
	int errcnt = 0;
	while( nit != endit ) {
		SubTiming &first = it->mutget_timing();
		const SubTiming &second = nit->get_timing();
		if(( first.IsSet() )&&( second.IsSet() )) {
			if(( first.GetStart() < second.GetStart() )&&( first.GetEnd() > second.GetStart() )) {
				first.SetEnd( second.GetStart() );
				++errcnt;
			}
		}
		it = nit;
		++nit;
	}
	return errcnt;
}

void Subtitles::printme()
{
	LOG_FILE_DETAIL("list size: %i\n", subtitle_list.size() );
	for( int i=0; i< (int)subtitle_list.size(); i++ )
	{
		Subtitle *sub = get_subtitle(i);
		sub->printme();
	};
};
