/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Subtitles_h
#define _Subtitles_h

#include <list>
#include <string>

#include "subtitle.h"
#include "shared/src/flag_macro.h"

class Subtitles
{
private:
	typedef std::list<Subtitle>	Subtitle_List_T;
	unsigned int flags;

public:
	typedef Subtitle_List_T::iterator	iter;

	CREATE_FLAG( timed, flags, 0 ); //set_timed(), is_timed()
	CREATE_FLAG( candy, flags, 1 ); //set_candy(), is_candy()
	CREATE_FLAG( fully_timed, flags, 2 ); //set_timed(), is_fully_timed()

	unsigned int get_flags() { return flags; };

private:

	static const int 	max_search_len = 256; 

	char		search_text[ 3*max_search_len + 1];	//it's 3x, because fl_utf_tolower says so.
	char		sub_text[ 3*max_search_len + 1];

	Subtitle_List_T	subtitle_list;

	iter	last_it;		//iterator of the last found subtitle
	int	last_n;		//number of the last found subtitle

	void	add_to_iterator_cache( iter it, int n );
	void clear_iterator_cache();

	iter	move_it( iter it, int n );

public:
	
	Subtitles();
	Subtitles( const Subtitles &other );

	void append( char *txt, int text_len = -1 );
	void append( const Subtitle &sub );
	void append_with_mark( bool marked, char *txt, int text_len = -1 );

	bool	insert( int pos, Subtitle &sub );
	bool	insert( int pos, Subtitles *subs, int from = 0, int to = -1 );

	/** Adds a value to the start and end of all subtitles in the range.
	 * @returns the number of affected subtitles.
	 */
	int shift_timing( long add_milli, int pos, int to = -1 );

	void	erase( int n );
	void	erase( int first, int last );

	bool best_fit_timing( long milliStart, long milliEnd, int from, int to = -1 );

	Subtitles::iter get_sub_iter( int n );

	void clear();

	/*
	 * returns the number of subtitles
	 */
	int size();

	/*
	 *	find a subtitle by its number
	 *	parameters:
	 *		n - the number of subtitle to find
	 * returns: a pointer to the subtitle or NULL if n is out of range.
	 */
	Subtitle* get_subtitle( int n );

	/*
	 * finds the number of the first subtitle containing given text
	 *	parameters:
	 *					txt - the text to search for
	 *					start - the number of subtitle to start at (this subtitle is skipped)
	 *					fwd - search forward (if true) or backward (if false) 
	 *					case - if the search is case sensitive
	 * returns:
	 *					the number of the first subtitle containing the text
	 *					or -1 if none found.
	 */
	int find_subtitle( const char *txt, int start=0, bool fwd=true, bool sensitive=false );

	/*
	 * finds the first marked subtitle
	 *	parameters:
	 *					start - the number of subtitle to start at (this subtitle is skipped)
	 *					fwd - search forward (if true) or backward (if false) 
	 *					case - if the search is case sensitive
	 * returns:
	 *					the number of the first subtitle containing the text
	 *					or -1 if none found.
	 */
	int find_marked_subtitle( int start, bool fwd=true );

	/*
	 * Checks if every single subtitle has valid timing
	 * and sets the is_fully_timed() flag accordingly.
	 * If these subtitles are not timed (i.e. is_timed() == false),
	 * the result is always false.
	 */
	bool check_fully_timed();

	/*
	 * Make sure no subsequent subtitles overlap.
	 * Untimed subtitles and subtitles preceding them are ignored
	 * as well as subtitles that are followed by a subtitle
	 * that starts before they start, e.g., subtitles aren't sorted.
	 * The result is the number of overlaps fixed
	 * or -1 if these subtitles are untimed.
	 */
	int fix_timing_overlaps();

	void printme();
};

#endif //_Subtitles_h