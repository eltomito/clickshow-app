/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "text_encoder.h"
#include <cstring>

TextEncodingInfo &TextEncodingInfo::operator=(const TextEncodingInfo &tei)
{
	is_utf8 = tei.is_utf8;
	can_utf8 = tei.can_utf8;
	unix_eol = tei.unix_eol;
	eol = tei.eol;
	return *this;
}

TextEncodingInfo &TextEncoder::GetInfo( const char *str, size_t len, TextEncodingInfo &info )
{
	info.is_utf8 = IsUTF8( str, len );
	info.can_utf8 = info.is_utf8 || !fl_utf8locale(); 
	info.unix_eol = IsNewlineUnix( str, len, info.eol );	
	return info;
};

bool TextEncoder::IsNewlineUnix( const char *str, size_t len, std::string &eol )
{
	char *nlpos = NULL;
	if( len == 0 ) {
		nlpos = (char *)strchr( str, '\r' );
	} else {
		nlpos = (char *)memchr( (const void *)str, '\r', len );
	}
	if( !nlpos ) {
		eol = "";
		return true;
	}
	if((nlpos > str )&&( nlpos[-1] == '\n' )) {
		eol = "\n\r";
		return false;
	}
	if( nlpos[1] == '\n' ) {
		eol = "\r\n";
		return false;
	}
	eol = "\r"; //very weird but whatever
	return false;
};

char *TextEncoder::ToNormalEasy( const char *src, size_t len, TextEncodingInfo &info, int &err )
{
	err = Errr::OK;
	GetInfo( src, len, info );
	char *dst = ToNormal( src, len, info, err );
	if( !dst && ( err == Errr::OK ) ) {
		dst = (char *)malloc( len + 1 );
		if( !dst ) {
			err = Errr::ALLOC;
			return NULL;
		}
		dst = strncpy( dst, src, len );
		dst[ len ] = 0;
	}
	return dst;
};

char *TextEncoder::ToNormal( const char *src, size_t len, const TextEncodingInfo &info, int &err )
{
	len = (len > 0) ? len : strlen( src );
	
	if( info.IsUtf8() && info.IsNewlineUnix() ) {
		err = Errr::OK;
		return NULL;
	}

	char *utf8data = (char *)src;
	size_t utf8size = len;

	if( !info.IsUtf8() ) {
		if( !info.CanUtf8() ) {
			err = Errr::ENCODING_UNKNOWN;
			return NULL;
		}
		utf8data = mb_to_utf8( src, utf8size, err );
		if( !utf8data ) {
			return NULL;
		}
	}

	char *final = utf8data;
	size_t finalsize = utf8size;

	if( !info.IsNewlineUnix() ) {
		final = nl_to_unix( utf8data, finalsize, err );
	}

	len = finalsize;
	return final;
};

/*
 * Modifies src! It's used as a temp buffer.
 */
char *TextEncoder::ToOriginal( std::string &src, const TextEncodingInfo &info, size_t &len, int &err )
{
	err = Errr::OK;
	if( !info.IsNewlineUnix() ) {
		lf_to_exotic( src, info.Newline() );
	}

	if( !info.IsUtf8() ) {
		return utf8_to_mb( src, len, err );
	}

	char *res = (char *)malloc( src.size() + 1 );
	if( res == NULL ) {
		err = Errr::ALLOC;
		return NULL;
	}

	strncpy( res, src.c_str(), src.size() );
	res[ src.size() ] = 0;
	len = src.size();
	return res;
};

char *TextEncoder::utf8_to_mb( const std::string &src, size_t &len, int &err )
{
	size_t mbsize = fl_utf8to_mb( src.c_str(), src.size(), NULL, 0 );
	if( mbsize == 0 ) {
		err = Errr::OK;
		len = 0;
		return NULL;
	}

	char *mbdata = (char *)malloc( mbsize+1 );
	if( mbdata == NULL ) {
		err = Errr::ALLOC;
		return NULL;
	}

	fl_utf8to_mb( src.c_str(), src.size(), mbdata, mbsize + 1 );
	mbdata[ mbsize ] = 0;
	err = Errr::OK;
	len = mbsize;
	return mbdata;
};

void TextEncoder::lf_to_exotic( std::string &str, const std::string &nl )
{
	size_t pos = str.find('\n', 0);
	size_t nlsize = nl.size();
	while( pos != std::string::npos ) {
		str.replace( pos, 1, nl );
		pos = str.find('\n', pos + nlsize );
	}
};

char *TextEncoder::nl_to_unix( const char *src, size_t &len, int &err )
{
	char *dst = (char *)malloc( len + 1 );
	if( dst == NULL ) {
		err = Errr::ALLOC;
		return NULL;
	}
	const char *curpos = src;
	const char *newpos;
	const char *endpos = src + len;
	char *pastestart = dst;
	int pastelen;
	while(( curpos != NULL ) && ( curpos < endpos )) {
		newpos = (const char *)memchr( (void *)curpos, '\r', endpos - curpos );
		if( newpos == NULL ) {
			pastelen = endpos - curpos;
		} else {
			if(( newpos > curpos )&&( newpos[-1] == '\n' )) {	//LFCR
				pastelen = newpos - curpos - 1;
				newpos += 1;
			} else if( newpos[1] == '\n' ) {	//CRLF
				pastelen = newpos - curpos;
				newpos += 2;
			} else { //CR
				pastelen = newpos - curpos;
				newpos += 1;
			}
		}
		strncpy( pastestart, curpos, pastelen );
		pastestart += pastelen + 1;
		pastestart[-1] = '\n';
		curpos = newpos;
	};
	if( !newpos ) {
		pastestart[-1] = 0;
		len = pastestart - dst - 1;
	} else {
		pastestart[0] = 0;
		len = pastestart - dst;
	}
	err = Errr::OK;
	return dst;
};

char *TextEncoder::mb_to_utf8( const char *src, size_t &len, int &err )
{
	size_t utf8size = fl_utf8from_mb( NULL, 0, src, len );
	char *utf8data = (char *)malloc( utf8size + 1 );
	if( utf8data == NULL )
	{
		err = Errr::ALLOC;
		return NULL;
	};
	fl_utf8from_mb( utf8data, utf8size + 1, src, len );
	utf8data[ utf8size ] = 0;
	len = utf8size;
	err =Errr::OK;
	return utf8data;
};

