/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Text_Encoder_h
#define _Text_Encoder_h 1

#include <string>
#include <cstdlib>

#include "utf8_tools.h"
#include "shared/src/err.h"

class TextEncoder;

class TextEncodingInfo
{
friend class TextEncoder;

public:
	TextEncodingInfo() : is_utf8(true), can_utf8(true), unix_eol(true), eol("\n") {};
	~TextEncodingInfo() {};

	TextEncodingInfo &operator=(const TextEncodingInfo &tei);

	bool IsUtf8() const { return is_utf8; };
	bool CanUtf8() const { return can_utf8; };
	bool IsNewlineUnix() const { return unix_eol; };
	const std::string &Newline() const { return eol; };

protected:
	bool is_utf8;
	bool can_utf8;
	bool unix_eol;
	std::string eol;
};

class TextEncoder
{
public:
	/* Gets information about the encoding and newlines in a string.
	 */
	static TextEncodingInfo &GetInfo( const char *str, size_t len, TextEncodingInfo &info );

	/* Converts a string to UTF-8 and Unix newlines
	 */
	static char *ToNormal( const char *src, size_t len, const TextEncodingInfo &info, int &err );
	static char *ToNormalEasy( const char *src, size_t len, TextEncodingInfo &info, int &err );

	/* Converts a string to the local multibyte encoding and original newlines
	 * Warning! src is used as a temp buffer.
	 */
	static char *ToOriginal( std::string &src, const TextEncodingInfo &info, size_t &len, int &err );

	/*
	 * @returns true if the first len bytes of str are valid utf8.
	 */
	static bool IsUTF8( const char *str, size_t len ) { return ( fl_utf8test( str, len ) > 0 ); };

   /*
    * @returns true if src uses unix newline. Otherwise, the result is false
    *          and the newline sequence is stored in eol.
    */
	static bool IsNewlineUnix( const char *str, size_t len, std::string &eol );

private:

	/*
	 * @returns a pointer to a new utf8 string or NULL if the conversion cannot be done.
	 */
	static char *mb_to_utf8( const char *src, size_t &len, int &err );

	/* Converts a string to the local multibyte encoding. Newlines are left untouched.
	 */
	static char *utf8_to_mb( const std::string &src, size_t &len, int &err );

   /*
    * Converts CRLF, CR and LFCR sequences to LF. 
    */
	static char *nl_to_unix( const char *src, size_t &len, int &err );

   /*
    * Converts LF to a given newline string in place
    */
	static void lf_to_exotic( std::string &str, const std::string &nl );
};

#endif //_Text_Encoder_h
