/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "title_panel.h"

#include "clicky_debug.h"

#include <FL/fl_draw.H>

// =================== class TitleButton =========================

TitleButton::TitleButton( Heart *hrt, int x, int y, int w, int h, const char *label )
	: Fl_Button( x, y, w, h, label )
{
	heart = hrt;
	last_right_x = -1;
	last_left_x = -1;
	markup_is_explicit = false;
	font_faces.setFont(0);
	last_font = 0;
};

TitleButton::~TitleButton()
{
};

void TitleButton::copy_markup( const RainbowMarkup *mkp )
{
	if( mkp == NULL ) {
		stored_markup.clear();
		markup_is_explicit = false;
	} else {
		stored_markup = *mkp;
		markup_is_explicit = true;
	}
}

void TitleButton::draw()
{
	int W, H, top_y, left_x, right_x;

	if( labelfont() != last_font ) {
		font_faces.setFont( labelfont() );
		last_font = labelfont();
	}

	//clear the background
	fl_rectf( x(), y(), w(), h(), FL_BLACK ); //0x33333300 for debugging

#ifdef DEBUG
	if( label() ) {
		LOG_RENDER_DETAIL2( "label = \"%s\".\n", label() );
	} else {
		LOG_RENDER_DETAIL2( "No label.\n" );
	}
#endif //DEBUG

	W = 0;
	H = 0;

	if( label() )
	{
		fl_font( labelfont(), labelsize() );
		fl_color( labelcolor() );

		if(( stored_markup.size() == 0 )||( !markup_is_explicit )) {
			stored_markup.clear();
			RainbowStyle tmpstyle( 0, labelcolor() );
			stored_markup.push_back( tmpstyle );
		}
	
		RainbowStyle initStyle( 0, labelcolor(), 0, RainbowStyle::M_ALL );
		rainbow_measure( (char *)label(), stored_markup, W, H, font_faces.getFaces(), initStyle );
		LOG_RENDER_DETAIL2( "label W = %i, H = %i.\n", W, H );
		LOG_RENDER_DETAIL2( "TITLE BUTTON: x = %i, y = %i, w = %i, h = %i, align: %s.\n", x(), y(), w(), h(), (align()&FL_ALIGN_TOP) ? "TOP":"BOTTOM" );
		LOG_CANDY_DETAIL( "title:\n%s\nmarkup:\n%s\n", (char *)label(), RainbowMarkupUtils::toString( stored_markup ).c_str() );
		//LOG_RENDER_DETAIL2("win x,y = %i,%i\n", x(), y() );
		initStyle.set( 0, labelcolor(), 0, RainbowStyle::M_ALL );
		rainbow_draw( (char *)label(), stored_markup, x(), y(), w(), h(), align(), &heart->get_prefs()->candy_colors[0], labelcolor(), font_faces.getFaces(), initStyle );
	};
};

	TitlePanel::TitlePanel(Heart *hrt, int x, int y, int w, int h, TitlePanelCfg &_cfg, bool ontop )
		: ControlPanel ( hrt, x, y, w, h, (const char *)"TITLE" ), cfg(_cfg)
	{
		heart = hrt;;

		button_title = new TitleButton( heart, 0, 0, 0, 0, "" );
//		button_title->align( FL_ALIGN_CENTER | FL_ALIGN_INSIDE | (ontop ? FL_ALIGN_TOP : FL_ALIGN_BOTTOM) );
		end();

		button_title->clear_visible_focus();
		set_position( ontop );

		//-- style --
		heart->get_style_engine()->assign_style( cfg.title_button_style, button_title );
		heart->get_style_engine()->assign_style( StylePrefs::NORMAL_PANEL, this );

		//-- Fl_Widget flags --
		set_flag( CLIP_CHILDREN );

		refresh_layout();
		refresh_style();
	};

	TitlePanel::~TitlePanel() {
		Fl_Widget* all_widgets[] = { button_title, this, NULL };
		heart->get_style_engine()->clear_style_from_widgets( all_widgets );
	};

	void	TitlePanel::set_text( const char *text, const RainbowMarkup *markup )
	{
		LOG_PLAYER_DETAIL2("Setting subtitle panel text to \"%s\".\n", text ? text : "(NULL)" );
		if( text == NULL ) {
			button_title->label( NULL );
			button_title->copy_markup();
		} else {
			button_title->copy_label( text );
			button_title->copy_markup( markup );
		}
		Fl::awake();
	};

	void TitlePanel::set_position( bool ontop )
	{
		on_top = ontop;
		if( ontop )
			button_title->align( FL_ALIGN_INSIDE|FL_ALIGN_TOP|FL_ALIGN_CENTER );
		else
			button_title->align( FL_ALIGN_INSIDE|FL_ALIGN_BOTTOM|FL_ALIGN_CENTER );
	};

	void TitlePanel::refresh_layout()
	{
		int border = *cfg.title_border_size;
		border = std::min( h() - min_visible_height, std::max( border, 0 ) );

		int hh = h() - border;
		int yy = y() + ( on_top ? border : 0 );

		LOG_RENDER_DETAIL2("TITLE button dimensions: %i, %i, %i, %i (%s)\n", x(), yy, w(), hh, on_top ? "top" : "bottom" );
		button_title->resize( x(), yy, w(), hh );
	};

/*
 * handle clicks on subtitles
 */
int TitlePanel::handle( int event )
{
  //LOG_RENDER_DETAIL2("traveling through TitleButton::handle!!\n");

	if(( event == FL_ENTER )||( event == FL_LEAVE ))
	{
	  //LOG_RENDER_DETAIL2("received FL_ENTER or FL_LEAVE!!\n");
		return 1;
	};

	if( event == FL_PUSH )
	{
		switch( Fl::event_button() )
		{
			case FL_LEFT_MOUSE :
		   	//LOG_RENDER_DETAIL2("Left-clicked me!\n" );
		   	heart->clicked_mark();
				return 1;
				break;
			case FL_RIGHT_MOUSE :
		   	//LOG_RENDER_DETAIL2("Right-clicked me!\n" );
		   	heart->clicked_edit_shown();
				return 1;
				break;
		};
	};
	return 0;
};
