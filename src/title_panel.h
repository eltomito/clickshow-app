/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Title_Panel_h
#define _Title_Panel_h

//#include <map>
//#include <list>
//#include <climits>
#include <string>

#include <FL/Fl.H>
#include <FL/Fl_Button.H>
#include "control_panel.h"
#include "style_engine.h"
#include "style_prefs.h"
#include "rainbow.h"
#include "font_faces.h"

class	Heart;

class TitleButton : public Fl_Button
{
private:
	Heart *heart;
	
	int	last_left_x;
	int	last_right_x;
	
	RainbowMarkup stored_markup;
	bool	markup_is_explicit;

	FontFaces	font_faces;
	Fl_Font		last_font;

public:
	TitleButton( Heart *hrt, int x, int y, int w, int h, const char *label );
	~TitleButton();

	void copy_markup( const RainbowMarkup *mkp = NULL );
	void draw();
};

class TitlePanelCfg
{
public:
	int	title_button_style;	//StylePrefs::TITLE_DISPLAY
	int	*title_border_size;

	TitlePanelCfg( int _title_style, int *_border_size )
		: title_button_style(_title_style), title_border_size(_border_size) {};
};

/*
 * the title panel class
 */
class TitlePanel : public ControlPanel
{
	TitleButton		*button_title;
	TitlePanelCfg	cfg;

public:

	bool	on_top;

	TitlePanel(Heart *hrt, int x, int y, int w, int h, TitlePanelCfg &_cfg, bool ontop=true );
	~TitlePanel();
	void set_text( const char *text = NULL, const RainbowMarkup *markup = NULL );
	void set_position( bool ontop );
	void refresh_layout();
	int handle( int event );

	static const int min_visible_height = 10;

};

#endif //_Title_Panel_h
