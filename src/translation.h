#ifndef _Translation_h_
#define _Translation_h_


#define lang_table { "English", "Česky", NULL };

#define trans_table { "Left Mouse Button - Mark subtitle\nRight Mouse Button - Edit subtitle\nCtrl+Left Mouse Button - Display subtitle", "Levé tlačíto myši - Označit titulek\nPravé tlačíto myši - Upravit titulek\nCtrl+Levé tlačítko myši - Hned titulek zobrazit",\
"On Top", "Nahoře",\
"Fullscreen", "Celá obrazovka",\
"Received subtitles are delayed by this number of seconds.", "Zpozdit přijímané titulky o tolik sekund.",\
"Connect", "Připojit",\
"Delay subtitles:", "Zpozdit titulky:",\
"Server:", "Server:",\
"Login:", "Login:",\
"Password:", "Heslo:",\
"Venue:", "Místo:",\
"Panic!", "Panika!",\
"Find current subtitle from other clickers if there are any.", "Skočte na aktuální titulek podle ostatních klikačů, pokud nějací jsou.",\
"Address of the server to connect to.", "Adresa serveru, na který se chcete připojit.",\
"Short description of where you are connecting from.", "Krátký popis místa, odkud se připojujete.",\
"Connect to a server.", "Připojte se k serveru.",\
"Cancel", "Zrušit",\
"Give up trying to connect.", "Vzdát pokusy o spojení.",\
"Connecting thru WWW...", "Připojuji přes WWW...",\
"Connecting...", "Připojování...",\
"Disconnect", "Odpojit",\
"Disconnect from the server.", "Odpojit od serveru.",\
"Connected", "Připojeno",\
"Connected thru WWW", "Připojeno přes WWW",\
"Don't wait for server", "Nečekat na server",\
"Don't wait for the server and disconnect now.", "Nečekat na odpověď serveru a odpojit se hned.",\
"Disconnecting...", "Odpojování...",\
"Projector", "Projektor",\
"Open a separate subtitle window.", "Otevřít oddělené okno s titulkem.",\
"Make the separate subtitle window fullscreen.", "Roztáhnout oddělené okno s titulkem na celou obrazovku.",\
"Put the title at the top\nof the separate subtitle window.", "Dát titulek v okně projektoru nahoru.",\
"\
The projector will make the controls invisible!\n\
Do you want to make it fullscreen anyway?\n\
(If you do, you can close it by pressing ESC.)", "Projektor vám zakryje ovládání!\nChcete ho přesto roztáhnout\npřes celou obrazovku?\n(Kdyžtak ho můžete zavřít klávesou ESC.)",\
"#0 #c0xff000000 Make it fullscreen!|#1 #c0x0000ff00 Do nothing", "#0 #c0xff000000 Na celou obrazovku!|#1 #c0x0000ff00 Radši ne",\
"Attention!", "Pozor!",\
"CANCEL|OK", "ZRUŠIT|OK",\
"Reset", "Výchozí",\
"Load", "Nahrát",\
"Save", "Uložit",\
"Off", "Vyp.",\
"On", "Zap.",\
"Auto", "Auto",\
"#@@*+-", "@@*+-",\
"Play or pause timed subtitles.", "Spustit nebo zastavit časované titulky.",\
"Skip 0.1 second forward.", "Skočit 0.1 sekundy dopředu.",\
"Rewind 0.1 second.", "Skočit 0.1 sekundy dozadu.",\
"Skip 1 second forward.", "Skočit 1 sekundu dopředu.",\
"Rewind 1 second.", "Skočit 1 sekundu dozadu.",\
"Skip 5 seconds forward.", "Skočit 5 sekund dopředu.",\
"Rewind 5 seconds.", "Skočit 5 sekund dozadu.",\
"Size: ", "Velikost: ",\
"Border: ", "Odsazení: ",\
"Test", "Test",\
"Color", "Barva",\
"Font", "Písmo",\
"Ok", "OK",\
"Play", "Spustit",\
"Speed", "Rychlost",\
"Browse subtitles up without displaying any.", "Projíždět titulky nahoru,\nale žádné neukazovat.",\
"Browse subtitles down without displaying any.", "Projíždět titulky dolů,\nale žádné neukazovat.",\
"Show or hide manual controls.", "Ukázat nebo skrýt ruční ovládání.",\
"Show/hide speed controls.", "Ukázat/skrýt ovládání rychlosti.",\
"Slow down or speed up the subtitles.\n1.0 is the normal speed.", "Zpomalit nebo zrychlit titulky.\nNormální rychlost je 1.0.",\
"Set subtitle speed to 1.", "Nastavit rychlost titulků na 1.",\
"Pause", "Pauza",\
"There are unsaved changes in the subtitles!", "V titulcích jsou neuložené změny!",\
"#0 #c0xff000000 Quit and lose changes|#2 #c0x0000ff00 Save subtitles|#1 #c0x0000ff00 Do nothing", "#0 #c0xff000000 Ukončit a změny zahodit|#2 #c0x0000ff00 Uložit titulky|#1 #c0x0000ff00 Zpátky",\
"Do you really want to quit?", "Vážně chcete skončit?",\
"#0 #c0xff000000 Quit|#1 #c0x0000ff00 Don't Quit", "#0 #c0xff000000 Konec|#1 #c0x0000ff00 Ne, zpátky",\
"Oops!", "Jejda!",\
"No files could be opened.", "Žádný soubor nešel otevřít.",\
"Uh huh.", "Hmm.",\
"These files could not be loaded:", "Tyto soubory nešly otevřít:",\
"Problem!", "Problém!",\
"#0 #c0xff000000 Cancel|#1 #c0x0000ff00 Continue anyway", "#0 #c0xff000000 Zrušit|#1 #c0x0000ff00 Pokračovat",\
"Failed to merge subtitles!", "Spojování titulků se nepovedlo!",\
"#0 #c0xff000000 Lose changes|#1 #c0x0000ff00 Save subtitles", "#0 #c0xff000000 Změny zahodit|#1 #c0x0000ff00 Uložit titulky",\
"Clickshow Color Config\t*.ccc\nAll files\t*", "Paleta barev\t*.ccc\nAll files\t*",\
"Keyboard Controls", "Klávesové zkratky",\
"@->|Show next subtitle|Space|Show next subtitle or start playing timed subtitles| 0 ; C ;Backspace;@<-|Hide subtitle|@2<-|Browse subtitles up|@2->|Browse subtitles down|\
Enter; * |Mark/Unmark displayed subtitle|Left Mouse Button|Mark/Unmark subtitle|Right Mouse Button|Edit subtitle\
|Ctrl+Left Mouse Button|Display subtitle immediately", "@->|Ukázat další titulek|Mezera|Ukázat další titulek nebo spustit časované titulky| 0 ; C ;Backspace;@<-|Skrýt titulek|@2<-|Posun titulků nahoru|@2->|Posun titulků dolů|\
Enter; * |Označit / Odznačit zobrazený titulek|Levé tlačítko myši|Označit / Odznačit titulek|Pravé tlačítko myši|Upravit titulek\
|Ctrl+Levé tlačítko myši|Hned titulek zobrazit",\
"About ClickShow", "Info o ClickShow",\
"Find @2->", "Najít @2->",\
"@2<-", "@2<-",\
"Show", "Zobrazit",\
"Go to #", "Jít na",\
"Search for text in the subtitles.", "Hledat text v titulkách.",\
"Backward search for text in the subtitles.", "Hledat text v titulkách pozpátku.",\
"Press this button and then \"Find\"\nto look for marked subtitles.\nPress this button twice to clear the text box.", "Stisknutím toto tlačítka a pak \"Najít\"\nmůžete hledat označené titulky.\nStisknutím tohoto tlačítka dvakrát\nsmažete textové pole.",\
"Shows the text in the box above as a subtitle.", "Zobrazí text jako titulek.",\
"Shows a sample text as a subtitle.", "Zobrazí zkušební test.",\
"Go to subtitle of the number typed in the text box.", "Skočit na titulek podle čísla.\nČíslo napřed napište do textového rámečku.",\
"Failed to create web tunnel :(\n", "Nepodařil se web tunel :(",\
"Web tunnel failed to start :(\n", "Web tunel nešel zpustit :(",\
"Failed to create network node :(\n", "Nepodařilo se vytvořit síťový uzel :(\n",\
"*  CLICKSHOW %i.%i  *\n\"Click for worlwide understanding!!\"", "*  CLICKSHOW %i.%i  *\nKlikáním k porozumění a míru ve světě.",\
"*** ClickShow %i.%i ***\nDEMO - expires at the end of 2013!!", "*** ClickShow %i.%i ***\nDEMO - funguje jen do konce roku 2013!!",\
"show usage information", "ukázat nápovědu",\
"input file", "vstupní soubor",\
"output file", "výstupní soubor",\
"Convert the color marks (*@+-~^`) in the input file to tags used in .srt.", "Zkonvertovat v titulkách barvicí značky (*@+-~^`) na tagy, co se používají v .srt.",\
"Start in non-fullscreen mode.", "Žádný vstupní soubor.",\
"network server address", "adresa serveru",\
"login user name", "přihlašovací jméno uživatele",\
"network venue name of this instance", "název místa, ze kterého se přihlašujeme",\
"network password", "heslo",\
"protocol version (1 or 2)", "verze síťového protokolu (1 nebo 2)",\
"No input file.", "Žádný vstupní soubor.",\
"There are unsaved recorded subtitles!", "Máte neuložené zaznamenané titulky!",\
"#0 #c0xff000000 Discard|#1 #c0x00ff0000 Save|#2 #c0x0000ff00 Cancel", "#0 #c0xff000000 Zahodit|#1 #c0x00ff0000 Uložit|#2 #c0x0000ff00 Zpátky",\
"Rec", "Rec",\
"Clear", "Smazat",\
"Start or pause recording timed subtitles.", "Spustit nebo zapauzovat nahrávání časovaných titulků.",\
"Save recorded timed subtitles.", "Uložit zaznamenané časované titulky.",\
"Forget what's been recorded.", "Smazat celý záznam.",\
"Number of recorded subtitles.", "Počet zaznamenaných titulků.",\
"Reset the timecode to 00:00:00,000.", "Nastavit čas záznamníků na 00:00:00,000.",\
"Recorded:", "Nahráno:",\
"Open Subtitles", "Otevřít titulky",\
"Save Subtitles", "Uložit titulky",\
"Quit", "Konec",\
"Text Settings", "Vzhled textu",\
"Options", "Možnosti",\
"Net", "Síť",\
"Recorder", "Záznamník",\
"Spaces", "Mezery",\
"Load new subtitles.", "Nahrát nové titulky.",\
"Save the current subtitles\nincluding all markings and edits.", "Uložit titulky\nvčetně všech označení a úprav.",\
"End this program.", "Ukončit program.",\
"Set the size, color, font\nand position of the displayed subtitles.", "Nastavit velikost, barvu, písmo\na umístění zobrazených titulků.",\
"Set the language, fullscreen mode\nand orientation of the user interface.", "Nastavit jazyk, režim celé obrazovky\na orientaci uživatelského rozhraní.",\
"Show more / less.", "Ukázat víc / míň.",\
"Show newtwork settings.", "Ukázat nastavení sítě.",\
"Show settings for a separate\nsubtitle window useful with a projector.", "Ukázat nastavení odděleného okna\ns titulkem pro použití na projektoru.",\
"Show the subtitle-timing recorder.", "Otevřít záznamník časování.",\
"If checked, a blank screen is displayed\nafter each subtitle.", "Pokud zvolíte \"Mezery\", ukáže se po každém titulku prázdná obrazovka.",\
"Choose a name for the subtitles", "Pojmenujte soubor s titulky",\
"Choose at most %d subtitle files to load.", "Vyberte titulkové soubory k nahrání (maximálně %d).",\
"Choose subtitles to load", "Vyberte titulky, které chcete nahrát.",\
"Plain or Timed Subtitles\t*.{txt,srt}\nPlain Subtitles\t*.txt\nTimed Subtitles\t*.srt\nAll files\t*", "Obyčejné nebo časované titulky\t*.{txt,srt}\nPlain Subtitles\t*.txt\nTimed Subtitles\t*.srt\nAll files\t*",\
"<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-%i Tomáš Pártl<br>\
released under GNU GPL 3.0 or later.\
</p>\
<p>\
Clickshow is available for Linux and Windows<br>\
and uses FLTK (www.fltk.org) and BOOST (www.boost.org)\
</p>\
<p>supported by:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>", "<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-%i Tomáš Pártl<br>\
vydáno pod GNU GPL 3.0 nebo pozdější.\
</p>\
<p>\
Clickshow je k mání pro Linux a Windows<br>\
a používá FLTK (www.fltk.org) a BOOST (www.boost.org)\
</p>\
<p>s podporou:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>",\
"Show\nNext", "Další\nTitulek",\
"Clear\nScreen", "Vymazat",\
"* Mark", "Označit *",\
"Display the next subtitle or a blank screen\ndepending on whether \"Spaces\" are checked.", "Ukázat další titulek nebo prázdnou obrazovku\npodle toho, zda jsou nastaveny \"Mezery\".",\
"Clear the subtitle view.", "Smazat pole titulků.",\
"Mark the currently displayed subtitle\nwith an asterisk (*).", "Označit právě zobrazený titulek hvězdičkou (*).",\
"Controls", "Ovládání",\
"About", "O programu",\
"  *@@+-", "  *@@+-",\
"Show keyboard shortcuts.", "Ukázat klávesové zkratky.",\
"Show information about this program.", "Ukázat informace o tomto programu.",\
"Show settings for colored subtitles.", "Ukázat nastavení barevných titulků.",\
"Use", "Použít",\
"Edit the subtitle text here\nand press \"Use\" when done\nor \"Cancel\" to discard your changes.\nAn empty line splits the subtitle in two.\n", "Zde můžete upravit text titulku.\nPokud chcete změny přijmout, klikněte na \"Použít\".\nKdyž kliknete na \"Zrušit\", žádné změny se neprovedou.",\
"Apply the edited text to the subtitle.", "Použít změněný text v titulku.",\
"Discard all changes and stop editing.", "Zahodit změny a ukončit editování.",\
"<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-2021 Tomáš Pártl<br>\
released under GNU GPL 3.0 or later.\
</p>\
<p>\
Clickshow is available for Linux and Windows<br>\
and uses FLTK (www.fltk.org) and BOOST (www.boost.org)\
</p>\
<p>supported by:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>", "<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-2021 Tomáš Pártl,<br>\
vydáno pod GNU GPL 3.0 nebo pozdější.\
</p>\
<p>\
Clickshow je k mání pro Linux a Windows<br>\
a používá FLTK (www.fltk.org) a BOOST (www.boost.org).\
</p>\
<p>S podporou:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>",\
"<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-2020 Tomáš Pártl<br>\
released under GNU GPL 3.0 or later.\
</p>\
<p>\
Clickshow is available for Linux and Windows<br>\
and uses FLTK (www.fltk.org) and BOOST (www.boost.org)\
</p>\
<p>supported by:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>", "<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-2020 Tomáš Pártl<br>\
vydáno pod licencí GNU GPL 3.0 nebo pozdější.\
</p>\
<p>\
Clickshow je k dostání pro Linux a Windows<br>\
a používá FLTK (www.fltk.org) a BOOST (www.boost.org)\
</p>\
<p>podpořili:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>",\
"<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-2019 Tomáš Pártl<br>\
released under GNU GPL 3.0 or later.\
</p>\
<p>\
Clickshow is available for Linux and Windows<br>\
and uses FLTK (www.fltk.org) and BOOST (www.boost.org)\
</p>\
<p>supported by:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>", "<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>clickshow.xf.cz\
</p>\
<p>Copyright 2013-2019 Tomáš Pártl<br>\
vydáno pod licencí GNU GPL 3.0 nebo pozdější.\
</p>\
<p>\
Clickshow je k dostání pro Linux a Windows<br>\
a používá FLTK (www.fltk.org) a BOOST (www.boost.org)\
</p>\
<p>podpořili:</p>\
<p>\
<pre>\
aerofilms.cz<br>\
theater.cz<br>\
easytalk.cz<br>\
aztranslations.cz<br>\
</pre>\
</p>\
</center><html>",\
"@->;Space|Show next subtitle| 0 ; C ;Backspace;@<-|Hide subtitle|@2<-|Browse subtitles up|@2->|Browse subtitles down|\
Enter; * |Mark/Unmark displayed subtitle|Left Mouse Button|Mark/Unmark subtitle|Right Mouse Button|Edit subtitle\
|Ctrl+Left Mouse Button|Display subtitle immediately", "@->;Mezera|Zobrazit další titulek| 0 ; C ;Backspace;@<-|Skrýt titulek|@2<-|Posun titulků nahoru|@2->|Posun titulků dolů|\
Enter; * |Označit / Odznačit zobrazený titulek|Levé tlačítko myši|Označit / Odznačit titulek|Pravé tlačítko myši|Upravit titulek\
|Ctrl+Levé tlačítko myši|Hned titulek zobrazit",\
"<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>© 2016-2018 Tomáš Pártl<br>\
released under GNU GPL 3.0 or later.\
</p>\
<p>clickshow.xf.cz\
</p>\
<p>\
Clickshow is available for Linux and Windows.\
</p>\
<p>\
This program uses FLTK (www.fltk.org) and BOOST (www.boost.org)\
</p>\
<p>supported by:</p>\
<p>\
<pre>\
aerofilms.cz    easytalk.cz    theater.cz<br>\
</pre>\
</p>\
</center><html>", "<html><center><h2>CLICKSHOW %i.%i</h2>\
<p>© 2016-2018 Tomáš Pártl<br>\
vydáno pod licencí GNU GPL 3.0 nebo pozdější.\
</p>\
<p>clickshow.xf.cz\
</p>\
<p>\
Klikouš je k mání pro Linux a Windows.\
</p>\
<p>\
Program používá FLTK (www.fltk.org) a BOOST (www.boost.org)\
</p>\
<p>podpořili:</p>\
<p>\
<pre>\
aerofilms.cz    easytalk.cz    theater.cz<br>\
</pre>\
</p>\
</center><html>",\
"Text\t*.txt\nAll files\t*", "Textové soubory\t*.txt\nVšechny soubory\t*",\
"unsaved_subtitles.txt", "neuložené_titulky.txt",\
"Number Subtitles", "Číslovat titulky",\
"Full Screen", "Celá obrazovka",\
"Make the separate subtitle window full screen.", "Ať je zvláštní okno s titulkem přes celou obrazovku.",\
"Show more.", "Ukázat víc.",\
"Show settings for a separate\ntitle window useful for a second display .", "Ukázat nastavení pro oddělené okno s titulkem\nužitečné pro druhý displej.",\
"Delay received subtitles by this number of seconds.", "Zpozdit přijímané titulky o tolik sekund.",\
"No license key entered.", "Žádný kód nebyl vložen.",\
"%Y-%m-%d", "%d. %m. %Y",\
"The license key is valid from ", "Licence je platná od ",\
" to ", " do ",\
"HOORAY!!! The license for this program is UNLIMITED!!!\n", "Clickshow běží v nelicencovaném ZKUŠEBNÍM PROVOZU.",\
"ClickShow is running in unlicensed TRIAL MODE.", "Clickshow běží v nelicencovaném ZKUŠEBNÍM PROVOZU.\n(Zadaný kód licence vypršel.)",\
"ClickShow is running in unlicensed TRIAL MODE.\n(The current license has expired)", "#0 OK",\
"License Information", "Informace o licenci",\
"License Key", "Kód licence",\
"Show license information.", "Ukázat informace o licenci.",\
"Enter a new license key:", "Zadejte kód licence:",\
"The current key is valid until the end of times.", "Licence je platná na věky věkův.",\
"\nRunning in TRIAL MODE.", "\nProgram běží ve ZKUŠEBNÍM PROVOZU",\
":-P Cannot open file!", ":-P Soubor nelze otevřít",\
"\n\nUnknown encoding.", "\n\nNeznámé kódování znaků.",\
"TRIAL MODE", "ZKUŠEBNÍ PROVOZ",\
"*** ClickShow 1.5 ***\nDEMO - expires at the end of 2013!!", "*** ClickShow 1.5 ***\nDEMO - funguje jen do konce roku 2013!!",\
"@->|Show next subtitle| 0 ; C ;Backspace|Clear Screen|@2<-|Browse subtitles up|@2->|Browse subtitles down|", "Zde můžete upravit text titulku\na změny přijmout tlačítkem \"Použít\"\nnebo zahodit tlačítkem \"Zrušit\".\nPrázdný řádek rozdělí titulek na dva.",\
 NULL };

#endif //_Translation_h_
