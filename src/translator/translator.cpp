/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "translator.h"

Translator::~Translator()
{
	//free the whole dictionary
	iter it = dictionary.begin();
	while( it != dictionary.end() )
	{
		free_entry( ( char **)it->second );
		it++;
	};
	dictionary.clear();
	
	//free language names
	if( copied_names )
	{
		free( lang_names );
		lang_names = NULL;
	};
}

void Translator::free_entry( char **entry )
{
	if( copied_strings )
	{
		for( int i=0; i < num_langs; i++ )
			free( entry[i] );
	};
	free( entry );
}

Translator::Translator( const char **langnames, const char **transtrings, bool copy )
{
	int langcnt = load_names( langnames, copy );
	load_strings( langcnt - 1, transtrings, copy );
}

/*
 * parameters:
 *		langcnt - number of translation strings per entry
 *		transtrings - {"Text1 in default language", "text1 in language a", , "text1 in language b",
 *							"Text2 in default language", "text2 in language a", , "text2 in language b",
 *							...,
 *							NULL };
 *		copy - make a copy of transtrings or reference the ones supplied
 */
Translator::Translator( int langcnt, const char **transtrings, bool copy )
{
	//init variables
	dictionary.clear();
	num_langs = 0;
	cur_lang = -1;

	load_strings( langcnt, transtrings, copy );
}

int Translator::load_names( const char **langnames, bool copy )
{
	if( langnames == NULL )
		return 0;

	const char **l = langnames;
	int langcnt = 0;
	int lang_bytes = 0;

	//count language names
	while( l[0] != NULL )
	{
		if( copy )
		{
			lang_bytes += strlen( l[0] ) + 1;
		};
		langcnt++;
		l++;
	};

	LOG_GEN_INFO("counted %i language names.\n", langcnt );

	if( langcnt == 0 )
	{
		lang_names = NULL;
		return 0;
	};

	//copy language names if requested
	if( copy )
	{

		LOG_GEN_INFO("copying language names:\n" );

		lang_names = (const char**)malloc( lang_bytes + ( langcnt + 1 )*sizeof( const char*) );
		if( lang_names == NULL )
		{
			LOG_GEN_INFO("FAILED to allocate memory for language names!\n" );
			copied_names = false;
			return 0;			//failed to alloc an array of language names
		};

		l = langnames;
		char **new_l = (char **)lang_names;
		char *new_n = (char *)&lang_names[langcnt+1];
		for( int i = 0; i < langcnt; i++ )
		{
			strcpy( new_n, l[0] );
			new_l[0] = new_n;

			LOG_GEN_INFO("\tcopied \"%s\"\n", new_l[0] );

			new_l++;
			l++;
			new_n += strlen( new_n ) + 1;
		};
		new_l[0] = NULL;

		copied_names = true;
	}
	else
	{
		lang_names = langnames;
		copied_names = false;
	};
	return langcnt;
}

void Translator::load_strings( int langcnt, const char **transtrings, bool copy )
{
	//check input sanity
	if(( transtrings == NULL )||( langcnt < 1 ))
	{
		num_langs = 0;
		return;
	}

	num_langs = langcnt + 1;

	copied_strings = copy;

	//digest knowledge
	const char *s = transtrings[0];
	const char **entry;

	while( transtrings[0] != NULL )
	{
		entry = new_dict_entry( langcnt, &transtrings[1], copy );
		if( entry == NULL )
		{
			//error - not enough translation strings supplied for a key string
			s = NULL;
			continue;
		};
		dictionary[ transtrings[0] ] = entry;
		transtrings += ( langcnt + 1 );
	};
}

const char **Translator::new_dict_entry( int langcnt, const char **src, bool copy )
{
	const char ** entry = (const char **)malloc( langcnt * sizeof(char **) );

	if( entry == NULL )
		return NULL;		//malloc failed

	//add translation strings to this entry
	for( int j=0; j < langcnt; j++ )
	{
		if( src[j] != NULL )
		{
			if( !copy )
				entry[j] = src[j];	//we can keep a pointer to the original string
			else
			{
				//make a copy of the string first
				int slen = strlen( src[j] );
				slen++;
				entry[j] = (const char *)malloc( slen * sizeof( char ) );
				if( entry[j] == NULL )
				{
					//malloc failed
					free( entry );
					return NULL;
				};
				strcpy( (char *)entry[j], src[j] );
			};
		}
		else
		{
			//a NULL string midway the translation list. Aborting.
			free( entry );
			return NULL;
		};
	};	
	return entry;	
}

/*
 * lang = 0 means no translation
 * lang = 1 is the first translated language
 */
bool Translator::language( int lang )
{
	if(( lang > num_langs )||( lang < 0 ))
		return false;

	cur_lang = lang;
	return true;
}

const char *Translator::lang_name( int lang )
{
	if(( lang > num_langs )||( lang < 0 ))
		return NULL;

	return	lang_names[ lang ];
}

	/*
	 * finds language id by language name
	 *	returns:
	 *				the id of the language or -1 if not found
	 */
int Translator::lang_id( const char *langname )
{
	int	id = -1;
	
	if( num_langs < 1 )
		return id;
	
	int i = 0;
	
	while(( lang_names[i] != NULL )&&( id == -1 ))
	{
		if( 0== strcmp( langname, lang_names[i] ) )
				id = i;
		else
			i++;
	};

	return id;
}

const char *Translator::translate( const char *str )
{
	if(( num_langs <= 0 )||( cur_lang <= 0 ))
		return str;

	if( dictionary.find( str ) == dictionary.end() )
		return str;

	return dictionary[ str ][ cur_lang - 1 ];
}

#ifdef INCLUDE_TEST_MAIN

#include <cstdio>

const char *testdict[] = {"Ok","Budiž","Cancel","Zrušit","Asshole","Zmrde"};

int main( int argc, char *argv[] )
{
//	string infile;

	if( argc < 2 )
	{
		printf("Usage: %s <string to translate> <language id>\n", argv[0] );
		//printf("      the dictionary is loaded from test.dct\n" );
		exit( 0 );
	};

	//parse the dictionary
	Translator *trans = new Translator( 1, &testdict[0], false );
	trans->language( 0 );

	printf( "%s --> %s\n", argv[1], trans->translate( argv[1] ) );

	delete trans;

}

#endif //INCLUDE_TEST_MAIN