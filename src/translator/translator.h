/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Translator_h
#define _Translator_h

//#define INCLUDE_TEST_MAIN 1

#include <map>
#include <string>
#include <cstring>
#include <cstdlib>
#include "../clicky_debug.h"

class Translator
{
private:
	std::map<const std::string, const char **> dictionary;

	typedef std::map<const std::string, const char **>::iterator iter;

	const char **lang_names;

	int num_langs;
	int cur_lang;
	bool copied_strings;
	bool copied_names;

public:
	Translator( int langcnt, const char **transtrings, bool copy = false );
	Translator( const char **langnames, const char **transtrings, bool copy = false );
	~Translator();

	//int build_dictionary( int langcnt, const char **transtrings, bool copy = false );

	bool language( int lang );
	int language() { return cur_lang; };
	
	/*
	 * finds language id by language name
	 *	returns:
	 *				the id of the language or -1 if not found
	 */
	int lang_id( const char *langname );
	
	int lang_count() { return num_langs; };

	const char *translate( const char *str );
	const char *lang_name( int lang );

private:
	int load_names( const char **langnames, bool copy = false );
	void load_strings( int langcnt, const char **transtrings, bool copy = false );

	const char **new_dict_entry( int langcnt, const char **src, bool copy = false );
	void free_entry( char **entry );
};

#endif //_Translator_h
