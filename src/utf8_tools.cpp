/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "utf8_tools.h"
#include <cassert>

unsigned int utf8_getchar( const char *str, int *byte_len )
{
	unsigned int ucs;
	int charlen = fl_utf8len( str[0] );
	int len;
	ucs = fl_utf8decode( str, str + charlen, &len );
	assert( len == charlen );
	if( byte_len != NULL ) byte_len[0] = charlen;
	return ucs;
};

const char *utf8_charstart( const char *str )
{
	int charlen;
	charlen = fl_utf8len( str[0] );
	while( charlen < 0 ) {
		str--;
		charlen = fl_utf8len( str[0] );
	}
	return str;
}

int utf8_cmpchar( const char *str, unsigned int ucs )
{
	unsigned int ucs2 = utf8_getchar( str );

	if( ucs2 > ucs ) {
		return 1;
	}
	if( ucs2 < ucs ) {
		return -1;
	}	
	return 0;
}

// TODO: utf8_offset is never used! I don't even know what it was supposed to do...
const char *utf8_find( const char *str, unsigned int ucs, size_t *utf8_offset )
{
	int charlen;
	while( ( str[0] != 0 ) && ( utf8_cmpchar( str, ucs ) != 0 ) ) {
		charlen = fl_utf8len( str[0] );
		if( charlen <= 0 ) {
			return NULL;
		}
		str += charlen;
	}
	if(( str[0] != 0 )||( ucs == 0 )) {
		return str;
	}
	return NULL;
}

int utf8_count( const char *str, unsigned int ucs )
{
	int cnt = 0;
	int clen = 0;
	while( str != NULL ) {
		str = utf8_find( str, ucs );
		if( str ) {
			++cnt;
			utf8_getchar( str, &clen );
			str += clen;
		}
	}
	return cnt;
}

int utf8_runlength( const char *str, unsigned int ucs, int *byteLen )
{
	int charlen;
	int cnt = 0;
	int bLen = 0;

	unsigned int u = utf8_getchar( str, &charlen );
	while( u == ucs ) {
		++cnt;
		bLen += charlen;
		str += charlen;
		ucs = utf8_getchar( str, &charlen );
	}

	if( byteLen ) { *byteLen = bLen; }
	return cnt;
};

/*
	These are all space characters:
	'\t'
	U+0020 SPACE
	U+00A0 NO-BREAK SPACE
	U+1680 OGHAM SPACE MARK
	U+2000 EN QUAD
	U+2001 EM QUAD
	U+2002 EN SPACE
	U+2003 EM SPACE
	U+2004 THREE-PER-EM SPACE
	U+2005 FOUR-PER-EM SPACE
	U+2006 SIX-PER-EM SPACE
	U+2007 FIGURE SPACE
	U+2008 PUNCTUATION SPACE
	U+2009 THIN SPACE
	U+200A HAIR SPACE
	U+200B ZERO WIDTH SPACE
	U+202F NARROW NO-BREAK SPACE
	U+205F MEDIUM MATHEMATICAL SPACE
	U+3000 IDEOGRAPHIC SPACE
*/
bool utf8_is_space( unsigned long uc )
{
	if(
			( uc == (unsigned long)' ' )||( uc == (unsigned long)'\t' )
			||( uc == (unsigned long)0x00a0 )
			||( uc == (unsigned long)0x1680 )
			||( (uc>=(unsigned long)0x2000)&&(uc<=(unsigned long)0x200B) )
			||( uc == (unsigned long)0x202f )
			||( uc == (unsigned long)0x205f )
			||( uc == (unsigned long)0x3000 )
		)
		return( true );

	return false;
};

bool utf8_is_all_space( const char *str, size_t len )
{
	if( !len ) { return true; }
	len = ( len >= 0 ) ? len : strlen( str );

	const char *endstr = str + len;
	int charlen;

	unsigned int u = utf8_getchar( str, &charlen );
	while(( utf8_is_space( u ) )&&( (str + charlen) < endstr )) {
		str += charlen;
		u = utf8_getchar( str, &charlen );
	}
	return ( (str+charlen) >= endstr) ? true : false;
};

bool utf8_is_all_space( const std::string str )
{
	return utf8_is_all_space( str.c_str(), str.size() );
};

/*
 * returns: pointer to the beginning of the next line or to the terminating 0.
 *				in other words, the returned pointer is NULL only if NULL pointer is passed as utf8str,
 *				otherwise, it always points either to a 0 character or just after a '\n'.
 */
const char *utf8_find_next_line( const char *utf8str )
{
	const char *s;

	if( utf8str == NULL )
		return NULL;

	s = utf8_find( utf8str, (unsigned long)'\n' );
	if( s[0] != 0 )
		s++;

	return s;
};

/*
 * returns a pointer to the beginning of the next empty line.
 *			if there are no empty lines in utf8str,
 *			it returns a pointer to the terminating 0.
 *			If utf8str is NULL, it returns NULL.
 */
const char *utf8_find_next_empty_line( const char *utf8str )
{
	unsigned long uc;
	const char *s = utf8str;
	const char *e;
	int	ulen;
	bool empty;

	if( s == NULL )
		return NULL;

	if( s[0] == 0 )
		return s;

	empty = false;
	e = s;

	while( (!empty) && ( s != NULL ) )
	{
		empty = true;
		s = e;

		//find the first '\n' and check if all characters are spaces
		uc = fl_utf8decode( e, e+5, &ulen );
		while( (  uc != (unsigned long)'\n' ) && ( uc != 0 ) )
		{
			//if( fl_nonspacing( uc ) )
			if( !utf8_is_space( uc ) )
				empty = false;

			e += ulen;
			if( e[0] == 0 )
				uc = 0;
			else
				uc = fl_utf8decode( e, e+5, &ulen );
		};
		if( uc!= 0 )
			e++;
	};
	return s;
};

/*
 * returns NULL if there are no non-empty lines left
 */
const char *utf8_find_next_nonempty_line( const char *utf8str, int &eol_count )
{
	unsigned long uc;
	const char *s = utf8str;
	int	ulen;

	eol_count = 0;

	if( s == NULL )
		return NULL;

	if( s[0] == 0 )
		return NULL;

	uc = fl_utf8decode( s, s+5, &ulen );
	while( ( ( utf8_is_space( uc ) )|| ( uc == (unsigned long)'\n' ) ) && ( uc != 0 ) )
	{
		if( uc == (unsigned long)'\n' )
			eol_count++;

		s += ulen;
		if( s[0] != 0 )
			uc = fl_utf8decode( s, s+5, &ulen );
		else
			uc = 0;
	};
	if( uc == 0 )
		return NULL;

	return s;
};


/*
 * returns a pointer to the \n that terminates the line or null of the line ends with a 0 (end of string)
 */
const char *utf8_get_line( const char *utf8str, std::string &dst )
{
	if( !utf8str ) { 	dst.clear(); return NULL; }

	const char *nl = utf8_find( utf8str, (unsigned long)'\n' );
	if( !nl ) {
		dst.assign( utf8str );
		return NULL;
	}
	dst.assign( utf8str, nl - utf8str );
	return nl;
}

const char *utf8_skip_bom( const char *s )
{
	if( (unsigned char)s[0] == 0xef )
		if( (unsigned char)s[1] == 0xbb )
			if( (unsigned char)s[2] == 0xbf )
				s = &s[3];
	return s;
};

/*
 * @returns failure or success
 */
bool utf8_to_lower( const std::string &str, std::string &dst )
{
	if( str.empty() ) {
		dst.clear();
		return true;
	}
	size_t buflen = str.size() * 4 + 1;
	char *buf = (char *)malloc( buflen );
	if( !buf ) {
		return false;
	}
	int len = fl_utf_tolower( (const unsigned char*)str.c_str(), str.size(), buf );
	if( len < 0 ) {
		free( buf );
		return false;
	}
	if( !len ) {
		dst.clear();
		free( buf );
		return true;
	}
	dst.assign( buf, len );
	free( buf );
	return true;
}
