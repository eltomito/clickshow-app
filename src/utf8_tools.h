/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Utf8_Tools_h
#define _Utf8_Tools_h 1

#include <string>

#include <FL/Fl.H>
#include <FL/fl_utf8.h>

unsigned int utf8_getchar( const char *str, int *byte_len = NULL );
int utf8_cmpchar( const char *str, unsigned int ucs );
const char *utf8_find( const char *str, unsigned int ucs, size_t *utf8_offset = NULL );
const char *utf8_charstart( const char *str );
int utf8_count( const char *str, unsigned int ucs );
int utf8_runlength( const char *str, unsigned int ucs, int *byteLen = NULL );
bool utf8_is_space( unsigned long uc );
bool utf8_is_all_space( const char *str, size_t len = -1 );
bool utf8_is_all_space( const std::string str );
const char *utf8_find_next_line( const char *utf8str );
const char *utf8_find_next_empty_line( const char *utf8str );
const char *utf8_find_next_nonempty_line( const char *utf8str, int &eol_count );
const char *utf8_get_line( const char *utf8str, std::string &dst );
const char *utf8_skip_bom( const char *s );
bool utf8_to_lower( const std::string &str, std::string &dst );

#endif //Utf8_Tools_h
