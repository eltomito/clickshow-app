/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "widget_brain.h"

WidgetBrain::WidgetBrain( Fl_Widget *w )
{
	body( w );
	body_geometry( NULL );
	userdata = NULL;
}

WidgetBrain::~WidgetBrain()
{
}

bool WidgetBrain::natural_size( int &W, int &H )
{
	if( body() != NULL )
		if( Cl::natural_size( body(), W, H ) )
		{
			W += border_w;
			H += border_h;
			return true;
		};
	return false;
}

void WidgetBrain::body_geometry( BodyGeometry *g, bool copy )
{
	if( !copy )
	{
		body_geom = g;
	}
	else
	{
		body_geom = (BodyGeometry *)malloc( sizeof( g ) );
		if( body_geom == NULL )
		{
			geom_copied = false;
			return;
		};
		*body_geom = *g;
	};
	geom_copied = copy;
}

void WidgetBrain::set_widget_brain( Fl_Widget *w, WidgetBrain *b )
{
	WidgetBrain *oldb = get_widget_brain( w );
	if( oldb != NULL )
		delete oldb;
	w->user_data( b );
}

void *WidgetBrain::user_data()
{
	return userdata;
}

void WidgetBrain::user_data( void *data )
{
	userdata = data;
}

bool WidgetBrain::set_user_data( Fl_Widget *w, void *data )
{
	WidgetBrain *b = get_widget_brain( w );
	if( b != NULL )
	{
		b->user_data( data );
		return true;
	};
	return false;
}

void *WidgetBrain::get_user_data( Fl_Widget *w )
{
	WidgetBrain *b = get_widget_brain( w );
	if( b != NULL )
		return( b->user_data() );
	return NULL;
}

// ===== check button brain =======

bool CheckButtonBrain::natural_size( int &W, int &H )
{
	int ww, hh;
	if( WidgetBrain::natural_size( ww, hh ) )
	{
		W = ww + check_width;
		H = hh;
		return true;
	};
	return false;
}
