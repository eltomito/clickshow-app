/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _WidgetBrain_h
#define _WidgetBrain_h

#include "cl.h"

/*
 * dummy definition of BodyGeometry which I don't wanna use yet
 */
class BodyGeometry
{
	int	dummy_data;
};

class WidgetBrain
{
private:

	Fl_Widget *body_widget;
	BodyGeometry *body_geom;
	bool	geom_copied;

	void *userdata;

public:
	static const int border_w = 20;
	static const int border_h = 6;

	void body( Fl_Widget *w ) { body_widget = w; };
	Fl_Widget *body() { return body_widget; };

	WidgetBrain( Fl_Widget *w = NULL );
	virtual ~WidgetBrain();

	void body_geometry( BodyGeometry *g, bool copy = false );
	void copy_body_geometry( BodyGeometry *g ) { body_geometry( g, true ); };
	BodyGeometry *body_geometry() { return body_geom; };

	void *user_data();
	void user_data( void *data );

	/*
	 * parameters:
	 *		W, H - references to where the natural width and height will be stored
	 * returns:
	 *		true if W and H have been set,
	 *		false if this widget doesn't care about its size and therefore W and H have been left untouched.
	 */
	virtual bool natural_size( int &W, int &H );

	/*
	 * gives the size limits for this widget
	 *
	 * parameters:
	 *		pref_W, pref_H - storage for preferred pixel dimensions
	 *		min_W, min_H - storage for minimum pixel dimensions
	 *		max_W, max_H - storage for maximum pixel dimensions (-1 means no limits )
	 *		resize_type - storage for the type of resize behavior this widget supports
	 */
/*
	virtual void size_limits( int &natural_W, int &natural_H, int &min_W, int &min_H, int &max_W, int &max_H,
							Cl_Resize &resize_type );
*/

	static WidgetBrain* get_widget_brain( Fl_Widget *w ) { return (WidgetBrain*)w->user_data(); };
	static void set_widget_brain( Fl_Widget *w, WidgetBrain *b );

	static bool set_user_data( Fl_Widget *w, void *data );
	static void *get_user_data( Fl_Widget *w );
};


// ===== check button brain =======
class CheckButtonBrain : public WidgetBrain
{
public:
	static const int check_width = 30;

	CheckButtonBrain( Fl_Widget *w = NULL ) : WidgetBrain( w ) {};
	~CheckButtonBrain(){};

	virtual bool natural_size( int &W, int &H );
};

#endif //_WidgetBrain_h
