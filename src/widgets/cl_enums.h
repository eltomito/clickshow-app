/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cl_Enums_h
#define _Cl_Enums_h

enum Cl_Widget_Type {
				INVALID = 0,
				GROUP,
				PACK,
				BUTTON
};

enum Cl_Align_Type {
	//alignment along the fast axis 
	ALIGN_FAST_START = 0,
	ALIGN_FAST_END = 1,
	ALIGN_FAST_CENTER = 1 << 1,
	ALIGN_FAST_EVEN = 1 << 2,

	//alignment along the slow axis 
	ALIGN_SLOW_START = 1 << 3,
	ALIGN_SLOW_END = 1 << 4,
	ALIGN_SLOW_CENTER = 1 << 5,
	ALIGN_SLOW_EVEN = 1 << 6
};

enum Cl_Resize {
	RESIZE_FREE	= 0,
	RESIZE_FIXED_W = 1,
	RESIZE_FIXED_H = 1 << 1,
	RESIZE_FIXED_ASPECT = 1 << 2,
	RESIZE_WIDTH_FOR_HEIGHT = 1 << 3,
	RESIZE_HEIGHT_FOR_WIDTH = 1 << 4,
	RESIZE_UNUSED_VALUE = 1 << 5
};

/*
enum Cl_Aspect_Type {
		ASPECT_FREE = 0,					//width and height are not related in any way
		ASPECT_FIXED = 1,					//the aspect is fixed
		ASPECT_CALCULATED = 1 << 1,		//widget calculates one dimension based on the other
		ASPECT_INVALID = 1 << 2			//invalid, used for reporting errors
};
*/

#endif //Cl_Enums
