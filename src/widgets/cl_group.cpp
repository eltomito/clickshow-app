/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cl_group.h"

	/*
	 * same as dims_of_n_widgets, except the "n widgets" are all child widgets.
	 *	returns:
	 *		aspect type or ASPECT_INVALID if this group has no children.
	 */
Cl_Aspect_Type Cl_Group::dims_of_all_children(
									int &sum_p, int &max_p,
									int &sum_min, int &max_min,
									int &sum_max, int &max_max,
									bool horiz )
{
	Cl_Widget *cdn;
	int	cnum;

	cnum = children();
	if( cnum <= 0 )
		return ASPECT_INVALID; 		//no children, no dimensions
	
	cdn = (Cl_Widget*)array();
	return dims_of_n_widgets( cdn, cnum,
									sum_p, max_p,
									sum_min, max_min,
									sum_max, max_max,
									horiz );
}

/*
 * determines the dimensions of n widgets when packed horizontally or vertically
 * parameters:
 * 	w - pointer to the array of Cl_Widgets
 *		num_widgets - how many widgets to pack
 *		sum_p - sum of the preferred lengths of widgets in the given axis (horiz. or vert.)
 *		max_p - maximum of the preferred breadhs in the transversal axis
 *		sum_min - sum of the minimum lengths of widgets in the given axis (horiz. or vert.)
 *		max_min - maximum of the minimum breadhs in the transversal axis
 *		sum_max - sum of the maximum lengths of widgets in the given axis (horiz. or vert.)
 *		max_max - maximum of the maximum breadhs in the transversal axis
 */
Cl_Resize Cl_Group::dims_of_n_widgets( Cl_Widget *w, int num_widgets,
									int &sum_p, int &max_p,
									int &sum_min, int &max_min,
									int &sum_max, int &max_max,
									bool horiz )
{
	int	pw, ph, minw, minh, maxw, maxh;
	Cl_Resize	rsz, rsz_or, rsz_and;

	sum_p = 0;
	max_p = 0;

	sum_min = 0;
	max_min = 0;

	sum_max = 0;
	max_max = 0;

	rsz_or = 0;
	rsz_and = ( RESIZE_UNUSED_VALUE -1 );

	while( num_widgets > 0 )
	{
		w->size_limits( pw, ph, minw, minh, maxw, maxh, rsz );
		rsz_or |= rsz;
		rsz_and &= rsz;

		if( horiz )
		{
			sum_p += pw;
			max_p = std::max( max_p, ph );
			sum_min += minw;
			max_min = std::max( max_min, minh ); 
			sum_max += maxw;
			if( maxh == -1 )
				max_max = -1;
			else	if( max_max != -1 )
				max_max = std::max( max_max, maxh );
		}
		else
		{
			sum_p += ph;
			max_p = std::max( max_p, pw );
			sum_min += minh;
			max_min = std::max( max_min, minw ); 
			sum_max += maxh;
			if( maxw == -1 )
				max_max = -1;
			else	if( max_max != -1 )
				max_max = std::max( max_max, maxw );
		};
		w++;
		num_widgets--;
	};

	if( rsz_or == RESIZE_FREE )
		return RESIZE_FREE;	//all widgets resize freely

	rsz = 0;

	//how do we resize width?
	if( rsz_and & RESIZE_FIXED_W )
	{
		rsz = RESIZE_FIXED_W;	//all children are fixed width
	}
	else
	{
		rsz = RESIZE_W_FOR_H;	//
	};

TODO: figure this shit out!

	//how do we resize height?
	if( rsz_and & RESIZE_FIXED_H )
	{
		rsz |= RESIZE_FIXED_H;
	}
	else
	{
		rsz |= RESIZE_H_FOR_W;
	};

	return rsz;	
};
