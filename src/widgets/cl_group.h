/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cl_Group_h
#define _Cl_Group_h

class Cl_Group : virtual Fl_Group, virtual Fl_Widget
{
public:
	Cl_Group() : Fl_Group( 0,0,0,0 ), Fl_Widget() {};	
	virtual ~Cl_Group() {};

	virtual Cl_Widget_Type	clw_type() { return Cl_Widget_Type::GROUP; };

	/*
	 * lays out all child widgets according to the size and type of this widget container
	 */
	virtual void layout_children() {};

	//-- helper functions for layout containers --

	/*
	 * determines the dimensions of n widgets when packed horizontally or vertically
	 * parameters:
	 * 	w - pointer to the array of Cl_Widgets
	 *		num_widgets - how many widgets to pack
	 *		sum_p - sum of the preferred lengths of widgets in the given axis (horiz. or vert.)
	 *		max_p - maximum of the preferred breadhs in the transversal axis
	 *		sum_min - sum of the minimum lengths of widgets in the given axis (horiz. or vert.)
	 *		max_min - maximum of the minimum breadhs in the transversal axis
	 *		sum_max - sum of the maximum lengths of widgets in the given axis (horiz. or vert.)
	 *		max_max - maximum of the maximum breadhs in the transversal axis
	 */
	Cl_Resize dims_of_n_widgets( Cl_Widget *w, int num_widgets,
									int &sum_p, int &max_p,
									int &sum_min, int &max_min,
									int &sum_max, int &max_max,
									bool horiz );

	/*
	 * same as dims_of_n_widgets, except the "n widgets" are all child widgets.
	 *	returns:
	 *		true if there is at least 1 child widget, false otherwise.
	 */
	bool dims_of_all_children(
									int &sum_p, int &max_p,
									int &sum_min, int &max_min,
									int &sum_max, int &max_max,
									Cl_Resize &resize_type,
									bool horiz );
};

#endif //_Cl_Group_h
