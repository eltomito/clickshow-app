/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#include "cl_pack.h"

Cl_Pack::Cl_Pack( int borderw, int borderh, int space, Cl_Align_Type algn, bool horiz )
{
	pack_borders( borderw, borderh );
	pack_spacing( space );
	pack_horizontal( horiz );
	pack_alignment( algn );
}

Cl_Pack::~Cl_Pack() {}

virtual bool Cl_Pack::natural_size( int &W, int &H )
{
	int	minW, minH, maxW, maxH;
	Cl_Aspect_Type asp;
	
	size_limits( W, H, minW, minH, maxW, maxH, asp );
	return true;
}

virtual void Cl_Pack::size_limits( int &pref_W, int &pref_H, int &min_W, int &min_H, int &max_W, int &max_H,
									Cl_Aspect_Type &aspect )
{
	Cl_Widget *cdn;
	int	cnum;
	int	i;

	int	maxw, maxh, sumw, sumh;

	cnum = children();
	if( cnum <= 0 )
	{
		//no children means zero size
		pref_W = 2*border_w;
		pref_H = 2*border_h;
		min_W = pref_W;
		min_H = pref_H;
		max_W = pref_W;
		max_H = pref_H;
		aspect = ASPECT_FIXED;
		return true;
	};
	
	cdn = (Cl_Widget*)array();

	if( cols == 0 )
		cols = cnum;

	i = 0;
	//get the dimensions of this row/column
	if( is_horiz )
	{
		aspect = dims_of_n_widgets( cdn, cnum,
							pref_W, pref_H,
							min_W, min_H,
							max_W, max_H,
							is_horiz );
	}
	else
	{
		aspect = dims_of_n_widgets( cdn, cnum,
							pref_H, pref_W,
							min_H, min_W,
							max_H, max_W,
							is_horiz );
	};
}

int Cl_Pack::width_for_height( int H )
{
	if( !is_horiz )
		return -1;	//width for height can be done for horizontal packs only

	int pref_H, pref_W, min_H, min_W, max_H, max_W;
	Cl_Aspect_type aspect;

	aspect = dims_of_all_children(
						pref_H, pref_W,
						min_H, min_W,
						max_H, max_W,
						is_horiz );

	if( aspect == ASPECT_INVALID )
		return 0;		//no children, no width



};



int Cl_Pack::height_for_width( int W );


/*
 * lays out all child widgets according to my size and their size preferences.
 *
 * E.g., If this is a horizontal pack,
 * the widgets will be arranged in a horizontal row
 * as high as this pack and as wide as necessary.
 */
void Cl_Pack::layout_children()
{








};
