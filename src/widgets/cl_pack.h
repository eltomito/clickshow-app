/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cl_Pack_h
#define _Cl_Pack_h

#include "cl_widget.h"
#include "cl_enums.h"

class Cl_Pack : public Cl_Group
{
private:
	int 				pack_border_w, pack_border_h;
	int 				space_len;
	bool 				is_horiz;
	Cl_Align_Type	pack_align;

public:

	Cl_Pack( int borderw, int borderh, int space, Cl_Align_Type algn = ALIGN_START, bool horiz = true );
	virtual ~CL_Pack() {};

	virtual Cl_Widget_Type	clw_type() { return Cl_Widget_Type::PACK; };

	//size information
	virtual bool natural_size( int &W, int &H );
	virtual void size_limits( int &pref_W, int &pref_H, int &min_W, int &min_H, int &max_W, int &max_H,
											Cl_Aspect_Type &aspect );

	virtual int width_for_height( int H );
	virtual int height_for_width( int W );

	//layout

	/*
	 * lays out all child widgets according to my size and their size preferences
	 * if this is a horizontal pack,
	 * the widgets will be arranged in a horizontal row
	 * as high as this pack and as wide as necessary.
	 */
	virtual void layout_children();

	//set methods
	void pack_borders( int bw, bh ) { border_w = bw; border_h = bh; };
	void pack_spacing( int s ) { space_len = space; };
	void pack_horizontal( bool h ) { is_horiz = h; };
	void pack_alignment( Cl_Align_Type algn ) { pack_align = algn; };

	//get methods
	int pack_h_border() { return border_w; };
	int pack_v_border() { return border_h; };
	int pack_spacing() { return space_len; };
	bool pack_horizontal() { return is_horiz; };
	Cl_Align_Type pack_alignment() { return pack_align; };
};

#endif //_Cl_Pack_h
