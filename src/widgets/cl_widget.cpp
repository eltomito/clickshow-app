/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
include "cl_widget.h"

Cl_Widget::Cl_Widget()
{
	border_w = 3;
	border_h = 2;
}

Cl_Widget::~Cl_Widget() {}

/*
 * calculate one dimension based on the other
 *
 *	parameters:
 *		one dimension
 *
 * returns:
 *		the other dimension or -1 if not implemented or out of range. 
 */
int Cl_Widget::width_for_height( int H )
{
	return -1;
}

int Cl_Widget::height_for_width( int W );
{
	return -1;
}

/*
 * this default implementation returns natural_size() as both the minimum and the maximum
 * and uses it to derive the aspect limits as well.
 */
void Cl_Widget::size_limits( int &pref_W, int &pref_H, int &min_W, int &min_H, int &max_W, int &max_H,
											Cl_Aspect_Type &aspect )
{
	int	W, H;

	W = 10;
	H = 10;	
	natural_size( W, H );
	
	pref_W = W;
	pref_H = H;
	min_W = W;
	min_H = H;
	max_W = W;
	max_H = H;
	aspect = ASPECT_FIXED;
};

/*
 * parameters:
 *		W, H - references to where the natural width and height will be stored
 * returns:
 *		true if W and H have been set,
 *		false if this widget doesn't care about it's size and therefore W and H have been left untouched.
 */
bool Cl_Widget::natural_size( int &W, int &H )
{
	Fl_Image	*img;
	int img_w, img_h;
	int txt_w, txt_h;

	//measure the label text

	txt_w = 0;	//important!! this is input to measure_label
	txt_h = 0;
	fl_font( Fl_Widget::labelfont(), Fl_Widget_labelsize() );
	fl_measure( Fl_Widget::label(), txt_w, txt_h );		//TODO: I don't understand if this considers the alignment somehow or what. See FLTK docs.

	//measure the label image

	img = Fl_Widget::Image();
	if( img != NULL )
	{
		img_w = img->w();
		img_h = img->h();
	}
	else
	{
		//no image, only text
		W = txt_w + border_w;
		H = txt_h + border_h;
		return true;
	};

	//combine image and text sizes according to the widget's align parameter
	if( Fl_Widget::align() & FL_ALIGN_IMAGE_BACKDROP )
	{
		W = std::max( img_w, txt_w ) + border_w;
		H = std::max( img_h, txt_h ) + border_h;
		return true;
	};
	
	if( Fl_Widget::align() & (FL_ALIGN_IMAGE_NEXT_TO_TEXT | FL_ALIGN_TEXT_NEXT_TO_IMAGE ) )
	{
		W = img_w + txt_w + border_w;
		H = std::max( img_h, txt_h ) + border_h;
		return true;
	};

	//assume the image and text are above each other
	W = std::max( img_w, txt_w ) + border_w;
	H = img_h + txt_h + border_h;
	return true;
};

/*
 * resize this widget to its natual size
 */
bool Cl_Widget::size_to_natural()
{
	int W, H;
	if( natural_size( W, H ) )
	{
		size( W, H );
		return true;
	};
	return false;
}

/*
 * sets the borders used to calculate the widget's natural size
 */
void Cl_Widget::label_borders( int width, int height )
{
	label_border_w = width;
	label_border_h = height;
};

/*
 * a convenience function combining Fl_Widget::label() and Fl_Widget::copy_label()
 */
void label( const char *new_label, bool copys )
{
	if( copy )
		Fl_Widget::copy_label( new_label );
	else
		Fl_Widget::label( new_label );
};
