/*
 * This file is part of ClickShow
 * Copyright 2013-2020 Tomáš Pártl, tomaspartl@centrum.cz
 *
 * ClickShow is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * ClickShow is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.
 */
#ifndef _Cl_Widget_h
#define _Cl_Widget_h

#include "cl_enums.h"

class Cl_Widget : virtual Fl_Widget
{
private:
	int label_border_w;
	int label_border_h;

public:

	Cl_Widget();
	virtual ~Cl_Widget();

	/*
	 * returns the type of this widget.
	 * Every class derived from Cl_Widget has to return its unique type.
	 */
	virtual Cl_Widget_Type	clw_type() = 0;

	/*
	 * parameters:
	 *		W, H - references to where the natural width and height will be stored
	 * returns:
	 *		true if W and H have been set,
	 *		false if this widget doesn't care about its size and therefore W and H have been left untouched.
	 */
	virtual bool natural_size( int &W, int &H );

	/*
	 * gives the size limits for this widget
	 *
	 * parameters:
	 *		pref_W, pref_H - storage for preferred pixel dimensions
	 *		min_W, min_H - storage for minimum pixel dimensions
	 *		max_W, max_H - storage for maximum pixel dimensions (-1 means no limits )
	 *		resize_type - storage for the type of resize behavior this widget supports
	 */
	virtual void size_limits( int &pref_W, int &pref_H, int &min_W, int &min_H, int &max_W, int &max_H,
							Cl_Resize &resize_type );

	/*
	 * calculate one dimension based on the other
	 *
	 *	parameters:
	 *		one dimension
	 *
	 * returns:
	 *		the other dimension or -1 if not supported or out of range. 
    */
	virtual int width_for_height( int H );
	virtual int height_for_width( int W );

	/*
	 * resize this widget to its natual size
	 */
	bool size_to_natural();

	/*
	 * sets the borders used to calculate the widget's natural size
	 */
	void label_borders( int width, int height );

	/*
	 * a convenience function combining Fl_Widget::label() and Fl_Widget::copy_label()
	 */
	void label( const char *new_label, bool copy = true );
};

#endif //_Cl_Widget_h
