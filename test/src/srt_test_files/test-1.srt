1
00:00:26,276 --> 00:00:31,036
Fantazie je v pořádku,
ale můj úkol je udržet vás u tématu.

2
00:00:31,476 --> 00:00:33,596
Zajímají nás fakta.

3
00:00:33,636 --> 00:00:37,636
Zdá se mi, že se do Káhiry a za mnou
vracíte, kdykoli máte problém.

4
00:00:38,636 --> 00:00:41,255
Před dvěma měsíci
jste z Káhiry odjel,

5
00:00:41,802 --> 00:00:45,958
opustil manželku,
to vše kvůli práci policisty v Evropě.

6
00:00:46,976 --> 00:00:50,076
A teď jste zase tu
a trápí vás bolesti hlavy.

7
00:00:50,356 --> 00:00:53,539
Jestli chcete, abych vám
od těch bolestí pomohl,

8
00:00:54,316 --> 00:00:57,516
musíme se vrátit o 2 měsíce zpět.

9
00:00:58,156 --> 00:01:01,116
Tam, kde to začalo.

10
00:01:01,676 --> 00:01:05,862
Zatím vím jen to,
že Evropa se vám stala posedlostí.

11
00:01:14,016 --> 00:01:15,776
Jak že se jmenuje?

12
00:01:16,116 --> 00:01:17,878
Pan Fisher.

13
00:01:19,936 --> 00:01:23,796
Posedlost, pane Fishere,
je stav mysli.

14
00:01:23,836 --> 00:01:27,176
Nepochybuji, že pobyt v Evropě
byl pro vás bolestivý...

15
00:01:27,516 --> 00:01:29,366
bolestivý.

16
00:01:30,856 --> 00:01:34,756
Proto musíte vstoupit
do této části svých vzpomínek.

17
00:01:35,736 --> 00:01:38,516
Říkáte, že je pro vás
obtížné si vzpomenout.

18
00:01:38,556 --> 00:01:40,996
Ano. Ano.

19
00:01:41,576 --> 00:01:44,356
Abych vám pomohl si vzpomenout,

20
00:01:44,436 --> 00:01:47,185
dohodli jsme se,
že zkusíme hypnózu.

21
00:01:48,116 --> 00:01:51,476
Hypnóza je osvědčená metoda.
Nemůže selhat.

22
00:01:51,556 --> 00:01:56,316
Pokud se dostaví
úzkost, únava, strach,

23
00:01:57,356 --> 00:01:59,316
vyvedu vás z ní.

24
00:01:59,756 --> 00:02:01,336
Stačí si říct.

25
00:02:01,376 --> 00:02:04,196
Nebude to žádný problém.
Vůbec žádný problém.

26
00:02:04,476 --> 00:02:07,120
Chci, abyste se vrátil

27
00:02:08,196 --> 00:02:10,596
v čase o 2 měsíce.

28
00:02:12,836 --> 00:02:15,062
Chci, abyste opustil Káhiru,

29
00:02:16,156 --> 00:02:18,556
písek, poušť...

30
00:02:20,516 --> 00:02:22,476
Hoppitiher,...

31
00:02:22,656 --> 00:02:24,056
Nyl,...

32
00:02:25,616 --> 00:02:27,196
Nilehralik.

33
00:02:27,236 --> 00:02:29,136
Na Evropany je to moc.

34
00:02:29,176 --> 00:02:30,276
Ano.

35
00:02:31,756 --> 00:02:34,396
Opět uvidíte Evropu,

36
00:02:34,436 --> 00:02:37,516
poprvé po 13 letech.

37
00:02:39,756 --> 00:02:39,916
„P“
Opět uvidím Evropu,

38
00:02:39,956 --> 00:02:40,116
„P r“
Opět uvidím Evropu,

39
00:02:40,156 --> 00:02:40,316
„P r v“
Opět uvidím Evropu,

40
00:02:40,356 --> 00:02:40,516
„P r v e“
Opět uvidím Evropu,

41
00:02:40,556 --> 00:02:40,716
„P r v e k“
Opět uvidím Evropu,

42
00:02:40,756 --> 00:02:40,916
„P r v e k z“
Opět uvidím Evropu,

43
00:02:40,956 --> 00:02:41,116
„P r v e k   z l“
Opět uvidím Evropu,

44
00:02:41,156 --> 00:02:41,316
”P r v e k   z l o”
Opět uvidím Evropu,

45
00:02:41,356 --> 00:02:41,516
„P r v e k   z l o č“
Opět uvidím Evropu,

46
00:02:41,556 --> 00:02:41,716
„P r v e k   z l o č i“
Opět uvidím Evropu,

47
00:02:41,756 --> 00:02:41,916
„P r v e k   z l o č i n“
Opět uvidím Evropu,

48
00:02:41,956 --> 00:02:42,736
„P r v e k   z l o č i n u“
Opět uvidím Evropu,

49
00:02:42,776 --> 00:02:45,996
poprvé po 13 letech.

50
00:02:52,116 --> 00:02:54,556
Ale Evropa se změnila.

51
00:02:58,076 --> 00:03:00,516
Přijel jsem za soumraku.

52
00:03:00,596 --> 00:03:03,236
Jsem rozrušený z cesty.

53
00:03:03,316 --> 00:03:05,676
Nemohu spát.

54
00:03:11,916 --> 00:03:14,916
Voda, všude voda...

55
00:03:14,996 --> 00:03:16,836
a ani kapka k pití.

56
00:03:16,916 --> 00:03:19,436
Děj! Co se děje?

57
00:03:25,716 --> 00:03:28,592
- Haló?
- Jsem policista.

58
00:03:29,236 --> 00:03:33,356
Konečně mě zavolali zpět
do Evropy, abych vyšetřil vraždu.

59
00:03:33,396 --> 00:03:37,676
- Haló?
- Řekli mi, že jsem schopný. Víc nevím.

60
00:03:38,156 --> 00:03:40,256
Hledám Osborna.

61
00:03:40,296 --> 00:03:43,536
- Haló?
- Jediného, koho chci zase vidět.

62
00:03:43,876 --> 00:03:46,756
Něco jsem mu přivezl
z Egypta.

63
00:03:51,436 --> 00:03:55,276
Kde jsou schody?
Přestěhoval jste schody!

64
00:04:03,195 --> 00:04:06,036
Musíte mi říkat vše, pane Fishere.

65
00:04:08,116 --> 00:04:09,969
Co vidíte?

66
00:04:11,017 --> 00:04:12,899
Je tam někdo?

67
00:04:17,036 --> 00:04:21,116
Vlhká chodba páchne
po spálené kůži.

68
00:04:21,156 --> 00:04:24,196
Komorná mě vede dál.

69
00:04:26,156 --> 00:04:28,196
Zestárla.

70
00:04:42,136 --> 00:04:43,536
Dobrý večer.

71
00:04:46,936 --> 00:04:48,554
To jsem já, Fisher.

72
00:05:01,116 --> 00:05:03,276
Už je to dlouho.

73
00:05:12,276 --> 00:05:16,116
Fishere, panebože!

74
00:05:21,796 --> 00:05:25,036
Vážně jste si myslel,
že bych vás nepoznal?

75
00:05:32,816 --> 00:05:36,796
Tak povězte, co vy?

76
00:05:39,216 --> 00:05:41,156
Itálie.

77
00:05:43,316 --> 00:05:44,996
Jel jste do Itálie.

78
00:05:45,036 --> 00:05:47,876
- Do Káhiry.
- Opravdu?

79
00:05:49,676 --> 00:05:51,076
Chviličku.

80
00:05:52,916 --> 00:05:54,928
-Zvedá se vítr.
-Pssst!

81
00:06:10,196 --> 00:06:14,101
To bylo moudré, odejít.
Nejlepší možnost.

82
00:06:14,956 --> 00:06:16,945
Chovali se k vám zle.

83
00:06:18,176 --> 00:06:20,716
Ale... do Káhiry?

84
00:06:22,396 --> 00:06:24,549
Jak to jde na akademii?

85
00:06:27,476 --> 00:06:29,439
Už léta neučím.

86
00:06:30,396 --> 00:06:33,196
Výuku restrukturalizovali.

87
00:06:34,856 --> 00:06:37,276
Přesně tohle slovo použili.

88
00:06:37,356 --> 00:06:40,516
Asi jsem byl také
restrukturalizován.

89
00:06:40,596 --> 00:06:45,636
Ale teď jsem docela spokojený.
Viděl jste se tam s někým?

90
00:06:45,716 --> 00:06:47,996
Kramer mi našel práci.

91
00:06:48,376 --> 00:06:50,876
Proč ne?
Jste schopný muž, Fishere.

92
00:06:50,956 --> 00:06:54,436
Už nezbývá moc lidí s vaším vzděláním.
Kramer to ví.

93
00:06:54,516 --> 00:06:58,556
Ano, musím přiznat,
že se na to opravdu těším.

94
00:07:14,776 --> 00:07:17,836
„Prvek zločinu“ v egyptštině.

95
00:07:17,916 --> 00:07:21,836
Vaše práce.
Překlad trval dva roky.

96
00:07:21,916 --> 00:07:25,116
S poznámkami, se vším.
Celá kniha.

97
00:07:25,156 --> 00:07:27,436
Bez mého svolení?

98
00:07:27,516 --> 00:07:30,196
Nenapadlo vás, že bych chtěl vědět,
co se děje s mou knihou?

99
00:07:30,236 --> 00:07:32,356
Napadlo.
Napsal jsem vám.

100
00:07:32,396 --> 00:07:35,470
-To bych si pamatoval.
-Odpověděl jste.

101
00:07:36,916 --> 00:07:39,476
Na to si nevzpomínám.

102
00:07:40,496 --> 00:07:42,831
Zemřela mi manželka.

103
00:07:44,136 --> 00:07:46,676
Všechny své knihy a práce jsem prodal.

104
00:07:48,696 --> 00:07:51,476
Eine Kugelkam geflogen,

105
00:07:51,516 --> 00:07:54,996
giltsie mit oder
giltsie dir?

106
00:07:55,336 --> 00:07:57,956
Ano, konečně jsem se
rozhodl oženit.

107
00:07:57,996 --> 00:08:00,396
Zemřela pro mě.

108
00:08:01,076 --> 00:08:02,376
To je mi líto.

109
00:08:02,936 --> 00:08:05,480
Nic neuměla udělat jen napůl.

110
00:08:07,516 --> 00:08:10,298
Rozhodně věděla,
jak se učinit nezapomenutelnou.

111
00:08:12,576 --> 00:08:16,441
To je to nejdůležitější:
 rodina.

112
00:08:16,916 --> 00:08:19,315
V Káhiře jsem měl manželku.

113
00:08:19,355 --> 00:08:22,951
Proč muži opouštějí své ženy?
Tomu nerozumím.

114
00:08:23,516 --> 00:08:25,556
Je to pro mě nepochopitelné.

115
00:08:25,596 --> 00:08:29,956
Město zasypával písek.
Nebylo možné tam zůstat.

116
00:08:31,896 --> 00:08:35,567
Ona se tam narodila.
Nemohla odjet.

117
00:08:38,276 --> 00:08:40,996
Byl jsem poslední Evropan,
co odjel.

118
00:08:41,036 --> 00:08:43,796
Smíšený sňatek, že?

119
00:08:45,696 --> 00:08:48,636
Pro mě je těžké
o těchto věcech mluvit.

120
00:08:48,716 --> 00:08:52,323
Miloval jsem ji,
jak jen jsem dokázal. To je vše.

121
00:08:52,976 --> 00:08:57,872
Pro mě to byla tak
nevýslovná radost, Fishere.

122
00:08:58,636 --> 00:09:02,909
Náruč rodiny,
šťastný úsměv...

123
00:09:14,076 --> 00:09:17,570
Nevím, co o vašich knihách říct.

124
00:09:19,516 --> 00:09:21,410
Vedly mě.

125
00:09:21,916 --> 00:09:25,956
Nejen v mé policejní práci,
ve všem mém myšlení.

126
00:09:27,336 --> 00:09:28,536
Ne, ne, ne, ne!

127
00:09:28,576 --> 00:09:31,836
Zuřil jsem, když mé knihy
odstranili z knihovny akademie.

128
00:09:31,876 --> 00:09:35,564
Teď to chápu.
Psal jsem beletrii. Nebyla to věda.

129
00:09:35,604 --> 00:09:37,596
Bylo to naivní, nebezpečné.

130
00:09:37,676 --> 00:09:41,559
Nejhorší, co se může stát
ve jménu vědy, je zbožštění systému.

131
00:09:41,599 --> 00:09:44,016
Vždy jsme hledali
prvek zločinu ve společnosti.

132
00:09:44,056 --> 00:09:46,800
Ale proč nehledat
v lidské podstatě?

133
00:09:47,576 --> 00:09:51,516
V Evropě se stalo tolik.
Ta kniha začala být nebezpečná.

134
00:09:59,416 --> 00:10:00,416
Omluvte mě.

135
00:10:19,836 --> 00:10:22,272
Fishere! Zpátky ze země zaslíbené, co?

136
00:10:22,512 --> 00:10:24,596
Říkal jsem si, že vás najdu u starouše.

137
00:10:24,676 --> 00:10:28,316
Přímo k němu, co?
Teď se ukáže, co ve vás je, Fishere.

138
00:10:28,396 --> 00:10:32,396
Právě našli další zohavené tělo,
malou holčičku. Někdo ji vyvrhl.

139
00:10:32,476 --> 00:10:36,476
Tentokrát dole v přístavu.
Takže šup, šup! Máte 15 minut.

140
00:10:49,116 --> 00:10:52,796
Promiňte.
Musel jsem si vzít léky.

141
00:10:52,876 --> 00:10:56,436
To vám volal Kramer?
Pořád má napilno.

142
00:11:02,556 --> 00:11:04,796
Je teď velice důležitý.

143
00:11:06,436 --> 00:11:08,359
Kramer je nula.

144
00:11:24,256 --> 00:11:27,709
Tohle je ta práce,
kterou pro vás Kramer měl?

145
00:11:30,796 --> 00:11:32,828
Neuvěřitelné. 

146
00:11:34,816 --> 00:11:36,836
Evropa spí...

147
00:11:36,916 --> 00:11:40,436
Vidím muže s koněm
zapřaženým za žebřiňák.

148
00:11:40,476 --> 00:11:44,476
Žebřiňák je naložený jablky.
Poklidná scéna.

149
00:11:53,856 --> 00:11:55,856
Našli ji nahoře v uhlí.

150
00:11:58,556 --> 00:12:01,116
Právě ji spouštějí.

151
00:12:03,156 --> 00:12:07,036
Tady je svíčka, 
 co tě podpálí v posteli,

152
00:12:07,076 --> 00:12:10,676
Tady je sekáček,
co hlavu ti oddělí,

153
00:12:10,716 --> 00:12:14,156
seky, sek, seky, sek.

154
00:12:14,236 --> 00:12:17,076
Poslední už sebou sek.

155
00:12:51,396 --> 00:12:54,192
Někdo tam nahoře spí.

156
00:12:58,836 --> 00:13:02,556
Ale, pane Fishere,
musíte mi říct víc o Kramerovi.

157
00:13:02,636 --> 00:13:05,026
Jak jste to myslel,
že Kramer je nula?

158
00:13:05,676 --> 00:13:09,556
Nikdo ani hnout! Mluví k vám
policejní ředitel Kramer.

159
00:13:09,636 --> 00:13:13,376
- Nikdo ani hnout!
- Znám ho ze starých časů.

160
00:13:13,876 --> 00:13:16,264
Z policejní akademie.

161
00:13:18,796 --> 00:13:21,086
Vždycky mi lezl na nervy...

162
00:13:22,096 --> 00:13:24,936
A teď je policejní ředitel.

163
00:13:25,756 --> 00:13:28,560
Fishere! Konečně jsi dorazil!

164
00:13:28,856 --> 00:13:31,284
Vidím, že jsi našel
svého přítele Osborna.

165
00:13:31,636 --> 00:13:34,244
Neviděl jsem tě na stanici.

166
00:13:34,476 --> 00:13:35,476
Buzerant!

167
00:13:44,256 --> 00:13:48,137
Svět je zvláštní, Fishere.
Vždycky jsi býval nadějnější než já.

168
00:13:48,816 --> 00:13:50,692
A teď jsem policejní ředitel.

169
00:13:51,236 --> 00:13:53,835
Víš, že to já jsem Osbornovi
nabídl tu práci,

170
00:13:53,875 --> 00:13:55,345
když ho vyhodili ze školy?

171
00:13:58,836 --> 00:14:01,139
Držím svět za koule, Fishere.

172
00:14:01,736 --> 00:14:03,835
A pořádně mačkám.

173
00:14:15,336 --> 00:14:18,316
Tohle je nejneohrabanější
vyšetřování, co jsem kdy viděl.

174
00:14:18,356 --> 00:14:21,899
- Pssst.
-Tak jo, Fishere.

175
00:14:29,716 --> 00:14:33,596
Řekněme, že neohrabanost je
jedním z principů, které používáme.

176
00:14:33,676 --> 00:14:37,356
Je tu ve vzduchu zvláštní zlo.
Je to cítit.

177
00:14:37,436 --> 00:14:40,156
Vidíš, že se změnilo
úplně všechno.

178
00:14:40,196 --> 00:14:44,276
Věř mi, Fishere,
stejná věc se stala i v lidech.

179
00:14:52,776 --> 00:14:57,836
Víš, co je to Pád?
Skok z výšky s provazem kolem kotníku.

180
00:14:57,916 --> 00:15:01,236
Říkají tomu rituál.
Já tomu říkám zločin.

181
00:15:01,316 --> 00:15:03,916
Musím je držet na uzdě.

182
00:15:17,176 --> 00:15:19,529
Vždycky jsi bejval hodnej kluk, Fishere.

183
00:15:19,569 --> 00:15:21,340
Samá práce, žádná zábava.

184
00:15:22,716 --> 00:15:25,103
Všemu chceš rozumět.

185
00:15:25,796 --> 00:15:27,909
Proč jsi vůbec šel k policii?

186
00:15:28,296 --> 00:15:30,696
Nemyslím, že by tě zajímala krev.

187
00:15:31,636 --> 00:15:34,093
Vy lidi
jste nikdy nepoznali radost.

188
00:15:41,556 --> 00:15:43,061
Víš, co to je?

189
00:15:46,836 --> 00:15:50,085
Tohle je rozdíl
mezi mnou a tebou.

190
00:15:50,976 --> 00:15:52,422
Pozor!

191
00:15:58,936 --> 00:16:01,262
Zbláznil ses?

192
00:16:07,156 --> 00:16:09,796
- Proč jsi utíkala?
- Nevím.

193
00:16:09,836 --> 00:16:12,427
Tam nahoře zavraždili dívku.

194
00:16:12,916 --> 00:16:17,022
-To já jsem jim řekla, kde hledat.
-Jsi snílek, Fishere.

195
00:16:18,756 --> 00:16:21,956
-Viděla jsi, kdo to udělal?
-Ne, právě jsem přišla.

196
00:16:22,036 --> 00:16:23,996
Znala jsi ji?

197
00:16:24,236 --> 00:16:25,969
Neměla sem dneska večer chodit.

198
00:16:26,756 --> 00:16:29,836
Snažila jsem se jí to říct.
Je to moje sestra.

199
00:16:30,316 --> 00:16:31,756
Prodáváme loterii.

200
00:16:31,936 --> 00:16:36,254
Asi to nechápeš, ale tvoje metody
jsou staromódní a příliš riskantní.

201
00:16:36,876 --> 00:16:37,876
Loterii?

202
00:16:39,676 --> 00:16:40,876
Loterii?

203
00:16:41,416 --> 00:16:43,646
To nás zabíjejí.

204
00:16:44,396 --> 00:16:47,756
Znala riziko.
Loterijní vraždy.

205
00:16:48,436 --> 00:16:50,916
Někdo zabíjí prodavače loterie.

206
00:16:50,956 --> 00:16:52,996
Proto držíme spolu.

207
00:16:53,076 --> 00:16:55,556
Vždycky chodíme po dvou, po třech.

208
00:16:55,636 --> 00:16:58,996
Řekl, že koupí spoustu tiketů,
když přijde dneska sama.

209
00:16:59,076 --> 00:17:01,916
Říkala, že je hloupost se bát.

210
00:17:03,336 --> 00:17:06,196
-Byla tam malá postava?
-Malá postava?

211
00:17:06,236 --> 00:17:08,715
Tak to dělá.
Copak to nevíš?

212
00:17:08,796 --> 00:17:12,076
-Viděla jsi ho mluvit se svou sestrou?
-Z dálky.

213
00:17:12,156 --> 00:17:14,476
- Kdy?
- Včera.

214
00:17:14,556 --> 00:17:16,956
- Poznala bys ho?
- Ne.

215
00:17:17,036 --> 00:17:19,876
Můžu si vzít sestřiny tikety?

216
00:17:21,636 --> 00:17:23,476
Jistě.

217
00:17:28,676 --> 00:17:31,276
Neměl jsem o tom
být informován?

218
00:17:31,356 --> 00:17:35,596
Já rozhoduji, co se bude dít.
Beze mě nic nedělej.

219
00:17:35,676 --> 00:17:38,436
Teď jsem tvůj šéf já,
ne Osborne.

220
00:17:38,476 --> 00:17:41,156
Takže žádný výstřednosti, Fishere.

221
00:17:43,436 --> 00:17:46,316
Když se takováhle fuška podaří,

222
00:17:46,396 --> 00:17:49,916
je to jako bych mrdal Boha.

223
00:17:52,396 --> 00:17:55,636
Vidíš ta světla?
Je to v pořádku, Fishere.

224
00:17:55,716 --> 00:17:57,716
Pořád ještě je to v pořádku.

225
00:17:59,836 --> 00:18:02,756
Cítím v kapse talisman.

226
00:18:02,836 --> 00:18:06,036
Je nepříjemně kluzký.

227
00:18:06,716 --> 00:18:09,876
Nikdy jsem neviděl takhle
zohavené tělo.

228
00:18:09,956 --> 00:18:11,796
Loterijní vraždy.

229
00:18:11,876 --> 00:18:16,116
Uklidněte se, Fishere.
Jde vám to výborně.

230
00:18:19,236 --> 00:18:21,116
Opravdu?

231
00:19:00,900 --> 00:19:03,780
Fisher.
Inspektor Fisher.

232
00:19:03,820 --> 00:19:06,180
Ukážete mi, kde mám kancelář?

233
00:19:07,760 --> 00:19:12,620
- Kramer pro mě poslal. Chci pitvu.
- Jen jednu?

234
00:19:12,660 --> 00:19:16,020
Prosím podržte průkaz výš,
musíme ho orazítkovat.

235
00:19:26,620 --> 00:19:28,940
Vrátil jsem se kvůli Osbornovi.

236
00:19:29,020 --> 00:19:31,100
Osborne.
Velký Osborne.

237
00:19:31,180 --> 00:19:34,020
Jestli jsem o něm někdy slyšel,
už jsem to zapomněl.

238
00:19:34,060 --> 00:19:38,980
Staral jsem se o svý
a vtom vjede do města novej šerif.

239
00:19:39,060 --> 00:19:43,260
Vejde do salonu,
vytáhne šestiraňák

240
00:19:43,300 --> 00:19:46,180
a říká: „Já to tady vyčistím!“

241
00:19:46,260 --> 00:19:51,340
A já věřím, že vyčistí, protože tak
rychle tasit jsem nikdy nikoho neviděl.

242
00:19:51,380 --> 00:19:54,220
Jak jsem se mohl zaplést
s takovou dvojkou debilů?

243
00:19:54,260 --> 00:19:55,796
Ani to nebylo vtipný.

244
00:19:56,500 --> 00:19:58,540
Podívej se na sebe!

245
00:20:07,380 --> 00:20:09,220
Policejní stanice.

246
00:20:09,300 --> 00:20:14,020
Chyběly mi ty nekonečné chodby,
kde stačí zajít za roh a nejste nikde.

247
00:20:14,100 --> 00:20:15,500
Nevím proč.

248
00:20:16,680 --> 00:20:19,240
Musím si vybalit.

249
00:20:20,740 --> 00:20:22,780
Kancelář je mi povědomá.

250
00:20:22,820 --> 00:20:25,820
Kdysi bývala Osbornova.

251
00:20:25,860 --> 00:20:28,140
V přehrávači byla videokazeta.

252
00:20:28,220 --> 00:20:31,220
Prosím, pane Osborne,
podržte to trochu výš.

253
00:20:32,980 --> 00:20:36,340
Čtyři vraždy.
V měsíčních intervalech. Talismany.

254
00:20:36,380 --> 00:20:39,380
Všechna těla zohavená
stejným obscénním způsobem.

255
00:20:39,420 --> 00:20:43,980
Veřejnost nikdy zohavení neviděla.
Práce jednoho a toho samého člověka.

256
00:20:44,060 --> 00:20:47,060
Poslední dobou se cítím starý.

257
00:20:47,200 --> 00:20:53,040
Policejní ředitel Kramer souhlasil, 
že povoláme odborníka na takové případy.

258
00:20:53,380 --> 00:20:58,140
Dáváte tím za pravdu kritice,
že jste především teoretik?

259
00:21:04,500 --> 00:21:08,880
Omluvte mě. Co byste řekl
o Pádu, pane Kramere?

260
00:21:42,020 --> 00:21:44,460
„Drahý Osborne,
děkuji za půjčku.

261
00:21:46,180 --> 00:21:52,860
Myslíme, že fotografie patří
do zprávy o sledování 19040616-JJ

262
00:21:53,840 --> 00:21:56,360
týkající se Harryho Greye.“

263
00:22:13,700 --> 00:22:17,340
Snažím se vzpomenout
na snazší vstup do archivu.

264
00:22:17,380 --> 00:22:19,980
Dřív nějaký musel existovat.

265
00:22:20,060 --> 00:22:25,220
Vzkaz v potrubní poště ve mně vzbudil
zvědavost, co je ve složce „Harry Grey“.

266
00:22:25,760 --> 00:22:30,540
Osborne po něčem šel.
Chci vědět, co to bylo.

267
00:23:21,680 --> 00:23:25,020
Zatím mi nikdo o ničem moc neřekl.

268
00:23:25,300 --> 00:23:27,980
Od Kramera jsem moc neočekával,

269
00:23:28,060 --> 00:23:30,980
ale Osborne mohl být sdílnější.

270
00:23:33,580 --> 00:23:37,300
Procházím vše krok za krokem
a jen obkresluji prázdná místa.

271
00:23:43,980 --> 00:23:48,700
Je spousta věcí, kterým bych dal
přednost před návštěvou pitvy holčičky.

272
00:23:49,380 --> 00:23:52,660
Ale to patří k postupu práce,
že, pane Fishere?

273
00:23:53,040 --> 00:23:56,520
Káhira mi připadá tak vzdálená.

274
00:23:58,620 --> 00:24:02,560
To je velice krásné tělo.
Jistě vidíte sám.

275
00:24:04,500 --> 00:24:07,700
Tělo je neosobní.

276
00:24:07,780 --> 00:24:13,100
Vědce však zajímají stopy vraha.

277
00:24:13,880 --> 00:24:17,940
Zkoumáme dílo mozku.
Mozku člověka.

278
00:24:18,680 --> 00:24:20,581
To je na tom vzrušující.

279
00:24:21,640 --> 00:24:25,180
-Je to Loterijní vražda?
-Ó, jistě, o tom není pochyby.

280
00:24:25,260 --> 00:24:28,220
Je rozřezaná přesně tím samým způsobem.

281
00:24:28,400 --> 00:24:30,620
Nemůžete si teď odpočinout?

282
00:24:41,340 --> 00:24:43,480
Může to být napodobenina?

283
00:24:43,520 --> 00:24:45,900
Pokud ano, je to dobrá napodobenina.

284
00:24:46,240 --> 00:24:50,620
Jednu věc o těch vraždách
tisk nikdy nenapsal.

285
00:24:50,700 --> 00:24:55,060
Že smrt nastala před zohavením.

286
00:24:56,700 --> 00:24:58,980
Jaká byla příčina smrti?

287
00:24:59,460 --> 00:25:01,280
Udušení. Podívejte!

288
00:25:05,240 --> 00:25:09,020
Prodavači loterie bývají
slabí lidé a... Ticho!

289
00:25:09,200 --> 00:25:14,660
A bezmocní invalidé,
mladé ženy atd.

290
00:25:15,740 --> 00:25:19,060
To zohavení...
Udělal to nožem?

291
00:25:19,600 --> 00:25:23,100
Rozbitou láhví.
Šokuje vás to?

292
00:25:26,680 --> 00:25:29,660
Pár let jsem mimo policejní službu.

293
00:25:29,700 --> 00:25:32,900
Ale jste profesionál.
Oba jsme.

294
00:25:33,280 --> 00:25:36,860
Obdivujeme zločince, nikoli oběť.

295
00:25:37,340 --> 00:25:40,580
Ona je pryč.
On stále ještě někde žije.

296
00:25:40,660 --> 00:25:44,100
Zanechává své drobné stopy,
svůj systém.

297
00:25:44,280 --> 00:25:47,500
Řízne a tím se obnaží.

298
00:25:55,900 --> 00:25:59,119
Kde jste se učil?
V Osvětimi?

299
00:26:03,520 --> 00:26:06,200
Pane Fishere, volají od Osborna.

300
00:26:09,060 --> 00:26:11,580
Ano, byli i tací.

301
00:26:14,060 --> 00:26:16,540
Pane Fishere, musíte okamžitě přijít.

302
00:26:16,620 --> 00:26:18,940
Myslím, že se panu Osbornovi něco stalo.

303
00:26:19,020 --> 00:26:21,340
Když jste odešel, řekl mi,
ať nikoho nepouštím dál.

304
00:26:21,420 --> 00:26:24,380
Ale právě jsem slyšela,
jak se s někým hádá.

305
00:26:24,460 --> 00:26:26,660
A teď neodpovídá.

306
00:27:12,180 --> 00:27:13,916
Tora, Tora.

307
00:27:48,020 --> 00:27:51,740
Upadl jsem.
Mám takové záchvaty, víte?

308
00:27:51,780 --> 00:27:54,380
T.. To bude všechno.

309
00:27:54,420 --> 00:27:57,020
Nevím, jak to mám vysvětlit, Fishere.

310
00:27:57,100 --> 00:28:01,820
Nevím, jestli rozumíte významu
takového slova jako „pokání“.

311
00:28:01,900 --> 00:28:06,100
Cítím, že musím nějak zaplatit.
Víte, jak to myslím?

312
00:28:06,640 --> 00:28:08,734
Raději nechtějte vědět.

313
00:28:09,680 --> 00:28:13,340
Večer mě zavolali k vraždě.

314
00:28:13,420 --> 00:28:17,700
Dívka. Případ se podobal něčemu,
na čem jste pracoval před důchodem.

315
00:28:18,180 --> 00:28:21,380
Máma to dělá.
Táta to dělá.

316
00:28:21,660 --> 00:28:26,100
- Koně to zkusí a...
- Myslím, že se vrátím zítra.

317
00:28:35,820 --> 00:28:37,683
Kdo je Harry Grey?

318
00:28:40,340 --> 00:28:43,500
Ano, to je dobrá otázka:
Kdo je Harry Grey?

319
00:28:44,280 --> 00:28:48,386
Harry Grey už neexistuje
a Loterijní vraždy jsou uzavřená věc.

320
00:28:52,260 --> 00:28:56,420
Ty vraždy byly rituální,

321
00:28:56,800 --> 00:28:58,200
bizarní,

322
00:28:59,300 --> 00:29:01,740
výplod nemocného mozku,

323
00:29:01,780 --> 00:29:03,700
maniaka.

324
00:29:03,780 --> 00:29:07,300
Ale provedl je systematicky,

325
00:29:07,380 --> 00:29:10,500
systematicky do posledního detailu.

326
00:29:11,380 --> 00:29:13,020
Nelidské.

327
00:29:14,920 --> 00:29:19,540
Toto je jediný existující
snímek Harryho Greye.

328
00:29:22,820 --> 00:29:24,861
Došlo ke čtyřem vraždám.

329
00:29:27,380 --> 00:29:33,660
Někdy pomůže prozkoumat
geografii zločinu, pamatujete?

330
00:29:33,740 --> 00:29:37,580
Ale byla tu spojitost se
3 roky starou zprávou o sledování.

331
00:29:37,660 --> 00:29:40,300
Jméno sledovaného bylo Harry Grey,

332
00:29:40,580 --> 00:29:43,060
byl podezřelý z podvratné činnosti.

333
00:29:43,140 --> 00:29:45,220
Neoprávněně, jak se ukázalo.

334
00:29:47,660 --> 00:29:52,140
Napadlo mě, jestli se
před těmi 3 lety připravoval.

335
00:29:53,380 --> 00:29:55,340
Víte, co myslím, připravoval...

336
00:29:55,420 --> 00:29:59,500
připravoval na Loterijní vraždy.

337
00:29:59,580 --> 00:30:01,720
Obhlídka terénu.

338
00:30:02,500 --> 00:30:03,500
Sakra.

339
00:30:09,380 --> 00:30:11,460
Držel jsem se starých postupů.

340
00:30:13,100 --> 00:30:15,860
Nastražil jsem past a čekal na něho.

341
00:30:18,800 --> 00:30:20,640
Musel to vědět.

342
00:30:22,740 --> 00:30:25,060
Pokusil se uprchnout v autě.

343
00:30:25,140 --> 00:30:28,740
Jel jsem za ním.
Jel rychle.

344
00:30:29,120 --> 00:30:33,220
Překvapivě dobře řídil,
v té tmě a mokru.

345
00:30:33,500 --> 00:30:37,259
Dostal smyk.
Vyletěl ze silnice.

346
00:30:38,000 --> 00:30:40,300
Narazil do betonového sloupu.

347
00:30:44,360 --> 00:30:46,178
Zemřel v plamenech.

348
00:30:47,380 --> 00:30:50,020
Jako by chtěl,
aby to tak dopadlo.

349
00:30:52,020 --> 00:30:53,900
Nemohl jsem mu pomoci.

350
00:31:09,620 --> 00:31:11,509
Jen jednu otázku.

351
00:31:12,760 --> 00:31:15,140
Koho má tahle fotka přesvědčit?

352
00:31:15,220 --> 00:31:17,360
Mě, nebo vás?

353
00:31:29,020 --> 00:31:32,300
Tyhle talismany byly k vidění
všude v novinách.

354
00:31:32,820 --> 00:31:35,340
Kdokoli si takový mohl vyrobit.

355
00:31:43,380 --> 00:31:47,460
Podle 3 roky staré zprávy o sledování
je tohle jeho trasa.

356
00:31:48,140 --> 00:31:50,500
Tohle jsou čtyři místa vražd.

357
00:31:50,580 --> 00:31:55,700
Halberstadt, Friedingen,
Oberdorf a Neukalkau.

358
00:31:55,780 --> 00:31:58,940
Jak vidíte, jsou v rozích
přesného čtverce.

359
00:32:01,660 --> 00:32:04,980
Čtyři vraždy v odstupech přibližně
1 měsíce.

360
00:32:05,060 --> 00:32:08,700
Grey je mrtev
a jeho dílo dokonáno.

361
00:32:09,980 --> 00:32:12,460
Krásný geometrický hlavolam.

362
00:32:12,800 --> 00:32:16,899
- Minulost a současnost...
- Ne, Osborne. To nestačí.

363
00:32:17,820 --> 00:32:20,060
Deportujete člověka

364
00:32:20,100 --> 00:32:22,620
na 13 let

365
00:32:22,700 --> 00:32:24,580
do písku.

366
00:32:26,580 --> 00:32:28,700
Přivedete ho zpátky do Evropy,

367
00:32:28,780 --> 00:32:31,820
kde se zjevně něco pokazilo.

368
00:32:32,000 --> 00:32:34,220
Najde svého bývalého učitele,

369
00:32:34,300 --> 00:32:37,100
učitele, který bohužel zešílel.

370
00:32:37,580 --> 00:32:40,260
Ptal jste se mě,
kdo byl Harry Grey.

371
00:32:40,840 --> 00:32:43,980
Řekl jsem: Harry Grey je mrtev.

372
00:32:44,520 --> 00:32:45,520
Ale

373
00:32:46,720 --> 00:32:48,769
nevylučuji...

374
00:32:49,380 --> 00:32:52,140
Tenhle fyzicky zdatný člověk

375
00:32:52,220 --> 00:32:54,732
dostane práci u policie.

376
00:32:56,060 --> 00:32:59,180
Až na to,
že žádná policie už není.

377
00:33:00,060 --> 00:33:02,460
Bývalému učiteli vyhrožovali.

378
00:33:02,500 --> 00:33:04,980
Je jako pes.
Bojí se.

379
00:33:05,260 --> 00:33:07,980
Mění fakta,
aby kryl zločince.

380
00:33:08,060 --> 00:33:12,460
Navíc zastává brilantní teorie
svého mládí.

381
00:33:13,340 --> 00:33:16,220
To je zrada.
To je ještě horší.

382
00:33:19,700 --> 00:33:21,740
Vidím, že se bojíte.

383
00:33:21,820 --> 00:33:25,460
Vyhrožují vám.
Kde je ta zpráva o sledování?

384
00:33:25,600 --> 00:33:28,620
Dnes je zločin jako chemická reakce.

385
00:33:28,900 --> 00:33:31,740
Může k němu dojít
jen ve správném prostředí.

386
00:33:32,080 --> 00:33:36,140
Inteligenci a cit
musí ovlivnit něco zvenčí.

387
00:33:36,220 --> 00:33:40,940
Nemluvte se mnou v těch pitomých klišé!
Už nejsme na akademii!

388
00:33:41,220 --> 00:33:44,060
Našel jsem tu dívku
rozřezanou v innenstadtském přístavu.

389
00:33:44,140 --> 00:33:48,620
Innenstadt je na tom čtverci také.
Tady, uprostřed. Grey žije.

390
00:33:50,640 --> 00:33:51,860
Nepovídejte.

391
00:33:51,900 --> 00:33:56,020
Pokud čtverec znázorňuje uzavřený tvar,
tohle je otvor v něm.

392
00:34:03,340 --> 00:34:05,403
Velmi dojemné.

393
00:34:06,619 --> 00:34:08,753
Teď jste spokojený, že?

394
00:34:11,400 --> 00:34:15,380
Ale... buďte opatrný, Fishere.

395
00:34:16,020 --> 00:34:18,609
Policejní práce je teď
o moc nebezpečnější,

396
00:34:18,649 --> 00:34:20,579
než bývala za vašich časů.

397
00:34:22,179 --> 00:34:24,739
Vím, že jsem jen starý prďola.

398
00:34:25,620 --> 00:34:29,268
Prokrista, vy stejně uděláte jen to,
co sám uznáte za vhodné.

399
00:34:56,400 --> 00:34:58,527
Nikdy dřív jsme se nepohádali.

400
00:34:59,140 --> 00:35:01,900
Má vás opravdu rád, pane Fishere.

401
00:35:02,080 --> 00:35:03,820
Já to vím.

402
00:35:06,480 --> 00:35:11,100
Tolik ho děsí
i ty nejmenší maličkosti.

403
00:35:11,140 --> 00:35:13,820
Dům, tma.

404
00:35:15,400 --> 00:35:17,260
Nikdy neodpočívá.

405
00:35:17,840 --> 00:35:21,320
Jen tam nahoře chodí sem a tam.
Poslouchejte.

406
00:35:23,200 --> 00:35:25,848
V tomhle ročním období
bývá vždy taková tma?

407
00:35:26,180 --> 00:35:28,540
Žádná roční období už nejsou.

408
00:35:28,720 --> 00:35:31,980
Poslední tři léta nebyla léta.

409
00:35:32,460 --> 00:35:35,500
Počasí se v jednom kuse mění.

410
00:35:36,280 --> 00:35:37,940
Na tom se nemění nic.

411
00:35:46,760 --> 00:35:48,700
Ublížil by si?

412
00:35:53,360 --> 00:35:57,580
Dnes ráno mě požádal, abych
spálila všechny jeho knihy a eseje.
