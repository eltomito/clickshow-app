1
00:00:26,276 --> 00:00:31,036
Fantazie je v pořádku,
ale můj úkol je udržet vás u tématu.

2
00:00:31,476 --> 00:00:33,596
Zajímají nás fakta.

3
00:00:33,636 --> 00:00:37,636
Zdá se mi, že se do Káhiry a za mnou
vracíte, kdykoli máte problém.

4
00:00:38,636 --> 00:00:41,255
Před dvěma měsíci
jste z Káhiry odjel,

5
00:00:41,802 --> 00:00:45,958
opustil manželku,
to vše kvůli práci policisty v Evropě.

6
00:00:46,976 --> 00:00:50,076
A teď jste zase tu
a trápí vás bolesti hlavy.

7
00:00:50,356 --> 00:00:53,539
Jestli chcete, abych vám
od těch bolestí pomohl,

8
00:00:54,316 --> 00:00:57,516
musíme se vrátit o 2 měsíce zpět.

9
00:00:58,156 --> 00:01:01,116
Tam, kde to začalo.

10
99:59:59,999 --> 99:59:59,999
Zatím vím jen to,
že Evropa se vám stala posedlostí.

11
00:01:14,016 --> 00:01:15,776
Jak že se jmenuje?

12
99:59:59,999 --> 99:59:59,999
Pan Fisher.

13
00:01:19,936 --> 00:01:23,796
Posedlost, pane Fishere,
je stav mysli.

14
00:01:23,836 --> 00:01:27,176
Nepochybuji, že pobyt v Evropě
byl pro vás bolestivý...

15
00:01:27,516 --> 00:01:29,366
bolestivý.

16
00:01:30,856 --> 00:01:34,756
Proto musíte vstoupit
do této části svých vzpomínek.

17
99:59:59,999 --> 99:59:59,999
Říkáte, že je pro vás
obtížné si vzpomenout.

18
99:59:59,999 --> 99:59:59,999
Ano. Ano.

19
99:59:59,999 --> 99:59:59,999
Abych vám pomohl si vzpomenout,

20
00:01:44,436 --> 00:01:47,185
dohodli jsme se,
že zkusíme hypnózu.
