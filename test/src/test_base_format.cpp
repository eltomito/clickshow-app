#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "subtitle_formats/base/base_format.h"
/*
#include "subtitles/subtitle.h"
#include "file_utils.h"
*/
#include "test_utils.h"
#include <string>
#include <cstring>

void test_one( std::string src, std::string res, bool mark = false, bool comment = false )
{
	ErrDescsT errs;
	BaseFormat bf;

	Subtitle sub;
	sub.set_disp_text( src );

	int err = bf.Morph( sub, errs );
	BOOST_TEST( (err == Errr::OK), "Base format parser failed: " << Errr::Message( err ) << "string:" << src );
	test_strings( sub.get_disp_text(), res );
	BOOST_TEST( ( sub.is_comment() == comment ), "Wrongly parsed comment! Should be " << ( comment ? "true":"false" ) );
	BOOST_TEST( ( sub.is_marked() == mark ), "Wrongly parsed mark! Should be " << ( mark ? "true":"false" ) );
};

BOOST_AUTO_TEST_CASE( Test_base_format )
{
	test_one("Nothing special.\nWhatever.","Nothing special.\nWhatever.");
	test_one("#An intelligent comment here!","#An intelligent comment here!",false,true);
	test_one("(*)A marked subtitle\nwith two lines!","A marked subtitle\nwith two lines!",true,false);
	test_one("\\(*)An escaped mark!","(*)An escaped mark!",false,false);
	test_one("\\#An esacped comment here!","#An esacped comment here!",false,false);
	test_one("(*)#A marked comment here!","#A marked comment here!",true,true);
};
