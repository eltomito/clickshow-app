#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "cmd_args.h"
#include "test_utils.h"
#include "rainbow_style.h"
#include "subtitles/subtitles.h"
#include "subtitle_formats/subtitle_formats.h"
#include <string>

void test_one_ctt( const std::string &src, const std::string &exp, const ColorSet *clrptr = NULL )
{
	const char *args[2] = {"fakename","--help"};
	ColorSet colors;
	if( clrptr ) {
		colors = *clrptr;
	} else {
		colors = {0xffffff00,0xff000000,0xff0000,0xff00};
	}

	CmdArgs ca( 2, &args[0], colors, 0x88888800 );
	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_PLAIN, SubtitleFormats::PARSE_ON, false );
	Subtitles subs;

	int err = SubtitleFormats::Parse( src.c_str(), subs, cfg );
	test_err( err );

	ErrDescsT errs;
	err = ca.UnMorphToSrtTags( subs, errs );
	test_err( err );

	std::string dst;
	err = SubtitleFormats::Serialize( subs, dst, SubtitleFormats::FMT_PLAIN );
	test_err( err );

	test_strings( dst, exp );
}

BOOST_AUTO_TEST_CASE( Test_candy_to_tags_one_line )
{
	std::string src("|0|White |1|Red |2|Green |3|Blue\n");
	std::string exp("<font color=#ffffff>White </font><font color=#ff0000>Red </font><font color=#00ff00>Green </font><font color=#0000ff>Blue</font>\n");
	test_one_ctt( src, exp );
};

BOOST_AUTO_TEST_CASE( Test_candy_to_tags_two_lines )
{
	std::string src("|0|White |1|Red |2|Green |3|Blue\n|1|Red |0|White |3||2|Green");
	std::string exp(
"<font color=#ffffff>White </font><font color=#ff0000>Red </font>\
<font color=#00ff00>Green </font><font color=#0000ff>Blue</font>\n\
<font color=#ff0000>Red </font><font color=#ffffff>White </font>\
<font color=#00ff00>Green</font>\n");
	test_one_ctt( src, exp );
};
