#include <boost/test/unit_test.hpp>

#include "shared/src/line_iterator.h"
#include "shared/src/str_line_iterator.h"
#include "test_utils.h"

BOOST_AUTO_TEST_CASE( Test_const_line_iterator )
{
	std::string str = "one\ntwo\nthree\nfour";

	const_line_iterator it( str );
	const_line_iterator endit;
	BOOST_TEST( (*it == std::string("one")) );
	++it;
	BOOST_TEST( (*it == std::string("two")) );
	++it;
	BOOST_TEST( (*it == std::string("three")) );
	++it;
	BOOST_TEST( (*it == std::string("four")) );
	++it;
	BOOST_TEST( (it == endit) );
}

BOOST_AUTO_TEST_CASE( Test_str_line_iterator )
{
	std::string str = "one\ntwo\nthree\nfour";

	str_line_iterator it( str.c_str() );
	str_line_iterator endit;
	test_strings( *it, "one" );
	++it;
	test_strings( *it, "two" );
	++it;
	test_strings( *it, "three" );
	++it;
	test_strings( *it, "four" );
	++it;
	BOOST_TEST( (it == endit) );
}

void test_it( str_line_iterator &it, int offset, int len, std::string line )
{
	BOOST_TEST( ( it.offset() == offset ), "offset - expected: " << offset << " real: " << it.offset() );
	BOOST_TEST( ( it.size() == len ), "len - expected: " << len << " real: " << it.size() );
	test_strings( *it, line );
}

BOOST_AUTO_TEST_CASE( Test_str_line_iterator_size )
{
	std::string str = "one\ntwo and two\nthree\nfour";

	str_line_iterator it( str.c_str() );
	str_line_iterator endit;
	test_it( it, 0, 3, "one");
	++it;
	test_it( it, 4, 11, "two and two");
	++it;
	test_it( it, 16, 5, "three");
	++it;
	test_it( it, 22, 4, "four");
}
