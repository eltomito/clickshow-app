#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "test_utils.h"
#include <string>
#include <cstring>

BOOST_AUTO_TEST_CASE( Test_err_desc )
{
	ErrDesc ed( 0, 0, "", ErrDesc::LOC_LINE );
	test_strings( ed.ToString(), "line 0: Ok" );
};

BOOST_AUTO_TEST_CASE( Test_err_descs )
{
	ErrDescsT errs;
	errs.push_back( ErrDesc( 0, 0, "", ErrDesc::LOC_LINE ) );
	test_strings( ErrDesc::DescsToString( errs ), "line 0: Ok" );
	errs.push_back( ErrDesc( Errr::FAILED, 22, "Something bad here.", ErrDesc::LOC_SUB ) );
	test_strings( ErrDesc::DescsToString( errs ), "line 0: Ok\nsubtitle 22: Failed: \"Something bad here.\"" );
	errs.push_back( ErrDesc( Errr::SUB_FMT_TIMING, 40, "12:20 - 13:30", 555 ) );
	test_strings( ErrDesc::DescsToString( errs ), "\
line 0: Ok\n\
subtitle 22: Failed: \"Something bad here.\"\n\
location 40: Timing not understood: \"12:20 - 13:30\"\
" );
};
