#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "subtitle_formats/subtitle_formats.h"
#include "subtitle_formats/multi/multi_format.h"
#include "subtitles/subtitle.h"
#include "subtitles/subtitles.h"
#include "test_utils.h"
#include <string>

BOOST_AUTO_TEST_CASE( Test_multi_merge )
{
	MultiFormat mf;
	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_PLAIN, SubtitleFormats::PARSE_OFF, false );
	Subtitles subs1,subs2;
	const char *src1="one\n\ntwo\n\nthree\nanother three\n\nfour\n\nfive\n";
	const char *src2="eins\n\nzwei\n\ndrei\nnochmal drei\n\nvier\n\nfünf\n";
	int err = SubtitleFormats::Parse( src1, subs1, cfg );
	BOOST_TEST_REQUIRE( (!err), "Subtitle format parser failed: " << Errr::Message( err ) << "string:" << src1 );
	err = SubtitleFormats::Parse( src2, subs2, cfg );
	BOOST_TEST_REQUIRE( (!err), "Subtitle format parser failed: " << Errr::Message( err ) << "string:" << src2 );

	std::vector<Subtitles *> list;
	list.push_back( &subs1 );
	list.push_back( &subs2 );

	Subtitles dst;
	err = mf.MergeSubtitles( dst, list );
	BOOST_TEST_REQUIRE( (!err), "Merging 2 subtitles failed!" );
	test_strings( dst.get_subtitle(0)->get_raw_text(), "one\n===\neins" );
	test_strings( dst.get_subtitle(1)->get_raw_text(), "two\n===\nzwei" );
	test_strings( dst.get_subtitle(2)->get_raw_text(), "three\nanother three\n===\ndrei\nnochmal drei" );
	test_strings( dst.get_subtitle(3)->get_raw_text(), "four\n===\nvier" );
	test_strings( dst.get_subtitle(4)->get_raw_text(), "five\n===\nfünf" );
};
