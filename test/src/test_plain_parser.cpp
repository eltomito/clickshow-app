#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "shared/src/err_desc.h"
#include "subtitle_formats/timing/plain.h"
#include "subtitles/subtitles.h"
#include "subtitles/subtitle.h"
#include "test_utils.h"
#include "file_utils.h"
#include <cstring>

void plain_file_back_and_forth( const char *fname )
{
	size_t len;
	int err;

	const char *src = FileUtils::read_file( fname, len, err, true );
	BOOST_TEST( src, "File not found: \"" << fname << "\"" );
	if( !src ) { return; }

	ErrDescsT error_list;
	Subtitles subs;
	err = PlainParser::Parse( src, subs, error_list );
	BOOST_TEST_REQUIRE( ( err == Errr::OK ), "Parser failed: " << Errr::Message( err ) );
	std::string dst;
	err = PlainParser::Serialize( subs, dst );
	BOOST_TEST_REQUIRE( ( err == Errr::OK ), "Serializer failed: " << err << ": " << Errr::Message( err ) );
	BOOST_TEST_REQUIRE( ( strlen( src ) == dst.size() ), "Serialized size differs: src = " << strlen( src ) << " dst = " << dst.size() ); 
	test_cstrings( src, dst.c_str() );

	free( (void *)src );
}

void test_parser( const std::string &src, int num_subs, const char **dst )
{
	ErrDescsT error_list;
	Subtitles subs;

	int err = PlainParser::Parse( src.c_str(), subs, error_list );

	BOOST_TEST( (err == Errr::OK), "Parser returned an error." );
	BOOST_TEST( (subs.size() == num_subs), "subs.size() != " << num_subs );
	Subtitle *s;
	for( int i = 0; i < num_subs; ++i ) {
		s = subs.get_subtitle( i );
		BOOST_TEST( (s != NULL), "Cannot access subtitle #" << i << "!" );
		if( s ) {
			test_cstrings( dst[i], s->get_raw_text().c_str() );
		}
	}
};

BOOST_AUTO_TEST_CASE( Test_plain_parser_load_one_subtitle )
{
	const char *res[1] = {"first line\nsecond line"};
	test_parser("first line\nsecond line\n", 1, res );
};

BOOST_AUTO_TEST_CASE( Test_plain_parser_load_empty_string )
{
	test_parser("", 0, NULL );
};

BOOST_AUTO_TEST_CASE( Test_plain_parser_load_several_subtitles )
{
	const char *src = "\
1 first line\n1 second line\n\n\
2 first line\n2 second line\n\n\
3 first line\n\n\
4 first line\n4 second line\n\n\
5 first line\n";

	const char *res[5] = {
		"1 first line\n1 second line",
		"2 first line\n2 second line",
		"3 first line",
		"4 first line\n4 second line",
		"5 first line"};

	test_parser(src, 5, res );
};

BOOST_AUTO_TEST_CASE( Test_plain_parser_trimming )
{
	const char *src = "\
 1 first line\n 1 second line\n\n\
   2 first line\n   2 second line\n\n\
\t3 first line\n\n\
\t \t  4 first line\n\t\t\t\t4 second line\n\n\
\t\xC2\xA0\t5 first line\n";	//0xC2 0xA0 is non-breakable space

	const char *res[5] = {
		"1 first line\n1 second line",
		"2 first line\n2 second line",
		"3 first line",
		"4 first line\n4 second line",
		"\xC2\xA0\t5 first line"};

	test_parser(src, 5, res );
};

BOOST_AUTO_TEST_CASE( Test_plain_parse_and_serialize_file )
{
	plain_file_back_and_forth( "../../src/plain_test_files/test-1.txt" );
};
