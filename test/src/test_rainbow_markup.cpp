#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "rainbow_markup.h"
#include "rainbow_style.h"
#include "shared/src/str_utils.h"
#include <string>

std::string &MarkupToString( std::string &dst, const RainbowMarkup &m )
{
	RainbowMarkup::const_iterator it = m.begin();
	RainbowMarkup::const_iterator endit = m.end();
	if( it == endit ) { return dst; }
	while( it != endit ) {
		StrUtils::Printf( dst, "[%i,%u ],", it->start(), it->color() );
		++it;
	}
	dst.erase( dst.size() - 1 );
	return dst;
};

std::string MarkupToString( const RainbowMarkup &m )
{
	std::string dst;
	return MarkupToString( dst, m );
};

bool test_markups( const RainbowMarkup &a, const RainbowMarkup &b )
{
	bool res = RainbowMarkupUtils::AreSame( a, b );
	BOOST_TEST( res, "Markups differ: A) " << MarkupToString( a ) << ", B) " << MarkupToString( b ) );
	return res;
};

BOOST_AUTO_TEST_CASE( Test_Rainbow_Markup )
{
	RainbowMarkup a,b,c,d;
	a.push_back( RainbowStyle(0,0) );
	a.push_back( RainbowStyle(10,1) );
	a.push_back( RainbowStyle(20,2) );
	b.push_back( RainbowStyle(0,0) );
	b.push_back( RainbowStyle(10,1) );
	b.push_back( RainbowStyle(20,2) );
	test_markups( a, b );

	c.push_back( RainbowStyle(0,0) );
	c.push_back( RainbowStyle(7,1) );
	c.push_back( RainbowStyle(14,2) );
	RainbowMarkupUtils::EraseString( b, 7, 6 );
	test_markups( b, c );

	c.push_back( RainbowStyle(0,0) );
	c.push_back( RainbowStyle(7,1) );
	c.push_back( RainbowStyle(14,2) );
	RainbowMarkupUtils::InsertString( b, 7, 3 );
	RainbowMarkupUtils::InsertString( b, 11, 3 );
	test_markups( a, b );
};
