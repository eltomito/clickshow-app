#include <boost/test/unit_test.hpp>

#include "rainbow.h"
#include "test_utils.h"
#include <cstring>

#include "rainbow_converter.h"

BOOST_AUTO_TEST_CASE( Test_simple_walk )
{
	RainbowMarkup m;
	m.push_back( RainbowStyle( 0, 0xaabbcc00, 0, RainbowStyle::M_COLOR ) );
	RainbowWalker rw( m );
	
	RainbowStyle initStyle( 0, 0xffffff00, 0, RainbowStyle::M_COLOR );
	rw.start( 0, 10, initStyle );
	BOOST_TEST( (rw.next()), "walker.next() failed!" );
	BOOST_CHECK_EQUAL( rw.style.start(), 0 );
	BOOST_CHECK_EQUAL( rw.spanLen, 10 );
	BOOST_TEST( (rw.style == RainbowStyle( 0, 0xaabbcc00, 0, RainbowStyle::M_COLOR ) ) );
};

BOOST_AUTO_TEST_CASE( Test_long_walk )
{
	RainbowMarkup m;
/*
0 start: 0, use: 16, features: 16, color: ffffff00
1 start: 15, use: 16, features: 16, color: ff
2 start: 39, use: 1, features: 1, color: 0
3 start: 55, use: 1, features: 0, color: 0
*/
	m.push_back( RainbowStyle( 0, 0xffffff00, RainbowStyle::M_COLOR, RainbowStyle::M_COLOR ) );
	m.push_back( RainbowStyle( 15, 0xff, RainbowStyle::M_COLOR, RainbowStyle::M_COLOR ) );
	m.push_back( RainbowStyle( 39, 0, RainbowStyle::M_BOLD, RainbowStyle::M_BOLD ) );
	m.push_back( RainbowStyle( 55, 0, 0, RainbowStyle::M_BOLD ) );
	RainbowWalker rw( m );

	RainbowStyle initStyle( 0, 0x000000ff, 0, RainbowStyle::M_ALL );
	rw.start( 0, 15, initStyle );
	BOOST_TEST( (rw.next()) );
	BOOST_TEST( (rw.next()) );
	BOOST_TEST( (!rw.next()) );

	rw.start( 16, 38, initStyle );
	BOOST_TEST( (rw.next()) );
	BOOST_TEST( (!rw.next()) );

	rw.start( 39, 55, initStyle );
	BOOST_TEST( (rw.next()) );
	BOOST_TEST( (rw.next()) );
	BOOST_TEST( (!rw.next()) );
};

BOOST_AUTO_TEST_CASE( Test_rainbow_convert ) {
	const char *str = "\
One white line.\n\
Second uncolored line.\n\
Third bold line.";

	const char *exp = "\
<font color=#ffffff>One white line.</font><font color=#888888></font>\n\
<font color=#888888>Second uncolored line.</font>\n\
<font color=#888888><b>Third bold line.</b></font>";

	RainbowMarkup m;
/*
0 start: 0, use: 16, features: 16, color: ffffff00
1 start: 15, use: 16, features: 16, color: ff
2 start: 39, use: 1, features: 1, color: 0
3 start: 55, use: 1, features: 0, color: 0
*/
	m.push_back( RainbowStyle( 0, 0xffffff00, RainbowStyle::M_COLOR, RainbowStyle::M_COLOR ) );
	m.push_back( RainbowStyle( 15, 0xff, RainbowStyle::M_COLOR, RainbowStyle::M_COLOR ) );
	m.push_back( RainbowStyle( 39, 0, RainbowStyle::M_BOLD, RainbowStyle::M_BOLD ) );
	m.push_back( RainbowStyle( 55, 0, 0, RainbowStyle::M_BOLD ) );

	std::vector<Fl_Color> colors = { 0x00ff0000, 0xffff0000, 0x00ffff00, 0xffffff };

	RainbowStyle initStyle( 0, 0x000000ff, 0, RainbowStyle::M_ALL );
	SrtTagsRC rc( colors, 0x88888800 );

	rainbow_convert( str, m, initStyle, rc ); 

	int res = rc.getText().compare( exp );
	BOOST_CHECK_EQUAL( 0, res );
};


