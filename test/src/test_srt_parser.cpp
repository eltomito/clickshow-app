#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "subtitle_formats/timing/srt.h"
#include "shared/src/err_desc.h"
#include "subtitles/subtitles.h"
#include "subtitles/subtitle.h"
#include "test_utils.h"
#include "file_utils.h"

class QuickSub {
public:
	QuickSub( int _sH, int _sM, int _sS, int _sm, int _eH, int _eM, int _eS, int _em, std::string _txt ) :
		start( (_sH*3600 + _sM*60 + _sS)*1000 + _sm ), end( (_eH*3600 + _eM*60 + _eS)*1000 + _em ), txt(_txt) {};
	~QuickSub() {};

	void test( const Subtitle &sub ) {
		BOOST_TEST( ( txt == sub.get_raw_text() ), "The text is: \n" << sub.get_raw_text() << "\n" );
		BOOST_TEST( ( sub.get_timing().GetStart() == start ), "bad start! " << sub.get_timing().GetStart() << " != " << start );
		BOOST_TEST( ( sub.get_timing().GetEnd() == end ), "bad end!" << sub.get_timing().GetEnd() << " != " << end );
	};

	void test( Subtitles &subs, int num ) {
		Subtitle *s = subs.get_subtitle( num );
		BOOST_TEST( s, "Subtitle not found!: " << num );
		test( *s );
	};

	long start;
	long end;
	std::string txt;
};

void srt_file_back_and_forth( const char *fname )
{
	size_t len;
	int err;

	const char *src = FileUtils::read_file( fname, len, err, true );
	BOOST_TEST( src, "File not found: \"" << fname << "\"" );
	if( !src ) { return; }

	ErrDescsT error_list;
	Subtitles subs;
	err = SrtParser::Parse( src, subs, error_list );
	BOOST_TEST_REQUIRE( ( err == Errr::OK ), "Parser failed: " << Errr::Message( err ) );
	std::string dst;
	err = SrtParser::Serialize( subs, dst );
	BOOST_TEST_REQUIRE( ( err == Errr::OK ), "Serializer failed: " << err << ": " << Errr::Message( err ) );
	BOOST_TEST_REQUIRE( ( strlen( src ) == dst.size() ), "Serialized size differs: src = " << strlen( src ) << " dst = " << dst.size() ); 
	test_cstrings( src, dst.c_str() );

	free( (void *)src );
}

BOOST_AUTO_TEST_CASE( Test_srt_parser_load_one_subtitle )
{
	std::string src = "1\n12:34:56,789 --> 98:56:54,321\nfirst line\nsecond line\n";

	ErrDescsT error_list;
	Subtitles subs;

	int err = SrtParser::Parse( src.c_str(), subs, error_list );

	BOOST_TEST( (err == Errr::OK), "Parser returned an error." );
	BOOST_TEST( (subs.size() == 1), "subs.size() != 1" );
	Subtitle *s = subs.get_subtitle( 0 );
	BOOST_TEST( (s != NULL), "No subtitle created!" );
	if( s ) {
		QuickSub qs( 12,34,56,789,98,56,54,321,"first line\nsecond line");
		qs.test( *s );
	}
}

BOOST_AUTO_TEST_CASE( Test_srt_parser_load_five_subtitles )
{
	std::string src("\
1\n00:00:00,000 --> 00:00:01,001\n1 first line\nsecond line\n\n\
2\n00:00:01,010 --> 00:00:02,200\n2 first line\n\n\
3\n00:00:03,000 --> 00:00:04,001\n3 first line\nsecond line\n\n\
4\n00:00:28,123 --> 00:00:29,456\n4 first line\nsecond line\n\n\
5\n01:05:50,555 --> 10:06:41,999\n5 first line\n");

	QuickSub s1( 0,0,0,0,0,0,1,1,"1 first line\nsecond line");
	QuickSub s2( 0,0,1,10,0,0,2,200,"2 first line");
	QuickSub s3( 0,0,3,0,0,0,4,1,"3 first line\nsecond line");
	QuickSub s4( 0,0,28,123,0,0,29,456,"4 first line\nsecond line");
	QuickSub s5( 1,5,50,555,10,6,41,999,"5 first line");

	ErrDescsT error_list;
	Subtitles subs;

	int err = SrtParser::Parse( src.c_str(), subs, error_list );

	BOOST_TEST( (err == Errr::OK), "Parser returned an error." );
	BOOST_TEST( (subs.size() == 5), "subs.size() != 5" );

	s1.test( subs, 0 );
	s2.test( subs, 1 );
	s3.test( subs, 2 );
	s4.test( subs, 3 );
	s5.test( subs, 4 );
}

BOOST_AUTO_TEST_CASE( Test_srt_parser_parse_and_serialize )
{
	std::string src = "1\n12:34:56,789 --> 98:54:54,321\nfirst line\nsecond line\n";

	ErrDescsT error_list;
	Subtitles subs;

	int err = SrtParser::Parse( src.c_str(), subs, error_list );
	BOOST_TEST( (err == Errr::OK), "Parser returned an error." );
	if( !err ) {
		std::string dst;
		err = SrtParser::Serialize( subs, dst );
		BOOST_TEST_REQUIRE( (err == Errr::OK), "Serializer returned an error." );
		test_strings( src, dst );
	}
}

BOOST_AUTO_TEST_CASE( Test_srt_parser_parse_and_serialize_files )
{
	srt_file_back_and_forth( "../../src/srt_test_files/test-1.srt" );
	srt_file_back_and_forth( "../../src/srt_test_files/test-2-with-untimed.srt" );
}
