#include <boost/test/unit_test.hpp>

#include "shared/src/err.h"
#include "subtitle_formats/subtitle_formats.h"
#include "subtitles/subtitle.h"
#include "subtitles/subtitles.h"
#include "rainbow_style.h"
#include "rainbow_markup.h"
/*
#include "file_utils.h"
*/
#include "test_utils.h"
#include <string>
#include <cstring>

class PlainSub {
public:
	PlainSub( std::string _txt, bool _mark, bool _comment, RainbowMarkup *_markup = NULL ) : txt(_txt), mark(_mark), comment(_comment), markup(_markup) {};
	~PlainSub() {};

	void test( const Subtitle &sub, bool testmarkup = false ) {
		test_strings( txt, sub.get_disp_text() );
		BOOST_TEST( ( sub.is_marked() == mark ), "The marked state should be " << (mark ? "true":"false") );
		BOOST_TEST( ( sub.is_comment() == comment ), "The comment state should be " << (comment ? "true":"false") );
		if( testmarkup ) {
			BOOST_TEST( ( markup != NULL ), "Do you really want to test markup against NULL???" );
			BOOST_TEST( RainbowMarkupUtils::AreSame( sub.get_markup(), *markup ), "Markups differ for \"" << txt << "\"" );
		}
	};

	void test( Subtitles &subs, int num, bool testmarkup = false ) {
		Subtitle *s = subs.get_subtitle( num );
		BOOST_TEST( s, "Subtitle not found!: " << num );
		test( *s, testmarkup );
	};

	std::string txt;
	bool mark;
	bool comment;
	RainbowMarkup *markup;
};

class TimedSub : public PlainSub {
public:
	TimedSub( int _sH, int _sM, int _sS, int _sm, int _eH, int _eM, int _eS, int _em, std::string _txt, bool _mark, bool _comment ) :
		PlainSub( _txt, _mark, _comment ), start( (_sH*3600 + _sM*60 + _sS)*1000 + _sm ), end( (_eH*3600 + _eM*60 + _eS)*1000 + _em ) {};
	~TimedSub() {};

	void test( const Subtitle &sub ) {
		PlainSub::test( sub );
		BOOST_TEST( ( sub.get_timing().GetStart() == start ), "bad start! " << sub.get_timing().GetStart() << " != " << start );
		BOOST_TEST( ( sub.get_timing().GetEnd() == end ), "bad end!" << sub.get_timing().GetEnd() << " != " << end );
	};

	void test( Subtitles &subs, int num ) {
		Subtitle *s = subs.get_subtitle( num );
		BOOST_TEST( s, "Subtitle not found!: " << num );
		if( s ) {
			test( *s );
		}
	};

	long start;
	long end;
};

void test_plain( std::string src, std::string txt, bool mark = false, bool comment = false, RainbowMarkup *markup = NULL, bool parsecandy = false, bool parsetags = false )
{
	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_PLAIN, parsecandy ? SubtitleFormats::PARSE_ON : SubtitleFormats::PARSE_OFF, parsetags );
	Subtitles subs;
	int err = SubtitleFormats::Parse( src.c_str(), subs, cfg );
	BOOST_TEST( (!err), "Subtitle format parser failed: " << Errr::Message( err ) << "string:" << src );
	if( !err ) {
		PlainSub ps( txt, mark, comment, markup );
		ps.test( subs, 0, parsecandy||parsetags );
	}
};
 
void test_timed( std::string src, int sH, int sM, int sS, int sm, int eH, int eM, int eS, int em,
						std::string txt, bool mark = false, bool comment = false )
{
	SubtitleFormats::ParserCfg cfg( SubtitleFormats::FMT_SRT, SubtitleFormats::PARSE_OFF, false );
	Subtitles subs;
	int err = SubtitleFormats::Parse( src.c_str(), subs, cfg );
	BOOST_TEST( (!err), "Subtitle format parser failed: " << Errr::Message( err ) << "string:" << src );
	if( !err ) {
		TimedSub ps( sH,sM,sS,sm,eH,eM,eS,em, txt, mark, comment );
		ps.test( subs, 0 );
	}
};

void test_error( std::string src, bool srt, int exp_err )
{
	SubtitleFormats::ParserCfg cfg( srt ? SubtitleFormats::FMT_SRT : SubtitleFormats::FMT_PLAIN, SubtitleFormats::PARSE_OFF, false );
	Subtitles subs;
	int err = SubtitleFormats::Parse( src.c_str(), subs, cfg );
	BOOST_TEST( (err == exp_err), "bad parse error. Expected \"" << Errr::Message( exp_err ) << "\", actual: \"" << Errr::Message( err ) << "\"" );
};

BOOST_AUTO_TEST_CASE( Test_subtitle_formats_plain )
{
	test_plain("Nothing special.\nWhatever 1.","Nothing special.\nWhatever 1.");
	test_plain("#An intelligent comment here!","#An intelligent comment here!",false,true);
	test_plain("(*)A marked subtitle\nwith two lines!","A marked subtitle\nwith two lines!",true,false);
	test_plain("\\(*)An escaped mark!","(*)An escaped mark!",false,false);
	test_plain("\\#An esacped comment here!","#An esacped comment here!",false,false);
	test_plain("(*)#A marked comment here!","#A marked comment here!",true,true);
};

BOOST_AUTO_TEST_CASE( Test_subtitle_formats_srt )
{
	test_timed("1\n01:02:03,456 --> 10:20:30,987\nNothing special.\nWhatever 2.",1,2,3,456,10,20,30,987,"Nothing special.\nWhatever 2.");
	test_timed("1\n01:02:03,456 --> 10:20:30,987\n#An intelligent comment here!",1,2,3,456,10,20,30,987,"#An intelligent comment here!",false,true);
	test_timed("1\n01:02:03,456 --> 10:20:30,987\n(*)A marked subtitle\nwith two lines!",1,2,3,456,10,20,30,987,"A marked subtitle\nwith two lines!",true,false);
	test_timed("1\n01:02:03,456 --> 10:20:30,987\n\\(*)An escaped mark!",1,2,3,456,10,20,30,987,"(*)An escaped mark!",false,false);
	test_timed("1\n01:02:03,456 --> 10:20:30,987\n\\#An esacped comment here!",1,2,3,456,10,20,30,987,"#An esacped comment here!",false,false);
	test_timed("1\n01:02:03,456 --> 10:20:30,987\n(*)#A marked comment here!",1,2,3,456,10,20,30,987,"#A marked comment here!",true,true);
};

BOOST_AUTO_TEST_CASE( Test_subtitle_formats_errors )
{
	test_error("01:02:03,456 --> 10:20:30,987\nNothing special.\nWhatever 3.", true, Errr::SUB_FMT_GARBAGE);
	test_error("2147483647\n01:02:03,456 --> 10:20:30,987\nNothing special.\nWhatever 4.", true, Errr::OK);
	test_error("2147483648\n01:02:03,456 --> 10:20:30,987\nNothing special.\nWhatever 5.", true, Errr::SUB_FMT_GARBAGE);
	test_error("1\n 01:02:03,456 --> 10:20:30,987\nNothing special.\nWhatever 6.", true, Errr::SUB_FMT_TIMING);
	test_error("1\n01:02:03,456 --> 10:20:30,987 \nNothing special.\nWhatever 7.", true, Errr::SUB_FMT_TIMING);
	test_error("1\na01:02:03,456 --> 10:20:30,987\nNothing special.\nWhatever 8.", true, Errr::SUB_FMT_TIMING);
	test_error("1\n01:02:03,456 -->10:20:30,987\nNothing special.\nWhatever 9.", true, Errr::SUB_FMT_TIMING);
	test_error("1\n01:60:03,456 --> 10:20:30,987\nNothing special.\nWhatever 10.", true, Errr::SUB_FMT_TIMING);
	test_error("1\n01:02:60,456 --> 10:20:30,987\nNothing special.\nWhatever 11.", true, Errr::SUB_FMT_TIMING);
	test_error("1\n01:02:03,456 --> 10:60:30,987\nNothing special.\nWhatever 12.", true, Errr::SUB_FMT_TIMING);
	test_error("1\n01:02:03,456 --> 10:20:60,987\nNothing special.\nWhatever 13.", true, Errr::SUB_FMT_TIMING);
};

BOOST_AUTO_TEST_CASE( Test_subtitle_formats_plain_candy )
{
	RainbowMarkup markup1;
	markup1.push_back( RainbowStyle( 0, 0, 0, RainbowStyle::M_COLOR ) );
	RainbowMarkup markup2;
	markup2.push_back( RainbowStyle( 0, 0, 0, RainbowStyle::M_COLOR ) );
	markup2.push_back( RainbowStyle( 8, 1, 0, RainbowStyle::M_COLOR ) );
	markup2.push_back( RainbowStyle( 17, 2, 0, RainbowStyle::M_COLOR ) );
	test_plain("|0|Nothing special.\nWhatever 14.","Nothing special.\nWhatever 14.", false, false, &markup1, true, false );
	test_plain("|0|Nothing |1|special.\n|2|Whatever 15.","Nothing special.\nWhatever 15.", false, false, &markup2, true, false );
};

BOOST_AUTO_TEST_CASE( Test_subtitle_formats_plain_multi )
{
	RainbowMarkup markup1;
	markup1.push_back( RainbowStyle( 0, 0 ) );
	markup1.push_back( RainbowStyle( 18, 1 ) );
	test_plain("Nothing special.\n===\nNichts spezial.\n","Nothing special.\nNichts spezial.", false, false, &markup1, false, false );
};
