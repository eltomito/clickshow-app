#define BOOST_TEST_MODULE TextEncoder test
#include <boost/test/unit_test.hpp>
//#include <boost/test/included/unit_test.hpp>

#include "text_encoder.h"
#include "file_utils.h"
#include "shared/src/line_iterator.h"

void back_and_forth( const char *test_str, const char *converted_str, const char *nl, bool utf8 = true, const char *back_converted = NULL ) {
	TextEncodingInfo info;
	TextEncoder::GetInfo( test_str, 0, info );

	bool unixnl = ( 0 == strcmp( nl, "\n" ) );

	BOOST_TEST( info.IsUtf8() == utf8 );
	BOOST_TEST( info.IsNewlineUnix() == unixnl );
	BOOST_TEST( info.Newline() == nl );

	int err;
	char *dst = TextEncoder::ToNormal( test_str, 0, info, err );
	BOOST_TEST( dst );
	BOOST_TEST( 0 == strcmp( dst, converted_str ) );

	std::string dst_string = dst;
	size_t len;
	char *orig = TextEncoder::ToOriginal( dst_string, info, len, err );
	BOOST_TEST( err == 0 );

	if( back_converted == NULL ) { back_converted = test_str; };

	BOOST_TEST( 0 == strcmp( orig, back_converted ), "\"" << test_str << "\" != \"" << orig << "\"" );
};

void file_back_and_forth( const char *fname )
{
	size_t len;
	int err;

	const char *src = FileUtils::read_file( fname, len, err, true );
	BOOST_TEST( src, "File not found: \"" << fname << "\"" );
	if( !src ) { return; }

	TextEncodingInfo info;
	TextEncoder::GetInfo( src, 0, info );

	BOOST_TEST_WARN(( info.IsUtf8() || info.CanUtf8() ), "Can't test file conversion: \"" << fname << "\"" );
	if( info.IsUtf8() || info.CanUtf8() )
	{
		char *dst = TextEncoder::ToNormal( src, 0, info, err );
		BOOST_TEST( dst, "Can't convert file: \"" << fname << "\"" );
	
		if( dst ) {
			std::string dst_string = dst;
			size_t len;
			char *back = TextEncoder::ToOriginal( dst_string, info, len, err );
			BOOST_TEST( back, "Can't convert file back: \"" << fname << "\"" );
			if( back ) {
				BOOST_TEST( 0 == strcmp( back, src ), "\"" << back << "\"\n--------- != ------------\n\"" << src << "\"" );
			}
		}
	}
	free( (void *)src );
}

BOOST_AUTO_TEST_CASE( TextEncoder_conversion_back_and_forth )
{
	back_and_forth( "Ahoj!\n\rVole!\n\r", "Ahoj!\nVole!\n", "\n\r" );
	back_and_forth( "Ahoj!\r\nVole!\r\n", "Ahoj!\nVole!\n", "\r\n" );
	back_and_forth( "Ahoj!\rVole!\r", "Ahoj!\nVole!\n" , "\r");
	back_and_forth( "Ahoj!\n\rVole!", "Ahoj!\nVole!", "\n\r" );
	back_and_forth( "Ahoj!\r\nVole!", "Ahoj!\nVole!", "\r\n" );
	back_and_forth( "\r\nAhoj!\rVole!\n\rJo jo jo.\n", "\nAhoj!\nVole!\nJo jo jo.\n", "\r\n", true, "\r\nAhoj!\r\nVole!\r\nJo jo jo.\r\n" );
	back_and_forth( "\r\n\r\nAhoj!\r\n\r\n\r\nVole!\r\nJo jo jo.", "\n\nAhoj!\n\n\nVole!\nJo jo jo.", "\r\n" );
}

BOOST_AUTO_TEST_CASE( TextEncoder_file_conversion_back_and_forth )
{
	file_back_and_forth("../../src/encoding_test_files/test_utf8_crlf.txt");
}
