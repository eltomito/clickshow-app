#!/usr/bin/ruby
# encoding: utf-8

# This file is part of ClickShow
# Copyright 2013-2019 Tomáš Pártl, tomaspartl@centrum.cz
#
# ClickShow is free software: you can redistribute it and/or modify
# the Free Software Foundation, either version 3 of the License, or
# it under the terms of the GNU General Public License as published by
# (at your option) any later version.
#
# ClickShow is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with ClickShow.  If not, see <https://www.gnu.org/licenses/>.

#=================================================
#
# extract translatable strings from c/c++ sources
#
#=================================================

#
# counts the languages in a dictionary
#
def get_lang_count( dict )

	keys = dict.keys
	if( keys.size ==  0 )
		return 0		#the dictionary is empty
	end

	langs = 0

	keys.each do |key|

		val = dict[ key ]

		if( val != nil )

			if( val.size > langs )
				langs = val.size
			end

		end

	end

	return langs

end

#
# takes a dictionary and turns it into a translation template
#
def keys_to_pot( dict )

	pot = String.new()
	
	dict.each do | key, val |
	
		pot += "#\nmsgid \"#{key}\"\nmsgstr \"\"\n\n"
	
	end

	return pot

end

#
# turns an array of language names into a cpp definition of the same
#
#	languages - the array of language names
#	varname - the name of the variable to assign language names to
#
# returns:
#	a string with the cpp code
#
def langs_to_cpp( languages, varname, default_lang = "English" )

	resdef = "#define #{varname} { "

	resdef += "\"#{default_lang}\", "

	languages.each do |lname|

		resdef += "\"#{lname}\", "

	end

	#delete the last ","
	#resdef = resdef[0...-1]

	resdef += "NULL };\n"

	return resdef

end

#
# turns a dictionary into a c-language constant definition
#
# parameters:
#	dictionary - the dictionary to convert
#	varname - the name of the variable to assign the dictionary contents to
#
# returns:
#	a string with the variable definition
#
def dict_to_cpp( dict, varname )

	resdef = "#define #{varname} { "

	dict.each do |key,value|
	
		resdef += "\"#{key}\", "

		if( value.size > 0 )
			resdef += "\""
			resdef += value.join("\", \"")
			resdef += "\",\\\n"				#"
		end

	end

	#delete the last ","
	#resdef = resdef[0...-2]

	#close the array definition
	resdef += " NULL };\n"

	return resdef

end

#
# makes a .po file for the given language index
#
# arguments:
#	dict - the dictionary to use
#	lang_i - the language index to extract
#
# returns:
#	a string representing the .po file for the given language or nil if a negative lang_id was given
#
def dict_to_po( dict, lang_i )

	if( lang_i < 0 )
		return nil
	end

	postr = ""

	dict.each do | key, val |

		if( lang_i < val.size )
		
			trans = val[ lang_i ]
			if( trans == nil )
				trans = ""
			end
		
		else
			trans = ""
		end

		postr += "#\nmsgid \"#{key}\"\nmsgstr \"#{trans}\"\n\n"

	end

	return postr

end

#
# determines language name from a given .po file content
#
#	parameters:
#		postr - .po file as a string
#
#	returns
#		language name given in postr as "#language: language_name"
#
def po_to_langname( postr )

		if( postr == nil )
			return nil
		end

		i = /^#[ \t]*language:[ \t]*[^ \t]+$/ =~ postr	#"
		if( i == nil )
			return nil
		end
		
		k = /^#[ \t]*language:[ \t]*/ =~ postr[i..-1]

		j = i+ $~.end(0)
		l = /\n/ =~ postr[ j..-1 ]

		if( l == nil )
			l = -1
		else
			l += j-1
		end

		lname = postr[ j..l ].strip

		return lname

end

#
# finds the end of a quoted string correctly parsing escape characters ( '\"', '\\', etc. )
#
# returns: index of the terminating "
#
def find_quoted_string_end( str, ignore_first_char = true )

	if( ignore_first_char )
		i = 1
	else
		i = 0
	end

	e = -1		#end index

	while(( e == -1 )&&( i > -1 ))

		#find the next " character
		j = /\"/ =~ str[i..-1]		#"

		if( i == nil )
			i = -1	#no quote character found
		else

			i += j

			#how many backslashes precede this quote? 
			run_s = i -1
			while( run_s >= 0 )&&( str[run_s]=='\\' )
				run_s -= 1
			end

			#if zero or an even number of backlashes precede the quote, it is valid
			slashcount = i - run_s -1
			if(( slashcount.even? )||( slashcount == 0 ))
				#terminating quote found
				e = i
			else
				i += 1
			end
		
		end	#quote character found?

	end	#the while loop
	return e
end

#
# inserts a translation from a po file into a dictionary as a language of the given number
#
# paramteres:
# 	dict - the hash serving as the dictionary to insert to
# 	postr - a string with the contents of a .po file
# 	lang_i - the language index to insert this translation at
#
# returns:
# 	the dictionary
#
def po_to_dict( dict, postr, lang_i )

	while( postr != nil )
	
		i = /msgid \"[^\"]+\"/ =~ postr	#"
		if( i != nil )

			j = find_quoted_string_end( postr[i+6..-1] )

			if( j == nil )
				puts "no terminating quote found on msgid!"
				return
			end

			j += i+6

			#extract the key

			k = /\"/ =~ postr[i...j]		#"
			k += i+1
			key = postr[ k...j ]

			#extract the translated string
			l = /msgstr \"[^\"]+\"/ =~ postr[ j..-1 ]		#"
			if( l== nil )

				puts "no msgstr found!"
				return

			end

			l += j
			m = find_quoted_string_end( postr[l+7..-1] )

			if( m == nil )
			
				puts "no terminating string found on msgstr!"
				return
			end

			m += l+7

			n = /\"/ =~ postr[l...m]		#"
			n += l+1
			val = postr[ n...m ]

			#add the new translated string
			arr = dict[ key ]
			if( arr == nil )
				arr = Array.new
			end

			arr[ lang_i ] = val
			dict[ key ] = arr

			postr = postr[ m..-1 ]
			
		else
			#no msgid found
			postr = nil
		end
	end

	return dict

end

#
# adds translatable strings found in src to dict
# parameters:
#		dict = a Hash serving as a dictionary
#		src = a program source
#
# returns:
#		returns the modified dictionary
#
def keys_from_source( dict, src )

	while( src != nil )

		#i = /_\(\"[^\"]+\"\)/ =~ src		#"
		i = /_\(\"/ =~ src	#"

		if( i != nil )

			#j = $~.end(0)
			j = find_quoted_string_end( src[i+2..-1] )
		
			if( j != nil )

				j += i+2

				newkey = src[ i+3...j ]
				dict[ newkey ] = Array.new
				src = src[j+1..-1]

			else
				#unterminated string
				src = nil
			end

		else
			src = nil
		end

	end

	return dict

end

#
#
#
def generate_pofiles( dict, languages, dirname, basename )

	num_langs = get_lang_count( dict )

	puts "num langs = " + num_langs.to_s

	if( num_langs < 1 )
		return 0		#no languages in this dictionary
	end

	for i in 0...num_langs
	
		path = dirname + "/" + '%03d' % i + "-" + basename +  ".po"
		f = File.new( path, "w" )
		if( f != nil )

			#write the file
			if( languages[ i ] != nil )
				potext = "#language: " + languages[i] + "\n\n"
			else
				potext = ""
			end
			potext += dict_to_po( dict, i )
			f.write( potext )
			f.close
			
		end

	end

end

#
# reads all files in the given directory into the dictionary
#
# returns: number of files read (= the number of language translations available)
#				or -1 if error
#
def gobble_pofiles( dict, languages, dirname )

	if( !File.directory?( dirname ) )
		return -1
	end

	num_langs = 0

	pofiles = Dir.entries( dirname )
	
	# delete all backup files (that end with a '~')
	pofiles.delete_if {|fname| fname[-1] == "~" } 

	pofiles.sort!

	pofiles.each_index do |i|

		filepath = dirname + "/" + pofiles[i]

		if( !File.directory?( filepath ) )

			pofile = File.new( filepath )
			potext = pofile.read
			lname = po_to_langname( potext )
			if( lname == nil )
				lname = "tongue #{num_langs+1}"
			end
			languages[num_langs]=lname
			po_to_dict( dict, potext, num_langs )
			num_langs += 1

		end

	end

	return num_langs
end

#
# ==== PROGRAM STARTS HERE ====
#

help_string = "operation modes:
x - extract translatable strings from given source files and dump them to stdout

	command: xtring x <file1> <file2> ...

u - update: extract translatable strings from given source files, read in all .po files from the po directory

				update the files and write them back

	command: xtring u <file1> <file2> ...

g - generate a translation cpp source and dump it to stdout

	command: xtring g <language index 1> <language index 2> ..."

dict = Hash.new()
languages = Array.new()		#language names

if( ARGV.size() < 1 )
	puts help_string
	exit 1
end

if( ARGV[0] == "t" )

	#test methods

	#test keys_from_source
	
	keys_from_source( dict, (File.new( ARGV[1] )).read )

	puts dict.to_s

	po_to_dict( dict, (File.new( ARGV[2] )).read, 1 )

	puts dict.to_s

=begin
	#test find_quoted_string_end
	puts ARGV[1]
	puts find_quoted_string_end( ARGV[1] )
=end

	exit 0
	
end

if( ARGV[0] == "g" )
	
	#-- generate a cpp source containing the dictionary compiled from all files in .po --

	if( nil == gobble_pofiles( dict, languages, "po" ) )
		puts "cannot read from the \"po\" directory.\nDoes it exist>"
		exit 1
	end

	puts "#ifndef _Translation_h_\n#define _Translation_h_\n\n"
	puts "\n"
	puts langs_to_cpp( languages, "lang_table", "English" )	#the last argument is the name of the key language
	puts "\n"
	puts dict_to_cpp( dict, "trans_table" )
	puts "\n"
	puts "#endif //_Translation_h_\n"

	exit 0
	
elsif(( ARGV[0] == "x" )||( ARGV[0] == "u" ))

	##-- read in all files given as cmd line arguments --

	for i in 1...ARGV.size

		puts ARGV[i]

		srcfile = File.new( ARGV[i] )
		srctext = srcfile.read

		keys_from_source( dict, srctext )

	end

	#read in all .po files
	num_langs = gobble_pofiles( dict, languages, "po" )

	#-- update all files in .po --

	if( ARGV[0] == "u" )

		generate_pofiles( dict, languages, "po", "msgs" )

	end

	#x or u - extract translatable strings
	puts keys_to_pot( dict )

else
	puts help_string
	exit 1
end
